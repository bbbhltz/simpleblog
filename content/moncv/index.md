---
title: "Bobby Hiltz"
subtitle: "Professeur d'anglais, Rouen, France"
description: "Vous cherchez un professeur d'anglais à Rouen ? Business English, TOEIC, IELTS, Communication, Relations publiques et plus !"
slug: moncv
next: false
nomenu: true
notitle: false
toc: true
type: cv
rss_ignore: true
date: 2025-01-24
lastmod: 2025-02-08
---

{{< box important >}}

[See my CV in English](/mycv/)

{{< /box >}}

Professeur d'anglais avec {{< years-since >}} ans d'expérience dans le supérieur. 

## Expériences Professionnelles

### INFN (Rouen, France)

[https://www.infn.fr/](https://www.infn.fr/)

*Professeur d'anglais, 2022.09--présent*

### NEOMA Business School (Mont-Saint-Aignan, France)

[https://neoma-bs.com/](https://neoma-bs.com/)

*Cocoordinateur du département d'anglais, 2024.09--présent*

*Professeur d'anglais, 2011.01--présent*

### UniLaSalle (Mont-Saint-Aignan, France)

[https://www.unilasalle.fr/](https://www.unilasalle.fr/en)

*Professeur d'anglais, 2023.09--2024.08*

### ESIGELEC (Saint-Étienne-du-Rouvray, France)

[https://esigelec.fr/](https://en.esigelec.fr/)

*Professeur d'anglais, 2010.09--2020.09*

### ISCOM (Rouen, France)

[https://www.iscom.fr/fr/iscom-rouen](https://www.iscom.fr/fr/iscom-rouen)

*Professeur d'anglais, 2018.11--2024.01*

### Université de Rouen (Rouen, France)

*Professeur de traduction et gestion de chaîne logistique, 2017.09--2018.08*

### Maison des Jeunes et de la Culture (Elbeuf, France)

*Professeur d'anglais, 2010.09--2012.08*

### ESITPA (Mont-Saint-Aignan, France)

*Professeur d'anglais, 2010.09--2011.04*

### Préfecture de l'Eure (Évreux, France)

*Formateur d'anglais, 2008.01--2009.06*

### Éducation Nationale (Elbeuf/Évreux, France)

*Assistant de langues, 2008.10--2010.05*

### Université de Rouen, IUT, IUFM (Rouen, France)

*Lecteur d'anglais, 2006.09--2008.08*

### Pharmasave (Chester, Nouvelle-Écosse, Canada)

*Responsable de rayon, 2002.04--2006.09*

## Formations

### Acadia University (Wolfville, Nouvelle-Écosse, Canada)

[https://www2.acadiau.ca/home.html](https://www2.acadiau.ca/home.html)

*BAC+4 : Bachelor of Arts (Français et Histoire), 2002--2006*

- Assistant de professeur assistant de recherche pour [un projet « Blended Learning »](https://plato.acadiau.ca/courses/fren/tutor/tutor2005/)

### CS50 | HarvardX

[https://www.edx.org/learn/python/harvard-university-cs50-s-introduction-to-programming-with-python](https://www.edx.org/learn/python/harvard-university-cs50-s-introduction-to-programming-with-python)

*Introduction to Programming with Python, 2022*

- [Certificat](https://cs50.harvard.edu/certificates/aab11061-9e60-464d-88bc-ac4354528609)

## Compétences

### Langues

- Anglais
- Français

### Enseignement

- Développement de cours
- Création de contenu
- Préparation TOEIC et IELTS
- Analyse et évaluation des besoins

### Informatique

- GNU/Linux (CLI et GUI)
- Bureautique (LibreOffice et Microsoft Office)
- Moodle
- Création et modification de fichiers audios et vidéos
- LaTeX (notions)
- Python (débutant)

## Intérêts

- PAO
- Développement Web
- Accessibilité
- Nouvelles technologies
- Logiciels Open Source

## Contact

[Courriel](mailto:contact@bobbyhiltz.com) | [Signal](https://signal.me/#eu/P8IamjYXrx8a9Y_k57F8RXv83i9-TE_12IU79OUsMGL2YBTxjAlJG6byTO-fbVut) | [Site Web](https://bobbyhiltz.com)

