---
title: "About"
description: "About Bobby and this blog"
# lastmod: 2024-10-17
toc: true
next: false
---

## About the Author

Born and raised in rural Canada, I do as little as possible as often as I can. 

Professionally, I teach English as a second language---my main employer being an international business school in France.

You can see my CV [here](/mycv) (*et [ici](/moncv) en français*).

My personal interests are technology, entertainment, science-fiction and music (see [BookWyrm](https://bookwyrm.social/user/bbbhltz) and [ListenBrainz](https://listenbrainz.org/user/pasdechance/)).

I keep track of my favourite albums here: [/tunes](/tunes).

If you like the things on this blog you can [email me](/contact).

## About the Content

Opinions on this blog are my own.

The content on this blog is written by me. I do not use generative AI or bots to help prepare it. My view on generative AI is quite negative, whether for personal or professional reasons. I also use my [`robots.txt`](/robots.txt) to prevent AI crawlers (I am aware they ignore it).

## About Hosting

### Codeberg

![Codeberg Horizontal Logo](https://design.codeberg.org/logo-kit/horizontal.svg)

This blog is hosted on **Codeberg Pages** ([https://codeberg.page/](https://codeberg.page/)).

### Infomaniak

![Infomaniak Blue Logo](logo_infomaniak_bleu.svg)

The domain for this site was registered through [Infomaniak](https://www.infomaniak.com/goto/en/home?utm_term=67af344eb9273) and the [email](https://www.infomaniak.com/goto/en/hosting.mail?utm_term=67af344eb9273) is also hosted by Infomaniak. Check out their [drive](https://www.infomaniak.com/en/kdrive/?utm_term=67af344eb9273) and [office suite](https://www.infomaniak.com/en/ksuite?utm_term=67af344eb9273) solutions.

## About Supporting this Blog

If you would like to support this blog, you can do so by using the affiliate links above, or more directly here [ko-fi.com/bbbhltz](https://ko-fi.com/bbbhltz) or here [liberapay.com/bbbhltz/](https://en.liberapay.com/bbbhltz/).

### Todo

- [X] buy domain name \
  *[bobbyhiltz.com](https://bobbyhiltz.com) should soon be live thanks to a generous donation*
- [ ] write more frequently
- [ ] pay for hosting
