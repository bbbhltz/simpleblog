---
title: "Bobby's Blog"
subtitle: "Welcome to my blog"
description: "A personal blog about technology, education, and other sundries like short stories and reviews"
outputs:
  - html
  - rss
---

# BobbyHiltz.com

{{< meta >}}
