---
title: "Blogroll"
description: "take off"
slug: blogroll
date: 2024-04-28T13:23:29+02:00
lastmod: 2024-10-10
# featured_image: /blog/2024/04/blogroll/featured.png
---

## Collections and Webrings

[Blogroll.org](https://blogroll.org/)\
*A curated list of personal and independent blogs*

[Indieseek.xyz Indieweb Directory](https://indieseek.xyz/)\
*A human edited, Indieweb directory*

[PersonalSit.es](https://personalsit.es/)\
*A site built to share and revel in each others' personal sites*

[Weird Wide Webring](https://weirdwidewebring.net/)\
*The web needs a little more weird*

[Fediring.net](https://fediring.net/)\
*Webring for the personal sites of any member of the fediverse*

[indieblog.page](https://indieblog.page/)\
*Discover the IndieWeb, one blog post at a time*

[ooh.directory](https://ooh.directory/)\
*A collection of blogs about every topic (someday my blog will be listed...someday...)*

## Some Blogs I follow

* [Adam's Desk](https://www.adamsdesk.com/) <a href="https://www.adamsdesk.com/feed/blog.xml">(RSS)</a>
* [BryceWray](https://www.brycewray.com/) <a href="https://www.brycewray.com/index.xml" >(RSS)</a>
* [Buck 65's Substack](https://buck65.substack.com/) <a href="https://buck65.substack.com/feed">(RSS)</a>
* [Citation Needed by Molly White](https://www.citationneeded.news/) <a href="https://www.citationneeded.news/rss/">(RSS)</a>
* [David Revoy](https://www.davidrevoy.com/) <a href="https://www.davidrevoy.com/feed/(RSS)">(RSS)</a>
* [Evan Boehs](https://boehs.org/in/blog) <a href="https://boehs.org/in/blog.xml">(RSS)</a>
* [Joelchrono](https://joelchrono.xyz) <a href="https://www.joelchrono.xyz/feed.xml" >(RSS)</a>
* [JP](https://moddedbear.com/) <a href="https://moddedbear.com/blog/index.xml" >(RSS)</a>
* [Hyde/Lazybear](https://lazybea.rs/) <a href="https://lazybea.rs/index.xml" >(RSS)</a>
* [Ploum](https://ploum.net/) [FR] <a href="https://ploum.net/atom.xml" >(RSS)</a>
* [McSinyx](https://cnx.gdn/) <a href="https://cnx.gdn/feed.xml" style="text-decoration:      none">(RSS)</a>
* [Mojeek Blog](https://blog.mojeek.com/) <a href="https://blog.mojeek.com/atom.xml" >(RSS)</a>
* [Seirdy](https://seirdy.one/) <a href="https://seirdy.one/atom.xml" >(RSS)</a>
* [So1o](https://so1o.xyz/) <a href="https://so1o.xyz/feed.xml" >(RSS)</a>
