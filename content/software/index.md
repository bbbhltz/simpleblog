---
title: "Software I use"
description: "The tools I use on a daily basis"
featured_image: /software/featured.webp
#date: 2025-03-01
#lastmod: 2025-03-01
toc: true
---

## Laptop

![Screenshot](current_desktop.webp "Standard LXQt Installation")

### Distribution & Desktop Environment

* [Debian](https://www.debian.org/) \
Stable.

* [LXQt](https://lxqt-project.org/) \
Lightweight and great.

### Office & Text Editing

* [Featherpad](https://github.com/tsujan/FeatherPad) \
  Plain-text editor without too many options.

* [Geany](https://www.geany.org/) \
A powerful, stable, cross-platorm text editor.

* [Calibre](https://calibre-ebook.com/) \
Manages ebooks. I like ebooks, shoot me!

* [LibreOffice](https://www.libreoffice.org/) \
General purpose office suite for word processing, spreadsheets, etc.

* [zathura](https://pwmt.org/projects/zathura/) \
A document viewer that saves screen space.

* [Xournal++](https://xournalpp.github.io/) \
Hand note-taking software. Useful for annotating PDF files.

### Internet

* [Claws Mail](https://www.claws-mail.org/) \
Tiny, simple, maintained, plaintext, bottom posting; it has all you need! It works with different email providers and focuses on email. That means that it is lacking a true calendar function and importing and syncing contacts is not always straightforward. You will get over this once you have things looking and working the way you like. \
Read about it on my blog [here](/blog/2023/01/claws-mail). \
[Remember to use plain text](https://useplaintext.email/)

* [Syncthing](https://syncthing.net/) \
P2P file synchronization between devices on a local network, or between remote devices over the Internet. I use it just for notes and photos but it can do lots more. This has replaced Dropbox and Google Drive for me.

### Browsers

* [Firefox](https://support.mozilla.org/en-US/kb/install-firefox-linux#w_install-firefox-deb-package-for-debian-based-distributions) \
Just Firefox with [Betterfox](https://github.com/yokoffing/BetterFox), [uBO](https://github.com/gorhill/uBlock#ublock-origin), [BPC](https://gitflic.ru/project/magnolia1234/bypass-paywalls-firefox-clean/), and [uBlacklist](https://github.com/iorate/ublacklist) makes for good browsing. \
Filter lists that I use: [laylavish's Huge AI Blocklist](https://github.com/laylavish/uBlockOrigin-HUGE-AI-Blocklist), [WDMPA's Content Farm List](https://github.com/wdmpa/content-farm-list), [Shockwave3301's](https://github.com/Shockwave3301/uBlacklistAIList), my own [16 Companies Filters](https://codeberg.org/bbbhltz/16CompaniesFilters).

* [Dillo](https://dillo-browser.github.io/) \
A "fast and small graphical web browser" with "personal security and privacy" as objectives. \
See [my post](/blog/2025/02/dillo/) on Dillo.

* [qutebrowser](https://qutebrowser.org/) \
Keyboard-focused browser with a minimal GUI. \
[Some configurations here on Codebeg](https://codeberg.org/bbbhltz/dotfiles/src/branch/main/configs/qutebrowser)

### Piracy

* [Nicotine+](https://nicotine-plus.github.io/nicotine-plus/) \
Free and Open Source (FOSS) alternative to the official Soulseek client.

### Images and Graphics

* [Flameshot](https://flameshot.org/) \
Powerful yet simple to use screenshot software.

* [Inkscape](https://inkscape.org/) \
Sometimes I need to make a pretty graphic to wow my students.

### Audio and Video

* [Audacity](https://www.audacityteam.org/) \
This is just the most straightforward way to record audio. It works and it works well.

* [mpv](https://mpv.io/) \
If I am going to watch a video on my computer, whether local or via YouTube, I use mpv. It does audio as well and there are plugins and extensions that allow for different functions.


### Music

* [beets](https://beets.io/) \
The music geek's album organiser. \
[My beets configuration](https://framagit.org/bbbhltz/dotfiles/-/tree/main/configs/beets)

* [Strawberry Music Player](https://www.strawberrymusicplayer.org/) \
Music player and music collection organizer.

* [cmus](https://cmus.github.io/)
A small, fast and powerful console music player. I use this over other options (like mpd) because it is so damn easy!

### Command Line Tools

* [Pandoc](https://pandoc.org/) \
A universal document converter. I use this to make 95% of my documents for work.
    * e.g. [some posts on Pandoc](/tags/pandoc)

* [ditaa](https://ditaa.sourceforge.net/) \
Turns ASCII art into diagrams.

* [gramma](https://caderek.github.io/gramma/) \
Commend-line grammar checker

* [ImageMagick](https://imagemagick.org/) \
Convert, edit, work with images (I make my [featured image](https://codeberg.org/bbbhltz/simpleblog/src/branch/master/featured-image-gen/featured-gen.sh) cards with it).

* [pdfannots](https://github.com/0xabu/pdfannots) \
A script that extracts annotations from a PDF file.

* [PDFtk](https://www.pdflabs.com/tools/pdftk-the-pdf-toolkit/)
A very simple tool that can do lots of things with PDF files. This is a necessity for my job and it has saved me and made me the hero of my department on many occasions.

* [scrcpy](https://github.com/Genymobile/scrcpy)
An application that provides display and control of Android devices connected on USB (or over TCP/IP).

* [yt-dlp](https://github.com/yt-dlp/yt-dlp) \
A youtube-dl fork with additional features and fixes.

* [Ytfzf](https://github.com/pystardust/ytfzf) \
A script that helps you find Youtube videos (without API) and opens/downloads them using mpv/youtube-dl.

## Android

* [universal-android-debloater](https://github.com/0x192/Universal-Android-Debloater) \
Use ADB to debloat rooted and non-rooted android devices. Improve your privacy, the security and battery life of your device. \
(some of this can be [done by hand](/blog/2023/05/on-debloating-android-phones))

### Launcher

* [YAM Launcher](https://codeberg.org/ottoptj/yamlauncher)

### App Stores

- [Droid-ify](https://github.com/NeoApplications/Neo-Store) \
Free and Open Source Android App Repository

- [Aurora Store](https://f-droid.org/en/packages/com.aurora.store/) \
Aurora Store is an alternate to Google's Play Store, with an elegant design

### Main Apps

* [FairEmail](https://github.com/M66B/FairEmail) \
  Fully featured, open source, privacy friendly email app for Android

* [NewPipe](https://github.com/TeamNewPipe/NewPipe) \
  A libre lightweight streaming front-end for Android

- [qksms](https://github.com/moezbhatti/qksms) \
  The most beautiful SMS messenger for Android

- [Etar Calendar](https://github.com/Etar-Group/Etar-Calendar) \
  Android open source calendar

- [Privacy Browser Android](https://www.stoutner.com/privacy-browser-android/) \
  Privacy Browser is an open source Android web browser focused on user privacy

- [Gadgetbridge](https://gadgetbridge.org/) \
  A free and open source Android application that allows you to pair and manage various gadgets such as smart watches

- [Total Commander](https://www.ghisler.com/android.htm) \
  Two-paned file manager

- [Simple Keyboard](https://github.com/rkkr/simple-keyboard) \
  Basic, no frills, keyboard

## PineTime

* [Infini Time](https://infinitime.io/) \
  Open Source firmware

* [ITD](https://gitea.elara.ws/Elara6331/itd)

## Other Tools, Hosting, Git

* [Markdown](https://daringfireball.net/projects/markdown/)
* [Codeberg](https://Codeberg.org/bbbhltz)
