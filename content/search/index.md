---
title: "Search this site"
subtitle: "Search this site with Mojeek"
description: "This page contains a simple search box for searching BobbyHiltz.com using the Mojeek.com search engine."
slug: search
---


<form method="get" action="https://www.mojeek.com/search">
	<div>
		<a href="https://www.mojeek.com/"> 
			<img src="https://www.mojeek.com/logos/logo_cc.svg" style="height:30px;padding-bottom:5px;">
		</a>
	</div>
	<input size="30" name="q">  
	<input type="submit" value="Search"> 
	<div> 
		 <select name="site">
			<option value="bobbyhiltz.com">search this site</option>
		</select> 
	</div>
</form>

[Read about Mojeek here](https://www.mojeek.com/about/why-mojeek) or [follow them on Mastodon](https://mastodon.social/@Mojeek).
