---
title: "Bobby Hiltz"
subtitle: "English Professor, Rouen, France"
description: "Looking for an English Teacher or Professor in Rouen, France? Experience in Business English, TOEIC, IELTS, Communication, Public Relations, and more!"
slug: mycv
next: false
nomenu: true
notitle: false
toc: true
type: cv
rss_ignore: true
date: 2025-01-24
lastmod: 2025-02-08
---

{{< box important >}}

[Voir mon CV en français](/moncv/)

{{< /box >}}

Professionally, I have been teaching English as a second language for over {{< years-since >}} years at universities and grandes écoles in France. Outside of work, my interests are Free and Open Source Technologies that respect our digital privacy.

## Professional Experience

### INFN (Rouen, France)

[https://www.infn.fr/](https://www.infn.fr/)

*English Professor, 2022.09--Present*

- Classes for future notaries.

### NEOMA Business School (Mont-Saint-Aignan, France)

[https://neoma-bs.com/](https://neoma-bs.com/)

*English Department Co-coordinator, 2024.09--Present*

- Co-coordinate 20 English Professors across two campuses.
- Organize Exams and Resits.

*English Professor, 2011.01--Present*
- Design and incorporation of content and language integrated learning techniques for Bachelor’s and Master’s students.
- Create, grade, and give feedback on exams. 
- Tutor students to achieve desired CEFR level. 
- Translate documents and web pages. 
- Interview potential candidates for enrolment.

### UniLaSalle (Mont-Saint-Aignan, France)

[https://www.unilasalle.fr/en](https://www.unilasalle.fr/en)

*English Professor, 2023.09--2024.08*

### ESIGELEC (Saint-Étienne-du-Rouvray, France)

[https://en.esigelec.fr/](https://en.esigelec.fr/)

*English Professor, 2010.09--2020.09*

### ISCOM (Rouen, France)

[https://www.iscom.fr/fr/iscom-rouen](https://www.iscom.fr/fr/iscom-rouen)

*English Professor, 2018.11--2024.01*

### University of Rouen (Rouen, France)

*Professor (Supply Chain Management and Translation), 2017.09--2018.08*

### Maison des Jeunes et de la Culture (Elbuef, France)

*English Teacher, 2010.09--2012.08*

### ESITPA (Mont-Saint-Aignan, France)

*English Professor, 2010.09--2011.04*

### Préfecture de l'Eure (Évreux, France)

*English Trainer, 2008.01--2009.06*

### Éducation Nationale (Elbeuf/Évreux, France)

*Language Assistant, 2008.10--2010.05*

### University of Rouen, IUT, IUFM (Rouen, France)

*Lecturer, 2006.09--2008.08*

### Pharmasave (Chester, Nova Scotia, Canada)

*Front Store Manager / Clerk, 2002.04--2006.09*

## Education & Certificates

### Acadia University (Wolfville, Nova Scotia, Canada)

[https://www2.acadiau.ca/home.html](https://www2.acadiau.ca/home.html)

*Bachelor of Arts (French and History), 2002--2006*

- Teacher's Assistant and Research Assistant for [Blended Learning Project](https://plato.acadiau.ca/courses/fren/tutor/tutor2005/)

### CS50 | HarvardX

[https://www.edx.org/learn/python/harvard-university-cs50-s-introduction-to-programming-with-python](https://www.edx.org/learn/python/harvard-university-cs50-s-introduction-to-programming-with-python)

*Introduction to Programming with Python, 2022*

- [Certificate](https://cs50.harvard.edu/certificates/aab11061-9e60-464d-88bc-ac4354528609)

## Skills

### Languages

- English
- French

### Teaching and Coordination

- Course design
- Content and exam creation
- TOEIC and IELTS preparation
- Analysis and Evaluation of needs

### Computers and Office Software

- GNU/Linux (CLI and GUI)
- Office Suites (LibreOffice and Microsoft Office)
- Troubleshooting
- Moodle
- Creation and modification of audio and video files
- LaTeX (notions)
- Python (beginner)

## Interests

- Desktop publishing
- Web development
- Accessibility
- New technologies
- FLOSS: Free and Libre Open Source Software

## What my students say about me...

>Bobby has been my teacher for nearly two years. Thanks to him, I perfected my English, increased my vocabulary and extended my general knowledge through interesting and fun topics. From new technologies to intercultural communication I enjoyed his informal and dynamic classes. Bobby stand out as a very committed and skilled English teacher and it was a privilege to be his student.
>
><cite>---D. S., a NEOMA Business School student</cite>

>I was lucky to have Bobby Hiltz for three years. He is very enthusiastic, dynamic and highly effective in teaching you English.
>
><cite>---F. B., an ESIGELEC student</cite>

## Contact

[Email](mailto:contact@bobbyhiltz.com) | [Signal](https://signal.me/#eu/P8IamjYXrx8a9Y_k57F8RXv83i9-TE_12IU79OUsMGL2YBTxjAlJG6byTO-fbVut) | [Website](https://bobbyhiltz.com)
