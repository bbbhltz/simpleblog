---
title: "Contact"
description: "Profiles around the Web"
---

* <a rel="me" href="https://framapiaf.org/@bbbhltz">Mastodon</a>
* [Beehaw (Lemmy)](https://beehaw.org/u/bbbhltz)
* [OpenStreetMap](https://www.openstreetmap.org/user/bbbhltz)
* [ListenBrainz](https://listenbrainz.org/user/pasdechance/) / [MusicBrainz](https://musicbrainz.org/user/pasdechance)
* [AlternativeTo](https://alternativeto.net/user/bobbyhiltz/)
* <a href="mailto:contact@bobbyhiltz.com?subject=Message%20regarding%20your%20blog">Email</a>
