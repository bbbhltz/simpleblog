---
title: "Tunes"
description: "Persistent Page of Favourite Albums"
slug: tunes
date: 2024-01-01
lastmod: 2025-02-06
# featured_image: /blog/2024/03/tunes/featured.png
# {{< bandcamp|qobuz "album" "artist" "link" >}}
toc: true
next: false
nomenu: false
---

*There is no order to this list. I add albums based on when I discover them.*

You can follow me on [Bandcamp](https://bandcamp.com/bobbyhiltz) or [ListenBrainz](https://listenbrainz.org/user/pasdechance/)

## 2025

{{< bandcamp "Goyard Ibn Said" "Ghais Guevara" "https://ghais.bandcamp.com/album/goyard-ibn-said" >}}

{{< bandcamp "A Sick Twist Ending" "Sage Francis" "https://sagefrancis.bandcamp.com/album/a-sick-twist-ending-2" >}}

{{< bandcamp "doseone & Steel Tipped Dove" "All Portrait, No Chorus" "https://doseone.bandcamp.com/album/all-portrait-no-chorus" >}}

## 2024

{{< bandcamp "GIANTS" "Brave Wave Productions" "https://bravewave.bandcamp.com/album/giants" >}}

{{< bandcamp "Fractures" "Kayla Painter" "https://kaylapainter.bandcamp.com/album/fractures-2" >}}

{{< bandcamp "Let's Be Havin' U?" "EVA808" "https://eva808.bandcamp.com/track/lets-be-havin-u" >}}

{{< qobuz "Pub Royal" "Les Cowboys Fringants" "https://www.qobuz.com/us-en/album/pub-royal-les-cowboys-fringants/dv06htb0vz8ob" >}}

{{< bandcamp  "HARDWAREZ" "MASTER BOOT RECORD" "https://masterbootrecord.bandcamp.com/album/hardwarez" >}}

{{< bandcamp "Chamber Songs" "We Hate You Please Die" "https://wehateyoupleasedie.bandcamp.com/album/chamber-songs" >}}

{{< bandcamp "Honey" "Caribou" "https://caribouband.bandcamp.com/album/honey-2" >}}

{{< bandcamp "I LAY DOWN MY LIFE FOR YOU" "JPEGMAFIA" "https://jpegmafia.bandcamp.com/album/i-lay-down-my-life-for-you" >}}

{{< bandcamp "She Told Me Where To Go" "Old Man Luedecke" "https://old-man-luedecke.bandcamp.com/album/she-told-me-where-to-go" >}}

{{< bandcamp "Les manières de table" "Annie-Claude Deschênes" "https://annie-claudedeschenes.bandcamp.com/album/les-mani-res-de-table" >}}

{{< bandcamp "Boss" "4man" "https://ohmresistance.bandcamp.com/album/boss" >}}

{{< bandcamp "META​{​physical} monsters" "Biomass" "https://ohmresistance.bandcamp.com/album/meta-physical-monsters" >}}

{{< bandcamp "Hex Dealer" "Lip Critic" "https://lipcritic.bandcamp.com/album/hex-dealer" >}}

{{< bandcamp "I AM JORDAN" "I. JORDAN" "https://i-jordan.bandcamp.com/album/i-am-jordan" >}}

{{< bandcamp "Luminous Rubble" "Blockhead" "https://blockheadnyc.bandcamp.com/album/luminous-rubble-2" >}}

{{< bandcamp "Rollercoaster" "Cadence Weapon" "https://cadenceweapon.bandcamp.com/album/rollercoaster" >}}

{{< bandcamp "Memory Empathy" "DJ Birdbath" "https://theorytherapy.bandcamp.com/album/memory-empathy" >}}

{{< bandcamp Three "Four Tet" "https://fourtet.bandcamp.com/album/three" >}}

{{< bandcamp Akoma Jlin "https://jlin.bandcamp.com/album/akoma" >}}

{{< bandcamp Dwelling JEWELSSEA "https://jewelssea.bandcamp.com/album/dwelling" >}}

{{< bandcamp "XT / Woozy" Sully "https://sullyuk.bandcamp.com/album/xt-woozy-remixes" >}}

{{< bandcamp Body Horskh "https://horskh.bandcamp.com/album/body-2" >}}

{{< bandcamp DDEEAATTHHMMEETTAALL "Escuela Grind" "https://escuelagrind.bandcamp.com/album/ddeeaatthhmmeettaall" >}}

{{< bandcamp "Adult Contemporary" Chromeo "https://chromeoduo.bandcamp.com/album/adult-contemporary" >}}

{{< bandcamp eNdgame Cyberaktif "https://cyberaktif.bandcamp.com/album/endgame" >}}

{{< bandcamp "Dreamfear​/​Boy Sent From Above" Burial "https://burial.bandcamp.com/album/dreamfear-boy-sent-from-above" >}}

{{< bandcamp "Raves of Future Past" "T.Williams" "https://twilliamsmusic.bandcamp.com/album/raves-of-future-past" >}}

{{< bandcamp "Nineteen Eighty-Fur" "Cat Temper" "https://cattemper.bandcamp.com/album/nineteen-eighty-fur" >}}

{{< bandcamp "All Life Long" "Kali Malone" "https://kalimalone.bandcamp.com/album/all-life-long" >}}

{{< bandcamp "Despectral Maid" "Apollo Bitrate" "https://caybee.bandcamp.com/album/despectral-maid-2" >}}

{{< bandcamp "The Collective" "Kim Gordon" "https://kimgordon.bandcamp.com/album/the-collective" >}}
