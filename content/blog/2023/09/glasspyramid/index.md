---
title: "The Glass Pyramid"
description: "A short story about life, glass ceilings, glass floors, and social classes."
subtitle: "A Short Story"
slug: glasspyramid
date: 2023-09-12T13:30:35+02:00
# lastmod:
featured_image: /blog/2023/09/glasspyramid/P1.png
draft: false
katex: false
tags:
  - Short story
next: true
nomenu: false
---

![](P1.png)

There once was a magical glass pyramid. \
It sat on top of a hill. \
All were welcome to come and live in the glass pyramid. \
All you had to do was follow the long and winding path up the hill.

Outside the pyramid, it was dangerous. \
There were no houses. \
There was no food.

Inside the pyramid, it was like a grand city. \
Everyone had a job to do. \
Some jobs, even though they were not difficult, were more important than others. \
The important people lived at the very top of the pyramid and made all the decisions. \
The important people knew best.

After a time, there were too many people in the pyramid. \
The important people were upset. \
There were too many people with less important jobs sharing their space. \
That is not the way of the pyramid. \
Something had to be done.

"We should make the pyramid bigger," said one person. \
"Amazing idea," said another. \
"But, that's not fair!" said one of the important people. \
"Why should I have to live near these less important people?" said another. \
"Well," said one of the most important people, "perhaps we should make it harder to get in as well."

And so, a vote was held. \
The higher you lived in the pyramid, the more your vote counted.

A fair and democratic solution was reached: \
They would raise the ceiling of their magical glass pyramid, to make more space for important people. \
And, each time they raised their glass ceiling, they would make their hill higher as well, to make it more difficult to get in. \
This was called *raising the glass floor*.

![A tall triangle on a hill](P2.png)

Eventually, the hill became a mountain. \
And the pyramid became a needle that went high into the sky. \
So high and close to the sun that it became very hot for the important people. \
They blamed the less important people. \
They told them it was their fault.

The least important people worked even harder: \
They only ate local, \
they recycled,  \
they spent their earnings on electric vehicles and solar panels, \
they never used air conditioning or plastic straws.

They were told to lower their carbon footprint. \
So they did.

They were told that everyone had to do their part. \
So they did.

But...

The important people were not doing their part. \
After all, it was not their fault. \
They did what they wanted, when they wanted. \
But, despite having everything they could possibly need, \
they were still not happy.

They decided that it was still too easy to get in. \
Raising the glass floor was not enough. \
Some of the less important people had to go.

"These people have it too easy," complained one of the important people. \
"Maybe they shouldn't even be in the pyramid," said another. \
And so, they came up with a plan.

They made things more expensive, \
like food and houses and doctors and school. \
The less important people worked harder and harder. \
They even stopped having children. \
No matter what they did though, it was not enough. \
And this was the plan: \
If you could not afford to live in the pyramid, \
you had to leave.

After a time, it became so difficult to get into the pyramid, \
and so hard to stay, \
that there was nobody left to do the work that had to be done. \
The important people had forgotten how to work. \
They had forgotten how to make food and clean and build things.

Little by little, the pyramid started to crack. \
People who used to be important became less important. \
Some were even forced to leave.

The people outside the pyramid saw that something was wrong. \
They shouted, "how can we help you!" \
And the important people shouted back, "nobody wants to work any more!" \
"Give us jobs, give us homes, let us help you!" clamoured the people outside the pyramid. \
"NO," shouted the important people, "you are not important enough to live in our pyramid!"

And as they slammed the door on the faces of those willing to help, \
a crack began to form. \
It started at the bottom. \
The grew and grew, spreading higher and higher. \
The people outside did not want the pyramid to fall, \
it would crush them if it did. \
So they all came together and tried to hold the pyramid up.

"Come down and help us," the people outside cried. \
"We only have one pyramid," they pleaded. \
But, the people inside were only interested in getting to the top. \
They all climbed to the top floor instead of helping hold the pyramid together. \
They were more afraid of the people outside getting in than they were of the crack.

All the important people reached the top of the pyramid. \
They held a meeting to decide what to do. \
"We just need to use the magic to fix the pyramid," they said. \
The most important person spoke up. \
They controlled the magic. \
They could fix everything in a snap. \
They said, "I will do this for a price!"

The other important people thought this was fair, \
but they did not want to spend their money. \
"What if we asked the least important people to donate? It is their fault, isn't it?" suggested someone.

A very famous and important person went down to see the people on the glass floor. \
They were very emotional. \
They spoke at length and finally asked the people at the bottom to donate their money.

The people at the bottom looked around at each other. \
One of them began to chuckle. \
And another. \
And another.

A contagious laughter spread among the people at the bottom. \
They could not stop. \
They fell over laughing. \
They laughed so hard the mountain began to shake. \
The more the mountain shook, the more the pyramid wobbled.

The crack spread faster and faster. \
"HELP US!" cried all the important people, \
as they clutched their pearls and wallets. \
"After all we did for you ungrateful people!" scolded one of them, \
sipping champagne. \
"Don't you know who I am?" growled another, \
sat in a rocket they had no idea how to fly.

The laughter and the shouting continued until a final crack echoed across the land.

The magical glass pyramid shattered and fell.

![](P3.png)

When the dust settled, the people at the bottom of the pyramid were relieved. \
They had not been crushed. \
They began fixing and rebuilding. \
They made their homes, and schools, and hospitals.

Many months later, a group of people came climbing up the hill. \
They stopped in awe of what they saw. \
The pyramid was still standing.

These people were hungry and tired. \
They wanted to bathe and sleep in beds. \
They walked straight to the front door, \
but their path was blocked.

"Unhand me, filth!" said one of the travellers to the person barring their way. \
"I am a very important person, and you will let me pass." \
"Well," started the person at the door, "you see, that is not how things work around here." \
"This is our magical pyramid," explained the guard. \
"If you want to get in, you need skills, you need to be able to work because everyone has a job to do!"

Alas, over the generations, the important people had forgotten how to work. \
So they could not enter the pyramid.

Years passed, and the pyramid began to grow, \
and the glass ceiling became further away, \
and the glass floor went up with it, \
and the story repeated itself, \
again, \
and again.
