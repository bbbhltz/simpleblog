---
title: "They Lie"
subtitle: "a story about promoting an antidote for the poison you sell"
description: "A fictional short story based on something I encountered in real life."
slug: sellingpoison
date: 2023-11-17T11:45:09+01:00
# lastmod:
featured_image: /blog/2023/11/sellingpoison/featured.webp
draft: false
katex: false
tags:
  - Privacy
  - Short story
next: true
nomenu: false
---

Opportunities are everywhere.

Sometimes, though, you need to make your own opportunities.

All it takes is an idea. And ideas can come from anywhere.

This is the story about a boy. One day, towards the end of the 1970s, he read a book. Some would call it *historical fiction*, some *science-fiction*, many others would call it *conspiracy theory*. I will not tell you the name of this book, or the author, because this book plants a seed. No tree of knowledge will sprout from this seed; it only festers and rots.

Growing older, the boy, now a man, was not getting what life had promised. He had gone to school, which is what we were told to do at the time. He wondered where his riches were. 'Life isn't fair,' he would think to himself. Thoughts like these were like water for the seed.

Mental healthcare, at this point in time, was not an available resource. In fact, the idea that these thoughts could be a warning sign did not in cross the man's mind. 

The Internet *was* an available resource. And the man dove---deep---into different corners of the web. Doing his own research paid off. The book he had read years before was just a springboard to the *truth*. The world *was* the problem; not him. And the world as the normies know it, is not as it seems.

He began to see himself like Nada in [*They Live*](https://en.wikipedia.org/wiki/They_Live), but, in reality, he had become the alien among us. The wolf in sheep's clothing.

Major history events? After doing his own research, the man discovered them to be fake. Hoaxes.

Health problems? It turns out, according to the man's sources, that Linus Pauling was right and was silenced by "Big Pharma".

It was all making sense now. It was all a lie. Lying, it seemed, was the key to success.

And, just like that, an idea came to fruition. Yes, the normies need to know the truth. They need to know that the those in power are lying about *everything*. Spreading the truth, however, does not pay the bills.

'If those in power are exempt from the rules,' thought the man, 'then why should I have to follow them?'

Taking advantage of innocent victims is, as the saying goes, like taking candy from a baby. The man founded a company, *Amazing Privacy Services LTD*, and became a scam artist. Unsolicited phone calls and classic email scams were practically victimless crimes. If the person on the end of the line wasn't *woke* enough to realize they were being scammed, that wasn't the man's fault, was it? In fact, from his point of view, he was doing them a favour by teaching them a lesson.

Alas, victims were becoming harder to get to. The people were waking up, and digital privacy was becoming more accessible to the normies. Unfortunately for the victims, digital privacy was something the man was concerned with. Running scams and hosting information about controversial topics and presenting fringe theories as fact on a website requires some attention to detail, and digital privacy.

Today, this man is present in forums and groups about privacy. On the outside, he is an individual with some controversial opinions and an interest is protecting people. In reality, he is using these groups to glean information on how people protect themselves, and taking advantage of weaknesses.

He is not just a silly looking little snake in the grass. He is a criminal. He is a cancer. He is a bad seed.

This story may be fiction, but it is based on truth, unlike the book that the man read decades ago.

The path to take here is not clear. Yes, we have a person selling the poison and fighting for an antidote at the same time. It is hypocrisy. Yes, the same person is also peddling lies, misinformation, and even making claims that are punishable by law, but I am not about to go on a wild goose chase to track this person down. That would just put a target on my back.

I am writing this as a simple warning, [one that I have made before](/blog/2022/03/low-friction-introduction-to-digital-privacy/#general-recommendations-), and everyone will agree with:

* Do not trust what you read on the web, even if it looks to be good advice
* One good piece of advice that you agree with does not mean all the advice on that site or forum is good
* Do not reveal private information, including details on how you protect yourself online, anywhere
