---
title: "My Best Albums 2023"
subtitle: "Albums and EPs that I can remember off the top of my head"
description: "An end-of-year listicle about the best music in 2023."
slug: bestalbums2023
date: 2023-12-18T10:00:00+01:00
# lastmod:
featured_image: /blog/2023/12/bestalbums2023/allalbums.webp
draft: false
katex: false
tags:
  - Music
  - Listicle
  - EOY
next: true
nomenu: false
---

Hello music nerds. The end of the year is creeping up on us and that means it is time to take a look back at the year in music.

As much as I *love* discovering new music, this year was not a huge year for that. As far as I can tell (using beets and [ListenBrainz](https://listenbrainz.org/user/pasdechance/) as sources) I listened to [about 190 albums](albums.txt) released between 2023-01-01 and today (2023-12-18). Now, normally I could easily rattle off my favourite 10 or 20 releases, but this year is a blur.

I will keep it short and unordered: some honourable mentions and my favourite release of the year.

## Honourable Mentions

![album covers](allalbums.webp)

| Artist | Album | Link(s) |
|--------|-------|-------|
| **Fiera**  | Pueblo Nuevo | [Bandcamp](https://fierafiera.bandcamp.com/album/pueblo-nuevo) |
| **Helena Hauff** | fabric presents Helena Hauff | [Bandcamp](https://helenahauff.bandcamp.com/album/fabric-presents-helena-hauff) |
| **Pangaea** | Changing Channels | [Bandcamp](https://pangaeauk.bandcamp.com/album/changing-channels)  |
| **Cyk** | Lore | [Bandcamp](https://sangomarecs.bandcamp.com/album/lore) |
| **Duane Betts** | Wild & Precious Life | [Bandcamp](https://duanebetts.bandcamp.com/album/wild-precious-life) |
| **Four Tet** | Live at Alexandra Palace London, 24th May 2023 | [Bandcamp](https://fourtet.bandcamp.com/album/live-at-alexandra-palace-london-24th-may-2023) |
| **Miss España** | Niebla Mental | [Bandcamp](https://missespanha.bandcamp.com/album/niebla-mental) |
| **Aesop Rock** | Integrated Tech Solutions | [Bandcamp](https://aesoprock.bandcamp.com/album/integrated-tech-solutions) |

## Best of

![buck65](buck65albums.webp "Super Dope, Punk Rock B-Boy, 14 KT Gold, Drum Study #3, #2, and #1, North American Adonis")

I was spoiled this year with not one, but **several** excellent Buck 65 releases. Last year, after coming back from hiatus with a surprise release, I was overjoyed. This year, **Super Dope** took the cake and the blue ribbon and the whatever. This ensemble of releases is my choice for best of the year.

* [Super Dope](https://buck65.bandcamp.com/album/super-dope)
* [Punk Rock B-Boy](https://buck65.bandcamp.com/album/punk-rock-b-boy)
* [Drum Study #3](https://buck65.bandcamp.com/album/drum-study-3)
* [Drum Study #2](https://buck65.bandcamp.com/album/drum-study-2)
* [Drum Study #1](https://buck65.bandcamp.com/album/drum-study-1)
* [14 KT Gold](https://buck65.bandcamp.com/album/14-kt-gold)
* [North American Adonis](https://doseone.bandcamp.com/album/north-american-adonis) (Buck 65, doseone, Jel)
