---
title: "On making a video for a podcast"
subtitle: "(FFmpeg to the rescue)"
description: "How to make a waveform visualisation from a sound file."
slug: "ffmpeg-waveform"
date: 2023-07-29
lastmod: 2023-10-13
featured_image: "/blog/2023/07/ffmpeg-waveform/featured.webp"
draft: false
katex: false
tags:
  - Technology
  - Guide
next: true
nomenu: false
---

My partner has a podcast! It's in French, and it is about the history of Spain. So, if you speak French, check out *Pourquoi les espagnols sont comme ça ?*:

- [Spotify](https://open.spotify.com/show/4udndfFY1zzel98pz1d4Qj)
- [Apple Podcasts](https://podcasts.apple.com/us/podcast/pourquoi-les-espagnols-sont-comme-%C3%A7a/id1697635099)
- [Google Podcasts](https://podcasts.google.com/feed/aHR0cHM6Ly9hbmNob3IuZm0vcy9lNTNkNDJmNC9wb2RjYXN0L3Jzcw==)
- [RSS](https://anchor.fm/s/e53d42f4/podcast/rss)

It is also on [YouTube](https://yewtu.be/channel/UCcB8z7bto_2Pbw1AIFG7-Aw), and that is what this post is really about. Now, my better half is an independent woman. Stubborn, some might say, but fundamentally determined to do things on her own. She isn't shy about testing things out on computers, either. I was a little surprised when I saw her YouTube videos. I decided to let her do her thing and not be a backseat driver.

![Screenshot, Episode 3](ep3.webp "Screenshot from episode 3")

I like this. It is to the point, and she did spend some time on that cover art (she made it with Paint, millennial flex) so it makes sense to put it front and centre. She wanted more, though. She wanted titles, and logos, and a waveform. And didn't I open my mouth and say I could do that in 2 or 3 commands? I did. I bit my tongue after, but the damage was done.

"Could you really?" she asked.

"Send me the audio, and I'll take a crack at it..."

Readers, I had no idea how to do this, but I knew it could be done. I knew I had the tools at hand: **FFmpeg** and **ImageMagick**. And, I knew that with a search engine and 20 minutes, I could sort it out.

Well... I had a few doubts, because I know from prior experience that FFmpeg has *lots* of options and flags.

Luckily for me, a couple searches and some trial and error was all that it took.

## Making the labels/overlays

**ImageMagick** is fun and easy. Here are the commands I used for Episode 5:

```bash
convert -background none -fill "#FFDE59" \
    -font Nickainley-Normal -size 1100 \
    -gravity center label:"Pourquoi les espagnols sont comme ça ?" \
    title.png

convert -background none -fill "#FFDE59" \
    -font Nickainley-Normal -size x150 \
    -gravity center label:"Épisode 5 : Conquête Hispania (2ème Partie)" \
    episode0.png

montage cover.jpg -label '' episode0.png -geometry x150 -geometry +5+5 \
    -background none episode.png
```

(I still don't know how to combine/chain commands together, but it works)

![title](title.webp "Podcast title overlay")

![episode](episode.webp "Episode title overlay")

## Making the waveform and overlaying the images

After some trial and error (and searching[^1]), this is what I came up with:

```bash
ffmpeg -i input.wav -i bull.png -i title.png -i episode.png -filter_complex \
    "[0:a]aformat=channel_layouts=mono,showwaves=mode=cline:s=1920x1080:colors=#FFDE59[sw]; \
     color=s=1920x1080:c=#FF5757[bg]; \
     [bg][sw]overlay=format=auto:shortest=1,format=yuv420p[v]; \
     [v][1]overlay=(W-w)/2:(H-h)/2[v1]; [v1][2]overlay=(W-w)/2:20[v2]; [v2][3]overlay=(W-w)/2:910[v3]" \
    -map "[v3]" -map 0:a \
    -c:a aac -b:a 384k -profile:a aac_low \
    -c:v libx264 -preset veryfast -profile:v high -crf 18 -coder 1 -pix_fmt yuv420p -movflags +faststart -g 30 -bf 2 \
    -metadata title="title" -metadata year="2023" -metadata album="podcast name" \
    final-draft.mp4
```

This takes the input (`-i input.wav`) and runs it through a filter. That filter will make a 1920x1080 video of the waveform and overlays it on a coloured background.

![waveform screenshot](waveform.webp "Screenshot of the waveform")

### Overlaying the pictures

I had three images that needed to be overlaid: the two I made above, and a little logo of a bull. We wanted the bull in the middle and the other titles at the top and bottom. I did manage to chain these commands together. I suspect there are better ways to achieve the same results.

![Screenshot of the Final.mp4](final.webp "Screenshot of Final.mp4")

I think it looks pretty sharp for something made with ImageMagick and FFmpeg.

### Next Steps

I think I should leave space at the bottom for subtitles. What do you think?


[^1]: [YouTube recommended encoding settings on FFmpeg](https://gist.github.com/mikoim/27e4e0dc64e384adbcb91ff10a2d3678)
---

**New episodes of the podcast launch every Sunday!**

Here is episode 5:

{{< youtube a4lCHp-LceE >}}

