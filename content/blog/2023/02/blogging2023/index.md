---
title: "On Blogging in 2023"
subtitle: "it's all warm and cosy"
description: "A positive look at blogging in 2023."
date: 2023-02-07T09:44:35+01:00
lastmod: 2024-10-12
featured_image: /blog/2023/02/blogging2023/featured_small.webp
draft: false
katex: false
tags:
  - Blogging
next: true
nomenu: false
---

The vinyl bandwagon has made a few laps around the block in my lifetime. I have occasionally run alongside it, too. While I have never climbed up on my soapbox to tout the strengths of the format or rally followers under an "It Just Sounds Warmer" banner, I must admit that the ritual and process is pleasant. That little crackle when the needle drops; the minor changes you need to make to the levels and equalizer; the size of the liner notes and elaborate gatefolds of the jackets; all of these give vinyl a nostalgic *je ne sais quoi*.

![Cartoon of two people standing in front of a sound system for listening to vinyl records, the quote below is "The two things that really drew me to vinyl were the expense and the inconvenience."](Gregory-vinyl.webp "by Alex Gregory, for The New Yorker")

<p style="text-align: center";><a href="https://www.newyorker.com/cartoon/a19180">source</a></p>

In my home, the turntable holds a central position. On a slightly higher shelf, out of the reach of children, it sits flanked by two speakers. Many are drawn to it, only to notice my lack of tube amps (cue disdain) and low-end, entry-level, hardware (activate disgust). In reality, I rarely use my turntable. I suspect I am not the only one like this. It sits, in wait, for the chance to be used, much like Andy's toys in the *Toy Story* franchise.

An album collection, or toys in a trunk, is a decent metaphor for how things seem to work now. We are given new and shiny, but sporadically decide to flick though the collection and listen to a certain album on vinyl, hoping for warm feelings. *Toy Story* is done, we thought it was over. The toys had a warm, happy, ending. But, over at Disney someone ran out of *new* and is [considering](https://web.archive.org/web/20190603231638/https://comicbook.com/movies/2019/05/26/toy-story-4-end-disney-pixar-not-ruling-out-toy-story-5/) pulling out the faithful, warm, sound of the *Toy Story* franchise for another spin.

Looking around we can see Disney is not the only one. Our own nostalgia closets aren't just open --- we've torn the doors off. Our shirts are plaid, our pants are oversized and of the cargo variety, our footwear is made by the good Dr. Marten or features a Michael Jordan silhouette, and the hairstyles are throwbacks to *Dawson's Creek*. With our vinyl records spinning in the background, we feel warm and cosy.

In September 2016, Andrew Sullivan wrote "[I Used to Be a Human Being](https://nymag.com/intelligencer/2016/09/andrew-sullivan-my-distraction-sickness-and-yours.html)" for New York Magazine. He put into words a sentiment that had been building for years: our collective resentment of social networks. The abruptness of this disruption is noted:

> "We almost forget that ten years ago, there were no smartphones, and as recently as 2011, only a third of Americans owned one. Now nearly two-thirds do. That figure reaches 85 percent when you're only counting young adults. And 46 percent of Americans told Pew surveyors last year a simple but remarkable thing: They could not live without one. The device went from unknown to indispensable in less than a decade."

Since then, the resentment has not stopped growing. Different trends, like *Digital Minimalism*, took root (just look at the wiki for the /r/digitalminimalism subreddit for a taste). We strive for the warm feeling, but it seems out of reach and intangible, and our closet of nostalgia seems to provide nothing old for us. And so, today, 2023, we have begun looking back with sentimentality at a time, towards the end of the 90s, when blogging was a thing.

![Screenshot of The Open Diary in 1999](the-open-diary-1999.webp "The Open Diary homepage, c. 1999")

Before the arrival of social media and microblogging services, before Web 2.0 took off, we had methods of sharing our thoughts with strangers on the web. We blogged. Well, I didn't really, but people did this. And today, more and more people are flicking through their album collection, and digging in their toy trunk, for warmth and declaring that blogging is *it*. Blogging must come back!

Blogging à la 90s was a ritual. Lots of bloggers used FTP servers. There was a "manual" feeling about it. Other users followed you via RSS. You couldn't really know if people read what you wrote. You bounced ideas off walls, and bounce by bounce became part of a community. Blogger, now part of the Alphabet family, has been around since then.

![screenshot of Blogger.com, c. 1999](blogger-1999.webp "Blogger.com in 1999")

Some notable attempts have been made to inject new life into blogging. WordPress, Medium, Substack, and a slew of other tools are all popular. Another tribe of bloggers swears by [static site generators](https://en.wikipedia.org/wiki/Static_site_generator).

And here we are now, new tools in hand, hoping that one of our old tricks will fulfil the need. Monique Judd (in "[Bring back personal blogging](https://www.theverge.com/23513418/bring-back-personal-blogging)") wrote that "Twitter threads just don't do the trick" and personal blogs are "primary sources in the annals of history," so you should,

> "[b]uy that domain name. Carve your space out on the web. Tell your stories, build your community, and talk to your people. It doesn't have to be big. It doesn't have to be fancy. You don't have to reinvent the wheel. It doesn't need to duplicate any space that already exists on the web — in fact, it shouldn't. This is your creation. It's your expression. It should reflect you.
>
> "Bring back personal blogging in 2023."

![cartoon of a person in a shoe store, the shoes are divided into sections labeled 'running,', 'hiking,' and 'blogging.'](diffee-blogging.webp "by Matthew Diffee for The New Yorker")

<p style="text-align: center";><a href="https://www.newyorker.com/cartoon/a14283">source</a></p>

Similar to Sullivan's article, Judd's article (among others) kicked off some [threads on HN](https://news.ycombinator.com/item?id=34207842) and garnered some buzz. The trend had been building before this article was published. Blogrolls, Webrings, directories[^dirs], and challenges, like [100 Days To Offload](https://100daystooffload.com/), never died. In a warm corner of the web, a very niche corner, people have continued to share their thoughts and ideas with strangers, similar to how vinyl addicts never stopped collecting.

This circle of bloggers sometimes overlaps with neighbouring circles on the web, forming a virtual Venn diagram where people who dislike mainstream social media or Big Tech chin-wag on the Fediverse, swapping blogs with activists, techies, privacy experts, gamers, retro geeks, and many more. The [lay-offs of 2022--2023](https://layoffs.fyi/) increased the size of these circles to a point where the content can overflow into the mainstream attracting more and more users. This phenomenon [is not new](https://web.archive.org/web/20050101004415/http://www.fortune.com/fortune/technology/articles/0%2C15114%2C1011763-1%2C00.html).

The optimist in me believes that personal blogging could become bigger than it is. Many users already have a tendency to post using threads, recounting stories or events in a digestible way. I would go as far as calling it a modern skill: taking a technical situation and managing to write a more or less layman-friendly thread. This type of writing works as a thread because with each post there is drama and anticipation.

Threads like the one mentioned above, I must admit, rouse the pessimist in me. Would I have read that story as a single post on a random blog? Would I have even been aware of the post? _Maybe_, and _probably not_, are the honest answers. Despite only ever using Twitter for a few months in total, the concept of the thread, despite what Judd says, strikes me as enough in many cases, and the perfect medium in rare cases.

Taking someone who has honed their skill as a thread-writer and getting them to post it on a blog takes away from the interactions we've been conditioned to love. I am left with doubts whether reading blogs hits the addictive nerve centres like social media and podcasts do.

>"As I'm sure most people who keep a blog will tell you, one of the big benefits is just having somewhere to organize your thoughts and solidify your ideas. I personally see it as an alternative to keeping a journal, which I've always been lousy at. Knowing my writing might have an audience, no matter how small, is a good motivator in keeping it up and thinking up interesting things to write about."
>
><cite>[moddedBear](https://moddedbear.com/a-blogging-retrospective/)</cite>

Many personal blogs do _not_ seek that attention, though. I recall people questioning the point of blogs back in the early 2000s. What is the point of sharing if you don't know who is reading? As stated, that isn't the point for many bloggers. The remaining bloggers have all the new tools to work with, and can give you an answer to that question. Today, you can have access to data and metrics about your readers that were not readily available in the early 2000s. You can attribute numbers to your posts, which would certainly appeal to a number of social media users.

Blogging is, in the end, a *Pharmakon*. It is an agent of change, in the written form. For the active blogger, it works as a cure, giving an outlet, a place to offload thoughts and share, in a world that is becoming lonelier each year --- *the warm crackle of the needle hitting the vinyl*. For others, it will offer no healing effects. It will act as poison preventing the blogger from thinking for themselves --- *the visible disgust of the audiophile when they see your turntable*.

<p style="text-align: center";>###</p>

This article you have just read is not an *article*. It is not published, it is *posted*. A blog post has no formal rules, structure, or standard. *Should* I structure this like an essay with a thesis? *Should* I have a clear conclusion? *Should* I ask fewer questions? I suppose if I were a professional blogger I could have presented a clear thesis, given the context, discussed problems, consequences, solutions, examples, and created something witty, meaningful, and [blogworthy](https://en.wiktionary.org/wiki/blogworthy). This is not, as you can tell, a professional blog. It is my personal blog to do with as I please. And today it pleases me to end here, without a true conclusion, because I feel that I have sufficiently offloaded for now.

[^dirs]: Some examples of these would be [https://blogroll.org/](https://blogroll.org/), [https://personalsit.es/](https://personalsit.es/), [https://indieseek.xyz/](https://indieseek.xyz/), [https://indieblog.page/](https://indieblog.page/), [The Indieweb Webring](https://xn--sr8hvo.ws/), and [https://fediring.net/](https://fediring.net/)
