---
title: "Digicams are still fun"
subtitle: "Point, shoot, enjoy"
description: "A blog post talking about point-and-shoot digital cameras (digicams) and how they are still relevant today."
slug: picturetaker
date: 2023-08-29T22:38:13+02:00
lastmod: 2025-02-17
featured_image: /blog/2023/08/picturetaker/featured.webp
draft: false
katex: false
tags:
  - Technology
next: true
nomenu: false
---

I bought a little digicam.

It looks like this:

![Yello Kodak Pixpro WPZ2](wpz2.webp "Kodak Pixpro WPZ2")

It is yellow and plastic and is exactly what you would expect to get when you go into the local electronics shop and ask for the cheapest thing they have. Well, this was the second cheapest. I probably should have gone for the cheapest since this model is from 2019. But it doesn't matter. This (very) little yellow gadget---it is practically a toy in some aspects---brought back childhood memories of [Kolorkins](https://youtube.com/watch?v=CCPyQ0sW3P0), and so I forked over my cash.

## Why?

I am not a photographer, but I think photography is an awesome hobby, passion, and art form. When I was younger, around the time Kodak was marketing Kolorkins, I always wanted to touch my mother's camera. That, however, was a big "no-no." It was such a mysterious device, and I was convinced at the time that it was either extremely high-tech, expensive, fragile, or all of the above. I don't think it was any of those things. It was just something she cherished.

Once, at a used clothing store of all places, I found a 35mm camera for sale. It was $5 or something, so I took my allowance and bought it. It was one of those junky plastic things that you needed to manually advance the film on. I used that for a few years, and into university. Digital cameras were definitely around by then, but still expensive. I remember taking it to a Matthew Good concert at the Marquee in Halifax. Melissa Auf der Maur was the opening act. I had a roll of 24 pictures to take. I had to keep my finger over the flash. One of those shots came out great (**UPDATE: Found the photo!**):

![Matthew Good performing at The Marquee Club in Halifax Nova Scotia, 2004-10-31](matthewgoodband.webp)

Even before this time, though, my mother had retired her treasured 35mm and moved on to a compact digital camera: a Samsung Digimax A5. No, I don't have an eidetic memory; I have EXIF Data and this picture of the sweetest dog ever:

![Phoebe, malamute/husky standing by her house](phoebe.webp "Phoebe (a good dog)")

This photo was taken on the 4th of July 2005, at 5:55 in the morning. I can smell this photo.

When I moved out, I was gifted a digicam of my own. It was a Fujifilm Finepix A805. That thing went places. The advent of phones with better cameras lead me to sell it sometime in 2009 when I bought an [LG Viewty](https://en.wikipedia.org/wiki/LG_Viewty) and thought to myself, "Why would I ever need a camera again?"

## I was wrong

Sometime, in 2014, I was fed up with the pictures my phone could take. So I marched down to the camera shop and walked out with a reasonable camera for someone who has no idea what they are doing: a Sony HX60V. I found an e-book about digital photography and read nearly the whole thing on a flight to Canada. I was going to get a picture of the *supermoon*.

I did. It turned out great. I was eaten alive by blackflies and mosquitos, but I got that picture. And then I took pictures of everything. I put them on Flickr and made a shared album on Google Photos. There were weeks that I was out taking pictures every single day. It was cool. That camera went on holiday with me too. Totally reasonable and worthwhile investment.

I did take some decent pictures, too. One was used on a tourist website, another in an article about trees, and another was used for the cover of a Moleskine notepad. But, below are two of my favourites:

![house and toad](new.webp "shack by the sea / tiny toad")

## Little fingers

When my son was born, I was so happy to have a camera to capture memories with. Phones can be stolen or hacked. I wanted those memories in a safe offline place. Children, though, have a tendency to grow. And they like to touch things. So, that camera found its way onto a shelf until this summer, when I was planning to take a very short trip. I took it out and was sad to discover, that through disuse or possibly a fall, the flash had stopped working. I will repair it, but I wanted to be able to walk around snapping pictures. And then, a little voice that I hadn't heard in years whispered in my ear.

"Fuck it," it said. "Just go buy a cheap digicam."

And I was out the door...

## It looks like a toy... because it is fun

I bought it, took it out of the box---in the street---popped in a microSD card and started snapping. It became clear that this is something that even my son will be able to use. A smile crept across my face.

All the fun stuff is built in:

* filters for colours, fisheye, negative, montages, etc.
* scene modes
* full auto and semi-auto (no true manual mode)
* you can control it with an app
* you can crop and rotate photos
* you can record videos, even slow-motion

But, most importantly: **you point** and **you shoot**. The LCD on this thing is piddly. You will not be stopping between shots to admire your craft or show to your friends. This is a thing that dangles from your wrist to be used at a moment's notice. Getting a steady picture requires holding your breath and genuine hope. You could turn on the full info viewfinder mode to have a grid and a histogram, but, honestly, it is just too small to care about.

![capture from the WPZ2 manual](screeninfo.webp "poor kitty")

## Keep it easy; call it a day

Backing up photos is really easy now. I just use [gphoto2](http://gphoto.org/). Plug it in and run 

```bash
gphoto2 --get-all-files --skip-existing \
--filename="%Y/%m/%d/%Y%m%d_%H%M%S.%C" && \
rm */*/*/*.THM
```

and call it a day.
