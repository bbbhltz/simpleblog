---
title: "On Debloating Android Phones"
subtitle: "Getting rid of the things you don't need"
description: "How to debloat or degoogle your Android phone using adb."
date: 2023-05-20
lastmod: 2024-08-01
featured_image: /blog/2023/05/on-debloating-android-phones/featured.png
draft: false
katex: false
tags:
  - Technology
  - Privacy
next: true
nomenu: false
---

Recently, a new Android phone has come into my possession. It is fortunately *very* light on the bloatware that comes on many devices. Just the standard Google stuff, the required packages, and 4 OEM packages. What's more, it is very up-to-date and will probably stay that way based on the company's reputation (I am purposely refraining from saying that company name because it is irrelevant here).

The unfortunate thing about even the least bloated phones on the market is that it is still impossible to remove certain applications. Gmail, for example, is of no use to me. It isn't exactly hurting me being there, but I want it out because that is what I do on my phones. Like all but one phone I have ever owned, this phone cannot be rooted. There is a tool for that.

[UAD-ng](https://github.com/Universal-Debloater-Alliance/universal-android-debloater-next-generation), the *Universal Android Debloater (Next Generation)* is a GUI that uses ADB to debloat non-rooted android devices. This is my usual go-to. It is self-explanatory in use and seems active enough by my standards. This time around, it wouldn't launch. I tried both binaries on my laptop before realizing that since [Alpine Linux](https://alpinelinux.org/) uses [musl libc](https://musl.libc.org/) instead of the GNU equivalent, I may need to build it myself. Well, I gave it my best and it wasn't good enough. I could not get that sucker to build (I think I may have sorted out my problem now, better late than never) and patience ran out, so I just did it manually.

Here is how I got the Google (sort of) off my phone.

## Requirements

* [Android SDK Platform Tools](https://developer.android.com/tools/releases/platform-tools): The Android tools (`adb` is the one we want) are usually easy enough to install. There are loads of other, more precise, resources on the web that explain how to enable [developer options](https://developer.android.com/studio/debug/dev-options) on your phone and install these tools on your computer.

* [jq](https://stedolan.github.io/jq/): an application included in many Linux distributions and also works on Mac and Windows.

* The [UAD Package List](https://github.com/Universal-Debloater-Alliance/universal-android-debloater-next-generation/raw/main/resources/assets/uad_lists.json): You can grab the package list from the website.

* A little time: I would start by making a directory to keep track of things. I called mine `debloat`. Now run

```
$ wget https://github.com/Universal-Debloater-Alliance/universal-android-debloater-next-generation/raw/main/resources/assets/uad_lists.json
```

to get the JSON file we want.

## Removal Decisions

1. Get the package list from your phone
2. Get the recommended removable packages from the UAD JSON
3. Compare
4. Double-check
5. Remove the packages from your phone

If your phone is connected to your computer and `adb devices` shows your device (sometimes you need to put in a pin or confirm on your phone), you should be able to run this command and be given a list of packages:

```
$ adb shell pm list packages | cut -f 2 -d ":"

* daemon not running; starting now at tcp:5037
* daemon started successfully
com.google.android.providers.media.module
com.qti.phone
com.google.android.overlay.modules.permissioncontroller.forframework
com.android.calllogbackup
com.qualcomm.qti.lpa
com.qualcomm.atfwd
com.qualcomm.qti.cne
com.android.dreams.phototable
com.google.android.overlay.gmsconfig.comms
com.android.providers.contacts
com.qualcomm.uimremoteserver
com.qti.pasrservice
com.android.dreams.basic
...
```

I put it in a text file by adding ` > packages.txt` to the command.

Next, we need to use **jq** to check the **UAD list** for removable Google packages.

```
$ jq -r '.[]' uad_lists.json | jq -r 'select(.list=="Google" and .removal=="Recommended")' | jq -r '.id' > removable.txt
```

Now we have a `packages.txt` file and a `removable.txt` file. There are lots of ways to compare, but on a quiet night with nothing to do, one may not feel like opening up LibreOffice Calc or using an online tool. A couple lines of Python works in this case.

```python
with open("packages.txt", "r") as installed:
    package_reader = installed.readlines()
    package_list = set()
    for package in package_reader:
        package_list.add(package.replace("\n", ""))
    # print(package_list)

with open("removable.txt", "r") as removable:
    removable_reader = removable.readlines()
    removable_list = set()
    for rpackage in removable_reader:
        removable_list.add(rpackage.replace("\n", ""))
    # print(removable_list)

debloat = set(package_list) & set(removable_list)

for bloat in debloat:
    print(f"adb shell pm uninstall --user 0 {bloat}")
```

This should print out a list of removable packages with the appropriate command prepended to each line.

```
$ python chck_pkgs.py
 
adb shell pm uninstall --user 0 com.google.android.feedback
adb shell pm uninstall --user 0 com.google.android.as
adb shell pm uninstall --user 0 com.google.android.apps.wellbeing
adb shell pm uninstall --user 0 com.google.android.marvin.talkback
adb shell pm uninstall --user 0 com.google.android.setupwizard
adb shell pm uninstall --user 0 com.google.android.deskclock

```

Some Google packages have obvious names, `com.android.chrome` is clearly Chrome. But, what is `com.google.android.apps.tachyon`? Well, that's Google Duo/Meet, of course! 

So, it is time to **double-check** instead of going crazy on the removals. Just because some people on the Internet suggest something does not mean it is the right thing for you to do. After all, breaking the functionalities of a device you just paid money for in the name of privacy is just silly.

I read through the descriptions of many of the packages before giving them the boot. Either open the UAD list file in a text editor or ask jq to do the work for us:

```
$ jq -r '.[]' uad_lists.json | jq -r 'select(.list=="Google" and .removal=="Recommended")' | jq -r '.id + ": " + .description'
```

If you decide that you need one of the packages that you removed, it can be restored using:

```
adb shell cmd package install-existing --user 0 <packagename>
```

This is because the packages were only removed for the user, but not removed from the device. Worst case scenario: factory reset will restore all packages and unbreak anything that needs to be unbroken.

## Final words

The best place for information on doing this likely the [XDA Forums](https://xdaforums.com/) where you can likely find device-specific recommendations.

I have found some benefits to "removing" these packages: you won't save much space, but possibly some battery will be saved. Also, if you live somewhere where it is not reasonable to pay for unlimited data, some of these packages might be using data that you want to keep for yourself. It is certainly worth looking into in you have devices by other brands that include OEM-specific bloatware that you never use.
