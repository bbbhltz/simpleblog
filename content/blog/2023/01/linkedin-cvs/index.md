---
title: "TMI: Sharing your CV on LinkedIn"
subtitle: "Sharer Beware"
description: "A warning for those sharing their CV on LinkedIn on what to avoid. Also, Canva is not as cool as you think."
date: 2023-01-30T22:05:02+01:00
lastmod: 2024-10-13
featured_image: /blog/2023/01/linkedin-cvs/featured.webp
draft: false
katex: false
tags:
  - Privacy
  - Social Networking
  - Education
next: true
nomenu: false
---

{{< box info >}}
The images provided here have been anonymized in such a way to preserve the original appearance. This is not meant to be a personal attack on anyone, but if you recognize your CV here, [email me](mailto:contact@bobbyhiltz.com) and I will make it disappear.
{{</box >}}

I am fed up.

LinkedIn, the "professional" network, is a valuable tool for jobseekers. Not a guarantee, mind you, of finding the dream job, just a tool. There are multiple functions on this site. There is the social function, where you can post thoughts and articles, and, there is the profile function, where you put your employment and educational history. The latter serves as an online CV (or résumé) to help you find the perfect job or perfect employee.

Schools around the world teach digital literacy and computer skills at different levels of education. We see stories online and on TV telling us to be wary. Just read [this story about a stalker who used social media and dating profiles to track a woman to her home address](https://scribe.rip/@major-grooves/how-we-turned-the-tables-to-catch-my-sisters-bumble-stalker-e1979d39670d). This story ended fine, with the stalker being punished. Not all stories end this way.

So, what about LinkedIn has me fed up?

## The Problem

I find it inconceivable, unbelievable, and downright moronic that many young people would double-down on a platform like LinkedIn by having a profile *and* using the social feature to share a PDF of their CV.

"Well," you'll say, "it looks nice and it is eye-catching. You don't understand, silly millennial!"

And, I will agree in part. Despite having nearly 20 years of experience with social networks, and over 25 years of Internet under my stressed out belt, I don't understand. In particular, I don't understand how you could be so careless to share **a)** a PDF file that contains metadata, sometimes about your device, and **b)** also include details like your **personal phone number, personal email address, home address, and date of birth**. And I'm not even mentioning the fact that you include your photo. Have you not heard of identity theft? Do you think that only the people in your 1st degree network can see that?

Now, clearly, there are some reasonable people out there that take the time to remove personal info from a document like that before sharing. And still others that set the sharing permissions. But, it is a site with a search bar. So, being curious, I decided to search for posts containing phrases like "my CV," or "my resume."

The expected happened. Post after post, 1st degree and 3rd degree contacts, of PDF files being shared.

![a collection of résumés](output.webp "Now I know where you live")

LinkedIn lets you download the PDF. So I downloaded enough to be able to get some numbers (I also sorted by most recent, and included all levels of contacts). Based on what I was able to see, both directly and indirectly by reading the CVs and looking at the metadata, I can say that:

- 20% of my contacts shared their full date of birth
- 50% used their school-provided email addresses
- 50% used their personal email addresses
- 40% shared their full home address, including street number
- 100% shared their personal mobile number

I literally know where you live. I can look at your addresses on street view. Not only that, but I can search for you on other social networks with your personal email. That is creepy.

Scratching away at the surface, I know that (roughly):

- 10% made their CV using Google Docs
- 10% used PowerPoint (why?)
- 20% used Word (one of which used [Hloom](https://www.hloom.com/); another the 2013 edition of Word)
- 40% used Canva
- 20% of the PDF files I downloaded included no information besides the creation date

{{< box info >}}
**re: Canva**

I cannot knock what I haven't tried, but if you are going to use Canva, at least be smart. One of the PDF files even included the name of the template. It is called ["Foncé Violet Femme Photo Service à la clientèle C.V."](https://www.canva.com/fr_fr/modeles/EADmzi_-K4Q-fonce-violet-femme-photo-service-a-la-clientele-c-v-/). Don't list *Canva* as a skill if you just know how to edit a template.
{{< /box >}}

## Simple Solutions

### TMI

Don't include that personal information when you share a PDF of your CV. Problem solved! It is, however, somewhat important to include your email address. The way around this issue is via **aliases**.

An alias is still your email address, but you will know where that email came from. Here is an example:

- If you use Gmail (so early 2000s) you actually have built-in aliases! Imagine your email address is `joan.dark@gmail.com`. Guess what? `joandark@gmail.com` is also your address. As is `j.oandark@gmail.com` (at least it was the last time I checked, let me check again right now...yup, still works!).
- You also have `+` addresses, so `joan.dark+linkedin@gmail.com` works too, but that doesn't look great on a CV.
- Now, when you open an email you will see in the `to:` field that it was sent to `joandark@gmail.com` instead of your regular address, and you will know that someone got it from your LinkedIn CV.

This is, as stated above, a simple solution. There are others, like creating a separate email address just for job searches, that many would see as a hassle.

## Standing out from the crowd

Looking at the picture above, you will notice that more than one of those CVs uses the 2-column template. If you have never encountered this before, just know that it is overwhelmingly used in France. So many CVs look the same. The lack of creativity comes from using templates, or perhaps there is some logic to using this template.

Canva, and many tools like it, seem like creative ways of doing things. In the end, it is overkill. There are colours and shapes, lists, icons, and other flourishes that neither reveal your creativity nor your actual skill level.

Tell me, for example, why in your soft skills section do you rate your communication skills as 4-stars? Is it out of 5? 6? If you are a skilled communicator, why can you not communicate how skilled you are at communicating *with words*? And what is a 4/5 in Excel? Does that mean you can use VBA, or do you just know how to filter columns?

You want to add pizazz to your CV? Try something out of the ordinary? Learn how to use the tool. Are you a creative person? Make an online portfolio.

## Don't blame yourself

A [study by Dell of 15,105 people, aged 18--26, across 15 markets](https://www.dell.com/en-us/dt/perspectives/gen-z.htm) revealed that *44% of Gen Z feel as though both schools and businesses should work together to bridge the digital skills gap; 1/3 feel that their school education did not prepare them with the technology skills needed for their planned career; and 56% received either very basic or no digital skills training*.

Digital literacy concerns us all, and educators need to underline the importance of digital privacy. Perhaps they do, and you just forgot. Be wary of social networks, even the professional ones. Become digital natives one step at a time. Help your friends along the way, and *stop telling us all where you live*.
