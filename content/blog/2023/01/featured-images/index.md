---
title: "Adding Featured Images to My Hugo Blog"
slug: featured-images
subtitle: "(the thing where you see a picture on social media, you know?)"
description: "A meta-post about how to add featured images to a Hugo SSG blog."
date: 2023-01-02T12:30:00+01:00
lastmod: 2024-10-13
featured_image: /blog/2023/01/featured-images/linkedin.webp
draft: false
katex: false
tags:
  - Blogging
  - Guide
next: true
nomenu: false
---

It is funny how little I understand the tools that make this blog work. Sure, I can (sort of) read HTML, and I can guess my way through CSS and JavaScript. But, when it came to making this blog, I just picked Hugo and stuck with it. I followed Mogwai's [Creating a simple.css Site with Hugo](https://mogwai.be/creating-a-simple.css-site-with-hugo/) guide and pushed it to the web and called it a day. Then I followed another guide, Simone Silvestroni's [A human-readable RSS feed with Jekyll](https://minutestomidnight.co.uk/blog/build-a-human-readable-rss-with-jekyll/), in order to add a [slightly different RSS feed](https://bbbhltz.codeberg.page/rss.xml) to my page. Then, [yet another guide](https://boundedinfinity.github.io/2017/09/admonitions-in-hugo/) for adding admonitions.

I would call it a *Frankenblog*, but that would imply that I am some sort of scientist who knows what's going on. It is a lot of guesswork, and it is fun, and when I screw up there is <kbd>CTRL+Z</kbd>. The last guide mentioned above, on admonitions, is a good example of how things go for me. Do you think I knew what an *admonition* was before I happened across that blog post? No, I did not. And today it happened again, for I didn't know what to call the "Featured Image" that appears on social media when I share my blog. I really didn't care about it, and I still don't, but it looks nice and turns out that once you know what you are looking for, the answers are out there.

I came across [https://metatags.io/](https://metatags.io/) which is *a tool to debug and generate meta tag code for any website*. I plugged one of my blog entries into it and...

![screenshot from metatags.io showing a lack of featured images](metatags.webp "missing something...")

...it looks a little dull.

From there I searched and quickly found the guide "[Adding meta tags in Hugo blog](https://erinjeong.com/posts/metatags-hugo/)" by Erin Jeong. It turns out that was what my blog was missing: meta tags!

You can see for yourself that there isn't much to it, and in my case I only needed to modify two files:

* `head.html` in `/layout/partials`
* my `config.toml`

Here is the line added to `head.html`.

```go
{{ if .Param "featured_image" }}<meta property="og:image" content="{{ .Param "featured_image" | absURL }}">{{ else }}<meta property="og:image" content="{{ $.Site.Params.Avatar | absURL }}">{{ end }}
```

Here is what I added to my `config.toml`

```go
[params]
  avatar = "Avatar1024.png"
```

As an experiment, I modified an older post that already had images in it. Returning to the Meta Tags site, it now shows a "card" with an image. That is what I was hoping for.

![screenshot from metatags.io showing featured images on an article](newcc.webp "success!")

Using post validation tools from LinkedIn and Facebook confirms that it should work!

![Facebook post validation tool screenshot](fb.webp "Facebook Sharing Debugger")

![LinkedIn post validation tool screenshot](linkedin.webp "LinkedIn Post Inspector")

(I decided to add the other meta tags to my blog while writing, for Twitter cards and Description)
