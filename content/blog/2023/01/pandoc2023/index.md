---
title: "Using Pandoc for My Slides: 2023 Update"
slug: pandoc2023
subtitle: "Look at the colours!"
description: "How to make a very basic custom Beamer theme to use with Markdown and Pandoc."
date: 2023-01-27T11:50:50+01:00
lastmod: 2024-10-12
featured_image: /blog/2023/01/pandoc2023/feature.webp
draft: false
katex: false
tags:
  - Pandoc
  - Markdown
  - Education
  - Guide
next: true
nomenu: false
---

In March 2021 [I wrote about using pandoc to make my presentations for class](/blog/2021/03/pandoc2021). Towards the end of that post, I wrote:

> My next quest will be to figure out how to make my own themes and styles. **If you know how or have a nice, simple, guide, let me know**.

Well, it turns out that I was not far from a *simple* solution. With the recent release of [pandoc 3.0](https://pandoc.org/releases.html#pandoc-3.0-2023-01-18) I was reminded that pandoc is, quite certainly, my most-used-tool for teaching --- I use it for my slides, and all the different documents that I need to make at my various teaching positions.

## Adding some colour

Being quite content with the built-in options for themes (`colortheme`, `innertheme` and `outertheme`) I was under no pressure to seek out a solution. That is, until it became necessary to my sanity to be able to open a presentation and immediately recognize what school it was for --- I teach at four different places!

The answer was in the [Beamer user Guide](https://ctan.gutenberg-asso.fr/macros/latex/contrib/beamer/doc/beameruserguide.pdf) and involves making a small `.tex` file.

Here is a (slightly modified) version that I use for one of my schools:

```latex

% define some new colours
% NB RGB and rgb are not the same thing!

\definecolor{sgreen}{RGB}{52,201,33}
\definecolor{lessdarkbackground}{RGB}{95,96,93}
\definecolor{darkbackground}{RGB}{40,38,39}
\definecolor{sorange}{RGB}{235,175,0}
\definecolor{sred}{RGB}{212,33,39}

% tell LaTeX to use one of those colours for the structure

\usecolortheme[named=darkbackground]{structure}

% tell LaTeX which colours to use for which elements

\setbeamertemplate{blocks}[shadow=true]
\setbeamercolor{background canvas}{bg=lessdarkbackground}
\setbeamercolor{block title}{bg=darkbackground, fg=sorange}
\setbeamercolor{block body}{bg=darkbackground, fg=white}
\setbeamercolor{normal text}{fg=white}
\setbeamercolor{titlelike}{fg=sorange}
\setbeamercolor{itemize item}{fg=sorange}
\setbeamercolor{itemize subitem}{fg=sorange}
\setbeamercolor{itemize subsubitem}{fg=sorange}
\setbeamercolor{enumerate item}{fg=sorange}
\setbeamercolor{enumerate subitem}{fg=sorange}
\setbeamercolor{enumerate subsubitem}{fg=sorange}
\setbeamercolor{section number projected}{fg=white, bg=sorange}

% some extra commands

\usepackage[os=mac]{menukeys}
\usepackage{fancybox}
\usepackage[many]{tcolorbox}
\usepackage{pgf}
\usepackage[normalem]{ulem}
```

These LaTeX instructions override the default blue theme, `whale`, and add a few extra functions that I rarely use, but don't want to bother looking up again.

Running pandoc with the appropriate commands gives me this:

![set of 6 slides using modified colour scheme made with pandoc](all.webp "Open the image to see the gory details")

Personally, this is not my favourite colour scheme. It does match what is used at the school, however, and that is what I was going for.

## Why the quote?

I included a slide with a quote to point out that some solutions are more obvious than others. I am not at all comfortable with LaTeX, maybe someday. For now, I have the tools I need, and time-wise I can manage. I believe that learning LaTeX would be a logical step for someone in my position: many academics use LaTeX --- I am just a professor, and do not need to publish, but knowing this skill would be an advantage.

Moving on to the quote.

I like to keep things simple. A quote, with italic or emphasized text, followed by the name of the person quoted, flushed right, boldfaced, preceded by "---."

See? Simple.

At one point, I had some trouble getting the author's name to the right of the slide. Then, I had trouble getting the text to be **bold** without resorting to LaTeX commands like `\textbf`.

As is usually the case, the problem was how I was positioning the Markdown markup.

This was wrong:

```markdown
\hfill **--- Kim Stanley Robinson** \
\hfill *The Ministry for the Future* (2021)
```
Why? Because of how pandoc interprets the `*` following the `\hfill` (horizontal fill) command. It just doesn't work like that.

This is the simple solution:

```markdown
**\hfill --- Kim Stanley Robinson** \
*\hfill The Ministry for the Future* (2021)
```

Wrapping the `\hfill` command in the markup takes care of the problem. No more resorting to LaTeX commands.

## Workflow 2023

*Nothing much has changed since [before](/blog/2021/03/pandoc2021/#the-workflow) in terms of workflow.*

Generally, I keep a "build" folder for each of my employers. Things have become disorganized since deciding on this structure, and I need to revamp it. But, right now, it might look like this:

```
SCHOOL
└── COURSE
    └── build
        ├── assets
        │   └── logo.webp
        ├── Slides.md
        └── style.tex

```

I would prepare my Markdown file with the appropriate front matter and run:

```
pandoc -t beamer --pdf-engine=xelatex -H style.tex input.md -o output.pdf
```

(`-t beamer` is the output type, `--pdf-engine=xelatex` because I like to use different fonts, `-H style.tex` includes my "theme" in the header)

Doing that is not what I call a fluid workflow. And I have seen many, many solutions. My "solution" is to use [Kate](https://kate-editor.org/).

Kate has an "External Tools" plugin. With that, it is possible to create a button, or menu item, to do the work for you.

![Kate screenshot, external tools config](kate-ext.webp "Several little tools, like spellchecking with LanguageTool")

![Kate screenshot, pandoc convert to beamer example](kate-beamer.webp "Under the hood")

So, now I have a single-click "build" button for Markdown files. That means I can "preview" my slides when I want by leaving [zathura](https://pwmt.org/projects/zathura/) open. I also tend to spellcheck things frequently.

Once I have the PDF file, I manually copy it into my shared [Syncthing](https://syncthing.net/) folder, so the file is on my phone. Then, when I turn on my work computer, the file will be synced to it as well.

Ready to teach.

## The Next Step

**Learning how to make a bloody Makefile!**

I don't need it (yet), but it would be nice to know how to do this. It would streamline things, for certain.

## Some links

**pandoc** has a [website](https://pandoc.org/) and a [GitHub](https://github.com/jgm/pandoc). It can be [installed](https://github.com/jgm/pandoc/blob/main/INSTALL.md) on different operating systems.

I highly recommend you follow the pandoc account on Mastodon [https://fosstodon.org/@pandoc](https://fosstodon.org/@pandoc) as well as [https://scholar.social/@tarleb](https://scholar.social/@tarleb); those accounts are overflowing with tips like [this trick to prevent pandoc from adding title, date or author to the document text](https://fosstodon.org/@pandoc/109703343312665935) or [how to create example and alert blocks in Beamer](https://fosstodon.org/@pandoc/109511136405074648).
