---
title: "Claws Mail"
slug: claws-mail
subtitle: "An email client that keeps things light and straightforward"
description: "A positive review of my favourite email client: Claws Mail."
date: 2023-01-06T10:42:46+01:00
lastmod: 2025-02-17
featured_image: /blog/2O23/01/claws-mail/claws-featured.webp
draft: false
katex: false
tags:
  - Technology
  - Review
  - Linux
next: true
nomenu: false
---

{{< box info >}}
*The following is neither a review nor a recommendation. It is a brief look at one of my [most used pieces of software](/software).*
{{< /box >}}

You turn on your computer. There is work to be done. Work sometimes requires communication, and despite all the amazing ways we have to communicate, email is still here.

I do not lament the persistence of email; it is my preferred method of communication. I like how an email is just *there*. Do you want to reply to it now? It doesn't matter. Email can wait awhile.

## Client Conundrum

Desktop email clients come in several varieties, or flavours:

* All Dressed
* Gourmet (think Sea Salt & Malt Vinegar)
* Plain Salted

While some of you may have never heard of [All Dressed](https://en.wikipedia.org/wiki/All-dressed), I am using it here to refer to email software that is, in fact, [Personal Information Management](https://en.wikipedia.org/wiki/Personal_information_manager), or **PIM**, software. PIM software is what we are used to when we think of "email software." It includes email, but also an address book, calendar, alerts, scheduling, and even RSS sometimes. Outlook and Thunderbird are the two most popular examples (forgive me, I have never used a Mac, so I am likely ignoring an obvious example).

Your Gourmet clients may have some elements of PIM software, but their focus is more on the visual panache; an attempt at adding zest to the bland potato that is email. These are the "minimal" clients that look a little nicer and newer, like [Geary](https://wiki.gnome.org/Apps/Geary) or [Mailspring](https://github.com/Foundry376/Mailspring).

Finally, we have the plain salted variety. This is where **Claws Mail** is found. It does email. It *can* do some other things, but really, it is all about email.

## Why Use Claws?

Claws was always on my radar, and I had tried it out multiple times. Two years ago, however, it started becoming easier to have a desktop email client. The number of employers providing email addresses began to grow, and they all insisted on using Office365. My workflow was thrown off.

Traditionally, I use the browser-based webmail client for all emails. They are generally just as quick to access, and they provide notifications. No problem. That is, until you have three different employers using Office365, and it becomes challenging to stay logged into several accounts at once! There are workarounds for this, but why bother?

And so, along came the *need* for a desktop email client. It goes without saying that I tried Thunderbird, Evolution, KMail, and the rest. Thunderbird worked the best, but it is an absolute behemoth. It is a *suite* of integrated software. At the time I was testing these out, my desktop was actually a Raspberry Pi 400, and Thunderbird was not friendly with that device --- even when running from an SSD.

This also coincided with moving my personal email away from Gmail and transferring emails over to Mailbox.org. The decision to move away from that service was part of a bigger personal project: [digital privacy](/blog/2022/03/low-friction-introduction-to-digital-privacy/). Furthermore, there was a growing frustration on my behalf with HTML emails; it seemed as though all of my employers decided that adding multiple images and graphics to their signatures was a great idea.

Therefore, a low-end computer, a multiplication of employers using Office365, and a distaste for "fancy" emails put me on the path to Claws.

## Overview

Claws Mail is, in their own words, *"user-friendly, lightweight, and fast"*. There are the [features](https://www.claws-mail.org/features.php) you'd expect, along with a few extras. Something that might surprise users coming from other desktop clients is the appearance. Although similar to modern clients, there is a nostalgia present as well --- in a good way.

![screenshot of Claws Mail in standard view with 3 panes](basic-claws2.webp "Keeping it simple")

There are all the panes one would expect to find: lists, columns, messages, etc. And there are the familiar buttons to check, send, reply, delete, and so on. There isn't a calendar (although it can be done using a plugin) but there is an address book. Diving into some of the preferences, it is apparent that some degree of personalization is available. There are a few themes, folder colours can be changed, signatures can be edited, and you can sign and encrypt emails. Nothing out of the ordinary, in other words. A set of [plugins](https://www.claws-mail.org/plugins.php) is available too, and with them, you can extend Claws Mail and use it as an RSS newsfeed reader, have access to some calendars, deal with spam, digitally sign and encrypt emails, and even show avatars.

## Does it work?

The short answer is, yes, it works wonderfully. It does require flexibility on the part of the user, though. There are methods to sync your address book [using Vdirsyncer](https://vdirsyncer.readthedocs.io/en/stable/tutorials/claws-mail.html) (read-only!). There is a plugin for calendars. I was convinced that lack of a calendar and synchronized contacts would be a deal-breaker. It turns out that it isn't a big deal.

I postponed synchronizing my contacts for a week or so, before realizing that it doesn't matter. Most of the time I reply to messages, sometimes I write messages. The messages that I write for my work are generally to the same people. It is quite rare that I need to have more than 25 email addresses in my address book. Claws lets you right-click > "Add contact" so that issue solved itself.

> Claws Mail is an email client aiming at being fast, easy-to-use and powerful. It is mostly desktop-independent, but tries to integrate with your desktop as best as possible. The Claws Mail developers try hard to keep it lightweight, so that it should be usable on low-end computers without much memory or CPU power.
>
> <cite>--- Claws Mail Documentation</cite>

To answer the question with some more detail, Claws is user-friendly. There is a familiarity to the functions. There might be a speed-bump here or there when adding accounts (what's the SMTP port for Office365 again?), but many desktop clients require manual adjustments for certain types of accounts.

![screenshot of KDE system monitor](performance.webp "Less than text editor while idling")

Claws is fast. That is true. RAM usage is low, it starts quickly, it quits quickly. There are moments of lag. For example, if I happen to try and do several operations at once at the same moment it tries to synchronize all of my accounts. It has never once crashed, however. Neither has it locked up for more than a few seconds. This is quite different from my experience with KMail and even Thunderbird.

Claws Mail is an *effective* piece of software, which is a sign of power. The [small team](https://www.claws-mail.org/theteam.php) behind the project has done a great job putting nearly everything a perfect email client needs in a small package.

## Favourite Extra Features

1. **Minimize to tray**: email needs to be put in the corner sometimes.\
*Claws Mail has a [notification plugin](https://www.claws-mail.org/plugin.php?plugin=notification) with a good number of options.*
2. **Themes**: a little visual flourish doesn't hurt.\
*A small [set of themes is available](https://www.claws-mail.org/themes.php). And sometimes you can find ones in the wild, like the [Papirus theme](https://github.com/PapirusDevelopmentTeam/papirus-claws-mail-theme).*
3. **Fancy HTML Plugin**: for when you do receive HTML email.\
*Can be configured to use custom CSS, disable JavaScript, images, etc.*

## Tips

### Hide your timezone

In your `~/.claws-mail/clawsrc` find the `hide_timezone` key and change it to **1**. This will prevent your timezone from being shared when you send emails.

```
hide_timezone=1
```

### Hide your user agent

In the account preferences, in the *Send* options, uncheck *Add user agent header*. Nobody needs to know what email client your are using.

## Conclusion

With a little effort, switching from the web frontend to a desktop client was painless. (If you are wondering, I do handle contacts and calendars with [khard](https://github.com/lucc/khard), [khal](https://github.com/pimutils/khal), and [vdirsyncer](https://github.com/pimutils/vdirsyncer). I keep [some config files for them in my repo](https://codeberg.org/bbbhltz/dotfiles/src/branch/main/configs)). Claws Mail gets the job done, is fast, and is [plaintext](https://useplaintext.email/)-friendly.

## Links

* [Claws Mail Homepage](https://www.claws-mail.org/index.php)
