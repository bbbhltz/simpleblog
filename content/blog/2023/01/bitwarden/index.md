---
title: "Using Bitwarden CLI to Create Login Items"
subtitle: "A quick guide, and a reminder to myself"
description: "How to use the Bitwarden CLI to create login items."
date: 2023-01-01T21:25:14+01:00
lastmod: 2024-10-12
draft: false
katex: false
tags:
  - Privacy
  - Programming
next: true
nomenu: false
---

I read the docs for the [Bitwarden CLI](https://bitwarden.com/help/cli/). All straightforward enough. But, I am not always clear-headed. Having, as I tend to do, skipped a step I was stalled for about 45 minutes trying to figure this out.

I use the Bitwarden CLI regularly through a qutebrowser userscript (and rofi --- [link to script](https://github.com/qutebrowser/qutebrowser/blob/master/misc/userscripts/qute-bitwarden)) to log in to websites. I tend to never use it to create login items. It honestly looked like a pain. This morning, though, coffee in hand and new calendar on the wall, I decided to take a look at it. As I just mentioned, I managed to bork it up, and then decided to make it easier on myself.

I don't know how to make Bash scripts, but the magic of search engines resulted in this:

```bash
#!/usr/bin/env bash

read -p "New Item Name: " name
read -p "New Item URI: " uriname
read -p "New Item Username: " username
read -sp "New Item Password: " secret

echo "{\"organizationId\":null,\"collectionIds\":null,\"folderId\":null,\"type\":1,\"name\":\"$name\",\"notes\":\"\",\"favorite\":false,\"fields\":[],\"login\":{\"uris\":[{\"match\": null,\"uri\": \"$uriname\"}],\"username\":\"$username\",\"password\":\"$secret\",\"totp\":\"\"},\"secureNote\":null,\"card\":null,\"identity\":null,\"reprompt\":0}" > tmpItem.json
clear
encodedItem=$(cat tmpItem.json | bw encode)
bw create item $encodedItem
rm tmpItem.json
```

This is just the basics. It only gets four things needed to create the login item: the name of the item, a single URI, a username, and a password. Those variables are thrown into a JSON file, encoded by Bitwarden, and transformed into a login item. The JSON file is deleted after.

Now, I am not a security expert either. But, this gets the job done. I also use `bw generate -usln` to make my passwords, so that helps. **AND 2FA**. Don't forget the 2FA.

You can go complain about my naïve understanding of how Bash scripts and privacy work here on Codeberg: [bbbhltz/newbw](https://codeberg.org/bbbhltz/newbw). I do hope this helps other beginners like myself.
