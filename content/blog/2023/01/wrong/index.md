---
title: "Wrong"
subtitle: "A tale as old as the web"
description: "A short story about trolls and sea lions on the Web, and how they can waste your time."
date: 2023-01-22T11:43:56+01:00
lastmod: 2023-01-31
# featured_image:
draft: false
katex: false
tags:
  - Short story
next: true
nomenu: false
---

<p style="text-align: center";>
Gather round, my younglings,<br>
hear my prose.<br>
I will tell you a story<br>
everyone knows...</p>

---

Our good hero has things to say and things to share. \
Belly full and coffee prepared, they take their seat and stare.

A blank screen beckons.

Fingers fly and letters coalesce into words and sentences and paragraphs.

Headings and subheadings, table of contents, references and footnotes; the œuvre is ready for public viewing.

The wordsmith climbs the rungs of the digital soapbox.

"Redditors, come read my righteous words, give me your karma!" they declare. "Good people of Hacker News, Lobsters, and Mastodon; Lemmy, you too may enjoy my writings. Twitter, Facebook, and LinkedIn, you have not been forgotten."

Finally, the tension our hero felt is released. They relax into their chair and sip their coffee. Content with their content.

*ping!*

"Ah, surely a word of encouragement from one of my many followers," they think.

Nay. It is not the case. Confused and perplexed, leaning closer in hopes of gaining insight from the pixels on the screen.

The chair creeks; a drop of coffee falls and lands on writer's keyboard; the wings of a housefly beat...

*thump thump thump*

They squint, reading the words, mouthing them:

*You. Are. Wrong.*

"Wrong? Me?" they think.

Our hero places their beverage aside, index fingers finding the home row, and prepares a polite retort.

"Please, stranger, enlighten me with proof of my fault, that I may amend my previous statement and learn from my mistake."

*ping!*

A hasty reply: "Everything is wrong, because you neglected this [<span style="color: var(--accent);">link to a personal blog that cites no sources</span>] and this [<span style="color: var(--accent);">article from three years ago on a conspiracy theory website</span>]. Not to mention, your claims contradict this statement made by [<span style="color: var(--accent);">a sockpuppet account of the mod of this forum</span>]."

"Ah, bless their heart. A troll attempting to draw out my ire," they think to themselves. "A waste of CO<sub>2</sub>."

*ping*

A tab calls, it is Hacker News. Lobsters, too, favours our hero.

Could it be? No. An army of trolls, for sure. For their is no other explanation.

"What preposterous nonsense is this? Hacker News is a joke, for shame! And I thought more of you Lobsters! I shall not engage you belittlers. I shall block you deriding denigrators. You cannot bait me into this most basest of disagreements. I will not, shall not, could not, cannot get into an argument on the Internet," they proclaim as they block, report, filter, and mute those who dare disagree.

But... \
what if...

A seed of doubt takes root, confirmation bias clouds, cognitive dissonance boils blood.

"I know what to do. Exactly, and most precisely, I shall riposte. The haters shall learn that this is something up with which I shall not put!"

Copy \
&nbsp;&nbsp;&nbsp;&nbsp;Paste \
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Format \
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Markdown \
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;BBCode \
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Plaintext

"See [<span style="color: var(--accent);">here</span>], misguided stranger, not to mention [<span style="color: var(--accent);">this</span>], [<span style="color: var(--accent);">that</span>], and [<span style="color: var(--accent);">still more things</span>] that confirm my position. Easy mistakes to make, but when you do your own research and forge your own path, you will understand. Also, your grammar needs work."

And so the day goes... \
Our hero's coffee is cold, their stomach growls as the sun travels from east to west. \
Duties are shirked, friends are ignored.

Far away, \
on another continent, \
another stranger awakens and joins the conversation.

But, that troll from before? \
They are unbothered. \
Their day was filled with friends and family and dogs to walk. \
Our hero is the furthest thing from their mind.

---

<p style="text-align: center";>
Who is the hero?<br>
Who is the troll?<br>
<br>
The end of this story?<br>
Nobody knows.<br>
One cannot win a battle<br>
against Internet foes...
</p>
