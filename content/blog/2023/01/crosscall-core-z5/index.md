---
title: "Review: Crosscall CORE-Z5"
subtitle: "A review of Crosscall's most recent rugged device"
description: "A mostly positive review of the Crosscall CORE-Z5 Android smartphone."
date: 2023-01-18T13:57:00+01:00
lastmod: 2024-04-13
featured_image: feature.webp
draft: false
katex: false
tags:
  - Technology
  - Review
next: true
nomenu: false
---

*Mid-November, 2022, my trusty Nokia 5.3 began acting up. I began looking for a new device. I was only looking, though, because I don't relish spending money on a device, or reading reviews, or dealing with transferring things from an old phone to a new phone. I rediscovered the French brand, [Crosscall](https://www.crosscall.com) and spent some time learning how to compare devices. I initially planned of getting an eco-friendly or privacy-oriented device, but [that idea didn't pan out](/blog/2022/11/coolpad-cool-s/). Before I knew it, I had spent way beyond what I was planning on a device just to get something different...*

# A Chonky Boy

![Crosscall CORE-Z5 front and back](z5.webp "Photo from Crosscall website")

## Judging the book by its cover

*First impressions mean a lot*

The [Crosscall CORE-Z5](https://www.crosscall.com/en_FR/core-z5-1001011601265.html) comes in a plain brown cardboard box (recycled, of course). What's inside the box?

- A chonky phone
- a printed guide
- a USB-C to USB-C cable and charging block
- a Crosscall X-BLOCKER (which I have no use for yet, so I won't mention it any more)
- three different sizes of earbud tips
- *no* headphones or earbuds
- *no* lanyard

There are first impressions, and there are *first impressions*. I knew beforehand that it would be bigger and heavier than most phones I have used in the past. This thing is a beast.

It doesn't just have bezels. It has a chin *and* a forehead. And a notch camera too. It is thick. There are buttons. Many buttons! But, most of all, it is heavy and quite solid feeling.

![inside the Crosscall CORE-Z5](inside.webp "Inside the CORE-Z5: the magnesium chassis keeps things safe, and heavy")

([source](https://youtube.com/watch?v=x0o7aW7wSNo))

Around the sides of the phone there are 7 (seven!) buttons. Two are for the volume rocker, one is the power button (which doubles as a fingerprint scanner). On the top there is a single red button, and on the left side there are three buttons --- these buttons can be configured with two functions each. This is a rugged device, so the headphone jack and USB port are protected.

![image showing the position of the buttons on the Crosscall CORE-Z5](buttons.webp "Button placement on the Crosscall CORE-Z5")

The body is made of polycarbonate and thermoplastic elastomer, the screen is Gorilla Glass 5. I find the device to be rather slippery. On the back we have a camera (a single camera!), a speaker, and a [Magconn](https://magconn.co.kr/) connection (Crosscall X-LINK).

Overall, I like the physical design of the device. I do wish Crosscall had used the rubbery elastomer on the back as well, for comfort and to make it easier to hold.

## Getting to the CORE of it

What's inside this unit?

- OS: Android 12
- Display: 1560 x 720, 6.1"
- Processor: Octa-core CPU @2.6GHz
- SoC: Qualcomm QCM6490 (ARMv8)
- GPU: Qualcomm Adreno 643
- RAM: 4 GB
- Camera: 48MP (f/1.8) and 8MP (f/2)
- Video: 4K 30fps capable
- Sensors, etc.: accelerometer, altimeter, Bluetooth, cameras, compass, GPS, gyroscope, light sensor, NFC, pedometer, proximity sensor, and Wi-Fi

Performance-wise, I don't have much to complain about, nor to compare with. Aside from a quick in-store test, I have never used a high-end flagship phone. That said, I find it fast enough to do what I do most: work. From quick emails to large, multi-tab, spreadsheets, the CORE-Z5 has made me realize how people are able to accomplish so much on their devices. I decided to run some tests on the phone and they confirm what I experienced: The phone is faster than what I was used to.

<figure><embed type="image/svg+xml" src="geekbench.svg" /><figcaption>Benchmark Comparison (interactive chart)</figcaption></figure>

The comparison chart above shows how the CORE-Z5 matches up against several other phones. The Nokia 5.3 (my previous phone), three phones that I considered buying (the Teracube 2e, the Murena One, and the Fairphone 4), and a recent flagship device (the Pixel 7 Pro). I won't claim to understand these tests, but I am pleased to see that the [PCMark Work](https://benchmarks.ul.com/pcmark-android) test nearly doubled the results of my former phone. The [Geekbench](https://www.geekbench.com/) benchmark was probably the most user-friendly of the tests that I ran, in case you are also looking to benchmark your phone and, like me, you have no idea where to begin.

Additionally, I ran the [GFXBench test](https://gfxbench.com/) on the CORE-Z5. I don't game on my phone, but for those who are interested, here are the results:

**GFXBench Results**

- **OpenGL Aztec Ruins High Tier**: 3034 Frames (47.2 Fps)
- **OpenGL 4K Aztec Ruins High Tier Offscreen**: 490 Frames (7.6 Fps)
- **OpenGL Aztec Ruins High Tier Offscreen**: 1086 Frames (16.9 Fps)
- **OpenGL Aztec Ruins Normal Tier**: 3844 Frames (59.8 Fps)
- **OpenGL Aztec Ruins Normal Tier Offscreen**: 2728 Frames (42.4 Fps)
- **Vulkan Aztec Ruins High Tier**: 3188 Frames (49.6 Fps)
- **Vulkan 4K Aztec Ruins High Tier Offscreen**: 523 Frames (8.1 Fps)
- **Vulkan Aztec Ruins High Tier Offscreen**: 1155 Frames (18.0 Fps)
- **Vulkan Aztec Ruins Normal Tier**: 3844 Frames (59.8 Fps)
- **Vulkan Aztec Ruins Normal Tier Offscreen**: 3131 Frames (48.7 Fps)

## Connectivity

**Note: I do not have 5G**

I tested the connectivity of the device on my home and work Wi-Fi networks, on 4G, and using it as a hotspot. Here are the results:

|   | Upload | Download |
|---|--------|----------|
| [Wi-Fi Home](https://librespeed.org/results/?id=0znhlx6) | 16.4 Mbps | 0.96 Mbps |
| [Wi-Fi Work](https://librespeed.org/results/?id=0vn8ke0) | 104 Mbps | 89.1 Mbps |
| [4G](https://librespeed.org/results/?id=0zselh6) | 82.9 Mbps | 23.8 Mbps |
| [Hotspot (4G)](https://librespeed.org/results/?id=0zjpcu2) | 59.5 Mbps | 6.61 Mbps|

## Photography

In a world where most phones have at least two cameras on the back, the CORE-Z5 stands out. It has just one. It is listed as a 48MP camera, but the default in the camera app is set to 12MP "Fusion<sup>4</sup>". Crosscall brands this as Fusion<sup>4</sup> technology, but what is happening here is [pixel binning](https://www.androidauthority.com/what-is-pixel-binning-966179/). The default photo app is in need of a little upgrade, and is missing options like panorama altogether, and hides common options in the settings (timers, exposure, white balance, etc.)

The results are fine for me. If you love to take and share photos, you will be let down. A rugged phone is not meant for shutterbugs.

![in-app preview of Kup action figure from Transformers](camera-app.webp "Kup has a face, and the phone detects it")

![photo of Kup action figure from Transformers](photo-test.webp "Bokeh achieved? (3000x4000 / 4.7mm focal length / 1/25 shutter speed / 1.8 aperture / ISO 834 / no flash)")

While this isn't the best example, it is realistic. One does not stage every photo, we do hope for things to turn out a little clearer. Even more so given that the product page for the CORE-Z5 states: "Accurate photos, even in low lighting."

## Daily driving

Weight and size notwithstanding, this is an easy device to use. I don't know why more phone-makers haven't put more programmable buttons on their devices. It makes the experience better.

My button layout looks like this:

(long/triple-tap)

- Button 1: answer call / torch
- Button 2: camera / emergency
- Button 3: OSMAnd~ / Total Commander
- Button 4: Silent toggle / Vibration toggle

Out of the box, you have Android 12 and a minimal, near-vanilla, experience. Crosscall has added a few apps (that cannot be removed --- *booooo!*):

- X-STORY: an app for making video montages
- X-Sensors: a neat little app that does nothing but show you that the sensors are working (the compass is nice)
- X-SAFE: a shortcut to the phone's emergency settings
- X-CAMP: a sort of social network
- X-Talk: a PTT / walkie-talkie app (**I deactivated this one because I have no use for it, and* it maintained a constant network connect --- *yuck**)

![screenshot of Crosscall X-Camp app showing where the users are](xcamp.webp "Screenshot of Crosscall X-CAMP app: not so international...")

**Note: the phone is only available in 14 countries ([see map](map.webp))**

Many of the close-to-vanilla Android devices can make the same claim: decent battery life. I have squeezed up to two days of moderate use from the CORE-Z5, three days of light use. I never need to worry about taking the charger. The phone itself can act as a charger, too, and I have used it to recharge my DAP while on the go. 

I also purchased a USB hub for it, so I can output via HDMI and connect other USB devices to it. It worked perfectly, and I was able to give a lecture that included slides and a video. On another occasion, I corrected 25 homework assignments and entered the grades in a spreadsheet. Work-wise, I am very content. Additionally, I am more than impressed with the fingerprint sensor and the ability to use the phone while it is wet.

## Conclusion and Complaints

A review without complaints is not a review. I have complaints, and here they are:

- Let us remove those Crosscall apps --- bloatware, even cool bloatware, is still bloatware.
- The X-CAMP app is neat, but users need a forum, a FAQ, and a wiki.
    - *A high percentage of the questions on the app are about storage space and Bluetooth issues --- give the users a place to refer to when those problems are encountered.*
- The X-CAMP map is provided by [Mapbox](https://www.mapbox.com/), which relies on OpenStreetMap, but OSM is not credited in the app itself --- this should be rectified.
- The X-Talk app needs an easy option for deactivation for those who do not wish to use it.
- Update the camera app and add additional modes, like panorama.

I have many positive remarks about this phone. Some are mentioned above (battery life, usefulness of the buttons, performance) while others fit into the category of "I forgot how much I missed that":

- Programmable buttons
- LED notifications
- Magconn charging
- Ruggedness
- Powerful torch
- Speaker volume
- Precise GPS (good enough for [Mapillary](https://www.mapillary.com/app/?pKey=680665076873078&lat=49.440399899998&lng=1.0982517999972&z=17&focus=photo))

I am not even close to the target market for this type of phone (no sports, not outdoorsy, etc.) and I can certainly see myself using it for the next five years, or more. Especially since it has a 5-year warranty and promises 10-year availability for parts and a high repairability score. Despite the many good things, I cannot give this phone a score higher than **3/5**.

**The Crosscall CORE-Z5 takes things somewhat beyond my expectations, but still falls short of exceptional.**
