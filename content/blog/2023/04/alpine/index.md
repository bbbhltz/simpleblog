---
title: "On using Alpine Linux as a Desktop"
subtitle: "my old laptop came back to life"
description: "A very positive review of the Alpine Linux operating system."
slug: alpine-linux
date: 2023-04-04
lastmod: 2025-01-28
featured_image: /blog/2023/04/alpine-featured.webp
draft: false
katex: false
tags:
  - Linux
  - Review
next: true
nomenu: false
---

## Introduction

I used to use [Gemini](https://gemini.circumlunar.space/). (I cannot recall why I stopped using it). Call it prescience, call it hoarding, but I happened to keep my bookmarks file from those days. During some Spring cleaning I opened it up to see what was there.

There were a number of bookmarks, one of them being a [post](https://drewdevault.com/2021/05/06/Praise-for-Alpine-Linux.html)<a id="dd"></a>. The post was about Alpine Linux. "Why would I bookmark this post?" I thought to myself. So, off I went to their website...

And, what to my wondering eyes should appear, but a proper and navigable website. This required my attention. While I am not such an expert on websites and design, I have navigated (or have attempted to navigate) many a Linux landing page. When [the Alpine Linux page](https://alpinelinux.org/) loaded I was obliged to give it a nod of approval. Debian, seriously, what is up with your site?

![Alpine Linux Logo](https://alpinelinux.org/alpinelinux-logo.svg "Great Logo")

The logo, as well, garnered yet another nod. [Hexagons are the bestagons](https://youtube.com/watch?v=thOifuHs6eY), and this logo is wonderful. The choice of colour, and the simplicity, make it stand out. The site even made it easy to track down the [epic tale of logo bikeshed-ification](https://lists.alpinelinux.org/~alpine/devel/%3C20140509112421.7a5339dd%40ncopa-desktop.alpinelinux.org%3E) and [notes on](https://lists.alpinelinux.org/~alpine/devel/%3C20140920154559.1ab7a63c%40ncopa-laptop%3E) their website design.

Within moments, I was on the Downloads page and remarked a logical selection of images to choose from. There are even idiot-friendly descriptions, which is perfect for me. I initially decided to take it for a spin on my disused Raspberry Pi400. And here came another surprise: the size of the download.

The image for the RPi is under 100 MB. And for desktop? Just over 150 MB. "Impossible," I thought. And I was wrong. Totally possible. Apparently their slogan --- "Small. Simple. Secure." --- is at least 33% true. To know more, continue on to my glowing review of this Linux distribution.

# Review

## Installation

{{< box info >}}

I have added my instructions to the Alpine Wiki, [here](https://wiki.alpinelinux.org/wiki/LXQt).

{{< /box >}}

Alpine Linux may turn off many desktop users. This is because installation is done via the command line. There is no [Calamares](https://calamares.io/) or Live image. After getting the image on a USB drive, for example, and booting, you are in the terminal (tty) with a blinking cursor and instructions to login as `root`. From here, you can run the `setup-alpine` script that walks you through getting the basics installed and configured. I followed this process.

After rebooting I was still at a command prompt, but now had Wi-Fi and began following the instructions from [the Wiki](https://wiki.alpinelinux.org/wiki/Main_Page). From here, all it took was reading the Wiki and learning to use the `apk` command.

### APK --- Alpine Package Keeper

`apk` works like `apt` on Debian-based distributions, but I do find it easier. I searched (`apk search x`) and I added (`apk add x`) without difficulty. `apk` also takes care of removing dependencies --- `apk del x` is like `apt purge x && apt autoremove` --- and this is just so... *simple*. Yes, there was the matter of uncommenting a repository, and I did need to pay special attention to how to use `rc-service` and `rc-update`, but in under 10 minutes I was able to install the necessary packages to install LXQt on my old laptop (an Acer Aspire from 2015).

The `apk` tool is also extremely fast.

## Use as a desktop

I could wag my chin all day and bore the pants off anyone reading this, but I'd rather keep it simple. Once installed, everything boots quick enough and the hardware works (trackpad, webcam, Bluetooth, optical drive, number pad, function keys, etc.). Of course, using something like [LXQt](https://lxqt-project.org/) makes the system snappy and quick. Desktop users, however, tend to want access to the latest and greatest software.

### Access to Software

One does not need to install a distro to learn these things. Alpine Linux Edge is quite up-to-date.

[![Repository status](https://repology.org/badge/repository-big/alpine_edge.svg)](https://repology.org/repository/alpine_edge)

(For comparison: [Arch Linux](https://repology.org/repository/arch) and [Debian Unstable](https://repology.org/repository/debian_unstable))

Using the Edge (rolling) repository gave me nearly everything needed to do what I do on a computer:

* use the web
* update my blog
* consume media

Some packages are out of date, and that can be a pain. But, here I am with a desktop up and running on an 8-year-old laptop that has not been used in years, and it is fast. ~~So, if the `pandoc` version is 7 months behind, I can grumble a little, but it won't stop me from using this distro~~.

Beyond this, up-to-date packages for browsers and many desktop environments are available. You can quickly have access to LibreOffice, Python, audio and video playing software, IDEs and other helpful tools that can get many users through the day.

## Downsides

Alpine is a "general purpose Linux distribution" but does not target desktop users. Anything that could be perceived as a downside stems from this distinction and should be considered a bit of a feature, rather than a bug.

Drew DeVault's article on Alpine ([mentioned above](#dd)) mentions the only downside I have noticed in my week or so of using this on my old laptop.

> Documentation is one of Alpine's weak points. This is generally offset by Alpine's simplicity — it can usually be understood reasonably quickly and easily even in the absence of documentation — but it remains an issue. That being said, Alpine has shown consistent progress in this respect in the past few releases, shipping more manual pages, improving the wiki, and standardizing processes for matters like release notes.
> 
> <cite>Drew DeVault</cite>

## Conclusion

![screenshot](scrot.webp "Obligatory screenshot")

I am looking forward to my coming holiday because I will likely switch my main laptop to Alpine Linux. In fact, I am even inclined to finally, after years of being a basic Desktop Linux user, contribute to a project with more than the rare bug report. I will probably need a little help before I do that though.

As I am writing this I have also came across [Bradley Taunt's *Alpine Suck* project](https://git.sr.ht/~bt/alpine-suck-installer), a script that installs the core suckless programs. If they don't already exist, I believe that other projects of this nature will appear. Perhaps, even, a desktop distro based on Alpine.

It may be apparent, but I will let you in on a secret: I have never tried to write a review of distribution --- ever. My Linux experience only goes back to about 2006. Since then, I have tried a good number of distributions. Never once have I ever thought about sharing my thoughts in more than a few words. Also take into consideration that I am a desktop Linux user.

If I were to apply the same grading methods to Alpine Linux as I do my students, I would state the following:

**Excellent --- above average with only minor weak points**.
