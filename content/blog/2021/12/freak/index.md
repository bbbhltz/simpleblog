---
title: "Freak"
subtitle: "A short story"
description: "A somewhat violent short story set in the the future where people are ostracized for not using the newest technology."
date: 2021-12-10T19:33:10+01:00
lastmod: 2023-01-31
draft: false
tags:
  - "Short story"
---

"Momma, look!"

A little boy is trying to get his mother's attention. She is staring off into space.

**"MOMMA!"**

"Honey! What is it? Mommy's busy, sweetie."

"Momma, look, the man!"

I look over to see the mother slide her glasses down and look over them at me. She could be the same age as me. Perhaps younger. Too far away to tell for sure. Close enough, though, that I see her expression. She makes one of those, "huh, would you look at that" faces. She knows why her son is staring and pointing at me. A can feel other eyes on me, so I know that attention has been drawn.

It doesn't take heightened situational awareness. Just experience. Going out in public means eyes on me. Some younger and less accepting individuals have called me names since I was in my teens. "Freak" is the most common.

The mother puts her glasses back into their position and continues ignoring her son, who, from the corner of my eye, is also staring off into space. I continue on my freaky way. Hunger beats humiliation and public shaming. There aren't too many places for people like me now, and walking is a little less humiliating than taking the bus.

The sound of boots on pavement catch my attention. Ahead and to my left, I see three police officers jogging in my direction. Like most people, freak or not, I am tempted to glance over my shoulder to see what could be the reason for their speedy response. Having just passed through the square, I know there is nothing that would merit their attention. Well, almost nothing.

I stop in my tracks and take my hands out of my pockets.

The police officers slow down. One walks around behind me. Another approaches me head on. The third stays back, mumbling something into her radio.

"Hello, sir. We've received several reports that someone fitting your description has been disturbing the peace. We need to ask you a few questions."

"Go ahead."

"Do you have any identification on you?"

"I do not. Sorry."

"Nothing at all?"

"No. I only have my wallet with me. I am going for lunch across town."

The officer closest to me holds up his phone and points the camera at me. He looks at his partners and shakes his head.

"You'll have to come with us, sir. And, you will need to be restrained."

"I understand."

For freaks like me, this is life. 

Their van isn't far away. Once we are seated inside, they remove the restraints.

"So, sir, are we going to the station or the store?" asks the officer who stayed in the back with me. She takes off her hat and looks at me, sincerely, because she already knows what I will say.

"Sorry, officer. But, I think we might have to do the paperwork again."

"It doesn't have to be this hard. We can help you. We can't stop you every week because people are afraid of you. Everyone is losing here. Just let us take you to the store."

"I don't see how this can be a crime. Being different used to me celebrated. Now, I am treated like a freak. All because I don't want to give my money to a company? Because I don't want their damn glasses and phones and watches? Because I can walk down the street with two free hands and see things through my own eyes?"

"Right. So..."

She huddles up with her partners for a moment.

"Where's lunch?"

"Pardon me, officer?"

"Where are you going for lunch"

"The café, near the museum."

"We'll drop you off."

Before I can speak, the vehicle is moving, silently and swiftly, in the direction of the café. In a matter of minutes, we are there. The officers hop out. I move to follow, getting ready to thank them.

"Oh, no. You can stay here. Watch this." Says the third officer.

The three officers walk into the café and come out almost immediately, dragging the owner behind them.

They slide the door of the van open and place the café owner's hand on the latch.

"We'll ask again. Do you want to go to the store, or not?"

Confused, and taken aback, I hesitate. Too long. They slam the door on the owner's hand.

Blood splatters across the inside of the van, and across my face. The owner screams. He looks at me, then the police officers.

"He's not welcome in my café. None of those freaks are allowed any more! I promise! Please, let me go!"

"Let you go? But, our friend here hasn't answered yet..."

They force his other hand onto the latch.

"Well, freak, what'll it be? Will we go to the store, or will we charge you with assault and take you to the station?"

Four sets of eyes stare at me. All hateful, but one also pleading.

I hang my head. "The store," I say.

Today is the day I stop being a freak, and become a sheep.

{{< box info >}}
**Jw** wrote a piece inspired by this story on his blog, *so1o*: [A Question of Price](https://so1o.xyz/blog/a-question-of-price)
{{< /box >}}
