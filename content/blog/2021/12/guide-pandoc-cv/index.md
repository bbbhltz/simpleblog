---
title: "Making my CV with Markdown didn't turn out as expected..."
slug: guide-pandoc-cv
subtitle: "...but, I did end up with a CV at the end."
description: "How to make a CV or résumé with Markdown and Pandoc."
date: 2021-12-19T21:11:08+01:00
lastmod: 2024-10-13
draft: false
tags:
  - Pandoc
  - Markdown
  - Guide
next: true
nomenu: false
---

You've been there.

If you haven't, you will be.

A lead on a job! Hurrah! Get out the old CV (or résumé) and drag and drop, and Bob's your uncle.

Alas, a minor error is noted. Easy fix. Open up the file in your favourite word processor, no muss, no fuss.

The file loads and... That's not right. Everything is out of place. It was just a few weeks ago that I changed this. No major updates have occurred since. Why is *that* like ***that***?

**Why can't we just send plain text files as CVs?** They are just going to be fed into some [Applicant tracking system](https://en.wikipedia.org/wiki/Applicant_tracking_system) anyway.

And so begins the easiest thing. Making a CV using a text editor.

## AN ATTEMPT WAS MADE

Right. Open the old text editor, throw in some words and dates, put a little Pandoc spin on it, and share the PDF.

Markdown and Pandoc are the two things I use the most on my computer. Sad, really, that I haven't even tried to do some of the more advanced things Pandoc is capable of. But, also lucky. After spending the better part of this past year remaking all of my course material as Markdown files, I have searched enough and seen enough that I saw the problems before they hopped up and bit me.

The content of the CV is easy enough, the layout is the little bump in the road.

I will be in need of **columns**, and I will need to be able to flush right certain elements.

![CV Header](header.webp "Two columns with alignment")

![CV Experience](exp.webp "Work experience example")

This calls for the [multicol package](https://www.ctan.org/pkg/multicol). That means I will have to deal with some LaTeX. In fact, it means that my CV will be some Frankenstein hodgepodge of a Markdown file.

### THE HEADER

```latex
\begin{multicols}{2}

\begin{huge}

\textbf{FIRST LAST}

\end{huge}

Title

\vfill\null

\columnbreak

\Begin{flushright}

Address 1 \\
Address 2

phone number \\
email address \\
website or linkedin

\End{flushright}

\end{multicols}
```

So far, so good. I have my basic info at the top of the page.

### EXPERIENCE AND EDUCATION

Originally, I started out with something like this:

```latex
\textbf{Company Name} \hfill \textbf{City, Country}

\textit{Job Title} \hfill \textit{MMM YYYY -- Present}

Details, details, details. \\
```

 But, it dawned on me that that is a lot of effort. So, I added this to the front matter:
 
```latex
\newcommand{\xp}[5]{\textbf{#1}\hfill\textbf{#2} \\ \textit{#3}\hfill\textit{#4} \ \textendash \ \textit{#5}}
```

This let me achieve the same results with fewer keystrokes:

```latex
\xp{A Company}{A City}{A Job Title}{JAN 2020}{SEP 2021}

Details \\
```

### SKILLS

I am lucky. My skills are... Let's just say it doesn't take up much space. I have lots of transferable skills, and I definitely know what I am doing. But, how can I, a language teacher, give some proof that I am OK with computers? Public speaking? I don't do conferences. I don't publish. As such, I am able to squeeze the necessary into a few lines.

## GETTING DISTRACTED

I did it. It didn't take long. It isn't exactly beautiful to look at, but it is what I wanted: a CV made with a plain text file.

I was wrapping things up, and sorting out things in my `Downloads` folder, when I came across the manual for [tcolorbox](https://www.ctan.org/pkg/tcolorbox), another LaTeX package that *provides an environment for coloured and framed text boxes with a heading line*. Pretty soon I was on page 200-and-some, and the notion of a 10-minute CV rewrite was risible.

## THE RESULT

### FRONT MATTER

```yaml
---
documentclass: letter
lang: en-GB
geometry:
- margin=1.5cm
mainfont: FreeSans
fontsize: 11pt
papersize: A4
header-includes:
    - \usepackage{multicol, tcolorbox}
    - \tcbuselibrary{skins}
    - \newcommand{\xp}[5]{\textbf{#1}\hfill\textbf{#2} \\ \textit{#3}\hfill\textit{#4} \ \textendash \ \textit{#5}}
    - \newcommand{\hideFromPandoc}[1]{#1}
    - \hideFromPandoc{
        \let\Begin\begin
        \let\End\end
      }
---
```

### BODY

```latex
\pagestyle{empty}

\begin{multicols}{2}

\begin{huge}

\textbf{FIRST LAST}

\end{huge}

Title

\vfill\null

\columnbreak

\Begin{flushright}

Address 1 \\
Address 2

phone number \\
email address \\
website or linkedin

\End{flushright}

\end{multicols}

\begin{tcolorbox}[colback=white,sharp corners,colframe=black,title=Experience,fonttitle=\Large\bfseries]

\xp{A Company}{A City}{A Job Title}{JAN 2020}{SEP 2021}

Details \\

\xp{A Company}{A City}{A Job Title}{JAN 2020}{SEP 2021}

Details

\tcblower

\begin{small}\color{gray}

\xp{Old Company}{Old City}{Old Job Title}{MAY 1999}{OCT 2020}

Did some stuff

\end{small}

\end{tcolorbox}

\begin{tcolorbox}[colback=white,sharp corners,colframe=black,title=Education,fonttitle=\Large\bfseries]

\xp{Univeristy of X}{Shitty Town}{Expensive Degree}{2000}{2004}

\end{tcolorbox}

\begin{tcolorbox}[colback=white,sharp corners,colframe=black,title=Skills,fonttitle=\Large\bfseries]

\textbf{Language}:

\textbf{Computer}:

\end{tcolorbox}
```

### OUTPUT

![A Dull CV](output.webp "That took longer than 10 minutes")

## CONCLUSION

**A weekend well wasted!**

I was able to apply to some different jobs. I will make it look a little *nicer* once inspiration strikes. Not only that, but I will have to get around to learning LaTeX for real. Is that something teachers put on their CVs? 

> Skills: LaTeX \
> Hobbies: Latex

Nuance is everything.
