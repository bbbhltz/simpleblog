---
title: "Tips for Students: The Browser"
slug: guide-browser
date: 2021-01-28
lastmod: 2024-10-13
draft: false
author: bbbhltz
subtitle: "Hint: don't open 100 tabs."
description: "How to use a Web Browser, a guide for students with tips and tricks."
tags:
  - Education
  - Guide
next: true
nomenu: false
---

**Hello Internets. This is a guide for the average student, particularly students in the first year of post-secondary education. If you are a power user or generally geeky person, nothing here will be of particular interest to you.**

## 0. Introduction

Good day all,

My name is at the top of this blog, but that does not tell you who I am. I am a professor at a business school in France. Over the course of 14 odd years of teaching I have become frustrated with how my students use their computers. This series of blog posts (there will be at least six) will address those frustrations.

So, what frustrates me? Not that much, in fact. It varies from the rather basic things like having loads of icons on your desktop to things that should be straightforward, like how to double-space a Word document and take a screenshot (no, it is not taking a picture of your computer with your phone). I have had several students that don't know how to do either.

Being a person born in the 80s I have been using computer for roughly 30 years. I experienced the glorious arrival of the web and Wi-Fi and watched as computers became mainstream for students in post-secondary education. In France, this was in contrast to what I had experienced in Canada where most, if not every student, used a computer or laptop while at university (even before the arrival of Wi-Fi and batteries that lasted more than a single lesson).

This experience left me with very general knowledge that I always considered common knowledge, but now I have committed myself to blogging a little and I will share with you at least six sets of recommendations:

* using your browser
* using your word processor for report writing ([link](/blog/2021/02/guide-reports/))
* making presentations (WIP)
* taking notes and being organized (WIP)
* using the keyboard (WIP)
* recommended software and other sundries (WIP)

Any software used will be free software, but the recommendations and tips can be applied to proprietary software used on Windows, Mac or Linux.

## 1. The Browser

### General Rules

As a student there are a few things we should keep in mind. In order to have a nice workflow I have always been a **SOUP** enthusiast. 

What is **SOUP**?

**S**imple: Giant, resource-heavy, complicated software will slow things down.
**O**ne App; One Purpose: You shouldn't need several browsers installed, the apps you use should be good at doing that one thing you need them to do.
**U**p-to-date: You should keep all of your software, and operating system, as up-to-date as possible.
**P**ay it forward: You should show your friends how to do things when possible, otherwise you will become the "PowerPoint Guy" or the "Premiere Pro Girl".

### Tabs and Toolbars

If your browser is open right now, at the top of your screen you will see a title bar, an address bar, maybe a bookmarks bar, and a list of open tabs. I don't want to be too annoying or pedantic, but when I look at a student's screen and see just how many tabs they have open and hear them complaining about how their computer is slow or their connection is bad, I want to grab them and shake them vigorously.

Of course things are slow! Your browser has over 20 tabs open, and it looks like at least four of them are Facebook.com, I see a Messenger.com, LinkedIn, two Zoom tabs for some reason, Teams, Outlook, Gmail, a Google Doc, and many Google searches, AND Netflix! 

I am not an expert, and there are things that I cannot always explain clearly despite being a professor. I have, time and again, tried to get this message across. Yes, your browser can open multiple tabs, but each tab is like opening a new browser. You have several browsers running inside your browser. Each one uses some memory. Websites today require more processing power and memory than some apps.

Let's look at some numbers.

**These screenshots come from a Raspberry Pi which is _not_ a powerful computer**

Here is the Task Manager of my browser with one tab open:

![Firefox Task Manager, not doing much](01.webp "Idle Task Manager")

The Task Manager and the New Tab use some memory. All of those puzzle piece are extensions, and they eat up a little memory for each tab too.

If we look at my system Task Manager we can see that the browser is using a certain percentage of CPU but also memory (RSS is "the portion of memory occupied by a process that is held in main memory (RAM)" but we don't need to know that because I'm going to make those numbers go up a bit later). The extensions, too, are using some memory.

![Firefox using some memory](02.webp "Firefox Running")

So, that is a browser doing nothing. Let's do some stuff, OK? I think I need to open my personal email, my work email, Teams, and why not IMGUR too. Strangely enough, my weak little Pi doesn't even show one of the tabs that are open, but Teams and Outlook use between 5 and 10x the memory of a page like IMGUR.

![Alright little Pi, I think you can, I think you can](03.webp "Memory Usage")

And system-wide you are starting to use up RAM.

![RAM-a-lam-a-ding-dong](04.webp "System-wide RAM Usage")

Well, you are clearly thinking the obvious. This is what computers are meant to do. So, why not, right? I certainly agree with you. Your fairly recent, probably expensive, laptop computer should be able to have many tabs open, but when things start slowing down, it is likely your fault and not because your computer is dying. Let's hit up Facebook and see how much memory we can use. YouTube can come along for the ride. Screw it, Google Drive and a Google search for "chocolate chip cookies".

I will be naughty and launch TWO instances of Firefox with TWO tabs each.

![You get the idea](05.webp "Multiple instances")

I think you get it. Numbers go up. Your RAM is not infinite. Combine this with perhaps Zoom or even Adobe Acrobat and things start to slow down. You can close some tabs.

### But, then I'll lose my place... {id="bookmarks"}

Yes, you will, but there is a super feature in your browser called **bookmarking**.

The main browsers that people generally use are Chrome, Firefox, and Safari. They all have cousins whose names are Brave, Vivaldi, Chromium, and Opera. Those browsers have bookmark managers. So, bookmark the hell out of the pages you want to keep. You can even make folders! That way things stay organized. Those bookmarks can sync across devices. Now and then, sit down and Marie Kondo your bookmarks. Delete the just, organize the other stuff.

Speaking of bookmarks, you might have a bookmark bar at the top of your browser. This is a nice place to put the links you use daily. One thing that lots of people do, but a few people do not, is set just the icon on their bookmark bar. It is easy. Bookmark a site, set it to the "Bookmarks Toolbar" or whatever it is called on your browser, and delete the name. Done. Just an icon. Yay.

![Purty](06.webp "This sparks joy")

You can also put folders on that bar. For students, I have seen some good ideas. Some students have a folder for each day of the week. Others for each class. Some students have their personal folder, a school folder, and a work folder, and others have the important links as icons on the toolbar, and a folder for "fun".

Finally, we get to extensions. I will have a post later on about some recommended extensions that I use. Right now, you just need to apply the same rules to extensions as you do tabs on your browser and apps on your phone. They take up some space and use memory. Not all extensions are good. Some extensions are useful but use a lot of memory. You are allowed to uninstall or disable them. Do not just keep them there out of complacency and laziness.

There are only two extensions that you should have: an adblocker and a password manager. You should never, ever, under any circumstances use the built-in browser password manager. Especially on a computer that you might leave on a table in a library. It is too easy to get passwords. Chrome, if I recall correctly, even has a password export feature. And you can access the passwords within the settings too. I would recommend using [uBlock Origin](https://github.com/gorhill/uBlock/#installation) as your adblocker, and [Bitwarden](https://bitwarden.com/) for passwords. They are both free. uBlock uses a little less memory and is up-to-date, it also has loads of options we can discuss in another blog post. Bitwarden has desktop applications, mobile phone applications, and an extension. It can also import your Chrome and Firefox passwords.

### Conclusion

Keep it **SIMPLE**: You don't need lots of tabs or lots of extensions.
Use **ONE** browser: You can uninstall the others. Unless it is Edge. I think you are stuck with that, but I haven't checked in a long time.
**UPDATE** your browser: This is good for security. You can also update the extensions.
**PAY IT FORWARD**: Give your friends advice when you can.

I hope somewhere, some student finds this useful. If you did, be sure to bookmark the blog. The next instalment is about writing documents and word processors.

Take care.
