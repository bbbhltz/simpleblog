---
title: "Technology, Ecology, and Whales"
date: 2021-04-29T14:36:27+01:00
lastmod: 2024-10-13
slug: technology-whales
draft: false
author: bbbhltz
subtitle: "Can we use NLP to decode whale chatter? Will we become more interested in ecology by doing so?"
description: "A blog post talking about decoding sperm whale communication."
tags:
  - Technology
  - Ecology
next: true
nomenu: false
---

***Yes. Whales. But also tangents that have nothing to do with whales.***

## Why whales?

On the 15th of April, National Geographic published a feature on their website, *Groundbreaking Effort Launched to Decode Whale Language* ([link](https://www.nationalgeographic.com/animals/article/scientists-plan-to-use-ai-to-try-to-decode-the-language-of-whales)). It is an interesting read; interesting enough that the link made it to [Slashdot](https://science.slashdot.org/story/21/04/22/003227/groundbreaking-effort-launched-to-decode-whale-language) in their Science and Communication categories.

If you are not in the mood to read the entire thing, here is the summary:

They want to decode sperm whale codas (communication patterns). That is cool. And it is noted at the end that: 

> “It's not about us talking to them, [it] is about listening to the whales in their own setting, on their own terms. It's the idea that we want to know what they're saying --- that we care.”

So, the goal isn't conversation, just understanding. It is still a great endeavour.

### How will they do it?

Science, baby! Computers and science!

Some buzzwords are peppered throughout the article: AI, Natural Language Processing (NLP), Robotics.

No surprises there. NLP is a very cool branch of AI. Looking up the definition, it is not so difficult to understand. But, videos are nice too, especially when they have concrete, real world, applicable examples. [This recently published TED Talk gives us a clear example](https://www.youtube.com/watch?v=Mkelhs_OVMc).

### Who is doing this?

The Avengers I guess? A dream team of scientists --- experts in linguistics, robotics, machine learning, and camera engineering --- are name-dropped:

* **Shane Gero**: biologist, 13 years spent tracking and studying sperm whales.
* **David Gruber**: explorer, City University of New York professor of biology and environmental science, developed robots.
* **Shafi Goldwasser**: fellow at Harvard University's Radcliffe Institute, computer scientist and one of the world's foremost experts in cryptography.
* **Michael Bronstein**: chair of machine learning at Imperial College London.
* **Roger Payne**: a MacArthur Award winner, popularized the mesmerizing songs of humpbacks in the 1960s and 1970s
* **Robert Wood**: Harvard roboticist who worked with Gruber.
* **Daniela Rus**: MacArthur recipient, director of computer science and artificial intelligence at the Massachusetts Institute of Technology.

[David Gruber also did a TED Talk about this very subject](https://www.youtube.com/watch?v=Qm02X0aE8uU).

And, like the Avengers, new members will show up later.

## Corporate Social Responsibility: Greenwashing

My personal opinion on the project is very positive. I think whales are amazing, and one of my dreams in life is to see whales. It won't be easy for me because I am afraid of deep water. National Geographic is all about saving the planet and the animals that live on it, so again, nothing negative to say about that. The negative is that I feel like the entire feature on National Geographic is a form of sponsored content. It is a call to action to remind people like me that National Geographic documentaries are available on Disney+. And, what new documentary was released this month, on Earth Day?

Yes. A 4-hour documentary called *Secrets of the Whales*, a Disney+ original series from National Geographic.

I watched it. It is remarkable. It has everything we want from nature documentaries: drones, underwater shots, slow motion shots, time-lapse, whale poop, a narrator, and a comment from the executive producer at the end. Basically, it is an Attenborough clone. Except, without Attenborough, it isn't the same. Unless, of course, you bring out the big guns:

* **Sigourney Weaver**: awesome, kills aliens, saves gorillas, busts ghosts, talks to blue people, has timeless catchphrases, narrates this documentary.
* **James Cameron**: Canadian, [made some movies](https://en.wikipedia.org/wiki/James_Cameron_filmography), likes submarines, executive produced the documentary and talked for 5 minutes at the end of each instalment.

Now we're talking! Scientists are cool, but anybody growing up in the 90s knows that a large part of the population thinks that scientists are boring or a source of mockery. Think Ross on Friends or the first few episodes of The Big Bang Theory. Give us Weaver and Cameron, who only mentions once that he is currently filming Avatar 2 and 3 in New Zealand, and now we might tune in.

What we're tuning into is visually stunning, and doesn't force-feed us the depressing stuff we know about. There are only two brief mentions of pollution, and two dead whales. It is also a prime example of a tried and true documentary technique: repetition.

Instead of telling us not to eat seafood, to boycott the fisheries, to stop polluting, Disney and National Geographic tell us over and over again that **whales have culture**. Whale behaviour isn't learned; *it is culture*. It isn't imitated; *it is culture*. It isn't simple animal instinct; *it is culture*. After 4 hours of cetaceous delights, we get it. Whales have culture. The call to action is indirect, but it is there: whales are like us, and we need to save them. Disney wants you to know that this is part of its mission statement.

## Ecology and Technology

It has come to this. Getting people interested in something as obvious as saving the planet requires marketing, propaganda, pandering, celebrities, technology, and a subscription to Disney's streaming platform. The money and the technology required to make all of this possible may outweigh the benefits (unless you are Disney and money is one of your goals). Could that money have been better spent on preservation efforts? And what about the CO<sub>2</sub> footprint of all of this? Sending film crews around the world for 3 years, streaming it all, storing the videos on servers around the world? We need to hurt the planet a little just to save it. I know, I am nitpicking, but things add up after a while.

## The Last Drop

Every little thing we do is a drop in a glass, including what we do on the net. A drop is nothing. We do it again and again. We let the tap run while we brush our teeth, or leave a light on, or use our car to go fetch the mail instead of walking. These things are visible, and tangible. Our digital activities are drops too, and they are easily ignored.

A lot of us are aware of this and actively fight against it. We try our best. We choose the eco-friendly option when it is available. It is not always easy, however, and sometimes we make exceptions. We might leave our PlayStation on by accident, or chose the wrong function on our dishwasher. Other times, the choice isn't available.

Our jobs might require us to use email. My employers remind us very frequently to delete our old emails and use the cloud as much as possible when sending files. This makes sense when everyone in our company is sent the same PDF, because we have heard again and again that [emails use C0<sub>2</sub> and have other environmental impacts](https://ademe.typepad.fr/files/acv_ntic_synthese_courrier_electronique.pdf):

A 1 MB email sent to one person:

* uses as much energy as a 60W bulb left on for 25 minutes;
* emits up to 20g of CO<sub>2</sub>.

This is because the sender and receiver are using energy, along with the data centres.

My house has multiple power-strips with energy sucking things plugged into them, but if I unplug them, then it resets everything. This website is running on a Raspberry Pi attached by USB to my router/modem,, and it is never turned off. The computer that I am using might not use a lot of power, but there is a monitor and speakers and a PLC that potentially do.

In order to watch that wonderful documentary, it required a computer to make the Disney+ account, a PlayStation attached to a TV, a router/modem, a remote to control it, a server to control it, an ISP, etc. Disney+ has around [100 million subscribers](https://variety.com/2021/tv/news/disney-plus-100-million-subscribers-worldwide-1234925654/) worldwide; that is a lot of drops. At some point, surface tension will give way and our glass will overflow.

## Conclusion

Clearly, I am overthinking and oversimplifying things. I am not a researcher, a journalist, or an editorialist. I am just a teacher. I like watching documentaries, no matter what the call to action, no matter what the opinion of the person preparing it. The point I am getting at, which should have been mentioned in an introduction somewhere (see, a researcher or journalist would have done just that), is that if a media company wants to play the green card, they should be 100% credible.

Credible would have been pointing the finger at those responsible for the damage. Credible would have been naming names. Credible would have been refusing to work with people, companies or nations that do this damage.

If we ever decode what the whales are saying, it will be amazing, even if we only figure out a word or two. Will it make us, as humans, become better consumers? Will we say to ourselves, "hey, this product or company hurts animals, and animals are earthlings like me." Will something like this ever lead the majority of the population to consider the environmental impact of a selfie on Instagram.

No. And it shouldn't, either.

It isn't the consumer at fault here. Social media companies give us technology that they know is addictive. Media companies make us pay for their services and gain, financially, from our attention. They should be the ones limiting our use.

Whales have been around for tens of millions of years. They survive extreme conditions. They rely on each other. We rely on technology for too much, and have trouble surviving something as natural as boredom. I hope that when we decode their codas and songs, we will be able to take a step back and re-evaluate our priorities.

**Update**: See [The Dominica Sperm Whale Project](https://www.thespermwhaleproject.org/) for updates!
