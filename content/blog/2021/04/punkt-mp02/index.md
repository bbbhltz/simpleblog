---
title: "The Punkt. MP02 Minimalist Mobile Phone"
slug: punkt-mp02-review
date: 2021-04-15T17:02:49+01:00
lastmod: 2024-10-13
draft: false
author: bbbhltz
featured_image: /blog/2021/04/punkt-mp02/controls.webp
subtitle: "What's the deal with this expensive little device?"
description: "A review of the Punkt. MP02 Minimalist Mobile Phone."
tags:
  - Technology
  - Review
next: true
nomenu: false
---

**TO SEE INFORMATION ON THE UPDATE, [CLICK HERE](/blog/2021/11/punkt-update/)**

***For the past week I have been using a new (old) phone. The Punkt. MP02. Here are my thoughts on this minimalist designer phone***

## Introduction

If you already know about this phone, you can skip straight to the end for my very short review. If you are of the uninitiated, it might be interesting for you to read about the company and the product before reading my thoughts.

I have only been using this phone for a week. It was not provided by the company for review, it is just a gadget that I have wanted to try for some time.

For the **privacy-conscious**: This is not the phone you are looking for. Read [*Spy Phone: Hands On With The Punkt MP-02*](https://neflabs.com/article/punkt-mp02-security/) for more information.

## Company

Punkt. is a Swiss company that makes designer consumer electronics. The company was established in 2008 and released its first product, a cordless phone (the DP01), in 2011. Since then, they have developed a handful of products including mobile phones (the MP01 and MP02), cases for those phones, an alarm clock, a USB charger and an extension cable.[^source1]

Ethics play a key role in the company and they claim they only use suppliers wih a "proven environmental track record". They also emphasise that they make their products to last.[^source2]

[^source1]: https://www.punkt.ch/en/products/
[^source2]: https://www.punkt.ch/en/about-us/punkt/

## Product

The **MP02** is a "feature-less" phone. Clearly, the picture above tells it all. It is distinctly Swiss in design: Buttons, arrows, a little screen. Black is the only choice for colour. It uses USB-C. You might be surprised, however, to know that the instruction manual for this device is 57 pages long (plus another 23 pages just for *Pigeon*) and there are security features provided by the immortal and trustworthy BlackBerry.

![Instruction Manual: Controls](controls.webp "Punkt. MP01 Controls")

![Instruction Manual: Keypad](keypad.webp "Punkt. MP01 Keypad")

Although they do not really market this device as a "digital detox" or "digital minimalism" device, it has been typecast as one. From a marketing perspective, they have made the right choice. Trends are ephemeral, Punkt. products are supposed to be long-lasting.

### Functions

Beyond calling and texting, you can:

* manage your contacts;
* set alarms;
* use a timer and stopwatch;
* set reminders using a simple calendar;
* do simple maths using a calculator;
* connect to Wi-Fi;
* use the phone as a hotspot;
* get OTA updates;
* take simple notes;
* switch languages;
* use predictive text;
* use Bluetooth headsets;
* send and receive group chats;
* communicate with your friends who use Signal using the *Pigeon* app.

You can also set hotkeys to access some of those functions without needing to go through the menu. All in all, this is positive (there is some negative to come). When you buy a phone like this, you don't want to have links to Twitter, Facebook or random games that you cannot delete. All of the options, or lack thereof, are present by design. And design is what you are paying for here.

### Design, Cost, Pricing

€329 is what this little chunk of plastic will cost you. Let's say €330 to make things easier.

Many people reading this knew beforehand that Punkt. phones are expensive. If you are discovering this now and checking the price online to make sure I am not punking you, you may be thinking, "who would buy that?"

Who would handicap themselves in this world of constant pings and beeps and notifications? People who have had enough! Remember, there are [people out there who don't use email](https://www-cs-faculty.stanford.edu/~knuth/email.html) because they have had enough of it. The target market here is not young people. The target is people who want a nice (different) looking device to keep in touch with friends, family, and colleagues. It is the people who do not want to become their phone. It is people like myself, who like gadgets, who have jobs, who take the time to look into the different options on the market.

When you buy an iPhone or an expensive pair of headphones, you are not just paying for the material and the markup. You are paying for the design. Design could be up to 50% of the cost of the product [^cost]. Think about it. Well-designed products are like well-scripted movies.

[^cost]: I don't remember where I read this, but if I come across it again I will update this article.

UX teams, UI teams, engineers, the whole crew, must sit down and create "user stories". Mudita, another company working on a minimalist phone that hasn't been released yet (priced in the €300 range), also talks about the importance of this necessary design step:

>In order to visualize the features of MuditaOS and create the requirements, we needed to develop user stories - a complete list of all the functionalities and actions that the system needs to include from a user perspective.
>
>All of the teams were involved in this process. We prepared a list of general functionalities that Mudita Pure should have, and then divided them into particular applications and tools.
>
>For each application, we prepared a separate diagram, which not only showed the functionalities, but also the connections between them and the possible paths that a user could take. In total, we defined 450+ user stories! [^mudita]

After preparing these stories, the product needs to be made possible through the magic of engineering. Parts need to be sourced; programmes written; prototypes tested. You pay for all of that.

If we imagine that 50% of the price is design, with something like an 80% markup, then the phone *might cost* about...(€330/2/8)...€20 to make! That seems reasonable for an Android device. Yes, you read that right: the Punkt. MP02 is an Android phone (more on that later).

(EDIT: there is a teardown available on [fccid.io](https://fccid.io/Z3PMP02/Internal-Photos/Internal-photos-4067101))

It is also reasonable for another reason. If you price your product low, the perception changes. It also means that more people will buy it. More customers means more complaints. It also makes it a disposable device, which is not the goal here.

[^mudita]: https://mudita.com/community/blog/adesign-awards-and-update-on-the-operating-system/

### Under the Hood

The phone runs Android, but what is it running? Android means we should be able to access developer options. Developer options means we should be able to inspect the device with the Android Developer Tools and perhaps see what role BlackBerry is playing and how many fingers Google has in this pie.

*Settings - About - \*Click on Build Number a bunch of times\*...SUCCESS!*

```
Package Name                            # Description

android.ext.services                    # Notification ranking service (I think?)
android.ext.shared                      # Android Shared Library
ch.punkt.mp02.appmgr                    # Guessing this is the appmanager for the MP02
ch.punkt.mp02.calendar                  # Calendar App
ch.punkt.mp02.registration              # Phone Registration App
com.android.backupconfirm               # Allows backup/reset/restore operations
com.android.bluetooth                   # Bluetooth
com.android.bluetoothmidiservice        # Bluetooth
com.android.calculator2                 # Calculator
com.android.calllogbackup               # Call logging
com.android.captiveportallogin          # Wi-fi login tool
com.android.carrierconfig               # Configures telephony features based on carrier
com.android.carrierdefaultapp           # Carrier toolkit
com.android.cellbroadcastreceiver       # Registers system and application events
com.android.certinstaller               # Credential storage
com.android.companiondevicemanager      # Manages companion devices (Bluetooth, etc.)
com.android.contacts                    # Contacts manager
com.android.cts.ctsshim                 # Compatibility Test Suite: solves problems between versions of Android
com.android.cts.priv.ctsshim            # Compatibility Test Suite: solves problems between versions of Android (private?)
com.android.defcontainer                # Package access helper for installing and uninstalling apps
com.android.deskclock                   # Clock widget
com.android.dialer                      # Dialer
com.android.documentsui                 # file picker (I've deleted this on other phones by accident, results are as you can imagine)
com.android.email                       # Email for some reason?
com.android.emergency                   # Emergency dialer for when the phone is locked (required by law)
com.android.exchange                    # MS Exchange?
com.android.externalstorage             # Handles access rights between content providers
com.android.gallery3d                   # Photo gallery
com.android.htmlviewer                  # HTML viewer
com.android.inputdevices                # Probably does what it says and listens for input devices like the USB earbud
com.android.keychain                    # Provides access to private keys and credentials
com.android.launcher3                   # Punkt. Launcher
com.android.location.fused              # Location
com.android.managedprovisioning         # Sets profiles for device
com.android.mms                         # MMS
com.android.mms.service                 # Handles MMS (even though the phone cannot receive MMS...)
com.android.mtp                         # Media Transfer Protocal
com.android.music                       # Music app (needed for ringtones)
com.android.onetimeinitializer          # First boot initialisation app (onboarding)
com.android.packageinstaller            # Package installer
com.android.pacprocessor                # Proxy auto-config: defines the proxies if used
com.android.phone                       # Phone app
com.android.protips                     # Provides tips on homescreen
com.android.providers.blockednumber     # Allows phone number blocking (very useful!)
com.android.providers.calendar          # Calendar storage
com.android.providers.contacts          # Contacts storage
com.android.providers.downloads         # Another content provider app
com.android.providers.media             # Scans and stores media file info for quick access
com.android.providers.settings          # Settings app
com.android.providers.telephony         # Provides core Android functionality related to MMS and SMS messages
com.android.providers.userdictionary    # User dictionary
com.android.provision                   # Required service: is needed when the user's phone moves from one cell-tower to the next, networking and Wi-fi.
com.android.proxyhandler                # Proxy handler
com.android.quicksearchbox              # Quick search widget (useless?)
com.android.server.telecom              # Manages phone calls, among other things
com.android.settings                    # Settings app
com.android.sharedstoragebackup         # Works as part of the backup/restore system
com.android.shell                       # Provides shell
com.android.sim                         # SIM manager
com.android.simnote                     # Unsure, but it is required and is related to the SIM
com.android.soundrecorder               # Sound recorder
com.android.statementservice            # App for the privacy statement
com.android.stk                         # SIM Application Toolkit: manages the SIM card (contacts, etc.)
com.android.storagemanager              # Handles storage
com.android.systemui                    # System UI
com.android.systemui.theme.dark         # Dark Theme for System (is there a light theme?)
com.android.vpndialogs                  # Dialogs for connecting to VPN
com.android.wallpaperbackup             # Wallpaper handler?
com.android.wallpapercropper            # Wandles wallpapers as well (useless?)
com.android.webview                     # Webview (useful when there is a CAPTCHA)
com.blackberry.bide                     # BlackBerry Integrity Detection: monitors the device for known security vulnerabilities
com.caf.fmradio                         # There isn't an FM radio app so...useless for now?
com.dsi.ant.server                      # ANT HAL (Hardware Abstraction Layer) Server (?)
com.dtinfo.tools                        # OTA updates
com.iqqijni.dv12key                     # I think this is the "Kika keyboard" app...provides predictive text and emoji
com.qti.confuridialer                   # Conference call service; could be removed
com.qti.launcherunreadservice           # Works with or is part of launcher
com.qti.qualcomm.datastatusnotification # unsure what it does; needs permissions to send sms
com.qualcomm.embms                      # background service for carriers to use to send you stuff?; can be removed
com.qualcomm.location                   # Location service (GPS off by default)
com.qualcomm.location.XT                # Location service (GPS off by default)
com.qualcomm.qcrilmsgtunnel             # Worrks with the phone dialer app
com.qualcomm.qti.callfeaturessetting    # Not mandatory; can be removed
com.qualcomm.qti.ims                    # Redundant IMS (?); needed for VoIP
com.qualcomm.qti.services.secureui      # WHO KNOWS?
com.qualcomm.qti.simsettings            # Allows settings for SIM cards (names, colours)
com.qualcomm.qti.telephonyservice       # Sound processing for phone calls
com.qualcomm.qti.uceShimService         # Unified Communications for Enterprise
com.qualcomm.simcontacts                # Access to SIM contacts
com.qualcomm.timeservice                # Soes something with the clock; no touchy
com.quicinc.cne.CNEService              # Automatically switchs between Wifi/3G/4G depending on network performances
org.codeaurora.bluetooth                # Handles Bluetooth
org.codeaurora.ims                      # Needed for VoIP
```

*If you see mistakes here, could you let me know by [sending me an email](/contact) or on Mastodon?*

As we could have guessed, it is close-to-stock Android with the necessary Qualcomm bits, a couple Punkt. apps, a single BlackBerry app, and some "who knows?"

Within the "who know?" category we have the package called `com.iqqijni.dv12key` which, when pulled from the device, is **Kika.apk**. I am unsure if this is the Kika keyboard app, but if it is the case Punkt. should consider replacing it somehow. It is probably there to provide predictive text features or emoji ([see entry on εxodus](https://reports.exodus-privacy.eu.org/en/reports/com.qisiemoji.inputmethod/latest/)). I uploaded the APK to [Pithus](https://beta.pithus.org/report/41029c7f5b4771a4d4ba81bef42bbc047d63a198cbdfd3e1698d1fcb0cffc334) to confirm that it is a 12-key keyboard from Kika.

It is easy enough to disable/uninstall a few of these, and for now I won't go messing around.

## Review

There are plenty of review available for you to read. The phone does its job. The newest feature, *Pigeon* (which is Signal) needs detailing.

### My Experience

This is not my first Punkt. phone. I had the MP01 in 2018 just before this model was released. I was not at all surprised to open the stylish box and extract the rather heavy phone from its neat, tilted, place of rest. The box contained what is expected. The earpiece is nice. It is USB-C and is just a single earbud; I didn't have one like that!

The differences became apparent after booting.

Booting is fast. I knew that I would need to update. I went through the options to find the Wi-Fi options, connected, found the update firmware option and waited.

It seemed a little long. There was no true visual to let me know if something was happening or even working. Finally there was a progress bar, then it rebooted, there was another progress bar, and I was up-to-date.

**Quick Tour of the Punkt. Menus [^scrots]**

[^scrots]: if you want to pull screenshots from your Punkt. MP02, you can do it with adb using the commands `adb shell screencap -p /sdcard/scrot.png` and `adb pull /sdcard/scrot.png`

*(I don't know why some of these screenshots are tinted green)*

![Punkt. Home screen](home.webp "Home screen")

![Quick Settings](upmenu.webp "Quick Settings")

![Main Menu](menu.webp "Main Menu")

**Interface and Experience**

*(not counting Pigeon)*

The main part of the interface is the device itself. It is solid and has a decent amount of heft. I think I may have preferred the texture of the MP01 over the MP02. The MP01 had a unique dimpled golf ball pattern on the back (as well as some other physical buttons). The unique shape of the phone is not awkward to use. The bonus is that it looks nice when sitting on a desk.

![Punkt. MP01](original.webp "The MPO1 Phone")

Navigating the phone is not unpleasant. It is actually quite snappy most of the time and there are some animated effects. Using the buttons to get around is intuitive and it the choice to include a special message and contacts button is a good one. Speaking of the buttons, they are a good size, even for my big fingers.

Moving around with just up, down, back, and OK is not at all hindering

There are occasional delays. Since we don't always have some sort of visual or icon to let us know something is happening, it might lead us to think that it isn't working. A good example is when you reboot the device and are asked for the password and SIM password. It is just long enough to begin asking yourself if it is working.

**Messaging and Contacts**

These are the two main apps on the device. As you can imagine, they work the way we want. The contacts app lets us add all the info we might want to have --- first and last name, nickname, email addresses, address, notes, and phone numbers.

Messaging on the MP01 was not threaded. It was fast, for sure, but if you forgot what the conversation was about it was a little challenging. Users complained and so the MP02 has threaded messaging. It is better, but the text can get a little tiny. Luckily as you scroll up and down the text pops out and each message is contained in a little call-out or speech bubble. 

There are a series of predefined templates that you can add to. This is something that most users should take advantage of, especially if you frequently use a word that is not in the dictionary, like the name of a shop.

The text input works much better than something like KaiOS, and is similar to what most of us remember from pre-smartphone days. You can input tap-by-tap, numbers, or use the predictive text mode. You can easily switch between languages, which I have to do all the time. I did need to use the guide to figure out how to do all caps.

There is also the standard set of characters that you can access with the <kbd>*</kbd> key, and the Android Emoji too (by holding the <kbd>\*</kbd> key).

![Character Map](charmap.webp "Character Map - available by pressing * when inputting text")

![Emoji](emoji.webp "Emoji - available by holding * when inputting text")

It is a good thing that these two apps function correctly *now*. I updated my device before use, but when you see the [changes](https://www.punkt.ch/en/products/mp02-4g-mobile-phone/#release) introduced you can see that the previous version was somewhat less usable:

> **Text input**
> 
> * An updated version of the text input software package has been installed.
> * User dictionary has been reintroduced.
> * When long pressing either the UP or DOWN keys in a text input field, the cursor will rapidly scroll through the text instead of jumping immediately to the start or end.
> * Several pages of ideograms have been added.
> * Default mode in the Input method has been reviewed and improved in numerous use cases.
> * The apostrophe and quote marks have been corrected on the special characters input panel.
> * Bug entering a new line instead of displaying the contextual menu when pressing the Punkt. key is now fixed.
> 
> **Contacts**
> 
> * When searching for contacts, the original search term is now displayed until the search has been cleared.
> * The contact list now only shows contacts stored on the handset. SIM contacts are accessible in the SIM contacts menu.
> * When using the Call or Message keys to contact a recipient with multiple phone numbers, it is now possible to select the desired number.

**Calendar**

![Calendar](calendar.webp "Calendar (it is just a calendar)")

There is nothing to say about this app. It is a calendar. It shows the calendar with days of the week and week numbers. That is all.

**Clock**

You can set the clock with this app, and list different time zones. The thing I like about the clock app is the graphical map for time selection. It is a nice touch.

![Map for Time zone Selection](map.webp "Map for Time zone Selection")

**Notes**

Just like on older devices, there is a notes app. It is useful because you can use it to set reminders. The reminders are then listed as notifications on your home screen.

What is missing here is an export feature. I would like to be able to access the notes when the device is attached to my computer. Similarly, it would be nice to be able to prepare a long note and then send it as a message.

**Calculator**

This app is broken. My CASIO calculator watch works better. What can be broken in a calculator you wonder? The decimal key, of course!

If zero is the first number, you cannot type a decimal after. So, if I want to type `1.9+0.1` I get this:

![Calculator Bug](calcbug.webp "Calculator has a problem with zeros")

This is because the same button is supposed to let you cycle through different inputs:

![Calculator Help Screen](calchelp.webp "Calculator Help Screen")

But, it does not want to let me put a decimal after a zero. That is one of the six main apps that does not work. 

**Settings**

Settings have all of the preferences you would expect for connections, setting the date, sounds, installing Pigeon, setting the voicemail number, caller ID, etc. You can set the hotkeys here as well, but I don't know how to delete them.

There is a security option that just shows you this:

![BlackBerry Security](bb.webp "Excellent security according to BlackBerry")

And of course, the expected Android about info that lets you know you just exchanged your hard-earned dough for Android 8.1.0.

![AOSP Version](aosp.webp "AOSP version 8.1.0")

### Pigeon

A surprise email landed in my inbox on April the 8th. Signal was available on the Punkt. MP02! The version on the Punkt. phone is called Pigeon but is still `org.thoughtcrime.securesms`. Punkt., as far as I can tell, released no media to show off this feature except for a few tweets and Instagram posts that tell us nothing.

The features are there: text, voice, call (no video, obviously). And, it can receive audio, video, and photo messages. Groups work, with a few caveats. 

The main menu gives you access the the basics, and each conversation has its own menu and settings.

![Pigeon Menu](pigmenu.webp "Pigeon Main Menu")

![Pigeon Conversation Menu](pigsession.webp "Pigeon Conversation Menu")

You can get to Pigeon with a long press on the message button or through the menu. As soon as you start messaging with Pigeon, one thing becomes clear: **this app was not ready for prime time**. I suppose the only way to work out bugs is to throw the app to the wolves. This app wouldn't survive an attack by puppies.

Sure, it is great that I can send voice messages to my mother in Canada. And I can receive some pics from friends. How I can get them off of the phone is a mystery for the moment. The app is, however, an exercise in frustration most of the time.

If you are having a conversation, you might not notice the problems. But, as soon as you want to look through your conversation, it scrolls randomly. Sometimes you can land on the message you want, and expand it to read the entire thing, sometimes it is hidden behind the input box. There are also instances of the text going outside of the dialogue balloon boundaries.

![Pigeon Thread](signal1.webp "Design is key!")

That is not even the bad part. The bad part is that you sometimes don't get messages until much later! So far, while testing with my partner, the difference can be anywhere between 4 and 50 minutes. It usually requires cycling the aeroplane mode to get those messages to download.

Punkt. is aware of these problems, among others:

> **Known issues in Pigeon v1.0.0777**
> 
> * Displaying images: when selecting received images in a message thread, the screen becomes darker.
> * Forwarding a video or document file: this sometimes causes the MP02 to return to the Home screen.
> * Exiting Pigeon: this may occasionally cause the system to freeze for a limited period.
> * Accessing a new message from a notification: the message is highlighted but its font size may not be enlarged.
> * Notifications: these may sometimes be produced with a slight delay, especially if the MP02 is locked.
> * Incoming calls: these may occasionally not be received, especially if the MP02 is locked.
> * Groups: it is not yet possible to appoint a group member as an additional
> * Notifications options screen: this currently has a scrolling issue.
> * Calls: an echo is heard in some circumstances.
> * Message thread: moving through a message thread will sometimes cause the highlighting to jump too
> * Confirmation messages: some of these still appear in English, despite the MP02 being set to a different language.

That is essentially a problem with nearly every aspect of the app! What they call "moving through a message thread will sometimes cause the highlighting to jump too" is what I mentioned above. Super jumpy.

Over on Reddit, the critiques are similar:

> Meh, pretty unelegant. It was such obvious solution to me to integrate it into the SMS app, maybe with an indicator for Signal threads, that I didn't expect something like this. Breaks all the design principles.
>Edit: Also, bad usability. I can't scroll through threads properly, making some messages unreadable.
>Edit2: Also, messages are delivered with half an hour delay. No Pigeon for me thx

via [/u/gruetzhaxe](https://old.reddit.com/r/Punkt/comments/mmp397/pigeon_ie_signal_is_out_now/gttk5gg/#c)

### Bottom Line

Outside of Pigeon, the phone is a good phone. Texting, calling, other bits and bobs are working correctly. Pigeon is a step in the right direction. The phone has Wi-Fi, so we might as well use it for something. The phone makes for a very good companion device for weekends when you want to focus on family and friends without being attached to a smartphone. The benefits of this device are somewhat opaque. Limited features should mean limited benefits, but the perceived value of the device could be very high, depending on the type of person you are.

If design matters to you, then this might be something to think about. If you want to give someone a nice designer gift, this might be an idea. If all of your work and socialising is done via calls and SMS, and you want to have a designer gadget to do this with, then this is an OK option. Anything this phone does, a cheaper Android can do better. The only difference will be the appearance of the device.

I like the device, despite what my comment above may imply. I like the appearance and the limited functionality. I like having a very light device in my pocket and I am very happy to have the MP02 because I also liked the MP01. **But**, I like gadgets. I like things like portable music players, Raspberry Pis, pedometers, calculator watches, and any type of end-user gizmo that I can find a use for. I think that with a few tweaks and fixes, this device could be much more user friendly, especially where Pigeon/Signal is concerned.

**65% --- good work overall with noticeable points of improvement**

*If you are thinking about this device, and reading this, do some research. Get on the [subreddit](https://old.reddit.com/r/Punkt/) and see what people are saying. Read the reviews. Remember that this thing came out in 2018 and consider other options too. The phone will likely reach end-of-life in 2 years.*

## Conclusion and Recommendations

If anyone from **Punkt.** ever comes across this, here are my TL;DR notes disguised as a conclusion:

The Punkt. MP02 is an attractive device with a purposefully limited set of features that make it wonderful to use. Seriously. It is light and easy and provides you with the *joy* of missing out that we have all forgotten about over the years. Future updates need to address glaring issues:

* **Calculator** needs to be fixed
* **Notes** needs an export feature (maybe with Markdown support)
* Give the **Wi-Fi** another purpose (Internet radio? **Podcasts**? Some sort of management software for our computers?)
* Include an **FM radio** app perhaps? There is about 12 GB of free space on the device, so it would be great to use it for something.
* **Increase the volume**
* **Pigeon audio and video messages need volume control** and an easy way to activate the speaker
* Pigeon should have been called **Pigeon Beta** or Testing to avoid too many complaints
* Pigeon needs to work with the **desktop app** (I haven't tried, but comments on forums make it sound like it does not)
* Give us a **roadmap** for updates
* Give us **a place to submit issues** for the different apps --- even github would be good. Or, give the community a blog.
* When the device reaches EOL, **open up the code** and let us root our devices.
