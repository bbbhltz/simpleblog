---
title: "The Privacy-Security Rabbit Hole"
slug: privacy-security-rabbit-hole
date: 2021-04-26T14:43:54+01:00
draft: false
author: bbbhltz
subtitle: "A Tale Absent of Advice, Facts, Research, and Best Practices"
description: "A short story about trying too hard to protect yourself online."
tags:
  - Privacy
next: true
nomenu: false
---

Have you heard?
The web is crawling with unscrupulous characters.
The web is *controlled* by those unscrupulous ne'er-do-wells!

For a long time, you assumed your up-to-date computer and phone with ad-block installed was enough to keep you safe from their prying eyes and sticky fingers. After all, you have nothing to hide. Then, you clicked on a YouTube video or post from a friend. Perhaps someone in your entourage gave up the Facebook (shock waves of awe rippled throughout the friend group). A thought coalesced in your brainbox: *"Maybe I'm...vulnerable?"*

Warily, you enter your query on the Google: "how to privacy web security"

*Drats!*

Quickly you learn that you have already made a mistake. The Google is evil. Big tech is bad. Corrupt! Cronyism runs through their Silicon Valley DNA. Your nascent research has filled your head with definitions that require deep-diving into a wiki-hole. Nobody is to be trusted.

But, right there, off in the distance. A twinkling, sparkling, light? A mirage perhaps? Carefully you creep. Yes. There is something. Squinting, the words take form, although you are confused as to how this relates your initial question.

Free...Libre...Open-source...

**NO!**

You have leaned in too close.

You are falling\
.\
.\
.\
down\
.\
.\
.\
the rabbit hole.

The sweat beading on your forehead, fingers flying across your keyboard, you act!

Change your browser. Your email provider. Should you self-host? What search engine is best? What is 5-eyes? 9-eyes? **14-eyes!?** Should I get a new computer? Change my OS? Is Apple safe? F-droid? Degoogle? Signal? No, Telegram? No! Briar! Block this, block that, blocklist, containers, threat models, hot single models in your area? Wait? How did that get through my impenetrable system? There is a weak link. But where?

Click, click, click...
Down, down, down...

A hooded figure appears.

> "Psst...hey...you...yeah, you! Come here. I gotta tell you something: Firefox, DuckDuckGo, Linux, Github...follow the money."

And, just like that, he is gone.

*Weird fellow that one, he has been down the hole too long. Got a little tinfoil hat and everything.*

But, your fingers are already caressing the keys or the screen in front of you.

*Follow the money...*

No. No. No...

That browser pockets money from the Google, that site is owned by Microsoft, that one shares your data with Bing, the founder of that one is a massive douche, and a lot of these are hosted on Amazon's servers.

Defeated. They have won. How do you go up against Goliath when your slingshot is made by Goliath, Inc.?

You could join them? Or just stop using their tech altogether? Maybe that is a solution. But, how to keep in touch with friends and family? A phone is all you need. A copy of Tails on a USB flash drive. A recycled laptop that you ripped the webcam and microphone from.

Down to the local rip-off shop you go, because Amazon is evil, and ordering by Internet requires a credit card, an address, a phone number, an account. Aisle by aisle, you peruse the plastic confections of Asian origin to find your pleasure: a basic phone.

You queue up, ready to buy that little gizmo and...

Nope.

That must be a mistake.

This one has Google on it. And Facebook. Qualcomm SoC too. Back to the aisle, you go to discover that the cancer has spread to these devices as well. Their fingers are in every pie. This basic phone is just Android. That one even has WhatsApp. Adjacent, you see another section of ye' olde gizmo and gadget shoppe: Portable music players. There too you see it. Google everywhere.

> "You look like you need some air, friend. Come with me."

It is your wise Facebook-quitting friend.

>"I'm not strong enough...how did you do it?" you plead.
>
>"Do what?"
>
>"Get away from big tech? Use the web privately and safely without giving up your personal information to those companies?"
>
>"Come. I'll tell you a few things."

You follow your wise friend to a place. Neither here nor there. Not the rabbit hole, but not *not* the rabbit hole. They sit you down. They tell you what should have been obvious all along:

>"There is no perfect solution.
>
>"If you push hard, it makes you more *unique* to the companies you want to avoid. You can still be profiled without an account.

>"You have likely already done more than most to keep yourself safe and your personal information private, what with your decentralized platforms and encrypted everything. But if you keep digging, you will go insane, because there is always shit to be turned up. And shit stinks.
>
>"It is one big stinky situation."

Still, you do not understand.

>"So, we lose. They win."
>
>"No. It isn't a win-lose thing...but if you want to talk about that, tell me something: You quit social media, hardened your devices, blocked cookies and JavaScript and trackers; is there anything that you miss?"
>
>"No. Not at all."
>
>"That, my friend, is a very big win. But, by the look of you, this is more of an obsession than anything else. How much time do you spend a day looking into this topic and interacting with strangers on the web?"
>
>"Five or six hours. Maybe more."
>
>"That is unhealthy. Stop doing that. Do something else with that time, and you will truly win."

The first days were difficult. Your friend was right. It was an obsession, but you did it. Months have gone by since you were pulled from the rabbit hole. You feel healthier, stronger, taller, even. You walk some days, see friends on others --- real friends. You shared your knowledge with your friends and family, they too wanted some distance from the evil-doers. You still read the different articles on privacy and security, but you no longer obsess over every possible point of attack or take the time to comment or reply with your hot take on the subject.

You use the Internet; it doesn't use you.
