---
title: "Update: Punkt MP02"
subtitle: "A quick note on changes to the device"
description: "An update on a previous post about the Punkt. MP02 phone."
slug: punkt-update
featured_image: /blog/2021/04/punkt-mp02/controls.webp
date: 2021-11-12T21:04:05+01:00
lastmod: 2024-04-14
draft: false
tags:
  - Technology
  - Review
next: true
nomenu: false
---

**UPDATE: THE PUNKT. MP02 IS NO LONGER *DU JOUR* AND THERE WILL SOON BE A <mark>*NEW GENERATION*</mark> RELEASED. THE INFORMATION HERE IS ABOUT THE ORIGINAL 2019 MODEL.**

In April 2021, I wrote [a quick review of a telephone](/blog/2021/04/punkt-mp02-review/), the Punkt. MP02. This is an update to that review.

## What's new...

Since my last post on this device, an update has been pushed. I didn't really bother to look under the hood until today to see what's going on.

### ...under the hood?

In April, when I checked the number of installed packages, there were 94 installed (not counting the Signal clone, Pigeon). I made a point of noting that:

> As we could have guessed, it is close-to-stock Android with the necessary Qualcomm bits, a couple Punkt. apps, a single BlackBerry app, and some "who knows?"

Punkt. has since debloated the device, reducing the number of installed packages to 79.

```
android.ext.services (ExtServices.apk)
android.ext.shared (ExtShared.apk)
ch.punkt.mp02.appmgr (AppManager.apk)
ch.punkt.mp02.calendar (PunktCalendar.apk)
ch.punkt.mp02.personalise (base.apk)
com.android.backupconfirm (BackupRestoreConfirmation.apk)
com.android.bluetooth (Bluetooth.apk)
com.android.bluetoothmidiservice (BluetoothMidiService.apk)
com.android.calculator2 (ExactCalculator.apk)
com.android.carrierconfig (CarrierConfig.apk)
com.android.carrierdefaultapp (CarrierDefaultApp.apk)
com.android.cellbroadcastreceiver (CellBroadcastReceiver.apk)
com.android.certinstaller (CertInstaller.apk)
com.android.contacts (SimcomContacts.apk)
com.android.cts.ctsshim (CtsShimPrebuilt.apk)
com.android.cts.priv.ctsshim (CtsShimPrivPrebuilt.apk)
com.android.defcontainer (DefaultContainerService.apk)
com.android.deskclock (DeskClock.apk)
com.android.dialer (SimDialer.apk)
com.android.documentsui (DocumentsUI.apk)
com.android.emergency (EmergencyInfo.apk)
com.android.externalstorage (ExternalStorageProvider.apk)
com.android.gallery3d (Gallery2.apk)
com.android.htmlviewer (HTMLViewer.apk)
com.android.inputdevices (InputDevices.apk)
com.android.keychain (KeyChain.apk)
com.android.launcher3 (SimLauncher.apk)
com.android.location.fused (FusedLocation.apk)
com.android.managedprovisioning (ManagedProvisioning.apk)
com.android.mms (Mms.apk)
com.android.mms.service (MmsService.apk)
com.android.mtp (MtpDocumentsProvider.apk)
com.android.music (Music.apk)
com.android.onetimeinitializer (OneTimeInitializer.apk)
com.android.packageinstaller (PackageInstaller.apk)
com.android.pacprocessor (PacProcessor.apk)
com.android.phone (TeleService.apk)
com.android.providers.blockednumber (BlockedNumberProvider.apk)
com.android.providers.contacts (ContactsProvider.apk)
com.android.providers.downloads (DownloadProvider.apk)
com.android.providers.media (MediaProvider.apk)
com.android.providers.settings (SettingsProvider.apk)
com.android.providers.telephony (TelephonyProvider.apk)
com.android.providers.userdictionary (UserDictionaryProvider.apk)
com.android.provision (Provision.apk)
com.android.proxyhandler (ProxyHandler.apk)
com.android.server.telecom (Telecom.apk)
com.android.settings (Settings.apk)
com.android.sharedstoragebackup (SharedStorageBackup.apk)
com.android.shell (Shell.apk)
com.android.sim (CIT.apk)
com.android.simnote (SimNote.apk)
com.android.statementservice (StatementService.apk)
com.android.stk (Stk.apk)
com.android.systemui (SystemUI.apk)
com.android.systemui.theme.dark (SysuiDarkThemeOverlay.apk)
com.android.webview (webview.apk)
com.blackberry.bide (BlackBerryBide.apk)
com.dsi.ant.server (AntHalService.apk)
com.dtinfo.tools (SystemFota.apk)
com.iqqijni.dv12key (Kika.apk)
com.qti.confuridialer (ConfURIDialer.apk)
com.qti.launcherunreadservice (LauncherUnreadService.apk)
com.qti.qualcomm.datastatusnotification (datastatusnotification.apk)
com.qualcomm.embms (embms.apk)
com.qualcomm.location (com.qualcomm.location.apk)
com.qualcomm.location.XT (xtra_t_app.apk)
com.qualcomm.qcrilmsgtunnel (qcrilmsgtunnel.apk)
com.qualcomm.qti.callfeaturessetting (CallFeaturesSetting.apk)
com.qualcomm.qti.ims (imssettings.apk)
com.qualcomm.qti.services.secureui (com.qualcomm.qti.services.secureui.apk)
com.qualcomm.qti.simsettings (SimSettings.apk)
com.qualcomm.qti.telephonyservice (QtiTelephonyService.apk)
com.qualcomm.qti.uceShimService (uceShimService.apk)
com.qualcomm.simcontacts (SimContacts.apk)
com.qualcomm.timeservice (TimeService.apk)
com.quicinc.cne.CNEService (CNEService.apk)
org.codeaurora.bluetooth (BluetoothExt.apk)
org.codeaurora.ims (ims.apk)
```

Gone are the packages that serve no purpose on a device like this: the call logger, the Wi-Fi login tool, the companion device manager, the android email client and MS Exchange package, calendar storage, quick search widget, the sound recorder, VPN tools, wallpaper tools, and the FM radio.

I guess my original comment vis-à-vis radio can be struck through as well:

> Include an **FM radio** app perhaps? There is about 12 GB of free space on the device, so it would be great to use it for something.

Oh, well.

All in all, this is a positive thing. Any device touting itself as something for minimalists should be working to lower the number of apps and packages installed on said device.

### ...my experience

The booting is still fast. The device is still easy to use.

**Calculator: Fixed**

My complaints about the calculator are no longer valid. It works as it should now.

Since April, my little phone has not gone unnoticed. My students and a few others, are not sure if I am joking when I tell them that it is my phone.

**New app: Personalization**

It allows you to add your name to the lock screen. I find it useful.

### ...Pigeon?

I gave up with Pigeon. The delays for a message were too long. The scrolling was not as effortless as it should be.

Once again, they are aware of the problems, and [there is a nice list of them available on their website](https://www.punkt.ch/en/support/software-release-notes/mp02/).

### The Bottommost line

<mark>**75% --- very good work overall, noticeable improvement has been made, keep up the good work!**</mark>

## Conclusion and Recommendations Revisited

What I said about the Punkt.MP02 still holds true. It is truly a device that I am happy to walk around with. My previous recommendations will now be reviewed: 

- [X] **Calculator** needs to be fixed
- [ ] **Notes** needs an export feature (maybe with Markdown support)
- [ ] Give the **Wi-Fi** another purpose (Internet radio? **Podcasts**? Some sort of management software for our computers?)
- [ ] ~~Include an **FM radio** app perhaps? There is about 12 GB of free space on the device, so it would be great to use it for something.~~ <mark>*The FM radio package was removed*</mark>
- [ ] **Increase the volume**
- [ ] ~~**Pigeon audio and video messages need volume control** and an easy way to activate the speaker~~ <mark>*I don't know if anything has changed here*</mark>
- [ ] Pigeon should have been called **Pigeon Beta** or Testing to avoid too many complaints
- [ ] Pigeon needs to work with the **desktop app** (I haven't tried, but comments on forums make it sound like it does not)
- [ ] Give us a **roadmap** for updates
- [ ] Give us **a place to submit issues** for the different apps --- even GitHub would be good. Or, give the community a blog.
- [ ] When the device reaches EOL, **open up the code** and let us root our devices.

Now that the calculator has been fixed, and I have used the device a while longer, I can add a few more recommendations:

- [ ] Contact editing still requires work
    - Adding or removing of prefixes (like Dr.)
    - More fields
- [ ] A few more notification sounds
- [ ] Make it easier to block numbers
