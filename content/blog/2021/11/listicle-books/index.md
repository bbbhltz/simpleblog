---
title: "An Annotated List of Science-Fiction Books"
subtitle: "A Listicle"
description: "A listicle of science-fiction books that I have enjoyed to date."
slug: listice-books
date: 2021-02-10
draft: false
author: bbbhltz
description: "A Sci-fi primer for dystopia."
tags:
  - Listicle
  - Books
next: true
nomenu: false
---

Where do innovators get those ideas? Sometimes it almost seems like a work of science-fiction. Sometimes that is the case. If you are in an entrepreneurial spirit and you are looking for inspiration, here are 10 of my favourites.

**“Brave New World” by Aldous Huxley (1931)**
* Themes: Concept of companies that have political sway, complicated entertainment machines that generate both harmless leisure and the high levels of consumption and production, power maintained through technological interventions.

**“Nineteen Eighty-Four” by George Orwell (1949)**
* Themes: Loss of freedom, use of surveillance technology.

**“The Shockwave Rider” by John Brunner (1975)**
* Themes: The Internet, everyone on a network.

**“True Names” by Vernor Vinge (1981)**
* Themes: Hackers, NSA, Privacy concerns, VR, social network, “true death" is what we call "doxxing" today.

**“Neuromancer” by William Gibson (1984)**
* Themes: More hackers, VR, augmentations, the word “Cyberspace”.

**“Snow Crash” by Neal Stephenson (1992)**
* Themes: Private organisations with lots of power, mandatory reading for Microsoft and MIT, modern online video games (the creator of "Fortnite" was influenced by this book), MMORPG, social networks, Second Life, etc.
* Stephenson has worked for NASA, Amazon and Magic Leap. All of his books are excellent.

**"Daemon" [Series] by Daniel Suarez (2006,2010)**
* Themes: Artificial Intelligence runs amock.

**"Nexus" [Series] by Ramez Naam (2012, 2013, 2015)**
* Themes: Nano technology used to link humans (BCI).

**"Children of the New World" by Alexander Weinstein (2016)**
* Themes: Social Media, VR, robotics, dependance on technology.

**"Cumulus" by Eliot Peper (2016)**
* Themes: Tech giant (Social Network) with too much power.
