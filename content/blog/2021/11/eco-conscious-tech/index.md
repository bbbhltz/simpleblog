---
title: "The Eco-Conscious Tech Sham"
subtitle: "Greenwashing, e-waste, and icebergs"
description: "A blog post complaining about greenwashing and how some companies that appear eco-conscious are deceiving us."
featured_image: /blog/2021/11/anglerfish.jpg
date: 2021-11-20
lastmod: 2024-04-14
draft: false
tags:
    - Ecology
    - Technology
next: true
nomenu: false
---

It is our responsibility, as humans, as earthlings, to do as much as we can to prevent disaster. Companies have been reminding us of that fact for around 50 years now. They claim to hold up one end of the bargain, and have grafted the buzzword du jour, *sustainable*, to every concept of their business. We must hold up our end, and do our part, for example, by monitoring our carbon footprints and controlling our consumer habits.

## The tip of the iceberg

![Values of the top 20 technology brands worldwide in 2021](statistic_valuable-technology-brands-2021.webp "Values of the top 20 technology brands worldwide in 2021 (billion USD) via Statista")

Jutting out of the ocean of tech is the tip of an iceberg. It is a majestic thing. *Get your camera, take a picture, when will you get the chance to see one again, right?* Most of us learn at some point that little factoid about how we can only see 10% of the iceberg, so we keep our distance and marvel at these companies.

Apple, a company started in a garage. Google, two guys with an idea to organise information. Microsoft, another garage. Facebook, a bitter college student. Amazing. They grew and grew. Now, they have so many eyes on them, they must make an effort to appease their loyal clients.

They have given us the things we want. Sometimes, they even do it for free with their search engines and social networks. They exchanged goods and services, and reap the profit. Profit is one of several bottom lines in today's world. There are also people and a planet involved. Customers must remain loyal, and employees must not leave. The planet should be taken care of as well. Corporate social responsibility, a not-so-new notion, has been a bane and a boon for these behemoths of the tech world.

So, what's a company to do when they are told to clean up their act? Make promises, of course, answer to the shareholders, try harder, do better. They all have a plan, those companies. Most of them have come up with the same solution:

* Be carbon neutral
* Use sustainable energy
* Recycle something
* Invest in carbon removal
* Reduce water usage, save trees
* Make products last longer
* Reduce e-waste
* Be part of the circular economy
* Fight climate change at every turn
* Use ethical labour and ethical sources

We could go on. All of those are mentioned somewhere on [Apple's Environment page](https://www.apple.com/environment/). We know similar promises can be found on Google's corporate page, and Microsoft's, and the other company sites as well. They have the financial flexibility to do these things, and it is in their interest to be transparent. Can we trust them? That's not the focus here. Opinions vary on the subject, and we know that shareholders can make things happen, [like they did with Apple very recently](https://www.theverge.com/2021/11/17/22787336/apple-right-to-repair-self-service-diy-reason-microsoft).

For the sake of this post, we will give them the benefit of the doubt. We will say they are, at least, doing everything they can, and at most, honestly giving it their best. The rest is up to us.

## What lies below

In 1971, we were given the gift of "[The Crying Indian](https://youtube.com/watch?v=j7OHG7tHrNM)." An ad, brought to us by Keep America Beautiful, a non-profit, told us that, “Some people have a deep abiding respect for the natural beauty that was once this country. And some people don't.” We were left with a call to action, <mark>**People start pollution. People can stop it.</mark>**

Pure genius. Pollution is not the fault of the producer, but rather that of the consumer. Problem solved, for both parties. In an alternate reality, that would have been the end of it, but as Finis Dunaway tells us in *Seeing Green*:

>[KAB was founded] in 1951 by the American Can Company and the Owens-Illinois Glass Company, a corporate roster that later included the likes of Coca-Cola and the Dixie Cup Company, KAB gained the support of the Advertising Council, the nation's pre-eminent public service advertising organization.[^kab]

Luckily, for the companies, that sort of news doesn't stir up consumers as much as a new flavour of some drink, or the newest flagship device from Apple or Samsung. In fact, the play-book was passed around. Such a deceptive PR stunt cannot be wasted, no, it must be reused and recycled.

In 2004, the good folks at **BP** (the company formerly known as **The British Petroleum Company**) decided it was time for *us* to go on a low-carbon diet, and began using the term "carbon footprint", and gave us the first "carbon footprint calculator."

In 2021, this is laughable. We are too aware now, just slightly less naïve, to fall for that garbage. In 2004, those who saw through the deception didn't have the platforms we have today. And so, once again, the blame was placed on the consumer.

> BP wants you to accept responsibility for the globally disrupted climate. Just like beverage industrialists wanted people to feel bad about the amassing pollution created by their plastics and cans, or more sinisterly, tobacco companies blamed smokers for becoming addicted to addictive carcinogenic products. We've seen this manipulative play-book before, and BP played it well.[^sham]

<!-- We have now given enough of our earnings to the aforementioned tech companies, and their shareholders and investors, that they are able to lean towards being green. Some of them can sleep well at night because, if anyone raises their voice, they can clear their throats, lean into the mic and say, "renewable this... solar that... something something clean..." Others are still troubled. -->

![Barriers to sustainable investing bar chart](statistic_barriers-to-sustainable-investing.webp "Main barriers to sustainable investing... via Statista")

For the consumer, at least a mildly eco-conscious one, sleep deprivation might be setting in.

>If you've ever tried doing your own taxes, think back to that convoluted task and multiply it by 10. That is carbon accounting, a practice so mind-bogglingly complex that it is hard to imagine anyone but the Big Four consultancy firms, or cloud giants like Salesforce Inc. and SAP SE, doing it properly.[^account]

We have been beguiled by 50 years of propaganda, targeted at the average consumer, leading us to believe that we are the problem. We get a gadget, they get money; that's totally fair. And two years later, they release a gadget again. If we take the argument made by Keep America Beautiful and British Petrol, it means (in a way) that by buying the new device, we are dropping the ball. Our end of the deal is not being upheld. They are doing green things, and we just created waste. Why couldn't we wait five years? Why couldn't we buy a reconditioned phone if we wanted something new? Why are we destroying the planet?

## The twilight zone

This dive is getting a little deep, we are well below the iceberg. The ocean twilight zone is full of curious creatures. The world of tech as well.

There are a few beacons of light. There are companies giving us options. [Fairphone](https://www.fairphone.com/en/) is an example. They create phones that are meant to be repaired. There is also the e-foundation, creators of /e/OS, [who sell some refurbished phones](https://e.foundation/). There are also the countless individuals who make the conscious efforts to keep their devices as long as possible, buy refurbished or used devices, make smart decisions, and use their tech in a *humane* way.

A little can go a long way. A dim light in a dark ocean can draw an audience. The right company, the right personality, the right moment can make waves.

And then there is this asshole[^fish]:

![Anglerfish, sea devil](anglerfish.webp "Photograph By Norbert Wu, Minden Pictures/NAT GEO Image Collection")

That ugly SOB is an anglerfish. They have a little light they use to draw in prey. They are cool, and different, and they can't be blamed for doing what they do. They didn't want to be like that. Still, though, they exist.

There are some anglerfish in the world of tech too. And if you have made it down this deep, you will have guessed that it must be companies that have found a niche market in the world of tech in the same way the anglerfish has evolved to hunt in a niche way.

## The niche

The landing page has all the hallmarks.

* [Alegria](https://knowyourmeme.com/memes/subcultures/corporate-art-style)-styled corporate art
* Minimalist design with white space
* Some links to social platforms
* An about us page with a "philosophy" and "ethics" section

They tell us that the "understand that what [they] do and the decisions [they] make affect many people around the world," and their "products are built to last" because they want to "lessen their impact on the environment and take a stand against the over-production of rapidly obsolete consumer goods."

Punkt. anglerfished their consumers this week. Punkt. had them wrapped around their finger because they were naïve. Without warning, Punkt. informed their followers that The MP02 New Generation 4G voicephone would soon be available for pre-orders. The OG MP02 was released in 2019, just two years ago. Now, along comes the New Generation?

The [New Generation](https://www.punkt.ch/en/products/mp02-4g-mobile-phone/#overview) is very nearly the same phone, with a different OS, a little more power, a €350 price tag, [minimised packaging leading to a 40% reduction in transportation CO<sub>2</sub>, and no charger in order to reduce e-waste.](https://twitter.com/punkt/status/1461681671946838017)

No, the older version will not be updated.

This tiny example, from a tiny manufacturer of a tiny phone, shows us that no matter how big you are, no matter how good you want to be, you can make a bad decision. Punkt. made a device that had its flaws. Instead of finding a solution, they made a new one. They claim they are reducing e-waste, by not bundling a charger, but will create e-waste if some of their customers decide to buy the new version. The older version will not longer receive updates, so informed consumers will probably not buy it.

They have created a scenario where they *technically* did nothing wrong, and where the blame can be placed on us, the consumers.

## Resurfacing

Apple does it. Samsung does it. Any tech company that makes a new version and abandons the old does it. The company can maintain their eco-consciousness at the end of the day, we have to make a decision that could hurt the planet.

The PR campaigns, the empty words, are what make this a sham. The companies know that we cannot resist a new gadget. That is why they don't do what needs to be done:

**Make things that last a least 5 years!**

That is all there is to it.

Make a device. Make it last. Make a new device, take back the old one, recycle it. **That is the circular economy**.

Stop making the consumer feel guilty because you made a gadget that is newer, better, and faster. You knew they would get rid of the old one. <mark>"*Ending is better than mending*."</mark> That's what Huxley was talking about in 1932.

The jig is up. It has been for a long time.

[^kab]: [An Excerpt from Seeing Green: The Use and Abuse of American Environmental Images by Finis Dunaway](https://press.uchicago.edu/books/excerpt/2015/Dunaway_Seeing_Green.html)

[^sham]: [The devious fossil fuel propaganda we all use](https://mashable.com/feature/carbon-footprint-pr-campaign-sham)

[^account]: [Commentary: Tech startups could close greenwashing loopholes - Portland Press Herald](https://www.pressherald.com/2021/11/09/commentary-tech-startups-could-close-greeenwashing-loopholes/)

[^fish]: https://www.nationalgeographic.com/animals/fish/facts/anglerfish
