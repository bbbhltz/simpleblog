---
title: "Using Markdown and Pandoc to make a simple invoice"
subtitle: "First attempt at making an invoice with Markdown. It won't save you any time, but it's a start!"
description: "How to use Pandoc and Markdown to make an invoice."
date: 2021-11-23T21:38:44+01:00
lastmod: 2024-10-13
draft: false
tags:
  - Pandoc
  - Markdown
  - LaTeX
next: true
nomenu: false
---

For the past few years I have had a reasonable solution for my invoicing needs: a spreadsheet. LibreOffice Calc, in my case, is really all you need. I only have to send a single invoice per month, 8 months of the year. You don't need a spreadsheet to do that math.

I had looked into using LaTeX or Markdown to make my invoice before, and decided it was not worth the time. I saw that there were lots of templates available, some of them even looked nice. And, again, I felt that it was too much hassle for something I can do in a spreadsheet.

Then I came across [The Plain Text Project](https://plaintextproject.online/index.html) by [Scott Nesbitt](https://scottnesbitt.net/). Inspired by all of the different links, I decided it was time to see what I had learned over the past few months.

*I have not learned LaTeX, so things may get a little bumpy. You will notice that a good portion of this document is made up of LaTeX commands.*

## Requirements for my invoice

I use my invoices for billing one of my employers, a school, as an *auto-entrepreneur* or *micro-entrepreneur*. It sounds a little cooler than it is, trust me.

The invoices that an auto-entrepreneur uses in France must contain certain information. The pictures and the Markdown that I will use in this example are in French, but you will get the idea.

* Addresses of the recipient and the sender
* Date
* Breakdown of the payment (name of good or service, dates, quantity, rate, total)
* Bank information (in a box)
* Some legalese
* An invoice number (in a box)

I wanted to present this information as follows:

* My contact information: Left column, top of page
* The company's contact information: Right column, top of page
* Invoice number and dates: Right column, top of page, above recipient address
* Breakdown below columns
* Legalese below breakdown
* Vertical fill until bank information
* Bank information
* Small footer in a box containing invoice number and a page number (this might be important if I ever make a two-page invoice)

## Invoice Frontmatter

The YAML frontmatter for Pandoc is fairly complete ([see manual](https://pandoc.org/MANUAL.html)), combined with the fact that some LaTeX can be incorporated, a basic invoice should be easy enough (it also helps to be able to search for things on the Internet, because I wouldn't have had the patience to guess my way through some of this).

```yaml
---
lang: fr-FR
geometry:
    - margin=1.5cm
documentclass: letter
mainfont: FreeSans
monofont: Hack
fontsize: 12pt
header-includes:
    - \newcommand{\facture}{2021-001}
    - \usepackage{multicol}
    - \usepackage{tcolorbox}
    - \newcommand{\hideFromPandoc}[1]{#1}
    - \hideFromPandoc{
        \let\Begin\begin
        \let\End\end
      }
---
```

From top to bottom:

* Sets document language to French (use `en-GB` for English)
* Sets the margins to 1.5 cm
* Sets the type of document to "letter"
* Sets a font
* Sets another font
* Adds some LaTeX commands to the header during conversion
    - creates a new command called "facture," which means "invoice," changing that number changes it throughout the document
    - lets Pandoc use the "multicol" package for columns
    - lets Pandoc use the "tcolorbox" package for the grey boxes
    - makes a minor modification (I don't know if I actually need this?)

It is possible that you will need to install something to make all of this work. On Debian, I haven't found myself unable to perform conversions, but it is possible that having installed many of the `texlive` packages helps with that.

## Creating two columns

We will use the `multicol` package to acheive this.

```latex
\Begin{multicols}{2}

Left column content

\columnbreak

Right columns content

\End{multicols}

```

To get everything lined up nicely, in the vertical sense, we will need an extra command or two.

By calling the `\vfill\null` command between the last line of the address and `\columnbreak` we can avoid a tiny issue: without that function, the second paragraph of the contact information, if there is any, will align with the bottom of the column. I don't want that. So, the columns now look like this (again, sorry for the French, I might make an English version later):

```latex
\Begin{multicols}{2}

**BBBHLTZ** \
**SIREN** : 987654321

321 rue Une \
99000 CITY \
Tél : 06.06.99.99.99\
bbbhltz@mailbox.org

\vfill\null

\columnbreak

\Begin{tcolorbox}

\Begin{center}

**Facture Client**

**n°** \textbf{\facture{}}

05/12/2021

\End{center}

\End{tcolorbox}

\Begin{center}

**novembre 2021** \
01/11/2021 → 30/11/2021

\End{center}

**COMPANY** \
123 rue Deux \
99000 City

\End{multicols}
```

You will also notice that I used `\tcolorbox`. That just adds a grey box around that section.

## The table

Using Markdown, we can make a five-column table showing the required information. The first column will be aligned left, the others right.

The titles can be bold, and before the total we can add a blank line.

```md
| **Désignation**                  |      **Dates** | **Qté** |   **PU HT** | **TOTAL HT** |
| :------------------------------- | -------------: | ------: | ----------: | -----------: |
| Class or Lesson                  | 24/11/2021     | 3,5     | 46,00 €     | 161,00 €     |
|                                  | 25/11/2021     | 1,5     | 46,00 €     |  69,00 €     |
|                                  |                |         |             |              |
|                                  |                |         | **TOTAL HT**| **230,00 €** |
```

## Some small print

It is a requirement that my invoice contain a sentence about taxes, late fees, and method of payment. I have decided to put these all in small print, but they look fine at normal size as well.

```latex
\Begin{center}

\tiny TVA non applicable, art. 293 B du CGI ◦
En cas de retard de paiement, indemnité forfaitaire légale pour frais de recouvrement : 40,00 € ◦
Mode de paiement : Virement

\End{center}
```

The only thing left to do is the bank account information. Before we do that, we need to tell the document to put it at the bottom of the page using `\vfill`.

## Bank information

The last bit is also in a coloured box, and I wanted it to be monospaced as well. No explanation required here.

~~~latex
**Coordonnées bancaires**

\Begin{tcolorbox}

```
Bénéficiaire MR BBBHLTZ
RIB          NUMBERS BANK
IBAN         NUMBERS
BIC          NUMBERS
```

\End{tcolorbox}

\pagestyle{empty}
~~~

To finish things off, I tell the document to have no page number with the `\pagestyle{empty}` command. If my invoice is ever over one page, I can change that easily.

If all goes well, a quick `pandoc --pdf-engine=xelatex invoice.md -o invoice.pdf` so give us something like this:

![Invoice, autoentrepreneur, pandoc, markdown](Invoice-1.webp "A simple invoice, made with Pandoc and Markdown")

You can download the Markdown file [here](bbbhltz-2021-001.md).

## Links

* [Pandoc](https://pandoc.org/index.html)
* [Markdown](https://daringfireball.net/projects/markdown/)
