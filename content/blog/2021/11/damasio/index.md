---
title: "The Brutal New Worlds of Alain Damasio"
subtitle: "The French author who sees the future. Spoiler: it isn't awesome."
description: "A brief look at the science-fiction of French author Alain Damasio."
slug: damasio
date: 2021-11-13
lastmod: 2024-10-13
draft: false
author: bbbhltz
tags:
  - Books
next: true
nomenu: false
---

{{< box warning >}}
I wrote this before knowing what I know now about the author. I will continue to read their work---because I like science-fiction, not because I agree with their views.
{{< /box >}}

Dystopia in literature is common. The worlds of Huxley and Orwell, when first described, seemed so far away. Today we read them and find ourselves surprised, shocked and even disgusted by how similar these fictions are to our reality. Companies with immense power; governance and leadership leveraging technology in unethical manners; and our social lives falling apart are common themes in many dystopic fictions.

Alain Damasio is a French author. While he may not be known throughout the world, he clearly falls into the same category as our dear Huxley and Orwell. Over the course of 20 years, he has published three novels, two of which we will look at. Not wanting to spoil anything in the event that you decide to procure a copy, or find a translation, I will do my best to keep the details of the actual stories limited and focus on specific details.

*La Zone du Dehors* was published in 1999, and *Les Furtifs* in 2019. The former is a story that takes place on a moon, the latter in France. Together, they describe things that have come to pass and things that have yet to pass. We can therefore look at these works as a diptych in which the themes of totalitarianism, social-democracy, mass surveillance, social hierarchy, freedom, multinational companies, technology and control (to name a few) play a major part of the story.

## Things that have come to pass

![Cover: La Zone du Dehors](cover_zone.webp "La Zone du Dehors")

*La Zone du Dehors* was written in 1999, before the arrival of social networks, before society began worrying deeply about mass surveillance, and certainly before terms like "social credit" became part of our working vocabulary. This work of fiction can be considered rather prescient today for different reasons. The major theme throughout is tracking and tracing: all citizens are tracked. Their actions, their interactions, their associations are all tracked and can be used against them.

### Creating a Social Hierarchy

Much like in Huxley's *Brave New World* and Orwell's *1984*, there is a social hierarchy in place in Damasio's *Zone* (a place called Cerclon). This hierarchy not only decides how you circulate and where you can go, but it also decides your name. The "president" is, of course, named "A" and the world is controlled by this person and the other single-letters. The two-letters have a little less power, and it goes down to the 5-letters. The protagonist is "Captp" and he associates with other 5-letters like "Kamio" and "Bdcht". 

Your name is therefore your social ranking.

It wouldn't be fair, however, if you were born a 5-letter and had no chance of social mobility, would it? So, there is a system in place. A voting system. The citizens rank each other. There are votes or elections that take place and your name, which is your rank, is corrected (Damasio uses the portmanteau of *class* and *caste* for this vote: the *Clastre*). You might move up, or down. It is others who decide. Like something from *Black Mirror*, only in real life. Like being an influencer, but being treated as special in real life because random people decided you were special.

Obviously, there are similarities here to a certain social credit system we can see in the east. I have not been to that particular country, so I won't attempt to draw parallels. We can all agree, though, that a system that rewards someone for reporting a social faux-pas does exist today, and in that system your circulation can be limited according to your social credit. It is also the basis for our social networks, which many of us consider part of ourselves. Reddit has upvoting and downvoting and a karma system, and Instagram and Facebook use different algorithms to decide who sees what first. This virtual karma-point system has spilled over into real life, where an action, whether recent or not, can result in "cancellation" and job loss when the hordes of the Internet and mass media decide to wage war, and the companies that employ those individuals fear loss of profit.

This concept has become a reality since publication. While the majority of people would never let a system like this dictate who they can be friends with (good for them!) there are some people who do. Since 1999 we have been witness to hate against groups and minorities. Thanks to (and I say that positively) the rise in access to the Internet, we have been made aware of this hate that we once thought rare, or something our grandparents' generation had done away with. We can also look at the younger generation and how they treat each other in public based on style or the brand of smartphone they own (look it up, there are kids bullied at school because they have Android instead of Apple).

(Another real example is the recent arrival of [immunity passports](https://en.wikipedia.org/wiki/Immunity_passport#EU_Digital_COVID_Certificate) for COVID-19. In Damasio's world, the concept is put forward as a method for controlling circulation: certain citizens have access to certain places that others do not. There is a slight difference with Damasio's idea, but the parallel remains: there is a group that *can* and another that *cannot*. I will come back to this idea below.)

Finally, on a larger scale, gentrification has forced some families to live in areas with access to less adequate education and therefore less access to higher paying jobs. Social mobility has been reduced in such a way that it took a pandemic to highlight this phenomenon in a clear and understandable way: think of the students who were unable to attend online classes because they did not have access to Internet or a computer in their homes.

### The Place of Technology

Glorious technology! Praise be to its creators!

* **Steve Jobs**, a college dropout that lied to his best friend, never programmed anything, and did not know how to build a computer (correct me if I am wrong) is seen as our saviour. Besmirching his name is a sin, blessed be his name, and all hail the iPhone.
* **Mark Zuckerberg**, who studied computer science and psychology, created a website to rank students on their appearance and grew into the largest social network, Facebook. This network collects and sells data (through brokers) that can be used by companies and governments to leverage power, and also spread lies. He is power, he is fear. He sat in front of congress and was treated as a peer. He is untouchable.
* **Jeff Bezos**, the richest person in the world (depending on the day of the week), worked in fintech and a hedge fund before borrowing money from his parents and banks to start a company that did not see profit for years. He pays his workers low wages and expects them to never take a break. Amazon kills local businesses and hurts the planet. He "got big fast" by investing in infrastructure, some of which is used today as a service by other companies. Beware Bezos, his blood runs cold.
* **Elon Musk** is not really a self-made man, and did not really found Tesla Motors. Our image of him is the new cool guy that smokes blunts, but he has been on the scene since the 1990s. Is he a genius? Is it all marketing? Is he just in it for the money? Approach with caution; do not follow on Twitter.

In the time since *La Zone du Dehors* was published, technology companies have become the new oil. A successful piece of technology or software can launch a company or individual to newfound glory and privilege. Amazon's Alexa is a key example, as is Instagram.

In this story, many citizens have a small black, voice-activated, box in their homes. Sometimes it doesn't understand what you say, requiring you to yell at it (sounds credible today, doesn't it?) in order to turn on the television, make a phone call, or read the news. The generalization of voice commands, to paraphrase the author, is both "magical" and "insidious":

> It had taken away the once common relationship with objects, the hand-to-hand contact, however stupid, with the machine, the buttons, the keyboard that we hammered away on... We didn't touch anything any more: we spoke, and the world activated. As such, we all saw ourselves as gods --- or as police [...] The worst is that we developed a taste for it[.] [...] It had become a common habit: command without resistance...

Another gadget found in many homes is the amazing *Mirécran*, or Mirror-screen. Today, we have all heard at least one time the term "body dysmorphia". In 1999, maybe not as much. Damasio foresaw something. Here is the elevator pitch for this remarkable device:

> You have a mirror, the most normal of things. In fact, it is a vertical screen with a series of cameras behind it. When you look in the mirror, the cameras turn on and record your face from various angles. The recordings are filtered and retouched by a computer --- often erasing wrinkles, highlighting eye colour or lip contours... The image is retouched and displayed on the screen. This happens in real time, to give you the impression that it is your face in the reflection...when in fact it is a video. People buy this, you know! Have a crooked nose, pimples, wrinkles? Not a problem with the Mirrorscreen. "Mirrorscreen, the mirror that lies". You laugh, but that is their slogan! I'm not making any of this up! Risky, isn't it? And it works, very well I might add, people love it: **they see themselves exactly how they want to be seen**.

How is that not a description of Instagram, Snapchat and any other app that lets you filter and modify your appearance? That would have seemed cute and innocuous in 1999, but now we know what happens when people have access to this technology: they try to get plastic surgery to look like their fictional selves, and the celebrities sue to have unflattering images removed from circulation.

*La Zone du Dehors* also gave us a template for the connected shopping experience, where your eyes are tracked and everything is automatically charged to your account. Shoppers do not look at each other or speak because they are engrossed in their connected experience. While this hasn't happened as described in the book, the other was right about many things. Eye tracking has been used and shopping has been optimized in ways that we are unaware of. We are not going to get to a point where we wear special glasses for shopping, or a little robot arm to pick and choose for us, but we are at a point where we never take off our headphones and keep one hand free for our phone --- this has removed any social aspect from shopping. It has also made the gesture of taking off/out your headphones at the cash the modern day equivalent of tipping your hat to be polite. We also have this now:

{{< invidious IMPbKVb8y8s "Inside Amazon's Smart Warehouse">}}

We know that devices like Alexa, that record us non-stop, and software like Instagram, which can lead to serious social and behavioural problems, are negative. We also are aware that this is a much larger concern with technology: it can affect entire populations.

### Leveraging Technology for Power

"Information Economy" is not a new term, but it was still closer to fiction in 1999 than it is today. In the world of *La Zone du Dehors* freedom and power are controlled through technology.

> "[...] I'm not talking about controlling ideas, gentle propaganda or the idealogical models that we maintain --- what government doesn't do that? I'm talking about a more subtle and powerful control, one that doesn't simply cover you from the outside like a straightjacket, but one that comes from you, the root, to purify it. An internal control, intimate, *in petto*, that acts on the primary emotional centres: fear, aggression, desire, love, pleasure, unease..."

This is the play-book for politicians today. Use social networks to control citizens. Not directly, but emotionally. The information that advertisers and politicians can have access to from Facebook, to name one, has been used to *"control the affective components of a motive to action"*. Marketers use techniques that governments have since used to achieve what one character in the book calls the "peak of power": ***An optimum alienation under the appearance of total freedom.***

Television is a notable tool in this undertaking, described in the book as something that "conforms more than it informs" allowing them to control us. Them? Who are they? We always say, "they control us," and this story gives us a possible answer:

> "Who are they?" \
> "You." \
> "Me?" \
> "You all, me, us. All citizens." \
> [...] \
> "Because some people or groups, due to their strategic position in the social network, boost this control more easily than others. They also suffer from it."

Damasio did not foresee the social networks that we currently have. He imagined television personalities as having that influencer power. Oprah-level power.

Damasio's *La Zone du Dehors* sounds like a call to action when read today. We are getting to the point where more people are asking questions about their private information because we learned the hard way what answering a quiz on Facebook could lead to.

Technology never stops. Things improve, accelerate, ameliorate. Since 1999, a large chunk of the world has gone from no Internet or low-bandwidth Internet, to high-speed Internet, to a wireless connected world. We now willingly carry tracking devices in our pockets. We share health data with companies, so they can feed us ads. There are numerous wireless protocols, drones, cameras, listening devices and other connected gadgets deployed around our towns and cities. What's next?

## Things that have yet to pass

![Cover: Les Furtifs](cover_furtifs.webp "Les Furtifs")

20 years after *La Zone du Dehors*, Damasio published *Les Furtifs* ("Stealthies", in English). While not as blatantly tech-focused as the former, this latter work carries the same message: Fighting against tracking is a worthy fight. This message is expressed through a story that takes place in France, rather than on a moon, just 20 years or so in the future. The themes of the previous work are magnified, with an emphasis added to the role of multinational corporations.

### The Role of MNCs

Damasio's vision of France in the future is one where entire cities belong to companies. The city of Orange belongs, accordingly, to Orange (a telecoms company). Paris belongs to LVMH, Cannes to *Dysflix* (Disney+Netflix). Outside of France, the same phenomenon: Brussels belongs to Alphabet, and is known as *AlphaBrux*. The role of the Multinational is that of control. Much like in Huxley's vision, the MNC has "freed" the population by controlling them.

> [Orange] was born the 7<sup>th</sup> of December 2021, when 70 protesters from the *Take Back* collective were crushed under 200 tonnes of rubble. And the 29 families who still lived in the tower, whom they were defending. It was born from the bankruptcy of a city strangled by banks, downgraded to a triple C by the international ranking agencies, and forced to borrow its budget at 18% interest rates; of a city declared in arrears in 2028, abandoned by the State and auctioned off in 2030 on the free city market.

Freed them from what? Bankruptcy. Bank loans. High interest rates. Take your pick. The cities were sold to the highest bidders. A "Free City" is free from the State, but wholly controlled by the company that purchased it. The mayor is chosen by shareholders. The cities then privatize services, and offer subscriptions in order to gain access. Certain streets are reserved for those who can afford.

The privatized cities are filled with AI-controlled taxis, drones keeping tabs on you, and panhandlers known as "vendiants", which is a French portmanteau of "vendeur" (seller) and "mendiant" (beggar) which I will call "sellhandlers". 

> The sellhandlers panhandle their sales. They don't even beg for themselves, like our old beggars: they beg for their brand, their product, their masters, for a platform floating in the cloud where they will never meet a single manager or see, except maybe on a phone, the start of a head of sales.

Most citizens wear a ring, which contains their profile, allowing the sensors to know their preferences. Without the ring, you will be solicited non-stop. Progress doesn't stop, however, and a newer and better---and more expensive---ring is always around the corner.

> As soon as you go out in the street, in any privatized city, you are systematically hit by three waves: *taxiles*, *sellhandlers*, and drones. [...] \
> Without a ring, you have no ID according to the sensors, the network. No profile, no preferences, no possible personalization with regard to solicitation or being left alone.

### The "Technococoon" and Controlling Citizens

Politicians maintain control through scrupulous and calibrated techniques. None of them are novel, but rather enhanced by technology. Different segments; different words; different promises. *Affecting* and *fiction scripting*, provoking emotional responses and controlling the narrative, are leveraged to the point of being a weapon. As such, it is possible to manipulate the population into choosing the next leaders.

Such a feat would not be possible without mass surveillance, and tracking.

> Without tracking, control is not possible [...]. You know it. No sustainable security. And without tracking, our algorithms cannot personalize your experience in the city. **How can we give you the city you deserve if we don't know what you like?** If we don't know about your habits, your preferred routes, your tastes?

And so, the citizens of these cities wear rings and use apps--like *MOA* (My Own Assistant). The publishers behind *Les Furtifs*, *La Volte*, even took the time to show us what this is like:

{{< invidious d6FIk0L9QKc "MOA: My Own Assistant" >}}

Chilling, if it were not already underway today with our current selection of social networking applications.

The choice is yours, however, to wear the ring and use the application. Your choice, until it isn't any longer. You can be forced to wear the ring as punishment if you commit a crime, for example.

The citizens of this speculative future live in an "electric smog," or "technococoon," where they remain a *chrysalis*, and never become a "butterfly". They keep themselves there, living their lives in a second, "ultimate", reality where they feel they are the masters.

In the *ultimate reality*, an individual can choose the people and animals they see. The first reality, the banal one, cannot be shared like the *ultimate reality*. The second reality can be individualized to the point of being a product. Gadgets and applications were just the foot in the door, the *ultimate reality*, a sort of *metaverse*, was the real invasion.

### Loss of Freedom and Social Decay

Saying the words, "technology studying behaviour," however common it already is, sounds creepy. Saying, "technology knowing your tastes, desires, and anticipating needs," is also creepy, but can at least be useful. They are one and the same, obviously, and not in any way synonymous with "freedom."

It is easy to imagine a world where the majority of the population is, in one way or another, addicted to something like a phone or a social network. When Apple produces a new device, or microfiber cleaning rag, people line up like automatons getting a recharge. When I go to the park, many of the parents are on their phones. I would be too, if I had a smartphone, and if my son wasn't keeping me on my toes.

Let's bump it up a notch and give our kids addictive gadgets too. Like a virtual *Pokémon* or *Tamagotchi* that they can see, and let others see, through connected contact lenses.

> The ring, it's like a hand. Losing it, that's an amputation! \
> [...] \
> [The children] take their virtual toy everywhere, they see it everywhere, in the school yard, on the bus, in the cafeteria...

**Update: The idea of virtual *children* is being looked into. See the [Soul Machines](https://www.soulmachines.com/) website.**

Some parents in *Les Furtifs* go as far as naming their children after a company. Why? Royalties.

Huxley's *Brave New World* talked about this possible future as well. A future where we are given complicated, addictive, entertainment machines. The machines don't come cheap. You need to work for them, and the update, and the upgrade, and the next disruptive gadget. Technology keeps people working.

Between working to pay for new things, and the time spent using new things, we will lose something: the art of the encounter. Meeting new people. Socializing. Damasio's future is one where socializing is decaying, and he is opposed to that.

The need to socialize is not gone in his story, though. There are even virtual cabbies that chat to you about subjects of interest. Socializing, meeting, encountering, discovering new things, is the last thing keeping us alive.

## Conclusion

I've been working on this little write-up for a few months. A sentence here. A correction there. Over the past few weeks I've found myself motivated to complete it, but also distracted, by [the drama surrounding Facebook](https://arstechnica.com/gaming/2021/11/everyone-pitching-the-metaverse-has-a-different-idea-of-what-it-is/). The concept of a metaverse has been visited many times in science-fiction, most notably by Neal Stephenson in *Snow Crash* (1992), and is soon to be a real thing. Seoul announced they will soon "[join the metaverse](https://qz.com/2086353/seoul-is-developing-a-metaverse-government-platform/)":

> "Seoul's metropolitan government will develop its own metaverse platform by the end of 2022. By the time it is fully operational in 2026, it will host a variety of public functions including a virtual mayor's office, as well as spaces serving the business sector; a fintech incubator; and a public investment organization. \
> [...] \
> The future of the metaverse is being built almost entirely by companies. Microsoft, Nike, and Facebook's parent company Meta are all staking claims to digital real estate. South Korea is among the only governments attempting to recreate the virtual public square. But if they can, it could expand the utility of the metaverse to millions of citizens whom might otherwise be excluded."

Some of what Damasio envisioned in *La Zone du Dehors* has come to pass, and now it seems that the future imagined in *Les Furtifs* is on our doorstep. I am at odds. The cynical angel on one shoulder has thrown in the towel, stating that "we are all screwed" between the companies taking over, and the climate crisis. The optimistic angel, on the other shoulder, has his fingers crossed, hoping that we will snap out of it.

The optimist, much like [Cal Newport argues in The New Yorker](https://www.newyorker.com/culture/office-space/the-question-weve-stopped-asking-about-teen-agers-and-social-media?utm_brand=the-new-yorker&utm_social-type=earned), and [Ray Nelson in "Eight O'Clock in the Morning,"](https://pvto.weebly.com/uploads/9/1/5/0/91508780/eight_o%E2%80%99clock_in_the_morning-nelson.pdf) hopes that we will see things clearly and ask the right questions. There is another way. We do not need to give ourselves, and [our souls, to companies](https://www.bit-tech.net/news/gaming/pc/gamestation-we-own-your-soul/1/). We are allowed, on an individual and collective level, to say *no* and call *bullshit* when people in power begin to use powerful, complicated, technology to do unethical things.

A utopia isn't the answer, or a possibility for the moment, but let's not aim for a dystopia.

--- 

These books are published by **La Volte**, an independent publisher.

As far as I can tell, they have not been published in other languages. If you want to read them in a language other than French, you may have to wait.

You can find more information, in French, on Alain Damasio on [lavolte.net](https://lavolte.net/pages/auteurice/alain-damasio). You can also follow **La Volte** on [Mastodon](https://imaginair.es/@lavolte), [Telegram](https://t.me/lavolte) and [Peertube](https://video.lavolte.net/accounts/lavolte/videos).
