---
title: "Documentaries, Films, and Series About IT and its Influence on Humanity"
description: "A Film and Video Listicle about Science-Fiction."
slug: listicle-film-tv
date: 2021-02-11
lastmod: 2024-10-13
draft: false
author: bbbhltz
subtitle: "A primer for people interested in how IT affects us."
tags:
  - Listicle
  - Information Technology
next: true
nomenu: false
---

Below is a list of documentaries, films, and series that have a point in common: They all, in one way or another, address our relationship with technology. In some cases, the the link is not as direct or as obvious as in other cases, but the link is there.

Documentaries make up the better part of this list but there are also some fictional examples that make for decent watching. Regarding the series, you will notice some comedies that play on the tropes surrounding Silicon Valley and the people that work in IT.

For more information, you can look up these films and series on [TMDB](https://www.themoviedb.org) or [TheTVDB](https://www.thetvdb.com/).

## Documentaries

* **After Truth: Disinformation and the Cost of Fake News** (2020)
* **American Selfie: One Nation Shoots Itself** (2020)
* **Bitcoin Big Bang: The Unbelievable Story of Mark Karpeles** (2018)
* **Citizenfour** (2014)
* **Digits** (2016)
* **Don't F\*\*k with Cats: Hunting an Internet Killer** (2019)
* **The Great Hack** (2019)
* **HyperNormalisation** (2016)
* **The Internet's Own Boy: The Story of Aaron Swartz** (2014)
* **Kill Chain: The Cyber War on America's Elections** (2020)
* **Lo and Behold: Reveries of the Connected World** (2016)
* **Nothing to Hide** (2017) ([Available for free on Vimeo](https://vimeo.com/nothingtohide) and on [Peertube](https://peer.tube/videos/watch/d2a5ec78-5f85-4090-8ec5-dc1102e022ea))
* **The Perfect Weapon** (2020)
* **Terms and Conditions May Apply** (2013)
* **The Social Dilemma** (2020)
* **TPB AFK: The Pirate Bay - Away from Keyboard** (2013)
* **We Are Legion: The Story of the Hacktivists** (2012)

## Films

* **Ex Machina** (2015)

## Series

* **Betas** (2013)
* **Black Mirror** (2011)
* **Brave New World** (2020)
* **Devs** (2020)
* **Halt and Catch Fire** (2014)
* **The IT Crowd** (2006)
* **Mr. Robot** (2015)
* **Philip K. Dick's Electric Dreams** (2017)
* **Silicon Valley** (2014)

## Stand-alone Episodes

* **The Amazing World of Gumball - S06E38 - The Web** (2019)
