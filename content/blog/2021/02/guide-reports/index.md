---
title: "Tips for Students: Report Writing"
date: 2021-02-21
lastmod: 2024-10-13
slug: guide-reports
draft: false
author: bbbhltz
subtitle: "A primer for writing simple reports."
description: "How to write a short report using basic Word Processing functions."
tags:
  - Education
  - Guide
next: true
nomenu: false
---

## 1 Important Shortcuts (Windows)!

<kbd>CTRL+B</kbd> for **Bold**

<kbd>CTRL+U</kbd> for <u>Underlined</u>

<kbd>CTRL+I</kbd> for *Italic*

<kbd>CTRL+Z</kbd> for Undo

<kbd>CTRL+Y</kbd> for Redo

<kbd>CTRL+C</kbd> for Copy

<kbd>CTRL+X</kbd> for Cut

<kbd>CTRL+V</kbd> for Paste

<kbd>CTRL+SHIFT+V</kbd> for Paste Without Formatting

(This shortcut is maybe my most used keybinding. It is useful when
borrowing text from online sources because it will erase the formatting
and any hyperlinks too.)

<kbd>CTRL+F</kbd> for Find

<kbd>CTRL+W</kbd> to Close a Window

<kbd>CTRL+A</kbd> to Select all

<kbd>CTRL+RETURN</kbd> insert a Page Break

### 1.1 Navigating in the document can be done with just the keyboard as well.

Hold <kbd>SHIFT</kbd> and use the arrow keys to select text. Hold <kbd>SHIFT+CTRL</kbd> and you can select word-by-word. Hold <kbd>CTRL</kbd> and use the arrow keys to move around in the paragraph; <kbd>CTRL+UP</kbd>, for example, will take you to the start of the line. Of course, that would mean holding <kbd>CTRL+SHIFT</kbd> and using the <kbd>UP</kbd> and <kbd>DOWN</kbd> keys will let you select text line-by-line.

Now you are *almost* ready to type something. Just a few minor things to go over first.

## 2 Outline

You cannot write something like an internship report without an outline. You can begin the outline as soon as you begin your internship. It will help you keep track of important details, avoid repetition, and help with flow.

Buy a cheap paper journal that fits in your pocket so you can jot ideas on the go without the distraction of your phone.

## 3 Prepare Your Document Before You Start Typing

Open your favourite Word Processor. **Do not just start typing**. Do not worry about fonts, formatting, footers or any of that stuff yet. We are not ready. Several moments of patience will save you some time in the end. You are going to do three things before you start typing:

1.  Set up the page. That means set the margins if you need to.

![margins](margins.webp "Margins settings")

2.  Set up the page numbers, and **make sure that the first page does not have a number on it**. (This is in the basic options for footers and page numbering).

    -   *additionally, the preliminary pages should use Roman numerals, the body Arabic numbering*

3.  Set the header to a small logo of the school and the title of your report. Set the footer to the page number (see above for more on page numbers).

4.  Set the language for the document. This is important for spell-checking later.

![](language.webp "Language settings")

5.  Install **LanguageTool** (we will come back to this later).

Now your Word Processor is ready to go. It knows the language. We do not care about the fonts or the overall appearance yet.

Go ahead and type what you need to type. Remember, **no formatting required just yet**.

## 4 What to Write

Here is a list of *suggestions* for topics to include in your
report:

-   Title Page
    -   Give your report a title
    -   Include your name and internship information

-   Acknowledgement Page
    -   Include a quick thank-you (teacher, academic advisor, etc.)

-   Table of Contents Page
    -   Include the acknowledgements in the ToC
    -   Also include graphs and charts

-   Summary Page
    -   Include a brief summary of your internship. Keep this short and sweet.

-   The Body
    -   This is the main part of your report. Include sections like: Introduction, Description of the Internship, Organisation of the Company, Internship activities, Reflections on the Internship, Conclusion, etc.
    -   Each section of your report needs a title or "heading."
    -   Your introduction should expand upon your summary and include facts about your employer.
    -   You should describe what part of the organisation that you worked for.
    -   Your responsibilities should be detailed.
    -   Try and honestly answer the question: What did you learn at this internship?
    -   You can critique the organization you worked for, but be as fair and neutral as possible.
    -   Think about how you performed during the internship.

-   The Bibliography
    -   Include an APA/MLA/Chicago formatted bibliography (⇐ yes, formatting counts!).

-   The Appendix (a separate file)
    -   The appendix section is for journals, published works, photos, recordings, and any other supplemental material you have. The amount of material you have will differ depending on your internship duties. Try to include some material to give the reader a taste of your accomplishments during the internship.
    -   e.g., if you worked in communications, include press releases, ads, or letters you worked on.
    -   This is also where you might include an org chart.

## 5 Set Your Styles

Styles are your Headings, Subtitles, Normal Text and Titles for your document. Why would you bother with styles? Well, for starters, it saves you loads of time. For instance, let's say you want to change the font or size of all of your headings: if you use styles you can do that in one click. Or, what if you want to insert a table of contents in one click? Styles help you do that do.

![Styles](styles.webp "Styles settings")

(if you are unclear how to use styles, hit up YouTube for some [tutorials](https://www.youtube.com/results?search_query=using+styles+word))

Once you have typed everything that you need to type you can go about choosing fonts and setting up your Title, Subtitle, Headings, and Subheadings.

## 6 What NOT to do

-   Include lots of graphics: graphics and pictures of any variety make your document look trashy and immature.
    -   All visuals should be put in the Appendix
-   Use poor quality graphics: Do not "screenshot" something you can recreate using Excel (simple bar charts, org charts).
-   Attempt to cheat by changing your font size from 12 to 12.2 or slightly changing your margins. If your document is a page short, so be it.
-   Cheat by borrowing from a previous student: all of our documents are scanned using Urkund which compares your document to anything found on the web, including translations, as well as any document any student has ever submitted.
-   Use clipart, text ornaments or fancy fonts (They are lame!)
-   Format as you type: it will slow you down.
-   Wait until the last minute.
-   Forget to review, spellcheck, and review again!

## 7 Review and Review Again

Hooray! You have finished a draught of your internship report. Congratulations are in order. Now, save that file, turn off your computer, and go out for a little bit. You need a clear head for reviewing.

### 7.1 Use the Tools

The first thing you want to do is run your text through **LanguageTool** (do not use their website, download and install the standalone app or use Google Docs). Make sure you tell LanguageTool which version of English you are using (British, American, etc.) and launch the tool. LanguageTool will highlight possible spelling mistakes as well as grammar, style, and formatting errors (like spacing or punctuation mistakes).

After LanguageTool, you will want to use something like **Grammarly's Plagiarism Checker**. Using the free version will not tell you *where* the mistakes are, but it will tell you how many mistakes and if plagiarism is detected.

**UPDATE 27/07/2021: Thanks to Sarah for suggesting this list of other plagiarism checking websites --- [LINK](https://www.websiteplanet.com/blog/best-plagiarism-checking-tools/)**

### 7.2 Your Review

When you review a large document that you wrote yourself, you should
start at the end. Print your document (yes, bad for trees) and start
reviewing paragraph by paragraph starting at the end. Here are things
you are looking for:

-   Expressions that you do not understand

    -   Replace them with things you understand

-   Vocabulary that you do not understand

    -   Replace with words you do understand

-   Repetition

-   Punctuation errors

-   Capitalisation errors

-   Run-on sentences

    -   If it is too long it might be confusing

-   Paragraphs that are too short

    -   There is no official rule, but a three-sentence paragraph looks nice

-   Subheadings, Headings, Titles, Subtitles, Footnotes: does everything look the way you want it to?

-   When you get back to the start, read it out loud to yourself. If you get stuck there is probably a tiny problem

### 7.3 3<sup>rd</sup> Party Review

Have somebody read over your report. Tell them to check for mistakes but also to see if it all makes sense. Do not be afraid of being judged.

## 8 External Tools and Websites

Here are some useful (and free) tools:

**Deepl Translator**

*Slightly better than Google Translate*

[https://www.deepl.com/translator](https://www.deepl.com/translator)

**WordReference**

*Excellent online Dictionary*

[http://www.wordreference.com/](http://www.wordreference.com/)

**GrandDictionnare**

*For those complicated terms*

[http://www.granddictionnaire.com/](http://www.granddictionnaire.com/)

**LanguageTool**

*Style and Grammar Checker (there is a free stand-alone version for Windows and Mac along with versions for Google Docs and LibreOffice)*

[https://www.languagetool.org/](https://www.languagetool.org/)

**Grammarly's Plagiarism Checker**

*Will check your document for mistake and plagiarism (the free version will not tell you where they are, but it helps nonetheless)*

[https://www.grammarly.com/plagiarism-checker](https://www.grammarly.com/plagiarism-checker)

**UPDATE 27/07/2021: Thanks to Sarah for suggesting this list of other plagiarism checking websites --- [LINK](https://www.websiteplanet.com/blog/best-plagiarism-checking-tools/)**

## 9 Finalise Your Document

Before exporting your document to PDF, double-check everything. **We tend to forget the easy things**, like:

-   putting our names on the title page,
-   page numbers,
-   headers and footers,
-   formatting the bibliography,
-   making sure you understand everything in the report,
-   looking at the instructions one last time to make sure you did not forget anything.

**Save your document with a title**. Not just "report.docx" and export it to PDF.

Open the PDF file on your computer and your phone to make sure things look the way you want. Pictures and page breaks do not always work.
