---
title: "The Show Must Go On: Making Slides with Markdown and Pandoc"
slug: pandoc2021
date: 2021-03-04
lastmod: 2024-10-12
draft: false
author: bbbhltz
subtitle: "There is more to presentations than PowerPoint and Keynote."
description: "How to use Markdown and Pandoc to make Beamer Slides"
tags:
  - Pandoc
  - Markdown
  - Design
next: true
nomenu: false
---

## Notes on PowerPoint

This is not about being anti-Microsoft. I have nothing against them. They did something amazing: getting people to pay for an operating system at a time when the average person could not justify owning a personal computer. This is not really being about anti-PowerPoint. PowerPoint has been around since the late 80s for a reason: there was and still is a need for that software. What this is about is the fact that I have had it with PowerPoint.

I was definitely using PowerPoint before 2002, but for the sake of this blog let's say I became a heavy user around September 2002. This was because I started university at that time. All university students know (well, at least those at my university in Canada do) that if you are studying anything in the arts you will be giving a lot of presentations. By my count I made at least one PowerPoint slide show per week of university (minus, obviously, exam weeks, reading weeks, etc.). 

My PowerPoint skills grew and grew. I became aware of the deepest secrets of the apps. I willingly helped others, shared my knowledge, and worked with them to craft awesome slides. It was a passion, some might say. But, my skills reached their limits with a presentation for a French class about Raoul Duguay and L'Infonie. The PPT file was about 80MB. The sound files were embedded. The slides were timed to advance without any user input, which made the presentation of the song lyrics all the better. The colours were carefully selected. It was magnificent. I guesstimate that I spent about 5 hours putting just the slides together, not counting the research and practice. Who knows, maybe I put 10 hours into that monster. It didn't matter at the time. It was all about those grades and being a difficult act to follow.

That was the peak of my skills. It was around 2006.

Since then, I have stopped trying. It isn't worth it. I am talking about the medium, not the content of course.

## This is not a guide for students

Originally, I wanted to write a guide for students on making PowerPoints. In particular, I wanted to show students how to use Masters in order to make nice slides and save time. Other tips were about white space, using (or not using) bullets, different notions like staying under 25 words per slide. All that stuff we have been hearing for years. I am positive that someone would find it helpful, but lots of students would not bother.

The alternatives to PowerPoint today are varied. I myself have used the following software to make slides over the years:

* Microsoft PowerPoint
* OpenOffice
* LibreOffice
* Scribus
* Inkscape
* Prezi
* Google Slides
* Office365
* ...I feel like there is one missing...

## Why Bother With Beamer?

Why make slides, at all, is a better question. I love whiteboards. I like it when students take notes. Most of what I teach is very introductory and general. I know for a fact that using computer-based slides is less helpful for student learning than, say, writing on a board, or having a discussion in class. Alas, post-secondary education and slide shows are two things that go together and I must bow to the wishes of my directors and managers. Nearly every class I teach has a slide show to go along with it. That wouldn't be so bad except all of the classes that I teach are 3 hours long.

**3 hours is too long, but who will listen to the professors?**

I have a work-provided Windows laptop. It comes with PowerPoint. I could use that. I also have LibreOffice Impress, which does the trick too. Who in their right mind would even fathom recreating all of their slides in another format? Especially since I have already done it twice! Once, when I stopped using Dropbox and switched to Google Drive and remade all of my slides in that format (not as bad as it sounds). Then again when I quit Google and began the switch back to LibreOffice. I will answer the question with another question: What is important in a slide? The only answer is, the **content**. After the content comes a very short list of requirements.

Here are the things that I require in a slide show:

* Content
* Titles, sections, subsections
* Slide numbers
* Images
* Tables (rarely)
* Columns (rarely)
* Consistency & Simplicity

I do not need animations, boxes, arrows, embedded media or even emojis. My university has a template that is horrendously busy. I just want to be able to sit down, type a few sentences, press a button and have a slide show. This is possible with Beamer.

BUT...I don't know LaTeX.

There is, however, **Pandoc**. Pandoc is a nice little package that converts certain documents to other types of documents. One type that it works well with is **Markdown**. It is quite likely that you know what Markdown is, but in case you don't it will be used in an example later on (it is dead simple). So, once you know Markdown, you just have to type. Why bother with Beamer? Because, with Markdown and Pandoc, it is much easier.

## RTFM

Pandoc, Beamer, and Markdown have guides and manuals online. I would have saved lots of time had I read them thoroughly. It has taken me several weeks to come to a final decision on how to prepare my slides. I feel as though they are, at least, 96% acceptable for what I am doing. The rest would require me to learn how to make a custom theme and style which I am not going to do unless I come across a nice guide or reread the manual (probably too busy to do that).

## The Workflow

I have a 2-step workflow for creating my slides.

1. Prepare my content in a text editor or IDE (Geany works fine for me and was pre-installed so...why not?) in Markdown.
2. Build the slide by running a command (in Geany I can just press F8 and it does this for me).

I have shortened it to two steps by making a template file (workslides.md) in my `~/Templates` folder with some front matter.

## The Front Matter

There are a certain number of options a variables that can be passed to Pandoc from the Markdown file. After a good amount of head-scratching I have settled on including the information required to:

* have a title slide with a title, subtitle, my name, the year, my school name, and a graphic;
* set the colour, theme, and fonts.

I could also include the table of contents, but that is not always necessary. 13 lines of text (not code, just text) is sufficient. The rest is all content, which is the important part.

## The Content

Markdown is easy, so all content can be specified with the following markup:

```markdown
# Section Heading

## Sub-heading
 
 ### Block (sub-sub-heading)
 
 * list
 * items
 
 1. numbered 
 2. list
 3. items
 
 *italics*
 
 **bold**

![image caption](image source){ height=70% } % that bit sets the image to 70% of the vertical space available, 
    you can use whatever you want but you need those spaces within the braces!

[link text](link)

::::columns
:::column
column content
:::
:::column
column content
:::
::::

+-------+
| TABLE |
+=======+
| blah  |
+-------+
```

## Example

**I made a fake set of slides for this example. I didn't put a lot of effort in. Don't judge me.**

### The Title Page

![Front Matter vs Output](frontmatter.webp "A very busy title slide")


Above you can see that the front matter does some of the work for me by setting up a consistent theme for the rest of the slide show. There are different types of themes available and this was one of the details that lead me to remind anyone reading this to read the manual.

There are full themes (called `theme` in the front matter), inner (`innertheme`), and outer (`outertheme`) as well as the `colortheme` which can be full, inner, or outer. You can see the themes [here](https://mpetroff.net/files/beamer-theme-matrix/).

* Complete themes are (mostly) named after flying animals: albatross, beetle, dove, fly, monarca, seagull, wolverine, beaver, spruce.
* Inner themes are generally named after flowers: lily, orchid, rose.
* Outer themes are sea animals: whale, seahorse, dolphin.

There are also presentation themes that take care of most of those details. They are named after cities. After lots of fiddling I decided not to use one because I couldn't find a nice balance. All of this information can be found in the [Beamer User's Guide](https://tug.ctan.org/macros/latex/contrib/beamer/doc/beameruserguide.pdf).

In my example, the `colortheme: whale` option tells the slide to set the outer colours to blue, along with the block around the title. The `outertheme: infolines` option gives me the header and footer. The `innertheme: rectangles` does something not seen in this slide: putting little boxes as bullets for lists.

### Other Slides

![Slide with Wheeljack](transformers-6.webp "Slide with a picture and a list")

The slide above shows a combination of an image, a sub-sub-heading, and a list (with little rectangles triggered by `innertheme: rectangles`)

![Slide with Ravage](transformers-8.webp "Slide with a YouTube link")

This slide also has a picture and a link. Clearly, I cannot include a video in the slide show. I can put a link and a title card. You can get the title card of a YouTube video using the `youtube-dl --get-thumbnail` command (if you use youtube-dl). It doesn't look awful and if I am teaching I can choose to skip the video and do something else.

## Tricky Bits

Most of my slides don't need a lot more detail but I have run into a handful of annoying situations.

### CJK Fonts

If I want to write Coca-Cola in Chinese (which I have had to do) I cannot just copy-paste 可口可乐 and be done with it. I needed to add two lines to the front matter to tell Pandoc what to do:

```yaml
usepackage: xeCJK
CJKmainfont: Noto Sans CJK SC Regular
```

### Pseudo-Animations

Sometimes it is nice to have things appear one-by-one. That is easy. There is more than one way to do this, I do it by added a ` \pause` like this:

```latex
yadda \pause

yadda \pause

yadda
```

The slide will launch with one "yadda" and end with three.

Doing the animations bottom-to-top, though, can be a pain because you must do it with LaTex markup like this:

```latex
\uncover<3->{yadda}

\uncover<2->{yadda}

\uncover<1->{yadda}
```

### Some Extra Colour

Adding text to a colour is pretty easy, you can use the `\alert{text}` markup and it will be red. Or you can use `\textcolor{colour}{colourful text}` and choose a colour for yourself:

```latex
\alert{ALTERTED YADDA}

\textcolor{violet}{VIOLET YADDA}
```

### Bigger or Smaller Text

LaTex has some built-in sizes if you need theme. You can get more info on [this wiki](https://en.wikibooks.org/wiki/LaTeX/Fonts#Built-in_sizes):

```latex
\tiny{yadda}

\scriptsize{yadda}

\footnotesize{yadda}

\small{yadda}

\normalsize{yadda}

\large{yadda}

\Large{yadda}

\LARGE{yadda}

\huge{yadda}

\Huge{yadda}
```

## Finishing Up

Once the content is ready, it is time for the moment of truth: building that slide show.

I am lazy so I have been doing this is Geany. My final front matter has changed a little and I have added some things since we've started. I have also set a Build Command in Geany so I can press <kbd>F8</kbd> and the PDF is made for me without going into the command line. I usually leave the PDF open in Zathura so I can check for inconsistencies.

![Geany Build Command](finalbuild.webp "Geany Build Parameters")

Either way, when I am ready I run a quick `pandoc -t beamer --pdf-engine=xelatex -o output.pdf input.md` and I have myself presentation.

~~You can download [the Markdown file I have used here](example.md) and try it for yourself. Or, [download the PDF](Transformers.md.pdf).~~

## Limitations and Unanswered Questions

Obviously, there are a few limitations to this method. One being that you need to install Pandoc and some LaTex libraries on the computers that you will use for creating your presentations. Another is you cannot just copy and paste an image from a website like you would with Google Slides or LibreOffice Impress. Perhaps that is a good thing, it will force you to think about whether or not you need that image.

My next quest will be to figure out how to make my own themes and styles. **If you know how or have a nice, simple, guide, let me know**.

**<mark>UPDATE</mark>: [Using pandoc for My Slides: 2023 Update](/blog/2023/01/pandoc2023)**

## Conclusions

As I am someone who likes to find reasons to plop myself down in front of a screen, it was normal for me to become attracted to something like this. It is neither too basic nor too complicated. I like markup languages, and a simple one like Markdown is just right for me. This process has lead me to be more focused and conscious of what I am doing and the information I am trying to convey to my students. I have found mistakes too. Many mistakes.

Is this something that other teachers should do? Maybe. If you already have a set of slides, I don't know if I would recommend that you start converting them one by one. If you are just starting out and plan on teaching anything that requires formulae, then you might want to consider LaTex and Beamer. I have noticed that since switching to this method, more students have downloaded by slides (Moodle tells me that). I also see some of them using their iPads to take notes on the slides. They didn't do that before!

Everything you have read here can be found in one of the references below. If, however, you have any questions, or comments, let me know over on Fosstodon.

## Further Reading

* [Pandoc Manual](https://pandoc.org/MANUAL.html)
* [Beamer User's Guide](https://tug.ctan.org/macros/latex/contrib/beamer/doc/beameruserguide.pdf)
* [Beamer Theme Matrix](https://mpetroff.net/files/beamer-theme-matrix/)
* [Joel's Blog (if you ever need to include a nice bibliography)](https://joelchrono12.xyz/blog/doing-school-work/)
* [LaTex Wikibook](https://en.wikibooks.org/wiki/LaTeX)
