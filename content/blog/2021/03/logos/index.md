---
title: "Linux Users Like Lovely Logos"
slug: linuxlogos
date: 2021-03-26T10:00:14Z
lastmode: 2024-10-28
draft: false
author: bbbhltz
subtitle: "So many distros, so many logos; do they mean anything?"
description: "Using possible hokum to analyse the meaning behind the colours and shapes of Linux logos."
tags:
  - Linux
  - Design
next: true
nomenu: false
---

*NOTE: I haven't gotten around to resizing these images... Sorry!*

**"Don't judge a book by the cover"**

This is a maxim to follow, and any one of us can think of examples *ad nauseam*. For the sake of procrastination however, let us judge the logos of some popular Linux distributions based solely on their logos.

This is not an exact science, so one would not expect things to match up perfectly. To begin with, I will outline the meanings of different shapes, then I will look at a few distributions. In additions to the shapes, we will also consider the colours used.

## Shapes, Colours and Their Meanings

According to different sources, most of which state nearly the same thing ([see example](https://looka.com/blog/logo-shapes-meanings/)), shapes express meaning.

The shapes we see regularly in logos are as follows:

* Circles
    * femininity
        - this is not femininity in the sense of *male* vs *female*, but rather *tough* vs *tender*, or, if you prefer, *forgiving* vs *unforgiving*
    * security
    * continuity
    * protection

* Rectangles and Squares
    * reliability
    * stability
    * strength
    * efficiency
    * professionalism

* Triangles
    * stability
    * ingenuity

* Vertical lines
    * strength
    * courage
    * dominance
    * progress

* Spirals and Organic Shapes
    * unique
    * inviting
    * wellness

![Colours and Emotions via The Logo Company](color.webp#center "Colours and Emotions via The Logo Company")

## The Distributions

I am not a masochist, so I won't be looking at every distro. I randomly selected some distros from the [Distrowatch Rankings](https://distrowatch.com/dwres.php?resource=ranking) based on no criteria at all besides seeing them mentioned regularly on different social networks and YouTube channels.

### Arch

![Arch Linux Logo](archlinux.webp "Arch Linux Logo")

**Description**: Arch Linux adheres to the KISS principle ("Keep It Simple, Stupid") and is focused on simplicity, modernity, pragmatism, user centrality, and versatility. In practice, this means the project attempts to have minimal distribution-specific changes, and therefore minimal breakage with updates, and be pragmatic over ideological design choices and focus on customizability rather than user-friendliness.[^arch]

**The Logo**: The logo for Arch is a triangle. It is common usage in logos to use a triangle to represent the letter **A**. As stated above, the triangle is supposed to represent things like *stability* and *ingenuity*. In the case of Arch, that could be fitting. While it is a rolling distro, there are plenty of users who state that it is stable. It can also be considered ingenuous due to its nature; minimal but customisable. The choice of colour, in this case blue, is apt. According to "The Logo Company" (and other sources), it is the colour of trust, dependability, and strength.

**Verdict**: There is a bias here, of course. I do not use Arch any longer, but I did in the past. Despite initial installation surprises I had no issues using Arch. Once you jump through a few hoops you can have yourself a dependable operating system. The nature of the operating system is not user friendly, but does force the user to read the documentation resulting in a deeper understanding of how things work; the user gets *stronger*, in a sense, and the distro does reflect some of that strength. I therefore approve the choice of logo and colour.

[^arch]: https://en.wikipedia.org/wiki/Arch_Linux

### Peppermint OS

![Peppermint OS Logo](peppermintos.webp "Peppermint OS Logo")

**Description**: Peppermint OS is a Linux OS based on Lubuntu, which itself is a derivative of the Ubuntu Linux operating system that uses the LXDE desktop environment. It aims to provide a familiar environment for newcomers to Linux, which requires relatively low hardware resources to run.[^peppermint]

**The Logo**: It is a circle. Circles represent femininity (i.e. tender and forgiving) and protection. The distro markets itself as *familiar* and requiring lower system resources. That, for me, is a good example of femininity. Relying on an Ubuntu base with some bits from Mint helps with this. I have never used it, but based on their website, the vocabulary used to describe it, the traditional out-of-the-box interface (and the choice of LXDE as a DE) I would expect a fair amount of hand-holding while using this distribution. The choice of colour, red here, is supposed to represent youth, boldness, and excitement. At first glance, that does not seem to represent what Peppermint is about. But, if you think about different companies that use red in their logos (Coca-Cola, Nintendo, McDonald's), they are the types of companies that give us what we expect.

**Verdict**: I have never used this distro, but perhaps when my computer gets a little older I will take it for a spin. Judging it just on the logo, I would expect to find something user-friendly with no surprises, that would make an older computer feel snappy and fast. I accept this logo.

[^peppermint]: https://en.wikipedia.org/wiki/Peppermint_OS

### OpenSUSE: Tumbleweed and Leap

![OpenSUSE Logo](opensuse.webp "OpenSUSE Logo")

![Tumbleweed Logo](tumbleweed.webp "Tumbleweed Logo")

![Leap Logo](leap.webp "Leap Logo")

**Description**: *Tumbleweed* is the flagship of the openSUSE Project. A stable and tested Rolling Release, which receives new software each day, and which is basically unbreakable: if a fault occurs as a result of system updates, a snapshot function allows users to revert to a previous system state. Tumbleweed is preferred by openSUSE users as a desktop system.

*Leap* is a classic stable distribution approach, one release each year and in between security and bugfixes. This makes Leap very attractive as server operating system, but as well for Desktops since it requires little maintenance effort. Online release upgrades are mostly so unspectacular and trouble-free that the community already proposed, the next release should be called 'boring'.[^opensuse]

**The Logos**: Some variations here, and combinations too. The main chameleon logo would fall under the organic category above, and should inspire feelings of uniqueness and invitation. Having never used any version of this distro, I do know that it has some unique features such as the reputation of being unbreakable. The *Tumbleweed* logo could be interpreted as having the *continuity* of the circle family of logos, and the *uniqueness* of the spirals. The *Leap* logo, on the other hand, echoes the promise of the distro through its square shape: stable, reliable, and efficient. Colour-wise, the green logos convey a sense of peace and growth, while the *Tumbleweed* logo leans more toward the trust and strength of blue.

**Verdict**: OpenSUSE is certainly unique, so using an animal shape with a spiral tail is fitting. The *Leap* logo is particular precise in the emotion it conveys. Like with Arch above, the rolling release *Tumbleweed*, went with a bluish tint to give us a feeling a trust, while also telling us that it is safe and inviting. I don't believe I have ever tried OpenSUSE, but the *Tumbleweed* variant does seem tempting. I really like these logos, and I think that there was some thought put into them. I say yay.

[^opensuse]: https://en.wikipedia.org/wiki/OpenSUSE

### Void

![Void Linux Logo](void.webp "Void Logo")

**Description**: Void is a notable exception to the majority of Linux distributions because it uses `runit` as its `init` system instead of the more common `systemd` used by other distributions including Arch Linux, CentOS, Debian, Fedora, Mageia and Ubuntu. It is also unique among distributions in that separate software repositories and installation media using both `glibc` and `musl` are available.

Due to its rolling release nature, a system running Void is kept up-to-date with binary updates always carrying the newest release.[^void1]

Unlike trillions of other existing distros, Void is not a modification of an existing distribution. Void's package manager and build system have been written from scratch.

Void focuses on stability, rather than on being bleeding-edge. Install once, update routinely and safely.[^void2]

**The Logo**: I would say that there are two things going on here. First of all, there is the circle. We have already seen circles, so no need to go over that again. Void is about security, continuity, and protection. There is also some vertical lines in the lettering that to underline strength and progress. The shades of green that are used might not be the most obvious choice, certainly for something called *Void*, but like the circle shape and its *continuity*, the green speaks of *growth*. For a distribution that is built from scratch, growth and continuity hopefully mean that Void is here to stay. The lettering is black, expressing balance and calm.

**Verdict**: I have been tempted to try Void after reading many different things. The thing that I was interested in the most is that is does not use systemd. I have since stopped worrying about that because there are other things to worry about. I feel that this is a decent logo (based on the shape, I thought it would be an Ubuntu derivative). Not perfect, but certainly well-adapted to Void's promises. Initially, when I first read about Void, I expected something darker as a logo. If these ideas about shapes and colours are true, however, I have no choice but to applaud the designer's choices.

[^void1]: https://en.wikipedia.org/wiki/Void_Linux
[^void2]: https://voidlinux.org/

### Debian

![Debian Swirl](debian.webp "Debian Swirl Logo")

**Description**: Debian is composed of free and open-source software, developed by the community-supported Debian Project. The Debian Stable branch is the most popular edition for personal computers and servers. Debian is also the basis for many other distributions, most notably Ubuntu.

One of the oldest operating systems based on the Linux kernel, the project is coordinated over the Internet by a team of volunteers guided by the Debian Project Leader and their foundational documents.[^debian]

**The Logo**: The reddish circle-spiral is supposed to tell us that it is inviting, safe, and forgiving, with a touch of youthfulness. Stability is not conveyed through this logo, as we would expect. In 1993, when the project began, this was a bold and exciting distribution. [The swirl was selected by vote after a contest in 1999 to replace the original chicken](https://www.debian.org/vote/1999/vote_0004).

**Verdict**: Debian's logo does not scream "NEW AND EXCITING". I suppose in 2021 we are used to seeing shapes that we can easily reproduce with InDesign and Inkscape. As a Debian user I do agree that it is a forgiving distribution with lots of documentation and a large community making it rather inviting. I would grade this logo as "good" but in need of rejuvenation.

[^debian]: https://en.wikipedia.org/wiki/Debian

### Linux Mint

![Linux Mint Logo](mint.webp "Linux Mint Logo")

**Description**: A community-driven Linux distribution based on Ubuntu (in turn based on Debian), bundled with a variety of free and open-source applications. It can provide full out-of-the-box multimedia support for those who choose (by ticking one box during its installation process) to include proprietary software such as multimedia codecs.[^mint]

**The Logo**: I guess we call that a [squircle](https://en.wikipedia.org/wiki/Squircle). It should combine elements of circle-emotions and square-emotions. I did use Mint for a time and can confirm that, at the time, it delivered on those promises. The ease of installation also lends a feeling of peace (which is green) and balance (the white lettering).

**Verdict**: Excellent choices all around based on our colour and shape theories (mint is green, so the colour choice is a no-brainer). Bravo.

[^mint]: https://en.wikipedia.org/wiki/Linux_Mint

### Manjaro

![Manjaro Logo](manjaro.webp "Manjaro Logo")

**Description**: Manjaro has a focus on user-friendliness and accessibility, and the system itself is designed to work fully "straight out of the box" with its variety of pre-installed software. It features a rolling release update model and uses Pacman as its package manager.[^manjaro]

**The Logo**: Square. Rectangles. Vertical lines. Strength, stability, and progress. Manjaro is all of those things, like it or not. Sitting at [number 2 on Distrowatch's "hits per day" ranking and 28 on their "visitor ratings" ranking](https://distrowatch.com/table.php?distribution=manjaro) is a good example of strength for a 9-year-old distro. Green is about peace and growth.

**Verdict**: I used to use Manjaro. I was never fond of the logo. I wouldn't want it on a t-shirt or even as a sticker on my laptop. I started using Manjaro when there was an Openbox edition and I absolutely LOVED it. The first time I used it, it was like having a new computer. The installation was efficient. The user experience, with the consistent visual theming, was always stable. Despite my unjust dislike of the logo, it cannot be denied that it represents Manjaro. It is no wonder it was chosen to be one of the default distros for Pine64's Pinebook Pro, for example. Very good overall.

[^manjaro]: https://en.wikipedia.org/wiki/Manjaro

### Ubuntu

![Ubuntu Logo](ubuntu.webp "Ubuntu Logo")

**Description**: Ubuntu is named after the Nguni philosophy of *ubuntu*, which Canonical[^canonical] indicates means "humanity to others" with a connotation of "I am what I am because of who we all are".

Ubuntu aims to be secure by default.[^ubuntuw]

> The mission for Ubuntu is both social and economic. First, we deliver the world's free software, freely, to everybody on the same terms. Whether you are a student in India or a global bank, you can download and use Ubuntu free of charge. Second, we aim to cut the cost of professional services - support, management, maintenance, operations - for people who use Ubuntu at scale, through a portfolio of services provided by Canonical which ultimately fund the improvement of the platform.[^ubuntu1]

**The Logo**: Circles and more circles. Using Ubuntu is like getting a hug from an old friend. You are hesitant at first, and then you feel protected. Ubuntu is the gateway drug of Linux distros. It is safe. The continuity can be seen through their very consistent update schedule. Ubuntu is a friendly and confident, like the colour orange.

**Verdict**: The year was 2006. I downloaded an ISO and burned it to a CD. I booted from that CD and that was when I stopped using Windows. The Ubuntu logo is just right. It is what we expect from the distro; something safe and friendly to use or recommend to friends. Excellent work.

[^canonical]: a UK-based privately held computer software company founded and funded by South African entrepreneur Mark Shuttleworth to market commercial support and related services for Ubuntu and related projects

[^ubuntuw]: https://en.wikipedia.org/wiki/Ubuntu

[^ubuntu1]: https://ubuntu.com/about

### Gentoo

![Gentoo Logo](gentoo.webp "Gentoo Logo")

**Description**: Gentoo Linux was named after the fast-swimming gentoo penguin. The name was chosen to reflect the potential speed improvements of machine-specific optimization, which is a major feature of Gentoo. Gentoo package management is designed to be modular, portable, easy to maintain, and flexible. Gentoo describes itself as a *meta-distribution* because of its adaptability, in that the majority of users have configurations and sets of installed programs which are unique to the system and the applications they use.[^gentoo]

**The Logo**: I guess it depends on the angle you look at it. It is a letter **g**, but stylised to appear as a squircle or even rounded triangle? And, there is some blue. I suppose it could be a way to convey uniqueness and stability. The blue tinting could also be interpreted as strength and trust. 

**Verdict**: I have never used Gentoo. I *might* have visited the website once and decided that Gentoo is not for me, and I may have been right. Over on Distrowatch [the description](https://distrowatch.com/table.php?distribution=gentoo) for Gentoo tells us that it is *"a versatile and fast, completely free Linux distribution geared towards developers and network professionals"*. I am neither of those things. This logo does not scream, "I'm friendly, let's hang out!". In that sense, it is a good logo. If they had gone with an actual penguin, it would have been too inviting.

[^gentoo]: https://en.wikipedia.org/wiki/Gentoo_Linux

### Fedora

![Fedora Logo (as of March 2021)](fedora.webp "Fedora Logo")

**Description**: Fedora has a reputation for focusing on innovation, integrating new technologies early on and working closely with upstream Linux communities.

Fedora users can upgrade from version to version without reinstalling.

The default desktop environment in Fedora is GNOME and the default user interface is the GNOME Shell.[^fedora]

**The Logo**: Lots of blue. A teardrop/squircle. An infinity symbol. Judging Fedora by this alone, we would expect to find a dependable and stable distro, that is inviting but also professional.

**Verdict**: Linus uses Fedora. [Fedora's website](https://getfedora.org/en/workstation/) uses words like *freedom*, *innovation*, *easy*, and mentions that it is "created for developers". I have never used Fedora, but I do like their website. It is clean and professional, and uses white space to keep things focused while not being off-putting for beginners. I think the logo is great and not too pretentious. This makes me want to try the distro. Very good.

[^fedora]: https://en.wikipedia.org/wiki/Fedora_(operating_system)

### Raspberry Pi OS

![Raspberry Pi Logo](rpios.webp "Raspberry Pi OS")

**Description**: Raspberry Pi OS is highly optimized for the Raspberry Pi line of compact single-board computers with ARM CPUs. It runs on every Raspberry Pi except the Pico microcontroller. Raspberry Pi OS uses a modified LXDE as its desktop environment with the Openbox stacking window manager, along with a unique theme. The distribution is shipped with a copy of the algebra program *Wolfram Mathematica* and *Minecraft: Pi Edition*, as well as a lightweight version of the Chromium web browser.[^pi]

**The Logo**: It is a raspberry. No surprise. Red, green, some circular shapes. Even without looking at what the shapes and colours mean, most people would expect something fun with this distro. Something easy, or, at the least, something nice looking. Red for boldness and excitement, green for growth and trust. That inviting organic shape filled with tender circles should mean that most users, certainly beginners, would be at home with the distro.

**Verdict**: I am writing this this blog on my Raspberry Pi400 and hosting it on an older Raspberry Pi 3B+. It may come as a surprise that I don't think this logo is suitable for the distro. It is easy to get up and running, it boots directly to the desktop and comes with helpful tools, and it is forgiving; if you mess up, you just have to flash the microSD again. But, it is also lacking. Yes, you can do lots of things with a Pi, but this logo is too bold and the out of the box experience is not entirely exciting. I mean, LXDE, I love it and use it on my other computers, but youthful and exciting it is not (unless you are into ricing). My verdict is is "satisfactory". The Raspberry Pi Foundation will continue to use a raspberry as a logo, and I hope that as the SBC technology improves, the OS will catch up with the sentiments of the logo.

[^pi]: https://en.wikipedia.org/wiki/Raspberry_Pi_OS

## Conclusions

When I began writing this I thought I would be writing more negative comments. Globally, the Linux logos (at least the ones here) match up very well, sometimes extremely well, with the proposed theories of shapes and colours. Whether by accident or not, they are examples of good design. Further analysis (e.g. looking at which fonts and typefaces are used) would be required to fully determine which distribution's visuals are the best. I know I have left out many excellent logos and distributions, but I am pretty sure you can analyse by yourselves. Whether it be the unique styling and calmness inspired by [elementary](https://elementary.io/) or the hexagonal dependability of [Zorin](https://zorinos.com/), it is good to know that people are putting effort into these projects.

Well done, and thanks to all the logo creators and icon makers out there for keeping our desktops looking good.
