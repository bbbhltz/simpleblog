---
title: "Woe are we who put put our eggs in Silicon Valley"
subtitle: "Many rabbit holes make a warren"
description: "A blog post in which I talk about quitting big tech and finding alternatives while trying to avoid misinformation"
slug: woe-in-silicon-valley
date: 2025-02-27T10:00:00+01:00
#lastmod: 2025-02-26
featured_image: /blog/2025/02/woe-in-silicon-valley/featured.webp
toc: false
bold: false
tags:
  - Opinion
  - Technology
  - AI
  - Privacy
next: true
nomenu: false
notitle: false
draft: false
syndication:
  - https://framapiaf.org/@bbbhltz/114075212437302511
---

>*++KANG++: Look at them, Kodos. Foolish humans, trusting Silicon Valley billionaires with all of their data!*
>
>*++KANG & KODOS TOGETHER++: \<LAUGH MANIACALLY\>*

Welcome to the IRL *Simpsons' Halloween Special* that is 2025.

From a never-ending list of woes, many of them preventable, there is one we can all do something about in under 10 minutes. Strictly for ourselves, of course. For businesses and larger infrastructures, it would be a much harder and longer process, but for individuals we can start today.

Some time ago, I went down a [rabbit hole](/blog/2021/04/privacy-security-rabbit-hole/) and came back wiser, stronger, and willing to learn [more](/blog/2022/03/privacy-browser/). Now, I sit at the precipice of another rabbit hole. This one is a complicated labyrinth of terms, conditions, and policies. If my previous spelunking excursions taught me one thing, it might be that spending lots of time on one obsession will not always yield results.

## Context

Users (and companies) made a poor decision and put all their eggs in the same basket. Their digital lives are, for the most part, hosted on American platforms controlled by a handful Silicon Valley billionaires that have put profits over people.

[Recently](/blog/2025/01/indefensible/), I posted about the indefensible actions of these tech oligarchs. It is my (very strong) opinion that we should not only quit using the solutions proffered by [Big Tech](https://en.wikipedia.org/wiki/Big_Tech), but refuse an even larger set of software, services, and applications. 

We are left with the question of *How?*, which is why I'm sat here now looking at this mess.

### The mess

The Web is a mess in general, and this rabbit hole is no exception. One does not simple *quit Big Tech*, after all. You entered your relationship thinking you were just getting something for free, but it turns out they wanted your personal info. Now you've had enough, and this marriage is about to become a divorce. Marriage was about compromise: you got a free email account and some cloud storage, they got to learn about your cats and your kinks. Divorce is also about compromise, but with less interest on the future relationship.

You want out but don't know where to go, so you turn to the Internet. Great knower of things and waster of free time. The hunt is on for not just getting away from Big Tech, but getting away from US-hosted services as a whole. And, like with any news or information, these rabbit holes can be categorized:

![truth vs intent to mislead diagram](truth-mislead.webp)

Obviously, you want to avoid the lies, conspiracies, and false information out there. The nuance between what is *true* and what is *true but misleading* is sometimes hard to see, but for this rabbit hole I noticed some differences right away.

You may have heard about how the truth is hard to handle? In this instance, the truth is *long* and *detailed*. If you want to *really* make your own decision you'll need to dig into terms and conditions, privacy policies, and other documents that you simply do not have the time to read.

Alternatively, you could turn to lists. I like a good list, but any curated list on the web can be biased. Is that bad? Not so much. But, it could give you a false sense of virtue.

I think the [European Alternatives](https://european-alternatives.eu/) project is wonderful. The presentation is nice, and it is easy to poke around. But, let's imagine I'm not a fan of a certain search engine. Qwant, for example, is [listed](https://european-alternatives.eu/category/search-engines) as an alternative search engine, but I'm not at all convinced they aren't sharing data with Bing and Microsoft. That's my bias and now that list has lost some credibility.

*Stupid rabbit holes.*

Other curated lists, like the [Outside US Jurisdiction](https://codeberg.org/Linux-Is-Best/Outside_Us_Jurisdiction/src/branch/main/Index.md) list is one I would put in the *true but misleading* category. The curator wants to make a good list and is doing the footwork, but they are also rejecting alternatives based on *their* understanding of those facts.

Take for example [this exchange](https://codeberg.org/Linux-Is-Best/Outside_Us_Jurisdiction/issues/11) where the curator accuses another user of "care trolling" and [here](https://codeberg.org/Linux-Is-Best/Outside_Us_Jurisdiction/issues/6#issuecomment-2864678) where they imply users are "simping" for corporations and then says they would "rather not have this turn into a debate" (they also gave themselves a *thumbs-up*). In the end they [acquiesce](https://codeberg.org/Linux-Is-Best/Outside_Us_Jurisdiction/issues/11#issuecomment-2877748) and add a service to their list, but they needed some nudging.

Yet another list, this time of [Canadian Alternatives](https://codeberg.org/Taffer/canadian-alternatives) appears more trustworthy to me. Plus, as a Canadian, I find it fascinating to see so many options that I have never heard of. Joan Westenberg also has a list, and straight up dubbed it the [Trump-Proof Tech Stack](https://www.joanwestenberg.com/american-tech-is-compromised-heres-my-replacement-stack-2/) (catchy), where they list their alternatives and why they like them. It is good to add that context.

## Prevention is the best cure

Trusting random people on the Internet is how we started this mess. That kid in the hoody with a social network site? *"Sure, I'll give him my photos and date of birth,"* you said as you signed up for Facebook. Google did have great products (search and mail), but they have since lost your trust. Much of your personal information is now in the hands of a few companies.

I'm tempted to make a list here, but I already have an older (and longer) [post](/blog/2022/03/guide-privacy/) about this, and a [slash page](/software/) with my favourite software. Instead, I will make two broad suggestions. One *curative*, the other *preventative*.

### Curative

What ails you? Make a list. Is it your email provider? Is it Spotify? Make a list, but don't expect to fix everything in one day.

Request your data from those companies, and delete your account. Do not suspend.

Many alternative services allow you to import everything. Gmail, for example, will let you download your emails in a batch or transfer them to another service.

Recently, I have seen people quitting most social media (Twitter, obviously, but also Facebook, WhatsApp, Instagram, and LinkedIn), Gmail, Outlook, cloud storage services, Spotify, Airbnb. Remember: **you don't need them, they need you. Your data is valuable to them**.

### Preventative

One thing I discovered after quitting Google was that I do not, in fact, need that cloud space or the photo service. Was it cool to be able to create a permanently shared photo album with my family? Yes, and I thought I would miss it. It turns out that I didn't. The same goes for the 25 GB of Google Drive space I had accumulated (they just kept gifting me space).

So, ask yourself if you even need a replacement before rushing in and spending money on something that may disappoint you. A few [DeGoogle](https://en.wikipedia.org/wiki/DeGoogle) waves ago, lots of users jumped on the Proton wagon. Now they are [pissed off at Andy Yen](https://theintercept.com/2025/01/28/proton-mail-andy-yen-trump-republicans/). Look for companies with track records and don't trust every list or tech influencer you read online.

To prevent future issues, start protecting your privacy. Don't install *every* possible add-on that claims to protect your privacy (are you looking to reduce tracking or evade tracking?) and don't be surprised when your [favourite browser introduces new terms of use](https://connect.mozilla.org/t5/discussions/information-about-the-new-terms-of-use-and-updated-privacy/m-p/87735#M33600). Fallback to *curative* measures (get your data; delete) and try out different solutions.

Protect your friends and family too. Use [LibreOffice](https://libreoffice.org/) instead of Microsoft Office so they can see it is just as good. Show them that [Signal](https://signal.org/) works better than the competition. Persuading people to give up something like WhatsApp can be hard, but if someone expects you to install an application to communicate with them and at the same time refuses to make the minimum effort for you, you'll just have to stand your ground.

Finally, refuse generative AI. My [anti-AI bias](/tags/ai/) is no secret, but as a preventative measure it is necessary to avoid AI. If these companies [were willing to train their models on pirated books](https://www.wired.com/story/new-documents-unredacted-meta-copyright-ai-lawsuit/), what do you think they will do with your data? As a reminder, [keystroke deanonymization](https://www.whonix.org/wiki/Keystroke_Deanonymization) is a thing:

>Keystroke biometric algorithms have advanced to the point where it is viable to fingerprint users based on soft biometric traits. This is a privacy risk because masking spatial information---such as the IP address via Tor---is insufficient to anonymize users.
>
>Users can be uniquely fingerprinted based on:
>
>- Typing speed.
>- Exactly when each key is located and pressed (seek time), how long it is held down before release (hold time), and when the next key is pressed (flight time).
>- How long the breaks/pauses are in typing.
>- How many errors are made and the most common errors produced.
>- How errors are corrected during the drafting of material.
>- The type of local keyboard that is being used.
>- Whether they are likely right or left-handed.
>- Rapidity of letter sequencing indicating the user's likely native language.
>- Keyboard Layout as the placement of the keys on the keyboard leads to different key seek times and typing mistakes.

The AI revolution is not here yet, OpenAI is "[burning through piles of money](https://www.nytimes.com/2024/09/27/technology/openai-chatgpt-investors-funding.html)" and is "[on course to burn over $26 billion in 2025 for a loss of $14.4 billion](https://www.wheresyoured.at/wheres-the-money/)." If people are investing in AI companies, it isn't for the "product," it is because they see *potential* value elsewhere, and the only thing they have is your data.

## Final thoughts

This entire rabbit hole stinks of <abbr title="Fear, uncertainty, and doubt">FUD</abbr>. Hell, this blog post does too! Even the title of this post refers to the idiomatic expression "do not put all your eggs in one basket", which is essentially a [cautionary tale](https://en.wikipedia.org/wiki/Cautionary_tale), implying that an unpleasant fate awaits all who use Silicon Valley tech. 

Initially, I had hoped to frame this as some sort of PSA, "and knowing is half the battle," type of post. Unfortunately, my own bias shines through line after line. In case it isn't clear enough, let me spell it out one more time:

Your data is valuable, and it is yours. The companies---many of which originated in Silicon Valley---that have your data put profit over people and use that profit for political favours and to pay fines for breaking the law. They have no interest in your well-being. We need to let them know that they have betrayed their users and themselves for the sake of shareholder value. Fuck them.
