---
title: "Dillo Web Browser"
subtitle: "The \"fast and lightweight browser\" you've probably been overlooking"
description: "Dillo, a fast and lightweight browser, really is more than meets the eye. This post talks about installing and using Dillo."
slug: dillo
date: 2025-02-09T14:30:10+01:00
lastmod: 2025-02-13
featured_image: /blog/2025/02/dillo/featured_image.webp
toc: false
bold: false
katex: false
tags:
  - Web
  - Technology
  - Privacy
  - Open Source
next: true
nomenu: false
notitle: false
draft: false
syndication:
  - https://framapiaf.org/@bbbhltz/113986521614034566
  - https://jstpst.net/f/technology/12620/dillo-web-browser
---

![Featured Image: Screenshot of the Dillo Browser serving a local file with the Title and Subtitle of this blog post](featured_image.webp)

Linux users have probably seen this software installed on their system and wondered, "where did that come from?" or "what's the point of this?"

After all, who wants a hamstringed browser that doesn't support JavaScript?

If the answer isn't obvious yet, I'll give you a hint: memory and system performance.

On a modern(-ish) system like my 2017 Lenovo Thinkpad, Dillo ([homepage](https://dillo-browser.github.io/)) is open and navigating a page in under a second. Firefox takes 5 or 6 seconds to launch and load a similar page. What if *you* were forced to use a older computer? Or perhaps use a device attached to a slower network.

More importantly, what if you just want to use a browser that works that way *because you do*?

Many years ago when I first started using Linux (sometime in 2006 after trying---and abandoning---Linux at university), Dillo was there. The [history of Dillo](https://dillo-browser.github.io/25-years/index.html) goes back to 1999, ends in 2016, and is granted a sequel in 2024.

Up until this *resurrection*, I kept Dillo around. I didn't browse the web with Dillo or try to figure out how it works, I mostly used it in combination with [Claws Mail](https://www.claws-mail.org/index.php) using the [Dillo HTML Viewer plugin](https://www.claws-mail.org/plugin.php?plugin=dillo).

I saw through Mastodon, "Hacker" "News" (I miss [n-gate.com](http://n-gate.com/)), and a thread or two on the Debian mailing list  that the project had been brought back to life! Since this announcement I have been using Dillo as more than just a Claws Mail plugin. Then, one week ago (2025-02-03), Rodrigo Arias Mallo gave a talk at [FOSDEM 2025](https://fosdem.org/2025/schedule/event/fosdem-2025-4100-resurrecting-the-minimalistic-dillo-web-browser/).

You can [watch the talk on YouTube](https://www.youtube.com/watch?v=sFJp8JDg8Yg) or take a look at the [slides](https://dillo-browser.github.io/fosdem-2025/).

In it, Rodrigo shows off a few lesser known capabilities of Dillo. Personally, I didn't know that there were plugins other than for Gemini (no, not the f-cking AI thing, the [protocol](https://geminiprotocol.net/)).

## Installing

As I am using Debian, the most recent version of Dillo is not in the stable repository. I chose to install Dillo using the [instructions](https://github.com/dillo-browser/dillo/blob/master/doc/install.md) on the GitHub page:

```bash
$ sudo apt install gcc g++ autoconf automake make zlib1g-dev \
    libfltk1.3-dev libssl-dev libc6-dev \
    libpng-dev libjpeg-dev libwebp-dev
$ git clone https://github.com/dillo-browser/dillo.git
$ cd dillo
$ ./autogen.sh
$ mkdir build
$ cd build
$ ../configure --prefix=/usr/local
$ make
$ sudo make install
$ cd ..
$ ./install-dpi-local
$ cp /usr/local/etc/dillo/dillorc ~/.dillo/
```

I also installed the [Rdrview plugin](https://github.com/dillo-browser/dillo-plugin-rdrview), the [Gemini plugin](https://github.com/dillo-browser/dillo-plugin-gemini) and the [Man plugin](https://github.com/dillo-browser/dillo-plugin-man). 

## Using Dillo on the Bloated Web

Much can be said about how broken and bloated the web is. Using a browser like Dillo highlights this fact. News websites are a mess, search engines need JavaScript, and social media sites are behemoths.

Dillo can still be useful for getting the news from certain "lite" versions ([CBC Lite](https://www.cbc.ca/lite/news?sort=latest), [The Brutalist Report](https://brutalist.report/), and [NPR's Text-Only Version](https://text.npr.org/) work well), checking the weather ([https://wttr.in/](https://wttr.in/)) and even searching ([Mojeek](https://mojeek.com), [DuckDuckGo](https://lite.duckduckgo.com), [Wiby](https://wiby.me/), and [Marginalia](https://marginalia-search.com/)). You can also use Dillo to keep up with your favourite blogs.

![montage of several sites rendered on Dillo](sites.webp)

With the Rdrview plugin (mentioned above) you can view other sites, which is one of the ways I use it in combination with [Newsboat](https://newsboat.org/) having set `browser "/usr/local/bin/dillo rdrview:%u"` in my config file. This makes for VERY fast browsing.

Additionally, limitations lead to interesting solutions. It is possible to use Mastodon through sites like [Brutaldon](https://brutaldon.org/) and even check out Bandcamp releases on [Tent](https://tn.vern.cc/). Adding `link_action="Open in MPV:mpv $url"` to `~/.dillo/dillorc` means you can right-click-play the tracks in MPV.

![Tent](tent.webp)

Another built-in functionality that eagle-eyed readers noticed is that little bug in the bottom right. This function helps beginners like me find mistakes on their site. It took me a few days to sort out all the little errors on my site, so I was very happy to see this the other day:

![good HTML](goodhtml.webp)

## Tips

### Bookmarks

Bookmarks are stored as a plaintext file in `~/.dillo/bm.txt` and can be separated into sections:

```txt {linenos=inline}
:s0: News
:s1: Blogs
:s2: Search
:s3: Social
:s4: Music
s0 https://www.cbc.ca/lite/news?sort=latest CBC News
s0 https://text.npr.org/ NPR
s0 https://brutaldon.org/home Brutaldon
s0 https://wttr.in/ Weather
s0 http://www.wiby.me Wiby
s1 https://bobbyhiltz.com/ My Blog (online)
s1 http://localhost:1313/ My Blog (localhost)
s2 https://www.mojeek.com/ Mojeek
s3 https://brutaldon.org/about brutaldon
s4 https://tn.vern.cc/ Tent
```

### Adding search engines

Edit `~/.dillo/dillorc` and find the section regarding search. Modify as such by commenting out search engines you don't use and adding those you do:

```txt {linenos=inline, linenostart=199}
# Set the URLs used by the web search dialog.
# "%s" is replaced with the search keywords separated by '+'.
# Format: search_url="[prefix ][<label> ]<url>"
# You can enable multiple search_url strings at once and select from among
# them at runtime, with the first being the default.
# (the prefix serves to search from the Location Bar. e.g. "dd dillo image")
search_url="dd DuckDuckGo (https) https://duckduckgo.com/lite/?kp=-1&kd=-1&q=%s"
search_url="Wikipedia http://www.wikipedia.org/w/index.php?search=%s&go=Go"
# search_url="Free Dictionary http://www.thefreedictionary.com/%s"
# search_url="Startpage (https) https://www.startpage.com/do/search?query=%s"
# search_url="Google https://www.google.com/search?ie=UTF-8&oe=UTF-8&gbv=1&q=%s"
search_url="Mojeek https://www.mojeek.com/search?q=%s"
search_url="Marginalia https://marginalia-search.com/search?query=%s"
```

### Toggle CSS

Depending on the site you visit, toggling the Remote CSS might make the site more usable.

### Changing Colours

The `~/.dillo/dillorc` proposes difference themes (search for "Gray theme" and you will see some built-in options) and other colour options:

```txt {linenos=inline, linenostart=283}
#-------------------------------------------------------------------------
#                            COLORS SECTION
#-------------------------------------------------------------------------

# Set the page background color
# bg_color=gray
# bg_color=0xd6d6c0
#bg_color=0xdcd1ba

# If your eyes suffer with white backgrounds, change this.
#allow_white_bg=YES

# If allow_white_bg is set to NO, white backgrounds are replaced by
# this color.
#white_bg_replacement=0xe0e0a3

# When set to YES, the page author's visited link color may be overridden
# to allow better contrast with text/links/background
contrast_visited_color=YES
```

The default is "[light grayish orange](https://www.colorhexa.com/dcd1ba)" and the other option is a "[grayish yello](https://www.colorhexa.com/d6d6c0)". I was put off by both at the start, but have come to find them comfortable.

### CSS

You can also make your very own CSS file and put it in your `~/.dillo/` directory and naming it `style.css`. I haven't bothered with this yet, but did test out using [Simple.css](https://simplecss.org/) and a few other pre-made frameworks. If you like making things homogenous, then this might be an interesting feature.

## Final Words

There is a reason why projects like Dillo exist. As long as there are inequalities in the world, we need tools like this and people supporting and maintaining them. Browsers are giants today, and Dillo fits on a floppy and does what it says on the tin.

### Supporting and Following Dillo

You can support the Dillo project on [Liberapay](https://liberapay.com/dillo/) and follow on  [Mastodon](https://fosstodon.org/@dillo) and [GitHub](https://github.com/dillo-browser/).


