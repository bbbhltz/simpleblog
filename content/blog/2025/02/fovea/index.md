---
title: "With apologies to your Fovea"
subtitle: "I broke my blog and burned your eyes"
description: "While updated and trying to make modifications to this blog, I inadvertedly broke my dark theme. This is my apology."
slug: fovea
date: 2025-02-16T13:57:03+01:00
lastmod: 2025-02-17
featured_image: /blog/2025/02/fovea/featured.webp
toc: false
bold: false
katex: false
tags:
  - Blog
  - Design
  - Technology
next: true
nomenu: false
notitle: false
draft: false
syndication:
  - https://framapiaf.org/@bbbhltz/114014014739822858
---

Dear readers,

Yesterday morning (2025-02-15) I woke up to a handful of irate emails.

I originally thought they might be spam. There was no clear indication of what the issue was.

Here is an example:

>Your website colors are making it too difficult to read any post.

The other messages, while mentioning the same issue, were equally unclear and coming from anonymous email addresses without signatures.

Weary-eyed, I rolled myself out of bed to fetch a coffee, eager to fix the problem and maybe learn something in the process.

Imagine my utter disappointment when I loaded my website on my phone and saw that it was completely legible. I activated "Battery Saver" (forcing everything into dark theme) and still saw my boring, yet legible, blog.

"Must be some weird SPAM or phishing thing," I thought to myself.

Since my laptop was on I spent the day tinkering around and found lots of superfluous stuff within the lines of code, HTML and CSS that make this blog. I am *very* amateur at this stuff. While I do pick up things here and there, the major foundation of this blog is composed of:

- default templates
- barklan's [Hugo Dead Simple Theme](https://hugo-dead-simple.netlify.app/post/hugo-dead-simple/)
- bits that I've copy-pasted from [other blogs](/more/#some-blogs-i-follow)

So, there are useless bits here and there that I have managed to remove without breaking anything, and other things I've figured out by reading documentation and comments. I wouldn't have been shocked if I had somehow managed to break something, especially the CSS part that I pay very little attention to.

Then at around 11:30 last night, another email. This one mentioned a "white background" and "dark theme." Again, I opened my phone and checked. Dark theme worked. Weird spam.

What if I tried a different browser, though?

On Android, I use Stoutner's [Privacy Browser](https://www.stoutner.com/privacy-browser-android/)[^pb]. But I also have Fennec installed (and yes, I know about [IronFox](https://gitlab.com/ironfox-oss/IronFox/), I just haven't tried it out yet). And guess what, when I looked at my blog using Fennec I saw the problem! White background; grey text; hard to read.

It took me about 30 minutes to sort it out and re-learn a lesson: **check everything in more than one browser on more than one device**.

## So you don't use dark theme?

I really only use dark theme late at night and it only activates on my phone when the battery is low. Dark themes are harder to read for me---the letters are very blurred and I quickly suffer from eye-strain. I understand this this might be related to astigmatism.

Privacy Browser uses Android WebView's built-in dark mode (Night Mode):

> [...] the dark WebView theme has also been switched from using a JavaScript CSS override, to using WebView’s relatively new built-in dark mode. The big takeaway is that JavaScript no longer needs to be enabled for the dark mode to work. In most cases, the resulting dark websites look better than they did before.
>
><cite>[source](https://www.stoutner.com/privacy-browser-3-5/)</cite>

If I'm reading this correctly, since I don't ever use dark mode on my laptop, I have never actually tested the dark theme of my blog until yesterday. It has always been the *default built-in dark theme*.

## Apologies

For burning your retinas and assaulting you with lumens, I am sorry. But, if you are going to email someone with a basic blog like mine, you need to choose your words.

One-line emails ending in exclamation points with no clear call to action come off as somewhat rude and the opposite of constructive.

*For your viewing pleasure, this post has been tested using Firefox (Light and Dark), Dillo, and Lynx.*

[^pb]: [see a previous post on Privacy Browser](/blog/2022/03/privacy-browser/)
