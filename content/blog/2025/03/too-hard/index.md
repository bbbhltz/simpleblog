---
title: "Teacher, it's too difficult"
subtitle: "Is it really?"
description: "How to check readability of articles for use in a classroom setting with Python, and the importance of adding tools to your teaching toolkit"
slug: too-hard
date: 2025-03-07T09:43:16+01:00
#lastmod: 2025-03-07
featured_image: /blog/2025/03/too-hard/featured.webp
toc: false
bold: false
tags:
  - Education
  - Pandoc
  - Programming
next: true
nomenu: false
notitle: false
draft: false
syndication:
  - https://framapiaf.org/@bbbhltz/114121198904042321
---

Let's set the scene.

I'm a professor. I have different employers, but in one school I have a *very* straightforward task: help students improve their level of English and attain a ["B1--B2" level](https://en.wikipedia.org/wiki/Common_European_Framework_of_Reference_for_Languages#Common_reference_levels), with a focus on reading a news article and summarizing it. The restrictions are boons for me. The final exam of this course will consist of reading an article that is "no longer than 45 lines or 450 words." Also, the exam the students take is a common exam for the [BTS](https://en.wikipedia.org/wiki/Brevet_de_technicien_sup%C3%A9rieur) certificate.

So, every week I prepare an article. Here is my process:

1. When I read the news, I save articles to [wallabag](https://www.wallabag.it/en).
2. The day before class I look at the articles I have saved, filter them by length (3--4 minutes), and choose something that is appropriate (usually "work culture" articles).
3. Copy the title, author, date, url, and body to a Markdown file.
4. Use Pandoc to convert the article and add line numbers.
    - If the article is too long, I shorten it and repeat.

I use a similar process for exams.

At least once a year the students panic. They think they'll never get it. 99% of my students are quite hard-working, so if they aren't getting it then it must be because the text is *too hard*, right?

I have a system for this. I am positive that there are *much* better ways of getting the same results, but this didn't take ages to sort out, so I'm happy with it.

Several years ago, I got this idea that I should try my hand at [programming](/tags/programming/). I learned a little Python and got one of [these](https://certificates.cs50.io/aab11061-9e60-464d-88bc-ac4354528609.pdf?size=letter). It was awesome. Even learning some beginner Python has helped me with my work and personally.

Since the articles are all in Markdown files---basically plain text---and Python can do stuff with text, I quickly discovered [Readability](https://github.com/andreasvc/readability/) for Python and thought to myself, "what if the articles I am choosing *are* too hard?"

## The Fix

The idea of [readability](https://en.wikipedia.org/wiki/Readability) was not new to me. I remember hearing about [Flesch-Kincaid](https://en.wikipedia.org/wiki/Flesch%E2%80%93Kincaid_readability_tests). A little script to check my articles is what I needed to make sure the level never exceeded first-year university (my students are not native speakers, so university-level would be too much for most).

I'll use [this short post I made in 2022](/blog/2022/05/digital-natives/) as an example.

If I were to prepare the post as an article to use in class, my Markdown file would usually look like this:

```markdown {linenos=inline,hl_lines=["17-20", 27, 31]}
---
papersize: A4
title: "In Search of Digital Natives"
subject: "Source: bobbyhiltz.com"
date: 2022-05-07
author:
    - Bobby Hiltz
keywords:
    - Technology
    - Education
documentclass: article
geometry:
    - margin=2.5cm
fontsize: 12pt
pagestyle: empty
header-includes:
    - \usepackage{lineno}
    - \modulolinenumbers[5]
    - \let\Begin\begin
    - \let\End\end
---

# In Search of Digital Natives

Author \ Date \ Source

\Begin{linenumbers}

CONTENT

\End{linenumbers}

```

You should note the lines in the front matter and body referencing `linenumbers`. The main content of the articles I use in class is always contained between `\Begin{linenumbers}` and `\End{linenumbers}`. As such, if I want to estimate the reading level of an article I use in class, I just need to check the readability of the text between those lines.

With a little searching, examples by the developer, trial and error, I made this (NB: in a previous version the modules worked differently, so this isn't the same version I made 3 years ago):


```python {linenos=inline}
import sys
import readability
import syntok.segmenter as segmenter

with open(sys.argv[1], "r") as file:
    lines = file.readlines()

    # get body
    parsing = False
    body = ""
    for line in lines:
        if line.startswith("\End{linenumbers}"):
            parsing = False
        if parsing:
            body += line
            body += " "
        if line.startswith("\Begin{linenumbers}"):
            parsing = True

    # get tokens
    tokenized = "\n\n".join(
        "\n".join(" ".join(token.value for token in sentence) for sentence in paragraph)
        for paragraph in segmenter.analyze(body)
    )
    results = readability.getmeasures(tokenized, lang="en")

    # print summary
    print(f"SMOG (<=13): {round(results['readability grades']['SMOGIndex'],2)}")
    print(f"Kincaid (<=13): {round(results['readability grades']['Kincaid'],2)}")
    print(
        f"Flesch Reading Ease (>=50): {round(results['readability grades']['FleschReadingEase'],2)}"
    )
    print(
        f"Gunning Fog Index (<=13): {round(results['readability grades']['GunningFogIndex'],2)}"
    )
```

This script should open my Markdown file, read the lines between two given points, and return different Readability scores: 

- [SMOG](https://en.wikipedia.org/wiki/SMOG): I want it to be as close to 13 (first-year university) as possible, but not over.
- [Flesch–Kincaid readability tests](https://en.wikipedia.org/wiki/Flesch%E2%80%93Kincaid_readability_tests): The Kincaid grade level should be 13 or under, and the Reading Ease score should be 5O or above.
- [Gunning fog](https://en.wikipedia.org/wiki/Gunning_fog_index): Should also be 13 or under.

It isn't necessary to do all of these tests, of course, but it is interesting to see differences. My post, as it turns out, uses simple English:

```
SMOG (<=13): 8.53
Kincaid (<=13): 3.86
Flesch Reading Ease (>=50): 87.57
Gunning Fog Index (<=13): 8.14
```

I would tend to agree with that estimate. I mean, I wrote the post and I spend my day teaching and grading English. But, what about a real article written by someone hired for their writing skills? What if I run the last test the students complained about through my script?

```
SMOG (<=13): 15.49
Kincaid (<=13): 14.35
Flesch Reading Ease (>=50): 45.7
Gunning Fog Index (<=13): 18.94
```

Ah... I see. I gave them an article that was above their level. I suppose that is ideal for a test or an exam in a way, and while they complained their scores on the test did not suffer because of this perceived difficulty. In fact, they understood this article better than some of the activities we did in class. Context, time, and place have a profound effect, I suppose.

## The Takeaway: Expanding the Teaching Toolkit

I wanted to be a teacher and took specific classes on becoming a teacher. I did not, however, learn anything about readability during those classes. I learned about it after university. I think teachers should be aware of this sort of thing. Maybe they are, though, and I somehow missed a lesson about that.

Most of my lessons focused on *scaffolding*, or building a *unit* out of different activities and exercises. Like giving the students the base (through memorization, for example), having them apply it in different circumstances (homework), using it again in a completely different case (test), and then having them create a *flipped lesson* on the topic.

After a number of years teaching, and having many colleagues that do the same, I cannot state how important it is to add to your toolkit from time to time. This little script saves me time and helps me teach better. Similar to my little [script for making IELTS exercises](https://codeberg.org/bbbhltz/ielts_gen), this tool was made using beginner Python skills.

If you are reading this and happen to be in charge of training teachers or professors, you should suggest a beginner programming course to your faculty members. If you are a teacher and don't see the value of learning a programming language, I assure you that I didn't think I would use it for work either---I'm an English teacher, after all. You will find a use.

## Blog Question Mini-Challenge for Teachers

Teachers, what are some of your tools that you've added to your toolkit in the last few years? Let me know via email, Mastodon, or on your own blogs. Also, send me your blogs!
