---
title: "Blog Questions Challenge 2025"
subtitle: "I shouldn't have replied to that toot..."
description: "This blog post answers several questions related to blogging. At the end, I nominate someone else to continue this game of tag."
slug: bqc
date: 2025-01-22T14:40:00+01:00
lastmod: 2025-02-13
featured_image: /blog/2025/01/bqc/featured.webp
toc: false
bold: false
katex: false
tags:
  - Blog Question Challenge 2025
next: true
nomenu: false
notitle: false
draft: false
syndication:
   - https://framapiaf.org/@bbbhltz/113872393895738213
---

I read [JP's post](https://moddedbear.com/blog-questions-challenge-2025/) about the Blog Questions Challenge and decided to comment on it. Hindsight is 20/20, and the damage was done. [Joel](https://joelchrono.xyz/) was camping and [clipped me with a nomination](https://fosstodon.org/@joel/113872133033042450).

Since I was not officially nominated in a blog post, I feel like this chain needs to loop back somehow, so I will include a link to [Ava's Bear Blog Quesion Challenge](https://blog.avas.space/bear-blog-challenge/) that started this whole thing.

## Why did you start blogging in the first place?

This blog began when I read something about the Gemini protocol on Mastodon or perhaps one of the Hacker News or Lobste.rs-like sites. I had a Raspberry Pi that wasn't doing anything, so I bought a cheap domain (bbbhltz.space) and got to work. Then I didn't feel great about paying for a domain just for a Gemini capsule, so I looked up how to host a blog as well.

## Have you blogged on other platforms before?

Years ago I had a [My Opera](https://en.wikipedia.org/wiki/My_Opera) blog. I fiddled with Blogger, WordPress, Medium and Write.as, but never stuck with any of them.

## What platform are you using to manage your blog, and why did you choose it?

I use [Hugo](https://gohugo.io/). I don't remember how I came to choose that one, but I do remember comparing and considering Pelican and Jekyll. I think it may have been the themes catalogue that pushed me to Hugo, because initially my blog used the [smigle theme](https://themes.gohugo.io/themes/smigle-hugo-theme/).

The blog started as being self-hosted, then moved to [sourcehut pages](https://srht.site/) before moving to [Codeberg Pages](https://codeberg.page/).

## How do you write your posts?

I open a terminal, I navigate to my blog folder, I run:

```
$ hugo new blog/{year}/{month}/{slug}/index.md
```

My `default.md` archetype looks like this:

```
---
title: "{{ replace .Name "-" " " | title }}"
subtitle: ""
description: ""
slug: {{ .Name }}
date: {{ .Date }}
#lastmod: {{ .Date | time.Format "2006-01-02" }}
#featured_image: /blog/{{ now.Format "2006" }}/{{ now.Format "01" }}/{{ .Name }}/featured.webp
toc: false
bold: false
katex: false
tags:
  - blog
next: true
nomenu: false
notitle: false
draft: true
#syndication:
---

content
```

Then, I will use any text editor ([Geany](https://www.geany.org/), [Featherpad](https://github.com/tsujan/FeatherPad), and [Micro](https://micro-editor.github.io/) are my go-tos---I am using Featherpad right now) and start typing away. When I am done, I use [gramma](https://caderek.github.io/gramma/) to check spelling and grammar.

## When do you feel most inspired to write?

Usually just after holidays when work starts to pile up. Procrastination kicks in and that little voice says, "Hey. Blog!"

(I like that voice.)

## Do you publish immediately after writing, or do you let it simmer a bit as a draft?

The plan is always to let it simmer, but most of the time I just publish (and then hunt for mistakes).

## What's your favourite post on your blog?

I think the post, [The Privacy-Security Rabbit Hole](https://bobbyhiltz.com/blog/2021/04/privacy-security-rabbit-hole/), is a pretty good. Not too long, has a story-like feel. It was written I realized I was creating [too much friction](https://bobbyhiltz.com/blog/2022/03/guide-privacy/) for myself while using the Internet.

Similarly, I feel that [Supernova Goes Pop](https://bobbyhiltz.com/blog/2024/03/supernova-goes-pop/) expresses my thoughts on AI quite well.

## Any future plans for your blog?

Today? No. I have added some superfluous bits that make me happy (see the my [404](/oops/) page that randomly changes the message on every build, and this page about [quitting](/quitter/)). And I am content with the very basic presentation. I keep wondering about adding webmentions. If you read this, let me know via email if I should add webmentions.

## Who will participate next? 

- [Light Thoughts](https://sanderium.codeberg.page/) ([Mastodon](https://social.linux.pizza/@sanderium))
  - UPDATE: See [Blog Questions Challenge](https://sanderium.codeberg.page/posts/blog-questions-challenge/) on their blog!

