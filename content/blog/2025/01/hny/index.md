---
title: "Welcome to 2025"
subtitle: "2024 was a lazy year"
description: "lamentations on a lazy year and possible resolutions for 2025 as well as additions to my blog"
slug: hny
date: 2025-01-01T14:09:08+01:00
featured_image: /blog/2025/01/hny/featured.webp
toc: true
bold: false
katex: false
tags:
  - eoy
next: true # show link to next post in footer
nomenu: false
notitle: false
draft: false
# syndication:
---

Hello all, and welcome to 2025. 

Last year I let things slip:

- I didn't read enough. 
- I played too many shmups. 
- I drank too many soft drinks. 
- I didn't blog enough.
- I didn't even make a "best of" albums list!

You get the idea.

I'm kicking things off as right as possible this year. To do so, I have a few *not-so-hidden* slash pages to tell you about, then I will go back to resting!

## Tunes

I did keep a persistent list of albums I liked at [/tunes](/tunes). No comments or thoughts, just a continuous list.

## Quitting

I have a nasty, awful, horrible habit. So, I made [/quitter](/quitter) to keep track of my progress. It isn't a dynamic page, so the durations and achievements will only update whenever I rebuild the blog. The maths are a little iffy, but I'm pretty happy that I sorted it out on my lonesome.

## Oops

My 404 page has been changed to include the [NetPostive Haiku Error messages](https://8325.org/haiku/). Like the /quitter page above, this isn't dynamic but I'm very happy that I made it work (the Haiku are in a `json` file and every rebuild chooses a random one)! Head over to [/oops](/oops) to see what I mean.
