---
title: "Indefensible"
subtitle: "If you're not fighting them, you're helping them."
description: "The most influential people in the world, who also happen to be the richest, have chosen to side with hate. We do not need to join them."
slug: indefensible
date: 2025-01-27T08:44:24+01:00
lastmod: 2025-02-13
featured_image: /blog/2025/01/indefensible/featured.webp
toc: false
bold: false
katex: false
tags:
  - Technology
  - Social Networking
  - Opinion
next: true
nomenu: false
notitle: false
draft: false
syndication:
    - https://framapiaf.org/@bbbhltz/113899877097956010
---

> People will come to love their oppression, to adore the technologies that undo their capacities to think.
>
> <cite>---Aldous Huxley</cite>

Let's put the cards on the table...

![The Bosses of the Senate](https://upload.wikimedia.org/wikipedia/commons/thumb/e/e5/The_Bosses_of_the_Senate_by_Joseph_Keppler.jpg/1091px-The_Bosses_of_the_Senate_by_Joseph_Keppler.jpg)

![Presidential inauguration 2025](wealth.webp)

First we have Joseph Keppler's oft-used cartoon, "[The Bosses of the Senate](https://en.wikipedia.org/wiki/The_Bosses_of_the_Senate)." Then we see some presidential inauguration guests, including Mark Zuckerberg, Jeff Bezos, Sundar Pichai, and Elon Musk, on 20 January 2025 (photo by Julia Demaree Nikhinson/AP).

Those cards make a pair. 

They are not *exactly* the same, of course. Keppler's cartoon is about the *rising* influence of monopolies. The photo shows several cowardly cronies, one of their partners, whatever Elon Musk is[^1], and the cabinet put in their place in the second row.

For the past few weeks, we have witnessed the beginning of something that shouldn't have come to be. 

Mark Zuckerberg, the un-firable head of Meta and MMA fighter, who speaks about "[masculine energy](https://theconversation.com/mark-zuckerberg-thinks-workplaces-need-to-man-up-heres-why-thats-bad-for-all-employees-no-matter-their-gender-247539)," whose apparent lack of empathy could be perceived as sociopathic, *could* have put his foot down. He has money, his platform has global reach. Instead, he gave a million dollars to Trump's inauguration fund and decided to transform Facebook and Instagram (the networks and the companies) into platforms of hate. This is indefensible; this is cowardice; this is causing unnecessary pain and suffering; this is evil.

Bezos, former CEO of Amazon, also could have done something instead of giving a million dollars to an inauguration fund. Instead, he is doubling down and leaning into intolerance. Workers in Quebec [formed a union and were punished](https://montreal.citynews.ca/2025/01/24/amazon-closing-quebec-warehouses-federal-government-could-review-relationship/) leading to "about 3,000" job losses. He exploited his workers, the current CEO (Andy Yassy) exploits workers. He owns a newspaper and has [put his finger on the scale](https://www.npr.org/2024/10/28/nx-s1-5168416/washington-post-bezos-endorsement-president-cancellations-resignations) on [more than one occasion](https://www.npr.org/2025/01/04/nx-s1-5248299/cartoonist-quits-wapo-over-bezos-trump-cartoon-washingtonpost). This is indefensible as well.

Sundar Pichai, CEO of Alphabet and Google---a search engine monopoly---and one of the most influential people in AI: what are you doing? I know Google's search results are iffy these days, but I'm sure you could have looked a few things up about the people you'd be standing with. You could have at least declined to be in the photo. Google---a company that once had the mantra, "Don't be evil"---you have lost any remaining credibility. Inexcusable.

The man on the *far-right* of that photo shall receive no further comment from me. He deserves no words because anyone who has been paying attention to the news knows exactly what he did later that same day. This [video by Led By Donkeys](https://www.youtube.com/watch?v=NjWl_RNDMSA) is enough of a primer if you're out of the loop. It is *indefensible* to downplay, in any way whatsoever, what happened. And, it is indefensible to continue using X as a social media platform.

On the 6<sup>th</sup> of October 1997, the episode "[Rocks and Shoals](https://en.wikipedia.org/wiki/Rocks_and_Shoals_(Star_Trek%3A_Deep_Space_Nine))" of *Star Trek: Deep Space Nine* aired. In it, Major Kira realizes that by not fighting an occupation, she has become a collaborator. She says,

>**If you're not fighting them, you're helping them.**

Zuckerberg, Bezos, Pichai, Cook, Yassy, Altman, and others: they are not fighting, they are helping. They have wealth and influence, and they have decided that they need more. [Crony capitalism](https://en.wikipedia.org/wiki/Crony_capitalism) wins again.

> Many of the actions by which men have become rich are far more harmful to the community than the obscure crimes of poor men, yet they go unpunished because they do not interfere with the existing order.
>
> <cite>---Bertrand Russell</cite>

## Stop Helping Them

It is time to stop helping them.

This is not an American issue. These west-coast American monopolies have forced their culture upon nations around the world. We don't have to accept it.

Quit Facebook, and Instagram. Stop using Amazon. Use a different search engine. Don't buy that new iPhone. (Please) Stop using OpenAI and ChatGPT.

There are alternatives. And *necessity is the mother of invention*. More alternatives will come.

Schools, students, teachers: you need to stop using Facebook, Instagram, and WhatsApp for events and associations. Stop locking yourself into a platform that forces others to use it.

Get your parents and grandparents off of these platforms while you're at it.

Will people refuse? Of course, they will. Some people have businesses on these platforms. Some people make a living from these platforms, and I truly feel for you. If you are so apathetic about the suffering of others and dedicated to making the richest people even richer, so be it.

Amazon hurts local businesses. Amazon hurts its employees. Stop buying everything on Amazon. If you can, quit Amazon and Prime.

Use something else as a search engine. [Mojeek](https://www.mojeek.com/) is nice, but [there are others](https://seirdy.one/posts/2021/03/10/search-engines-with-own-indexes/#general-indexing-search-engines).

Buy your next phone used, or try and keep your phone for at least five years.

And just stop using anything OpenAI. In fact, you can stop using Generative AI altogether. They are not search engines, they are not intelligent, and they are flooding the Internet with slop.

## Conclusion

These might seem like the extreme ramblings of a radicalized person. Perhaps that's what I am.

Did I spend too much time watch *Star Trek* as a kid? Was it *Star Wars*? *Transformers*? Was it reading *X-Men* comics that did it to me?

If it was, I was certainly too naive to realize it at the time. Maybe it was Rage Against the Machine?

I am not a millionaire or billionaire. Nor am I the CEO of a platform with global reach. My pulpit is microscopic, but it would be indefensible and hypocritical for me to continue using, or promote the use of, any of these large platforms.

## My plan of action

My personal plan of action is the following:

- [X] Quit LinkedIn.
  - My [CV](/mycv/) will be available on this site.
- [ ] Fully quit Amazon.
- [X] Refuse to use Generative AI products.
  - So far, so good!
- [X] Refuse to use Meta products
- [X] Use alternative Search Engines.
- [X] Use alternative Social Networks.

[^1]: there answer is [here](https://en.wikipedia.org/wiki/Fascism) I think...
