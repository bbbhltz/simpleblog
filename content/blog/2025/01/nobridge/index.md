---
title: "Adding #nobridge to this website"
subtitle: "Keeping Bridgy Fed off your site"
description: "How to add a #nobridge tag to your website. This is an example of how to add that to a site made with the Hugo SSG"
slug: nobridge
date: 2025-01-02T12:08:30+01:00
lastmod: 2025-01-06
featured_image: /blog/2025/01/nobridge/featured.webp
toc: false
bold: false
katex: false
tags:
  - Blog
  - Social Networking
  - Web
next: true # show link to next post in footer
nomenu: false
notitle: false
draft: false
syndication:
  - https://framapiaf.org/@bbbhltz/113758626993031031
---

snarfed (Ryan Barrett) has a neat set of [projects](https://snarfed.org/software). [Bridgy Fed](https://fed.brid.gy/) and [Bridgy](https://brid.gy/) connect your ActivityPub accounts and websites to Bluesky, for example. You can post on your site and *poof* it is shared and people can comment on it and share it. 

I can see why people would find this great, and I tried it out for a bit. I bridged my Mastodon account and my website. Very quickly, I realized that bridging my website was not for me. It shared things that were not ready for the public (hidden slash pages that I wasn't done working on, for example) and if I am going to share a blog post, I usually wait until a certain time to do it.

More problematic, though, was bridging my Mastodon account. I am not a prolific tooter, but I do share a few posts. It dawned on me after noticing that at least 10 of the users I follow have `#nobridge` in their profile: 

**Was I committing a fedi-faux pas?**

I decided to opt out. For Mastodon, the [instructions](https://fed.brid.gy/docs#opt-out) were easy enough. For the website, it wasn't clear. I ended up emailing snarfed to nuke my bridge, and they took care of it very quickly. 

They also responded to my question of *how the hell do I add `#nobridge` to my site?*

>As for #nobridge, you'd put it in your microformats2 h-card's p-name or p-summary property

Well, *obviously*, right?

## ¿Qué?

![darmok meme](featured.webp "Microformats2, when the bridge fell")

Apparently, what this means is adding a few bits to a website. I updated the following lines in my ~~footer~~ different [partials](https://gohugo.io/templates/partial/) after reviewing the [instructions](https://microformats.org/wiki/microformats2#h-card):

{{< tabs >}}

{{< tab "layouts/partials/header.html" >}}
```html {linenos=inline,linenostart=89}
<div style="display: none;">
  <a class="u-url" href="{{ .Permalink }}">{{ .Permalink }}</a>
  <span class="h-card p-author author vcard">
    <a href="https://bobbyhiltz.com/" rel="author me" class="u-url u-uid url">
      <span class="p-name fn">Bobby Hiltz</span>
      <a class="u-email" href="mailto:contact@bobbyhiltz.com">contact@bobbyhiltz.com</a>
      <img src="{{ .Site.BaseURL }}favicon-32x32.png" class="u-photo photo" alt="hcard photo for bobbyhiltz.com" />
      <span class="p-summary">#nobridge #nobot</span>
      <span class="p-note">English Teacher, Open Source Enthusiast, #nobridge #nobot</span>
    </a>
  </span>
</div>
```
{{< /tab >}}

{{< tab "layouts/blog/single.html" >}}
```html {linenos=inline,hl_lines=["1-8", 12, 17, 20]}
<article class="h-entry hentry">
  {{ if .Params.Syndication}}
  <div style="display: none;">
    {{ range $syndication := .Params.Syndication }}
    <a rel="syndication" class="u-syndication" href="{{ $syndication }}">{{ $syndication }}</a>
    {{ end }}
  </div>
  {{ end }}
  {{ partial "header.html" . }}
  {{ if not .Params.notitle }}
  <div id="single-header">
    <h1 class="p-name">
      {{ .Title | markdownify }}
    </h1>
    <div id="single-meta">
      {{ if .Params.description }}
      <p id="subtitle" class="p-summary">{{ .Params.description }}</p>
      {{ end }}
      {{ if .Params.date }}
      <span class="datesub  dt-published">{{ .Date.Format "2006-01-02" }}</span>
      {{ end }}
      {{ if .Lastmod }}
      <span class="datesub"> &nbsp;&nbsp; m. {{ .Lastmod.Format "2006-01-02" }}</span>
      {{ end }}
      <span class="datesub"> &nbsp;&nbsp; about {{ .WordCount }} words / {{ .ReadingTime }} minutes <br>
        <a href="https://archive.today/{{ .Permalink }}">archive.today</a>
      </span>
      <br>
      {{ partial "tags.html" .}}
    </div>
  </div>
```
{{< /tab >}}

{{< tab "layouts/partials/tags.html" >}}
```html {linenos=inline,hl_lines=[6]}
{{ $taxonomy := "tags" }} {{ with .Param $taxonomy }}
<span id="tags">
  {{ range $index, $tag := . }} {{ with $.Site.GetPage (printf "/%s/%s"
  $taxonomy (urlize $tag)) -}}
  <span>
    <a href="{{ .Permalink }}" style="text-decoration: none;" class="p-category">{{ $tag | urlize }}</a>
  </span>
  {{- end -}} {{- end -}}
</span>
{{ end }}
```
{{< /tab >}}

{{< /tabs >}}

It was unclear whether I should use **p-note** or **p-summary** for the `#nobridge` part, so I used both. One of them is probably incorrect. I continued on with the Microformats2 fields and added a few other lines to be able to pass the [h-card validator on indiewebify.me](https://indiewebify.me/validate-h-card/) and then further added some other lines to my layouts to comply with [h-entry microformat](https://microformats.org/wiki/microformats2#h-entry).

(I use a *Syndication* parameter in the front matter of my posts.)

{{< box info >}}
**UPDATE**

According to snarfed:

> Bridgy Fed recognizes #nobridge in microformats hCards, microformats2 h-cards, and HTML meta tags for og:description, twitter:description, and description.

So, that makes things easier!
{{< /box >}}


If you want to keep your website from being bridged, this is something you should look into. It is, in my opinion, unfair that this is *opt out* rather than *opt in*. It took me longer than it should have to sort this out.

Once it is set up, you should be able to head over to [https://fed.brid.gy/](https://fed.brid.gy/) and check.

**Also, please let me know if this is incorrect. I only learned what an h-card was yesterday.**
