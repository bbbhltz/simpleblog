---
title: "IndieWeb Carnival: multilingualism in a global Web"
subtitle: "French Immersion took me places"
description: "A response to the October IndieWeb Carnival, the author talks about French Immersion and their personal and professional life"
slug: multilingualism
date: 2024-10-19T14:45:18+02:00
# lastmod:
# featured_image: /blog/2024/10/multilingualism/featured.webp
draft: false
katex: false
tags:
  - Language
  - Education
next: true
nomenu: false
---

*This is my entry for October’s [IndieWeb Carnival](https://indieweb.org/IndieWeb_Carnival), hosted by [ZinRicky](https://tilde.team/~zinricky/multilingualism/).*

***

I come from a tiny village on Nova Scotia's south shore. In case you didn't know, that is the east coast of Canada (America's toque).

Us village-folk speak English, or something close enough to English that we get by. Like almost any country of a certain size, there are regional differences, but that is not what we're here to talk about today.

The theme today is about multilingualism in a global Web, and that quaint little English-speaking village is no longer my home. I live in Europe now, France to be specific. But, I wouldn't be living here if I hadn't grown up in that little village and gone to school there and been part of the French Immersion program.

For six years I took all of my classes in French. I didn't know it, but I was functionally bilingual after those six years. I found out when I made it to university and spoke with native speakers of French. I continued studying French at university and even took part in a project that underlines the importance of multilingualism and the Web.

I helped out with a research project about online learning. It was a straightforward website, with some Flash elements, about learning a specific grammatical feature of French[^1]. Lo and behold, nobody working in the IT department could speak French, and the professor doing the research needed someone who could speak French. So, I helped make the site.

This was in 2005. How many people before or since were unable to share something on the Web due to a language barrier?

Shortly after university I moved to France. I happened to arrive on the eve of Facebook opening the doors to the public. My university was granted access to Facebook in 2004 or 2005. In 2006, none of the French people I was meeting had ever heard of it. They were still enamoured with Myspace and [Skyrock.com](https://en.wikipedia.org/wiki/Skyrock_(social_network_site)).

Question: Was Facebook available in French in September 2006?

**a.** yes \
**b.** no \
**c.** yes, but only Québécois French

If you answered **b**, you're right, and we can skip right to the part where the French all made accounts and collectively proclaimed, with great exasperation, that Facebook is *nul* [^nul]. It was *nul* because it was only in English (and maybe Spanish?) and you couldn't change the colours like Myspace. It was *nul* because even if you created an account, your friends were not, because it was in English only, or because they had already tried it and quit because of the English.

Furthermore, it was also during this period (fall semester 2006) that a French friend and colleague saw me working on my laptop and said, "In France, we are bad at computers and the Web, because we are bad at English. You should stop teaching English and do something with computers here, you could make a lot of money."

I cannot say that the French are bad at computers. I don't think they are that bad at English---they just don't get enough practice. I can sympathize with the statement about language barriers. It is awfully true that knowing English helps me enormously.

Being about to speak, read, and write French and use it on the Web, though, is a powerful thing. The quickest example I could give is Wikipedia. Very often, there are articles on Wikipedia where the English version is the most detailed (i.e. the longest), but the French one is the better version for being concise. Sometimes the sources are different too, so that is great if I'm actually trying to *research* something.

While, the *lingua franca* of the Web will likely always be English, I would have been disheartened to have not learned French and miss out on so much. The French Web, and all of its jargon, patois, argot, and parlance made the years of study worthwhile. 

[^1]: and it still lives: https://plato.acadiau.ca/courses/fren/tutor/tutor2005/
[^nul]: it sucks
