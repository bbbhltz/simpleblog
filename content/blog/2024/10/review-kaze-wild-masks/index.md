---
title: "Review: Kaze and the Wild Masks"
subtitle: "Run, Jump, Repeat"
description: "A positive review of the sidescrolling video game Kaze and the Wild Masks"
date: 2024-10-27T10:22:13+01:00
slug: review-kaze-wild-masks
featured_image: /blog/2024/10/review-kaze-wild-masks/kazelogo.webp
toc: false
bold: false
katex: false
tags:
  - Review
  - Video Games
next: true # show link to next post in footer
nomenu: false # hide site menu
notitle: false # hide title
draft: false
toc: true
syndication:
  - https://beehaw.org/post/16752976
---

![Logo](kazelogo.webp)

**Released on**: 2021-04-26 \
**Developer**: PixelHIVE / SOEDESCO\
**Official Website**: [https://www.playkaze.com/](https://www.playkaze.com/)

*Getting straight to the point, **Kaze and the Wild Masks** is a *90s classic platformer*. That means lovely pixel art and well-crafted sprites---among other things---that stands out where it matters.*

## Review

I'm a sucker for this exact style of game. Back in the day we'd head down to the local rental place and pick up a game for the weekend. We generally had from Friday evening until Sunday evening to enjoy it. Kaze would have fit in perfectly back then---although I'm not sure if I would have been able to 100% at the time.

![Kaze Copter](copter.webp "Kaze using her ear-copter to collect gems (via press kit)")

The gimmick here is as follows: Imagine a DKC-like game. There are gems to collect, enemies to jump and bounce on, different worlds with themes, challenge/precision levels, bosses. However, instead of having an animal helper, as in the DKC series, you have the masks. The Tiger, Eagle, Lizard, and Shark masks give you additional abilities that you will need to master to complete the 50+ levels.

This is a game that leans on its strengths. The visuals are fantastic for the genre. Animations are smooth, and they don't stop at the character sprites. Backgrounds and the overworld map also look great. Use of classic gaming animation techniques, like parallax backgrounds, draw you into the 16-bit experience.

Everything sounds amazing too. The songs are catchy, and the other effects add depth to the action and help you time your jumps and movements. An amazing soundtrack for an action platformer like this.

Any 90s game worth its rental fee needs---**NEEDS**---tight controls. Kaze's jump, float, and other abilities are easy to learn, but hard to master. Controlling Kaze is never a struggle, though. Every level requires *muscle memory*, and you'll need to time everything just so if you plan on getting 100%. Fortunately for Kaze, this game has spot-on perfect controls.

![Kaze eagle](eagle.webp "Kaze with the Eagle Mask (via press kit)")

Finally, like having great controls, 90s games had to hope players would want to play again. If you wanted someone to shell out some cash on a game or wanted the best reviews, your game needed *replay value*. Would you play this game again and again to get 100%? Would you start another save file and do it again? I definitely *needed* to 100% this game. My son, sat on the couch next to me, urged me on through nearly all the levels. When I lost, did I give up? No. I tried again and again until I could finish the level. Then I played it again to get all the collectables, or find the secrets.

## Verdict

Overall, Kaze is a great platformer with enough challenge for laid-back casual gamers nostalgic for the days of weekend rentals.

I highly recommend picking this game up if you ever see it on sale, and even if you can't get it on sale, the value for money is very high.

**18/20**

{{< youtube id=5fFry9TnXy8 title="Kaze and the Wild Masks Trailer" >}}
