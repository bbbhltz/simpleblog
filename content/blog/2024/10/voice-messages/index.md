---
title: "My Beef with Voice Messages"
subtitle: "a quotidian aggravation"
description: "A blog post complaining about the use of voice messages when"
slug: voice-messages
date: 2024-10-15T13:03:33+02:00
# lastmod:
featured_image: /blog/2024/10/voice-messages/featured.webp
draft: false
katex: false
tags:
  - Technology
next: true
nomenu: false
---

Scrolling through Mastodon a [post](https://mastodon.social/@mariejulien/113300334251313568) caught my eye.

To save your the trouble of translating, the gist is this:

>A voice message is the height of selfishness, it transfers the burden completely onto the person who didn't ask for it.
>
>What's more, 90% of the time it starts with, "I'm sending you a voice message because it'll be easier for me."
>
><cite>--- @mariejulien@mastodon.social</cite>

Now, I'm not sure how this revelation escaped me for all these years, but it hit me like a brick. 

I don't get many voice  messages. Actually, I don't receive loads of messages at all. That's beside the point. The real point of this quick post is point out the obvious:

While voice messages have a use, they have invaded our lives.

We've all seen people talking to themselves while holding their phone in that tell-tale position---you know, palm pointing to the sky, arse-end of their phone either pointing to their mouth or to their ear. While driving, while in public places, and even while eating.

90% of the messages my partner receives are vocals. Luckily you can speed them up. But still, what's the deal with eight-minute long voice messages? Is it a podcast or a conversation? How are you supposed to *converse* with a cascade of words?

I could say this is an "old man yelling at clouds" situation, except that over time, I have started doing it too! Only with my partner, mind you, but something peculiar has happened over time.

The messages get longer!

The first time I gave in and sent a voice message, it wasn't more than a sentence fragment to let them know I would be late. Now? Now I leave audio books that I could sell to BBC or NPR. If I downloaded our exchanges of voice messages, it would be like a soap opera of old.

I'm sure there are some people studying this. It seems like a topic worth studying. At the height of SMS messaging, when people still didn't have unlimited SMS credit, we learned to be concise. And, during the heyday of ICQ, I recall an anecdote recounted by a colleague.

The colleague had twins who were in their early teens. For fun, one evening my colleague installed ICQ on the family computer. Over the course of a week my colleague would leave and receive messages. He was a changed man after that. He assumed that instant messaging would be *silly* and that his children would have trouble communicating clearly. They didn't. He learned about his children's lives, their friends, school, and so on, by leaving them written messages that they could reply to on their own time.

I think we might be losing something by relying on voice messages, and I'm pretty positive that it is disruptive and annoying in public places. I still communicate with nearly everyone via written messages, but some colleagues have begun using voice messaging. How am I supposed to find the information I need if it is in some audio file that cannot be searched.

I'm therefore giving myself a little challenge: no voice messages for a month!

If you feel like sending me a written message on Signal, my contact is `bbbhltz<dot>48`.
