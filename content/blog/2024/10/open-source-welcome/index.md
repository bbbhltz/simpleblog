---
title: "Open Source Welcome"
subtitle: "All* are welcome (* some conditions apply)"
description: "A blog post lamenting the phenomenon within the open source community of gatekeeping users for not being open source enough"
slug: open-source-welcome
date: 2024-10-29T10:59:59+01:00
lastmod: 2024-11-07
# featured_image: /blog/2024/10/open-source-welcome/featured.webp
toc: false
bold: false
katex: false
tags:
  - Open source
  - Technology
next: true
nomenu: false
notitle: false
draft: false
---

Hi,

Welcome to the wonderful world of open source!

You may not know it, but you have probably been using open source software on your computers for quite some time.

Do you know a website called YouTube.com?

Yes?

Well, it turns out that despite being a monster of a website, and one of the many Alphabet properties, some of the technology under the hood is open source, like [FFmpeg](https://ffmpeg.org/).

Neat, huh?

More directly, you've maybe used things like [VLC](https://www.videolan.org/vlc/) or Google Chrome (which is based on [Chromium](https://www.chromium.org/)), which are both open source. If you are a student or teacher who has used [Moodle](https://moodle.org/), you've used open source software. Have you ever emulated a game? Many of those emulators are open source, like [RetroArch](https://www.retroarch.com/). And, guess what, even Snapchat's map is open source because it uses [OpenStreetMap](https://www.openstreetmap.org/).

Every single day, if you are using something connected to the web, open source technology plays a role: 

> [curl](https://curl.se/) is used in command lines or scripts to transfer data. curl is also libcurl, used in cars, television sets, routers, printers, audio equipment, mobile phones, tablets, medical devices, settop boxes, computer games, media players and is the Internet transfer engine for countless software applications in over twenty billion installations.
> 
> curl is used daily by virtually every Internet-using human on the globe. 

Using open source software is a veritable way of life. If you want to be part of our community, there are a few things you need to know...

{{< box warning >}}
**INCOMING SARCASM**
{{< /box >}}

Firstly, you should probably just not use Windows any more. Stop using proprietary software altogether. Do not buy that new Apple device.

What you *want* to do, friends, is acquire devices to specifically use open source software with them. Get a computer, install Linux on it. Not dual-boot, not from RAM or live mediums, and not a virtual machine; wipe that drive and install Linux on it. Not Ubuntu, though.

Then get yourself a phone that works with [GrapheneOS](https://grapheneos.org/), because you need to completely deGoogle your life. Also, it goes without saying that using open source is also committing to digital privacy.

That's right, you'll need to give up your social networks like X and Facebook and Instagram. Get on the [Fediverse](https://en.wikipedia.org/wiki/Fediverse)!

But, none of those big major instances.

No, no, **NO**...

That's *not* how it is done.

*Apologies, I got carried away there...*

You need to get on one of the *cool* instances.

Who decides what is cool? Strangers on the Internet, of course.

Oh, yes! Judgement will be upon you, friends. But it will make you stronger.

Perhaps you think you're doing fine. Literally every piece of software you use is open source! Some of them you've even compiled yourself. Well done, friend.

But...

What's that I see there?

Yup, thought so...

Are you using *Brave* as a browser? **BRAVE?** Don't do that.

Why not?

Sit tight, I'll prepare public thread to explain why...

[28 messages later...]

...and, in conclusion, that is why nobody should use Brave.

Now that you are browsing the web the *right* way. Do not, under any circumstance even *visit* or let your mouse hover over the link to any of these places:

* Google or Microsoft Forms,
* Medium,
* GitHub,
* Reddit,
* X,
* Facebook,
* Bluesky
* YouTube,
* LinkedIn,
* etc.,
* etc.,
* etc.,
* or any site with a paywall.

Next comes a fundamental pillar of being open source: Spreading the word!

It is now your turn to judge!

Someone asks a question about software? Tell them to install Linux.

Someone shares a survey link, but the link is a Google Form? Let them have it!

Don't stop there, though. Now is the time to proselytize. Get a blog. No, not Medium or Substack; self-host your own, and don't forget the RSS feed!

This is the way, friends, to begin your path to becoming a *true* open source user. 

Practice will make perfect. Don't forget to take up every opportunity to *judge*, *correct*, *instruct*, *shame*, and *tell strangers on the Internet that they are doing it wrong*. Become the greatest of open source reply guys!

Please fill out the Framadate below to sign up for my next conferences:

* Everything can be political if you want it to be
* Exceptions can be made for me, but not for thee
* That bearded guy didn't do anything wrong
* If you disagree with me, I'll block you (or, When playing chess with a pigeon, it's better to be the pigeon)
* Magic Internet points and followers means you can be an ass to anyone
* Advanced users only: Submitting issues to GitHub to tell the devs to stop using GitHub
