---
title: "O BlackBerry, Where Art thou?"
subtitle: "(a belated elegy)"
description: "A nostalgic blog post about BlackBerry phones and how much easier it seemed to deal with emails and work-related tasks."
slug: blackberryelegy
date: 2024-11-15T15:05:17+01:00
featured_image: /blog/2024/11/blackberryelegy/featured.webp
toc: false
bold: false
katex: false
tags:
  - BlackBerry
  - Technology
next: true # show link to next post in footer
nomenu: false
notitle: false
draft: false
syndication:
  - https://jstpst.net/f/just_post/11834/o-blackberry-where-art-thou
---

![BlackBerry Classic Patent US D781,801 S](featured.webp)

A plasticky sounding vibration on my wrist gets my attention, followed in canon by alerts on my phone and the open tab on my browser.

*It has only been 75 days since I accepted the temporary mission of keeping this ship on course. The ship is the English department of my university. I am not alone on my quest like Odysseus was, thankfully. This ship needs two captains to avoid the dangers, for Scylla and Charybdis have modern forms that require careful navigation to avoid.*

My watch and phone give me a preview of the message. Do I dare open it and mark it as read? What if it is urgent? Is it a 5-minute affair, a "<kbd>CTRL+C</kbd> <kbd>CTRL+V</kbd> <kbd>CTRL+ENTER</kbd>" situation? Would I be sacrificing a few of my precious minutes to the Calabrian shoal, or would this steer me in the direction of a whirlpool that would suck up more time than necessary?

We should all be circumspect when dealing with emails. I keep mine colour-coded and never mix personal and professional inboxes. With caution, I gingerly pull down the shade on my phone. The Siren promises to reveal the future.

*By Poseidon!* This email is important, urgent, and specifically directed at me.

*I begin to circle Charybdis' maw.*

I begin my reply on my phone, but midway through it becomes clear that is a job that requires a keyboard. Draft saved and synchronized, I pick up where I left off. My work computer, however, has clearly been partaking in the eating of lotus leaves and lost track of time. Without warning, the screen goes black.

*Spinning; circling; down I go. This isn't the first time I've been in a situation like this. Once upon a time, there was a branch overhanging this deadly whirlpool that eats my time. A fig, if I recall. Or, perhaps it was a blackberry bush.*

*My inner chorus laments. It cries out, "O BlackBerry, Where Art Thou?"*

Before we were possessed by rectangles that barely fit in our pockets and swayed by the number of apps on their app store, there was another. An honourable tool (weapon?) worthy of a task like this. Its 35 clicky keys, trackpad, and other shortcuts would have made short work of this!

*Yielding, I raise my arms in the air and curse Poseidon. When my arms fall back to my sides, my hand taps against something in my pocket. Patting my trousers down, I realize there is something there. A wallet? The size is about right.*

*I extract and examine the mystery object. It is indeed a leather pocket. Flipping it over I see seven familiar shapes forming a symbol...*

<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" width="200" height="200" stroke-width="0.75"> <path d="M7 6a1 1 0 0 0 -1 -1h-2l-.5 2h2.5a1 1 0 0 0 1 -1z"></path> <path d="M6 12a1 1 0 0 0 -1 -1h-2l-.5 2h2.5a1 1 0 0 0 1 -1z"></path> <path d="M13 12a1 1 0 0 0 -1 -1h-2l-.5 2h2.5a1 1 0 0 0 1 -1z"></path> <path d="M14 6a1 1 0 0 0 -1 -1h-2l-.5 2h2.5a1 1 0 0 0 1 -1z"></path> <path d="M12 18a1 1 0 0 0 -1 -1h-2l-.5 2h2.5a1 1 0 0 0 1 -1z"></path> <path d="M20 15a1 1 0 0 0 -1 -1h-2l-.5 2h2.5a1 1 0 0 0 1 -1z"></path> <path d="M21 9a1 1 0 0 0 -1 -1h-2l-.5 2h2.5a1 1 0 0 0 1 -1z"></path> </svg> 

*Athena! It must be. She has taken pity on me in my time of need. Tears well up in my eyes. I slide the device from its cosy resting place, like a sword from its sheath, admiring the work of Hephaestus in the palm of my hand. The sun reflects off the square screen, Charybdis screams in terror. The BlackBerry Hub is there, waiting. "I remember you," I whisper. Muscle memory kicks in. My thumbs assume the position (although years have passed, the callouses remain) and they run across the keys like Hermes. A symphony of clicks drowns out the sounds of the waves. Words appear on the screen, 96 of them every minute. Autocorrect cannot hinder me here. Joy and sadness intermingle within me---is this nostalgia?*

*It is done. I am saved. My thumb hovers over "Send" and... it has disappeared! The BlackBerry has disappeared! Witchcraft! Another trick by Circe, surely. A ruse. I am not saved; I am done for.*

Begrudgingly, I complete the mundane feat. Spreadsheets from a Teams channel are attached (couldn't they find them themselves?), checking an email sent in 2023 (couldn't they search themselves?), and searching the directory for the email address I need take longer than expected. I fill up my clipboard with fragments and words and use the unintuitive gestures to place everything in the right order (the trackpad was so much better for this). The sun is setting. Lumbago threatens to ruin my evening. My wrists feel strained from holding the rectangular device for too long.

I toss it on the coffee table with intentional force and stare at the ceiling contemplating the series of decisions that it took for BlackBerry to f-ck up so royally. They shit the bed and left us with the mess, between a rock and a hard place, always making a sacrifice. No other option to choose from---no, feature phones don't count---just variations on rectangles. The treated us like complacent consumers, and so that is what we became.

The past few weeks have left me feeling very nostalgic for my BlackBerry. Answering at least 50 emails each and every day is maddeningly frustrating on a touch screen. I even miss BBM, even though it wasn't really a safe way to communicate. I miss the BlackBerry Hub, even though it can be emulated on Android.

[Malikie Innovations](https://www.malikieinnovations.ie/), you own 32,000 BlackBerry patents, what are you doing? Just give me a phone with a keyboard and email that can read and write the standard office documents. Replace BBM with Signal. F-cking Punkt. has [has a phone with buttons and Signal](https://www.punkt.ch/en/products/mp02-4g-mobile-phone/#pigeon). Why can't somebody else have a phone with buttons?

Nobody else wants this, I know. And I've seen the [Titan](https://www.unihertz.com/en-fr/products/titan). I don't actually want to buy a new phone, it would just be a pleasure to see something other than Androids and iPhones on the shelves in the stores. We fell for something shiny and new, and the tried and true withered and died when they attempted to keep up with the competition. Gone, but not forgotten. Adieu, BlackBerry.
