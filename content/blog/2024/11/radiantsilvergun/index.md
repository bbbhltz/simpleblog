---
title: "Radiant Silvergun"
subtitle: "What the hell did I just play?"
description: "A blog post reviewing, commenting, and praising the classic 1998 shmup Radiant Silvergun"
slug: radiantsilvergun
date: 2024-11-07T15:52:53+01:00
featured_image: /blog/2024/11/radiantsilvergun/tengaiBW.webp
toc: true
bold: false
katex: false
tags:
  - Review
  - Video Games
next: true # show link to next post in footer
nomenu: false
notitle: false
draft: false
syndication:
  - https://beehaw.org/post/16936582
  - https://jstpst.net/f/games/11738/radiant-silvergun

---

This week I tried a new game.

New to me, I should say. I tried a game that was released in 1998 on the Sega Saturn. The Saturn was one of those consoles that I never bothered to even glance at because at the time I only owned a Game Boy and a Super Nintendo. Also, I think this might have been a Japan-only release.

![Radiant Silvergun Ship Drawing](ship.webp)

As such, I never even heard of the game **Radiant Silvergun**. In fact, the first time I saw the name of this game was just a few weeks ago when I decided that I would try may hand at *shmups*, or *Shoot 'em ups* or *STG* if you prefer. Instead of organically discovering the genre---the good and the bad---I made a list on Deku Deals based on recommendations I found listed on forums and generic best-of lists.

It had been an enjoyable couple of weeks until **Radiant Silvergun** went on sale on the Nintendo eShop at 50% off.

People, this game is on all the lists. Sometimes at the top, and when it isn't at the top it is lauded and praised as a game-changer---a rebirth of the genre! No review hesitates to bring up the lore surrounding the game and the studio, [Treasure](https://en.wikipedia.org/wiki/Treasure_(company)). Described as more than a shmup, it is a "puzzle shooter."

Naïve as can be, I purchased this so-called marvel and waited for the 899 MB download to complete.

I launched the game a played for a bit.

![Missiles launching screenshot](Missiles.webp "Find a safe space")

I wasn't impressed.

It was everything it was supposed to be, though: Challenging, bundled with the quality of life features from the Xbox port, tight controls. I just didn't feel *it*.

Well, that's because I was looking at it all wrong.

Many shmups send you through a few levels, that have a boss at the end, and then loop that. This isn't the case here. This is meant to be a single, long, non-stop, boss rush, with a story. When I tried again I finally got it. I understood.

And did I ever fall in deep...

## Review

![Warning Incoming Nerdout](warning-glitch.webp)

Ok.

Right.

So, here's the thing: We're all about labelling things. We want things organized. Our libraries have sections, the music store has genres, we have sock drawers, and most of us have a junk drawer. Video games are like to do this too.

**Radiant Silvergun**, isn't *exactly* a shmup (as mentioned above). There is no drawer for it. It is an adventure game that happens to take place in a ship, in space, and you survive by shooting other things in space. But, here's another thing I missed: you are rewarded for your play time and *how* you play.

I'm sure someone out there would call the story mode of this game a shmup-roguelike mash-up. When you lose, you carry over your weapon upgrades---I know, I skipped the mechanics, but I'm still shell shocked, so forgive me---to the next run. When I realized this, I was blown away. You also gain lives for every 15 minutes of play.

Similarly, in the arcade mode, which doesn't have continues, you can earn ships (but not maintain weapon upgrades).

The mechanics of **Radiant Silvergun** differ from other shmups. It is common in this style of game to pick up weapon upgrades, or different weapons and bombs, while playing. You sometimes need to strategize and not pick up a weapon because you know the weapon you already have is better for the upcoming section.

**Radiant Silvergun** says "nah" to all that. You start off with **six** shot types and your *Radiant Sword* instead of a bomb. The shot types are upgraded throughout by earning points---destroying enemies---using that style of shooting. You can use your sword to attack and swipe away certain pink projectiles. Get ten of those, and you can use the *Hyper Sword*---this game's equivalent of a hyperbomb, or screen-clearing weapon.

The six shot types are separated into three styles: Vulcan, Homing, and Spread. The Vulcan is your generic forward-facing weapon, and your rear-facing weapon. Homing weapons include a rather weak homing laser, and a homing plasma weapon that can shoot through obstacles. The two Spread shots are very useful, shoots at a 22.5-degree angle from your ship and is 4x stronger than the standard Vulcan, the other is a lock-on laser weapon.

By the time I figured this out, I was hooked. Hooked on getting those points, getting the chains, and seeing the next boss or "assailant." The boss situation was very interesting for me. There are 24 bosses and 2 sub-bosses. Even on Very Easy difficulty and completed overpowered, this game offers a white-knuckle experience for players like me. I like a hard game, though.

![Ca2-ekzo](Ca2-ekzo.webp "Ca2-ekzo is one of the easier bosses")

The chain mechanic is the source of points, and best part of what makes this a puzzle game as well as a shooter.  All enemies are coloured red, blue, or yellow.  By destroying three enemies of the same colour in a row, you get a *chain bonus*. Continue doing that, every three enemies of the same colour will add a *step* to your multiplier (with some limits):

`min(min(totalScoreOfEnemiesInChain,10000)*step,100000)`

As mentioned before, this game does have a story. Shit went down on Earth, a cruiser on a mission races back to help and encounters enemies. So, logically, you begin at level 3 in the story mode, and then you go to level 2, then 4, 5, 6, and you end the game at 1 and get to watch an awesome little anime sequence[^arcade]. Then you strike a pose like this:

[^arcade]: In arcade mode there is a moment where you chose a route, and there are not 24 bosses.

![Guy Posing](guyBW.webp "Guy by Tetsuhiko Kikuchi")

And you ask yourself, "What the hell did I just play and why did I like it so much?"

You've just invested time and effort in flying through space going pew pew pew and swearing like a sailor. Some of those bosses you've only seen once. There are leader boards to fill, dammit!  You've got to dive back in. 

Dive like Ohtrigen...

![Ohtrigen from Radiant Silvergun](Ohtrigen.webp "I died just after taking this screenshot")

Because before magical Internet points, before online shops where you could purchase and download a game on a rainy Sunday, you had to wait for the shops to open. You had to wait to have money, and have someone to take you to the shop, maybe. Same-day delivery? Think six to eight weeks.

Games had to find a way to keep you playing. **Radiant Silvergun** does this with points, as mentioned above, but also other achievements. There are secret dogs (called *Merry*) hidden throughout the game, and the bosses, cannot simply be beaten. In order to 100-percent the game, you need to 100-percent the bosses.

![Varas from Radiant Silvergun](Varas.webp "Varas, because every shooter needs a space shark!")

## Verdict

Shmups are known for being addictive. **Radiant Silvergun** is addictive with high replay value. I liked the music---I mean, I didn't find it annoying. It certainly puts you in the mood to shoot things in space. The final battle sequence underlines the importance of good sound and music.

Graphically, the mix of 2D sprites and 3D polygons works. The main bosses have the monopoly on the polygons in this game. Another very important aspect in shmups is being able to see the projectiles the enemies shoot at you. Luckily, back in 1998, they seemed to have got everything right graphically.

I mentioned above, but I'll say it again. The controls are tight. Your hit-box is small and depending on the controller you have, you should be able to *scrape* between bullets and projectiles. I did play this on the Switch, so input lag is a thing, but after a while you get used to it.

Modern systems don't have the slowdown of older systems, so that is something I've seen people complain about. Luckily, after a certain number of hours you can unlock the speed options. I didn't use that. I wanted to see what was waiting at the end.

![Xiga Running in Radiant Silvergun](XigaRunning.webp "Xiga, one of the final bosses, running")

All said and done, I think I will plagiarize the words of Maximiliano Peñalver in *Next Level* magazine to complete my review:

>Lo Mejor: Excelente, Excelente
>
>Lo Peor: Nada, Nada[^nl]

[^nl]: https://archive.org/details/next-level-issue-1-nov-1998/page/n41/mode/2up

**20/20**

## Sources

Kwan, Alan. (n.d.). *Radiant Silvergun FAQ v1.0*. Retrieved from https://web.archive.org/web/20071229080445/http://home.netvigator.com/~tarot/Games/SvGQ1v10.txt

Radiant Silvergun. (1998 September). *Gamers' Republic*, Issue 04, 82--83. Retrieved from https://archive.org/details/Gamers_Republic_Issue_04_September_1998/page/n83/mode/2up and  https://archive.org/details/Gamers_Republic_Issue_04_September_1998/page/n84/mode/2up

Tetsuhiko Kikuchi (HAN). (n.d.). *Radiant Silvergun Works*. Retrieved from https://archive.org/details/radiant-silvergun-works
