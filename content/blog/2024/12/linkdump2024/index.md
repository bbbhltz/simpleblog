---
title: "Linkdump 2024"
subtitle: "A collection of mildly interesting links and bookmarks"
description: "An end-of-year listicle of links and other mildly interesting sites"
slug: linkdump2024
date: 2024-12-27T09:14:16+01:00
#featured_image: /blog/2024/12/linkdump2024/featured.webp
toc: true
bold: false
katex: false
tags:
  - Listicle
  - EOY
next: true # show link to next post in footer
nomenu: false
notitle: false
draft: false
syndication:
  - https://framapiaf.org/@bbbhltz/113724155472049736
---

This is not a "best of" list.

Below is a selection of links (sometimes annotated, occasionally interesting) that I have collected or bookmarked in 2024. I have filtered out articles that I saved for work as well as many articles that I saved for work (that would include loads of articles about the environment, circular economy, AI, etc., but I have left in a few). I have decided to share only [archive.today](https://archive.today) links, but I may come back and add Internet Archive links and even the originals.

*These are articles added to Wallabag to read later. In total 662 articles were saved in 2024. That's 1.8 articles per day.*

## Fun links...

- [NetPositive error messages](https://archive.ph/c3lzu)\
  *A cute selection of poems from the BeOS NetPositive browser. The errors were presented as haiku.*

- [Katy Perry Wins Trademark Suit Brought By Katie Perry In Australia](https://archive.ph/JPfNi)\
  *This one, from Techdirt, is just fun to read out loud.*

- [What Your Favorite ’90s Rock Band Says About the Type of Bored Suburban Dad You Are Today](https://archive.ph/k09Jz) & [What Your Favorite ’90s Band Says About the Kind of Bored Suburban Mom You Are Today](https://archive.ph/4rsuq)\
  *Well, McSweeney's is what it is. I must admit a few of these were spot on, though not all.*
  
- [How people laugh online](https://web.archive.org/web/20241225131213/https://restofworld.org/2023/how-people-laugh-online/)\
  *I am a native English speaker, my partner is Spanish, and we live in France. The way people type things online is a fun mess.*
  
- [Animal sounds in most nature documentaries are made by humans. How they do it and why it matters](https://archive.ph/LFoAM)\
  *Has videos. Funny and mildly interesting.*
  
- [Radio station baffled after 200-foot radio tower disappears](https://archive.ph/Wl8vc)\
  *A little mystery story that I recall making me laugh.*


## Environment...

*No comment needed here. I like whales because I have never seen one. I had to filter this section down otherwise the post would have been titled "Selected doomscrolling fuel."*

- [Plastic Industry Is Selling False Promise of New Recycling Tech. Don’t Buy It.](https://archive.ph/tImhO)
- [Whales Are Dying but Not from Offshore Wind](https://archive.ph/iYQ5k)
- [How to Talk to Whales](https://archive.ph/vtrJe)


## Tech...

*As above, I had to reduce the number of links in this category.*

### AI dunking...

- [I Will Fucking Piledrive You If You Mention AI Again](https://archive.ph/bQlrw)\
  *Well written, and funny too.*
  
- [The Sam Altman Playbook](https://archive.ph/GlsxT)\
  *I made a bet with my students that this guy would be in trouble by 2027. Big trouble.*

- [Just how rich are businesses getting in the AI gold rush?](https://archive.ph/4LYSm)

- [Note to Our Energy Sucking Overlords](https://archive.ph/8YbWk)

### Misc.

- [How TED talks became the Picotop of millennial intellectualism](https://archive.ph/pUrxn)\
  *Big truthbomb here. TED Talks used to be a big deal in my work, now we hardly use them.*
  >To have been young and thoughtful in the late 2000s was, in many ways, to have been a citizen of the TED nation — a community of dreamers more than doers, united by a common creed: that ideas matter, that inspiration is power, that the future belongs to those who can capture imaginations. Call it naïve if you like. But TED’s prominence said as much about the aspirations of a generation as it did about the force of their ideas. Whatever its flaws or foibles, it shaped how we thought about ourselves. It held out the shining possibility: you, too, could have an idea worth spreading. You, too, could be special.

- [Multitasking in video meetings causes the same cognitive drop as smoking weed](https://archive.ph/ShBLe)
  >If you have nothing to say, if there are no actions, if there are too many people, and if you have nothing to learn, you may as well skip the meeting rather than multitask, as you shouldn’t have been at the meeting in the first place.
  
- [What To Use Instead of PGP](https://archive.ph/MARoU)\
  *I learned a lot reading this. As a non-dev, non-infosec computer user with an interest in online privacy, I was convinced that using PGP was good enough. Now I know better. This isn't just an opinion piece, there are plenty of sources to help with the arguments laid out here.*
  

## Good to know...

- [How to Pronounce Chinese Names a Little Better](https://archive.ph/jDeg3)\
  *I have lots of Chinese students, I always try to pronounce their names as best as possible.*
  
- [Fuck the Cult of Productivity](https://archive.ph/NWIrJ)
  >We live in a world that worships at the altar of productivity. From the moment we wake up to the sound of our alarm clocks, to the final emails we send before collapsing into bed, we are caught in an endless cycle of tasks, deadlines, and expectations.  

- [How much growth is required to achieve good lives for all? Insights from needs-based analysis](https://archive.ph/aGehH) \
  *Related to the post above* 
  >Provisioning decent living standards (DLS) for 8.5 billion people would require only 30% of current global resource and energy use, leaving a substantial surplus for additional consumption, public luxury, scientific advancement, and other social investments. Such a future requires planning to provision public services, to deploy efficient technology, and to build sovereign industrial capacity in the global South.

- [How we almost ended up with a bull’s-eye bar code](https://archive.ph/KYYGn) \
  *Could you imagine?*

![The bull’s-eye bar code introduced in Woodland and Silver’s 1949 patent. Source: U.S. Patent and Trademark Office.](https://images.theconversation.com/files/564563/original/file-20231208-27-rpouoq.jpg "The bull’s-eye bar code introduced in Woodland and Silver’s 1949 patent. Source: U.S. Patent and Trademark Office.")

