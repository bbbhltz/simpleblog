---
title: "Education Rant: Solutions need problems"
subtitle: "We are overloading teachers and students with apps and accounts"
description: "A blog post on education and the multiplication of applications and websites that teachers need to use to do their job"
slug: educationsolutions
date: 2024-12-14T15:01:03+01:00
# featured_image: /blog/2024/12/educationsolutions/featured.webp
toc: true
bold: false
katex: false
tags:
  - education
  - technology
next: true # show link to next post in footer
nomenu: false
notitle: false
draft: false
syndication:
  - https://framapiaf.org/@bbbhltz/113652061353292023
---

## Introduction

It is hard for me to imagine being a student today. I don't even want to think about the social side of it, or the stress and anxiety. I'm thinking more about what I, as a student, thought the profession of teaching entailed. I've been on the teaching side of the table for 18 years now, and with every year that passes, my job becomes more complicated.

Again, it isn't the social side of it, or the stress and anxiety. And it isn't the slow-as-molasses pace at which the country where I teach catches up with the country where I was taught in terms of technology and inclusion. Regarding the latter, years ago, during a fire-drill, the one student in a wheelchair was just left inside. Today, one of the schools I used to work for boasts about how many wheelchair-bound and autistic students they have.

Technology-wise, when I first arrived in France, some professors wouldn't share their university email addresses with students. They said it was like giving them their home phone number. That was 2006. Since then, I've seen new solutions popping up like porn ads in the early 2000s.

Many of these solutions do indeed solve problems. [Pronote][1], created in 1999, has a purpose: make it easier to keep track of grades and absences. But, it also adds unneeded friction. Teachers could just use a book or even a spreadsheet to keep track of this. Post-secondary teachers and professors have a similar app (owned by the same company), [Hyperplanning][2]. It is useful because you can use it to assign homework. But, just like Pronote, this app adds friction. A professor should be able to come in the class and *teach* instead of dealing with the technological side of things.

(I'm partial to the spreadsheet, but I did try using the old pen and paper method this year. The results were the same.)

The previously mentioned software, while problematic in terms of implementation within a classroom, has created a generation of parents and students used to having hour-by-hour updates on the comings and goings of their children. Those children are now older and sitting in class with me asking why we cannot have the same system. My students have never had it any other way. They clamour for the simpler times of their youth when they could see their homework and due dates on a JavaScript-laden web app, instead of *writing them down*.

Decision-makers are not professors (they may have been once upon a time), so they see this as a problem and keep presenting solutions. These solutions have piled up like last season's H&M styles in a Kenyan landfill.

I would like to present my thoughts on this because I see it as *solutionism*, which is not always positive.

## My issues with needing so many apps

1. Many of these solutions do not take into account mobile phones and promote the installation of an app to do what a website can do. On occasion, the solutions are not compatible with a certain browser. Here are two recent examples:

   - An online learning platform my students were obliged to use that had no mobile version for their website, or an app.
   - A new online attendance software that doesn't work on mobile and requires the use of an app (unless you tell your mobile browser to "show desktop site").
   
   I think I would find it more comfortable to use an online learning site on a computer rather than a phone, but I am *twice as old as my students*. I know my students would be more inclined to use this online learning software if they could do it on their phones.
   
   Using any online attendance software presents the same issue: the first minutes of the class are lost to turning on the computer, logging in, opening a browser, crossing fingers that there are no updates, crossing more fingers for the Wi-Fi to continue working, logging into the platform, navigating to the class, and "validating" the presence of the students.

2. Many of these solutions do not encourage the use of paper-based methods. I could print off a sign-in sheet, but I would still need to connect at some point to validate. 

   The most recent iteration of our attendance software requires the scanning of a QR code. I'm getting my popcorn ready for when the administration realizes that students can just take a picture of it and send it to their missing classmates unless the teacher double-checks. Clever students will find a way to drag this out, and then the teacher will still have to do the work of calling out names.
   
3. My colleagues and I have multiple employers in the same sector. They all use Outlook and single sign-on. There is no alternative. We have an account and multiple apps and websites we need can only be used with this SSO. So, that occasionally means logging out and logging in between institutions, creating more friction and wasting precious time.

4. None of these apps work offline.

Altogether, the big issue here is that it sets a poor example. I might say, "no phone or computers for this lesson" and then immediately boot up my computer and spend 10 minutes doing something that could be accomplished by *counting the number of students in my class* and asking "does anyone know if Dave is coming today?" and making a note in my logbook. I have to spend minutes *that my students are paying for* showing them how to use a certain website. Then, there are the emails asking where to upload homework and the time spent resetting passwords.

Beyond this, none of these apps work without the Internet or electricity. They need bandwidth and energy. Without those two things, a teacher is prevented from doing their job. I once worked in a high school where a teacher cancelled class because there was no Internet that day. The lesson plan called for a YouTube video and they had no backup. It is also worth pointing out that there is an environmental cost to all of this, now further exacerbated by the use of generative AI in education.

Finally, if we stretch our imaginations a little, it is biased to put these tools and solutions in place. We assume that the youngest generation is at ease with technology compared to the technologically illiterate teachers in front of them. Technological illiteracy touches all cohorts. While purely anecdotal, my experience is that the current batch of 20-somethings is worse with technology than 10 years ago and these digital hoops we make them jump through cause unnecessary stress and confusion.

## Conclusion

It is unclear to me if teachers actually want these solutions or if there was even a problem to solve in the first place. Office 365, Moodle, Teams, Zoom, Kahoot, Wooclap, Articulate Rise, language learning apps, sign-up forms, "engagement" trackers, interactive slideshows, gamified experiences, and the list goes on. Last week I needed a whiteboard marker, so I asked the teachers in the same hallway as me. None of them had one. An entire gaggle of professors without one of their most iconic accessories is rather shameful if you think about it, and an entire generation learning through screens and projectors benefits only the creators of software, services, and apps.

Digital sobriety in education might be a solution to these *solutions looking for a problem*. Schools, like companies, tout their trendy labels, like "well-being" and "ecological," and then spend money on services that are neither. A solution that causes more problems is just a rip-off.

[1]: https://fr.wikipedia.org/wiki/Pronote
[2]: https://www.index-education.com/fr/presentation-hyperplanning.php
