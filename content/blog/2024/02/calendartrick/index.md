---
title: "How to import events using Khal"
subtitle: "why don't they provide an .ics file?"
description: "A quick example of how to import a list of events from a text file using Khal"
slug: calendartrick
date: 2024-02-02T10:07:16+01:00
lastmod: 2024-02-25
featured_image: /blog/2024/02/calendartrick/featured.png
draft: false
katex: false
tags:
  - Guide
  - Useful Commands
next: true
nomenu: false
---

I keep a little file on my computer. It synchronizes with my other devices using Syncthing. The file is called `useful-commands.md`. Basically, if I ever had to spend more than 5 minutes figuring something out, I keep the commands in that file.

## Problem

I have multiple employers that have all, for some reason, decided to not only use the same planning software, but to hide the link to the calendar. This is an option, one of the employers has activated it, the others chose not to because:

>If we activate the iCal link, nobody will download the app.

Yep. They have all paid (the same developer) to make an app using their branding and colours to inform students and professors of their schedule.

Stupid.

I can still see my schedule online, though. And, as it turns out, when you export your schedule as a PDF each event is listed like this:

```
DD.MM.YYYY HH:MM HH:MM Class (Group)
```

This is *extraordinarily* convenient because [Khal][khal]---a CLI calendar program that synchronizes with CalDAV calendars---uses that format for adding [new events][new], so I only need to copy and paste each event into the terminal:

```
$ khal new 27.02.2024 08:45 10:15 Class (Group)
```

That is awesome. But, if you need to do this 120 times per semester, it gets old fast.

That is why, in my `useful-commands.md`, I have this noted:

```
# How to... use khal to add a list of classes
file="lessons.txt"
while read -r line; do khal new $line; done < $file
```

This is my reminder to copy all the events to a `.txt` file and loop through the list of lessons and make Khal add them.

[explainshell.com][explain] can break it down if you like that sort of thing. I just copied things I found around the web and modified them to my need.

Since I will be importing these events, I can also add a description for each one while I'm at it. So, I make a tiny modification on each line:

```
27.02.2024 08:45 10:15 Communication Skills (Group S3) :: Public Speaking Evaluation
```

Running the commands takes little to no time, even with hundreds of events.

After, I force it to [synchronize][sync] with

```
$ vdirsyncer sync
```

and it is added to my shared calendar for my partner to see.

[khal]: https://lostpackets.de/khal/ "Khal homepage"
[new]: https://lostpackets.de/khal/usage.html#new "Khal documentation for adding new event"
[explain]: https://explainshell.com/explain?cmd=file%3D%22lessons.txt%22%3Bwhile+read+-r+line%3B+do+khal+new+%24line%3B+done+%3C+%24file#
[sync]: https://vdirsyncer.pimutils.org/en/stable/ "vdirsyncer documentation"
