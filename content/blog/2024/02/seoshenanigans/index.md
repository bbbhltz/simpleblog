---
title: "Sneaky SEO Shenanigans Suck"
subtitle: "a little fun inspired by the 16 companies that dominate search"
description: "A blog post about how a collection of websites uses SEO to dominate search, and a link to a blocklist for those sites"
slug: seoshenanigans
date: 2024-02-23T16:36:54+01:00
lastmod: 2024-10-13
featured_image: /blog/2024/02/seoshenanigans/featured.webp
draft: false
katex: false
tags:
  - Search Engines
  - Web
  - SEO
  - Technology
next: true
nomenu: false
---

Earlier this week I came across a post on Mastodon:

<div class='embed-container' align='center'>
<iframe src="https://saturation.social/@clive/111964703856082717/embed" class="mastodon-embed" style="overflow: hidden; max-width: 100%; border: 0" width="600" height="560"></iframe>
<figcaption>Toot from @clive@saturation.social regarding online product reviews</figcaption>
</div>

Of course I had to read the article. I read reviews quite a bit. I know you shouldn't trust them, but I do it anyway. The article shared in the post, *[How Google is killing independent sites like ours](https://housefresh.com/david-vs-digital-goliaths/)*, does an excellent job explaining the situation and putting things into perspective:

1. Searching for reviews will net you poorly written articles and listicles.
2. The reviews appear on websites that many would deem trustworthy, like *Popular Science* or *Rolling Stone*.
3. Many of the sites break Google's rules.
4. Google is doing nothing about it.

{{< box info >}}
The *Housefresh* article has a follow-up that is worth reading: [HouseFresh has virtually disappeared from Google Search results. Now what?](https://housefresh.com/how-google-decimated-housefresh/)
{{< /box >}}

If you run a legitimate site providing expert reviews, you cannot stand up against the SEO of these publishing companies. The *HouseFresh* article uses the example of trying to find an air purifier, and I'm not about to repeat their experiment with other products; we all know what will happen.

The article also links to the very interesting *[How 16 Companies are Dominating the World's Google Search Results (2023 Edition)](https://detailed.com/google-control/)* from Detailed.com.

Look at this infographic!

![Detailed.com Infographic](https://detailed.com/wp-content/uploads/16-companies-final.jpg "The 16 Companies Dominating Search Results")

The report notes:

> The 16 companies in this report are behind at least **562 individual brands** which get traffic from Google each day.
>
>Combined, Semrush estimates they pick up around 3.7 billion clicks from the search engine each month. An average of **6.5 million monthly clicks per site**. 

What to do?

What... to... do?

You may be familiar with a browser extension called **[uBlacklist](https://iorate.github.io/ublacklist/docs)**. It is a search filter for Google and other search engines. It removes results from the page based on a blocklist. The blocklists are `.txt` files. I can make a `.txt` file!

So I did.

In fact, I made 16 of them. And then I made a 17<sup>th</sup>.

I put them here:

**[16 COMPANIES FILTERS](https://codeberg.org/bbbhltz/16CompaniesFilters)**

Now you too can subscribe to these lists, and if you see any mistakes let me know.

Just install the browser extension (see the link above) and copy the SUBSCRIBE links. The [official document](https://iorate.github.io/ublacklist/docs/advanced-features#subscription) shows you how to do it. 

Then search!

Before you ask, yes, this is just for fun and spite. Some of these sites were already on my list of avoidable sources: sites like *Make Use Of* and *CNET* that are notoriously bad at recommending *anything* other than the latest Samsung or Apple product.

While preparing these files, it surprised me just how many similar sites were in fact subsidiaries of the same parent company. It also surprised me that some of these massive media companies were unknown to me. Not once have I ever heard of *Hearst* or *Recurrent* or *PMC*. It was like an epiphany, the pieces fell into place, my eyes opened. Search results are crap because companies like these have been gaming the SEO system with their keywords and other shenanigans.

So, I will filter them out. I do wish the extension worked with Mojeek.
