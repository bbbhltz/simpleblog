---
title: "Why did I waver?"
subtitle: "Questioning allegiances and doubling back"
description: "A personal reflection on video games and how I stopped having fun when I stopped playing Nintendo"
slug: wavering
date: 2024-01-09T17:58:33+01:00
# lastmod:
featured_image: /blog/2024/01/wavering/featured.webp
draft: false
katex: false
tags:
  - Blog
  - Personal
  - Nintendo
next: true
nomenu: false
---

Sometimes peculiar thoughts cross our minds. I'm not a deep, philosophical person; I hate overthinking. But, actively fighting against overthinking is just *overthinking*, isn't it?

Now, you can call me silly---I have laughed at myself over this---because somehow a video game console brought about a minor epiphany. A Nintendo Switch, a console released in 2017, left me scratching my head and pondering the titular question: **Why did I waver?**

Let's back up a bit.

And a bit more.

And still a bit more.

The year was 1990 or 1991. My brother had a Nintendo Entertainment System. It was amazing. But, it was his, and in his room. The NES was sweet, but it was not this:

![Game Boy](featured.webp "the DMG-01")

Holy smokes, did I ever want one of those! And so, I irresponsibly used the great power all children are imbued with: emotional manipulation. And, much to the surprise of nobody, my efforts paid off, and I was rewarded with a Game Boy.

I was a gamer and Nintendo fanboy from that day forward.

Despite having just that portable console, I still bought the magazines. I even *subscribed* to [Nintendo Power](https://archive.org/details/NintendoPower1988-2004/). I read reviews and guides for games on systems I didn't own, like the Super Nintendo after it launched.

That Game Boy was one day upgraded to a Game Boy Color. I did get a Super Nintendo (the pack with Killer Instinct and the soundtrack CD). Around the time the GameCube dropped, I managed to finagle a Nintendo 64.

Alas, I was off to university and working four jobs. Still, my mother decided that video games had never caused me any harm and surprised me with a GameCube.

Then I moved to France, and told myself: gaming is for children, and I cannot afford it now that I am independent. I couldn't afford it, but I did get a Nintendo DS Lite as a gift along with one of the Zelda games.

You have noticed a very Nintendo-themed pattern here. It was always Nintendo. It always will be Nintendo, or nothing.

Then one day I came home from work, and there, in *my* home, was a sight of which the memory still makes me shudder. There was an Xbox 360 attached to my television! My roommate had gone out and purchased it that day.

Shame. Shame on me. I picked up the controller and tried the game. I had that machine for several years before giving it away and telling myself: next time, Nintendo.

Then one day my partner and I decided that holiday was too expensive, so why not buy a console for fun. A great and glorious idea! Off to the store we went. I drifted towards the Nintendo section, trying to hide my joy.

From the corner of my eye I saw my partner coming in my direction carrying a garish blue box. A Sony PlayStation 4 and Crash Bandicoot. I looked at her, ready to put my foot down. Our eyes met. I steadied myself, I knew what was coming. I had nothing to parry with. I was staring down the barrel of a loaded gun filled with armour-piercing ammo: *emotional manipulation*.

*Well played… well played*

I wavered, and, up until December 2023, I have been playing another console I didn't really enjoy, except for a handful of games.

Why did I waver and continue to waver? I don't have an answer.

I do, however, have a few words to say about the Nintendo Switch.

It is an amazing device. I don't have any of the big games for it yet, so I am not even taking content into account. I'm talking about the whole design of the product.

Wavering took away some of the fun. Why didn't I enjoy the PS4 as much? Let's make a list:

* noisy
* slow startup time
* buying a physical edition and coming home to be forced to download a 7.8GB file because the disc doesn't have the game on it?
* slow startup of games
* in-game load-times
* slow shutdown time

I would *not* play the PS4 unless I was positive I had at least a 60-minute window for playing.

Now, Nintendo may have opted to low-end silicon, but that did not prevent them from creating a machine that:

* is quiet
* turns on and off quickly
* has shorter load times
* uses cartridges and smaller update sizes for physical games
* can suspend overnight without overheating
* is **PORTABLE**
* looks nice
* is small

The list can go on, but you get the point. I'm enamoured with this gadget. Once a Nintendo guy, always a Nintendo guy.

And *that* was the epiphany.

I wavered, and have just now discovered that I've been missing out on something joyful for several years.

What if I applied this idea to other things in life?

What if that could be a resolution for 2024?

The time has come for me to put my foot down, to draw a line in the sand, and hold my ground. Fun and joy could be a simple decision away, but first, I have a game to finish.

I didn't say I would stop procrastinating, did I?
