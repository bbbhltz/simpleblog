---
title: "Happy Birthday Blog"
subtitle: "🎂🎂🎂"
description: "A meta blog post celebrating the anniversary of this little personal blog"
slug: happybirthday
date: 2024-01-28T00:00:00+01:00
lastmod: 2024-10-14
draft: false
katex: false
tags:
  - Personal
next: true
nomenu: false
---


The first post I made to this blog was on 2021-01-28. That was three years ago.

Since then, I have made around 60 posts. The intention to write more was there, but time and inspiration were not on my side.

Nonetheless, I feel that the past three years have been a success.

This is a *blog*. It serves no commercial purpose. It is made with free software and hosted on a free service. (**note to self:** [*make a donation to codeberg*](https://docs.codeberg.org/improving-codeberg/#donate-to-codeberg)) I have no idea how many people have ever read it. Even if 10 people have read something I wrote, I am happy. I don't want to be an influencer, I am not a certified expert on anything, and I consider most of my hobbies a little dull. I like it that way!

How then has this blog been a success?

It let me express myself. It doesn't paint a picture of who I am; it is more of a doodle. An honest one, mind you.

Over on the [stats](https://bbbhltz.codeberg.page/stats/) page, there is a word cloud that is somewhat representative of how I would describe my interests, because the biggest words in the cloud are:
* [technology](https://bbbhltz.codeberg.page/tags/technology) (but not really anything specific),
* [education](https://bbbhltz.codeberg.page/tags/education) (because I am a professor),
* [digital privacy](https://bbbhltz.codeberg.page/tags/privacy) (it is one of my personal obsessions),
* and things like [Linux](https://bbbhltz.codeberg.page/tags/linux) and [Pandoc](https://bbbhltz.codeberg.page/tags/pandoc) (because those are tools that make my life easier).

## Best of...

I know that some people have read things from my blog. They even sent me kind words via email about what they liked, disliked, and disagreed with.

If we count that engagement alone, along with maybe some mention on Mastodon, my best guess as to a "BEST OF" for this blog are (in no order):

* These three short stories: [Freak](/blog/2021/12/freak/), [Homework](/blog/2022/07/homework/), and [We Have a Winner](/blog/2022/07/we-have-a-winner/)

* [The Privacy-Security Rabbit Hole](/blog/2021/04/privacy-security-rabbit-hole/)

* The (rather long) [Low Friction Introduction to Digital Privacy](/blog/2022/03/guide-privacy/)

And, much to my surprise, I've received two emails thanking me for [a post about making an invoice with Pandoc](/blog/2021/11/using-markdown-and-pandoc-to-make-a-simple-invoice/)

Finally, I've received no less than 10 requests to write sponsored articles. Strangely enough, they all decide to retract their request when I answer. This is the email that I use when I respond:

> ## Rates for Sponsored Content
> 
> Product review with photos and links:
> 
> - Basic 300--500 words: €90
> - Detailed 500--800 words: €120
> - In-depth 800--1000+ words: €200
> 
> Sponsored content will include a "disclaimer" or note informing the reader that the content they are reading was requested by the company. Transparency is key, but rates will not be revealed.
> 
> I do not use X (Twitter), Meta (Facebook, Instagram). I may share the review on Mastodon, depending on the product or service.

## My favourite things to write about

If I had to narrow the focus of this blog, I simply would not. A blog that takes on a singular focus is great. Give me a blog that is 100% music or 100% gadgets and I won't complain. I could probably stick to one thing 80% of the time, but I am so infrequent when it comes to blogging that I would end up breaking my own rule after a month or two.

As much fun as I had writing about different phones (see [here](/blog/2021/04/the-punkt.-mp02-minimalist-mobile-phone/), [here](/blog/2021/11/update-punkt-mp02/), and [here](/blog/2023/01/review-crosscall-core-z5/)) I could not *just* do that. In fact, one of those companies, Crosscall, even let me beta test an unreleased model for several months. Did I write about it? The thought didn't even cross my mind. If you are interested, though, the STELLAR-X5 is fast and fluid in all the right ways.

I would like to write more about education, but as a non-researcher with only 17--18 years of experience in higher education, mostly private, my main thoughts have already been laid out, i.e., students are less adept with technology now, the digital experience is hindering instead of helping, AI and LLMs are going to fuck things up. **My students are not clamouring to learn how to write prompts or be taught lessons that were planned by OpenAI's ~~carbon footprint~~ chatbot.**

## Blogrolls and Collections

This little blog is part of a few [collections, blogrolls, and webrings](/more/#blogroll). It is also indexed by some search engines.

Obviously, Google can find it, as well as the Bing-based metasearch engines. I am very happy to see it on [Marginalia](https://search.marginalia.nu/). The [info](https://search.marginalia.nu/site/bbbhltz.codeberg.page?view=info) page is fun.

![Marginalia](marginalia.png "Similarity is a measure of how often they appear in the same contexts")

All of those links are somewhere on my blog, so it isn't like we are that similar. I do like seeing the landing page for the developer of Privacy Browser [www.stoutner.com](https://www.stoutner.com), the [Mojeek Blog](https://blog.mojeek.com), a fedi contact (Hi [Joel](https://joelchrono.xyz)!), and Scott Nesbitt's [Random Notes](https://scottnesbitt.online/index.html) (I do follow their [Open Source Musings](https://opensourcemusings.com/) blog too) on the same page as mine, because they deserve some of your attention.

(unfortunately, the blog is still in the queue for the [ooh! directory](https://ooh.directory/))

## Should I set goals?

The #100daystooffload idea is tempting. But, it has taken me 3 years to write 60 odd posts. I don't think I would set such a high goal. Maybe 2 a month? We'll see how that goes. Place your bets: will I have posted 24 times in the next 365 days? I don't know how to calculate odds, so maybe no bets.

Finally, an easy goal: changing the appearance. I think I would be very content with using one of the minimalist Hugo themes, preferably one with no JavaScript. Maybe I will switch to something that isn't Hugo? Who knows.

Anyway, Happy Birthday Blog!
 
🎂🎂🎂

