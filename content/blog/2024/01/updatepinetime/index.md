---
title: "Note to self: How to update PineTime"
subtitle: "I'll still forget how to do this later"
description: "How to update the PineTime from a computer over Bluetooth using ITD"
slug: updatepinetime
date: 2024-01-16T09:48:04+01:00
lastmod: 2024-12-04
featured_image: /blog/2024/01/updatepinetime/prompt.png
draft: false
katex: false
tags:
  - Technology
  - Guide
next: true
nomenu: false
---

The [PineTime](https://pine64.org/devices/pinetime/) is great. I say this with some bias because it is the [the only smartwatch I have ever used](/blog/2022/10/pinetime/).

Long story short: after a year and change of use, the PineTime has not let me down. Updates have improved battery life and added new watch-faces. I am not a hacker so I cannot speak for the development capacities of this cheap little gadget.

Speaking of updates, without fail, I always forget how to update this thing. I get the files. I read the manpages. And, finally, I remember.

It isn't complicated, though. It is clearly a *me* issue.

## Get ITD and start the daemon

The **InfiniTime Daemon** can be found here: [https://gitea.elara.ws/Elara6331/itd](https://gitea.elara.ws/Elara6331/itd).

I use Debian, but have used this daemon under other Linux distributions. Head over to the [releases](https://gitea.elara.ws/Elara6331/itd/releases) and snatch the file you need. For me, it was the `.deb`.

```bash
$ wget https://gitea.elara.ws/Elara6331/itd/releases/download/v1.1.0/itd-1.1.0-linux-x86_64.deb
$ sudo apt install ./itd-1.1.0-linux-x86_64.deb
```

Now it is time to **turn off the Bluetooth connection with your phone**.

Since I only need to connect to my PC for these updates, I will not "enable" this itd daemon permanently. As a regular user (not root) you can start and stop the service with these commands:

```bash
$ systemctl --user start itd
$ systemctl --user stop itd
```

If Bluetooth is active and working on your laptop, you should get some sort of prompt asking you for a passkey. On my computer it looks like this:

![image](prompt.png "It wants my digits")

Now you need to get the new firmware and resources.

## Get InfiniTime firmware and flash it

You can grab necessary files from the [InfiniTime releases page](https://github.com/InfiniTimeOrg/InfiniTime/releases). You will need two files.

As of writing, the current release is v. 1.15 "Ribes rubrum":

```bash
$ wget https://github.com/InfiniTimeOrg/InfiniTime/releases/download/1.15.0/pinetime-mcuboot-app-dfu-1.15.0.zip
$ wget https://github.com/InfiniTimeOrg/InfiniTime/releases/download/1.15.0/infinitime-resources-1.15.0.zip
```

I don't know if there is a specific order to do this, but the last few times I have started by installing the `infinitime-resources-version.zip` file, then flashed the firmware. These are the two little commands I always forget:

```bash
$ itctl res load infinitime-resources-1.15.0.zip
$ itctl firmware upgrade -a pinetime-mcuboot-app-dfu-1.15.0.zip 
```

Remember to read any prompts and follow instructions and your PineTime will be up-to-date. 

## External Links

* [PineTime Official Shop](https://pine64.org/devices/pinetime/)
* [Elara6331's InfiniTime Daemon](https://gitea.elara.ws/Elara6331/itd)
* [InfiniTime GitHub](https://github.com/InfiniTimeOrg/InfiniTime)
* [InfiniTime Official](https://infinitime.io/)
* [@JF@mastodon.codingfield.com ](https://mastodon.codingfield.com/@JF) (follow this account to know about updates)
