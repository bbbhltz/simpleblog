---
title: "Make Digital Privacy Sexy"
subtitle: "a rant styled as an opinion piece"
description: "A blog post talking about digital privacy and how the public has become bored with hearing about it"
slug: sexyprivacy
date: 2024-01-17T22:42:44+01:00
lastmod: 2025-01-28
# featured_image: /blog/2024/01/sexyprivacy/featured.png
draft: false
katex: false
tags:
  - Opinion
  - Privacy
  - Technology
next: true
nomenu: false
toc: true
---

Digital Privacy has an image problem. At the moment, there isn't anything *sexy* about it. If it were a trailer for a film, you'd be playing on your phone while it was on. It needs an image makeover and a re-branding. Many movements have the right idea and are presenting it as inviting and ethical. We need more of this, and less fearmongering.

# Who says it's not sexy?

This is all opinion.

I am just a teacher with [an interest in digital privacy](/blog/2022/03/guide-privacy/). Yes, [I degoogled my phone](/blog/2023/05/on-debloating-android-phones/). Yes, I use [Linux](/tags/linux/). No, I don't use Gmail or Facebook. I am not a researcher or hacker. I would call myself a (non-militant) advocate for humane, ethical, and privacy-respecting technology and services.

Browsing the web, one can come across innumerable sites and communities dedicated to this topic. In some instances, these communities come off as slightly unwelcoming. That is not sexy. Digital Privacy also comes with its own myths and legends that make it harder to get people interested in their own personal digital privacy. The methods of persuasion are often flawed, repetitive, and are filled with a dictionary's worth of terms that to the uninitiated mean nothing. Finally, there is the occasional call for an "all or nothing" approach and commitment to the cause.

## Tinfoil hats and Neckbeards

A simple question on a forum or on social media can sometimes open a can of worms. In the case of digital privacy, some of those worms are of paranoid and conspiratorial leanings.

You just had a question, but the replies sound more like quotes from *1984* and *Brave New World*. You've walked into a circle-jerk of self-appointed experts (**NB.** Yes, there are very knowledgeable people among them). Now, they have things to share, but if you are not aware of the culture you will find it surprising to referred to as a "normie" (are they being derogatory?).

I find it rather contradictory that any community, big or small, that wants to "bring privacy to the masses" could be off-putting in any way. I won't be brandishing the t-word around (toxic!) to describe this little corner of the web. The best description is that some forums and subreddits come off as "No Normies Clubs" where asking a beginner question will earn you manifesto-length essays filled with calls to "wake up," "question authority," "do your own research," and "develop your critical thinking skills."

Once you decipher their lexicon---"Goolag," "Micro$hit," etc.---you'll discover their advice, and their own disagreements. "Just install GrapheneOS," one might say, while another will call them a shill. Soon, you'll be directed to websites with black backgrounds and green monospace fonts. As you *dig deeper* you'll discover the inherent *insecurities* of Android, Windows, iOS, and even (\*gasp\*) Linux. Signal will be good enough in one group, and rubbish in the other.

The communication style of some of these communities needs to be brought into check. It is not their fault, they were a sort of echo-chamber of freelance, grassroots, DIY people with a common interest. They injected their hatred of Big Tech into their rhetoric. It is 2024, it isn't appealing any more, and it will not get the next generation of Internet users to pay attention.

## Myths and Legends

Another issue with digital privacy is the prevalence of certain myths. All these years, and still people will say they have [nothing to hide](https://www.youtube.com/watch?v=M3mQu9YQesk). Clearly, the messages are not making it downstream.

It is true that many companies already have plenty of information on you. [You are tracked](https://themarkup.org/privacy/2024/01/17/each-facebook-user-is-monitored-by-thousands-of-companies-study-indicates), yes, but that doesn't mean it is pointless to start protecting yourself now. How has this myth not been dispelled? 

Similarly, it has been put forward that it is too hard to protect yourself. It is not. It is just different. You change an app, you make a new habit. The "all or nothing" approach mentioned above does not help. When you learn maths as a child, the teacher does not explain addition, subtraction, division and multiplication on Monday and give a test on it on Tuesday.

Simplicity of use has lead many a user to the walled garden of the iOS ecosystem. I have no personal experience with these devices, but it is said they are secure. Privacy is not security though. Perhaps the police cannot easily unlock your phone, but your privacy is still being invaded the same way it is with any connected devices.

The urban legend of infallible Apple hardware is at least understandable to an extent. It is part of their marketing. Incognito mode, on the contrary, is just poorly understood. Perhaps [the greater public will soon see for themselves](https://www.npr.org/2023/12/30/1222268415/google-settles-5-billion-privacy-lawsuit) that companies still track you no matter what. It is, after all, another marketing trick.

## Persuasion and Marketing

Privacy advocates have been using different techniques to persuade users to change their ways. Fear, uncertainty and doubt make up a decent portion of these techniques.

Fear of a dystopian world of mass surveillance, and clichéd metaphors and analogies about "your life on the cover of the newspaper" have failed to appeal to emotion and logic. We've been bombarded with words like tracker and cookie, instructed to use a password manager and 2FA, and YouTubers remind us of the existence of VPNs around the clock. Did we mention E2EE and why it is important? Because of [MITM attacks](https://en.wikipedia.org/wiki/Man-in-the-middle_attack), obviously.

This word soup goes hand in hand with the broken record named "The Best of Toxic Tech"...

**WE KNOW ALREADY!**

It is important to know, but this dead horse has been flogged. People still use Facebook, they still tweet their lives and post pictures on Instagram in between group chats on WhatsApp. Google knows what time they wake up and Amazon is better at suggesting gifts for my partner than I am. And, for crying out loud, we all know about Microsoft. It isn't a secret that when they invested in the next hype (generative AI) that they'd find a way to [cram it into their OS](https://www.theverge.com/2024/1/15/24038726/microsoft-copilot-microsoft-365-business-launch-availability).

Go to the top of the highest mountain and scream your lungs out about it, the mountains will echo back "we know!"

Occasionally mainstream media will do a piece on this. Bill Maher did one in 2017 called [Social Media is the new Nicotine](https://www.youtube.com/watch?v=KDqoTDM7tio) and told viewers that the tech bros were just "tobacco farmers in t-shirts" and concluded with "Philip Morris just wanted your lungs; the App Store wants your
soul."

I've used that video in class with my students. They laugh. They compare the hours spent on their devices with pride. However true this is, we've reached Silicon Valley Sleaze Saturation. If a whistleblower came forward with a new scandal, we wouldn't be surprised. We wouldn't bat an eye.

It will be essential for digital privacy advocates to tackle the problem from a different angle, otherwise getting the next generation to commit will be an even bigger one.

## Commitment Friction

Getting people on track usually starts off fine: use a different browser you say? As long as I can binge-watch Netflix on Firefox, not an issue. Easy. No friction.

But it isn't all as simple as getting your friend or relative to make that switch. In fact, switching browsers isn't even a commitment. It's parallel to parting your hair on the other side. Someone might notice, but the end result is the same.

A real commitment is, for example, doing sport three times a week. That takes a little forethought and planning, much like changing your email provider. If you jump ship too quick, you might end up paying for something you won't use or is missing that one little feature you didn't know you wanted. 

But, today, the commitment of all commitments might be [quitting social media](https://www.ted.com/talks/cal_newport_why_you_should_quit_social_media). That will turn heads. That will make you a weirdo. When my partner stopped using WhatsApp, she was called "selfish." When I told a student I didn't use Google search they thought I was pulling their leg, and when I told another I won't use ChatGPT they asked if I was Amish. Quitting these services that we've been [domesticated to use](https://seirdy.one/posts/2021/01/27/whatsapp-and-the-domestication-of-users/) (*read that article!*) is not just hard for the quitter. It can lead to a form of cognitive dissonance for observers too.

I am not all doom and gloom. There are things to be optimistic about.

# Hippies today... Squares tomorrow

Hippies are optimistic, aren't they? Sexual revolution and all that, you know?

Seriously, though, it is important to look back (briefly, this is a blog after all) at how other movements evolved. It is also worthwhile to make note of a handful of truths that we tend to forget.

## The Nature of Counterculture

Values change. The counterculture of yesteryear is the norm today. Peace and love in one decade is war in another. Progressive then, conservative now.

If we consider the fight for digital privacy as a movement against the norm, it might be viewed as a counterculture. It does seem like that sometimes, and even gets political like with the [GDPR](https://en.wikipedia.org/wiki/General_Data_Protection_Regulation) or how France will soon put "[large-scale, real-time, algorithm-supported video surveillance cameras](https://www.politico.eu/article/france-surveillance-cameras-privacy-security-big-brother-paris-olympics/)" to use. Digital rights are becoming more and more important, and calls to protect them are getting louder.

So, if today the advocates for digital privacy are the counterculture, there is a hope that it will become the norm. Not tomorrow, but someday. That doesn't mean we have to forget established truths about big tech.

## More Truths

Technology as a whole, the Internet, the web, social networks, and other ICT have done wonders for the world. The creativity and connectivity that has stemmed from these inventions is jaw-dropping. 

- Apple has more than once revolutionized technology: with home computing, design, iPods, iPhones, and other gadgets. Now, they regularly crank out new versions of their rectangles that are just evolutions requiring new accessories to function.
- Microsoft made computers accessible to the masses. Now, they make an operating system that sucks up your data and auction it off to data brokers.
- Amazon was a revolution. Granted, Bezos wasn't just some guy in a garage making his way in the world, but still, he started something that many have tried to emulate, and exploited some employees along the way. 
- Google organized the web and made searching simple at a time when a generation was just embarking on a journey into the World Wide Web, and Gmail was a great free email when compared to Hotmail. Now, they maintain [a browser monopoly](https://gs.statcounter.com/browser-market-share/) and are trying to dictate standards while selling data to the highest bidder.
- Facebook was cool in 2004; Zuck too. That didn't last long. Plus, nobody forgot about the origins of Facebook.
- Twitter opened the fire hose and appealed to our anger and rage. Today, it is owned by another face of tech that may have a vision, but is no Tony Stark.
- OpenAI is hyped, like Blockchain and cryptocurrency, but was positioned as "the thing" we need everywhere. So they put it everywhere they could. Not a day has gone by without *some* story mentioning the failures and harms this toy has caused. 

Something "cool" can quickly turn into a dumpster fire. The "Pirates of Silicon Valley" are in the same boat as a well-known "Pirate of the Caribbean". They do deserve recognition, but not veneration. The worm has turned. It turned some time ago, in fact.

## Time and Place

The great work of so many digital privacy advocates and whistle-blowers cannot be for naught. We know, thanks to the effort of many, the [negative effects](https://www.humanetech.com/key-issues) of technology. The Millennials, and older Gen Zs, grew up with the technology. We watched it change, we watched the canary keel over and turn to dust, and we mustered all of our strength into an apathetic shrug, thinking "what can I do about it?"

Well, here's the thing. Millennials are politicians now, and educators, and parents, and aunts, uncles, and mentors. We have responsibilities, a flock to tend to, and an audience that sometimes listens when they look up from their phones.

We need to begin sharing our experience and knowledge. We have the information, or at least know where to find it. We need to make it available.

## But that's *haaaaaard*

*Too fuckin' bad.*

You can't just whinge and whine and cram buzzwords like "[enshittification](https://en.wikipedia.org/wiki/Enshittification)" into your tweets and toots and whatever for Internet points and pat yourself on the back (no offence meant to Cory Doctorow). 

Have you ever walked a senior citizen through a Windows install over the phone for 5 hours? That's hard, but you take it screen by screen, one hair-pulling baby step at a time, and try not to break the phone.

That is what it is time to do. It is time to start taking these baby steps together, instead of creating insular communities on obscure forums. If you are reading this, you have likely taken these steps. That makes you a credible individual who can teach by example rather than impose your ideas of what is the right way to be private.

For example, instead of indirectly berating someone for using Google, show them that other search engines can find better results sometimes, and casually tell them that [Google's results are getting worse](https://www.404media.co/google-search-really-has-gotten-worse-researchers-find/). You might tell them to use Firefox and Kagi, and they end up using Brave. Take the win, even if you are one of the anti-Brave folks.

Because we already tried fear, and hate, and "those guys are evil" as methods of persuasion. We need to change the script, and focus on helping the younger generation find the tools that reflect their values and teach them that their private information deserves respect.

We walked on dial-up, so they can run on fibre optic.

We've been around. We've seen stuff. We can furrow our brows and think outside the box to find solutions that work.

# Pray tell, random Internet person

Here are two.

## 1. Sex Education

See, more sex... it wasn't clickbait!

Depending on your age, culture, country of origin, whatever, it is possible that you took some classes in elementary, middle, or high school on sex education. It was usually part of a general health class. It was sometimes controversial. I remember the clutching of pearls when we watched [this raunchy cinematic masterpiece](https://www.youtube.com/watch?v=IIuq0tYAvzo) in class.

Moving on.

Sex education has saved lives. Rates of teenage pregnancy start dropping, STDs are less frequent among teenagers, and when they do happen stigmatization is lower than it was before. It isn't perfect, but it teaches young people about perfectly normal things by presenting knowledge, and giving access to information that they may not get elsewhere. When taught in the right conditions, it respects their individuality and freedom.

We should make digital privacy part of the curriculum. I know, digital literacy is a big thing already. But, digital privacy can be a separate part. You can even throw in some scary stuff if you want to, like sex education does with photos of warts. You can use it to dispel myths, like how we learned that it is, in fact, possible to get pregnant your first time. And who better to teach this than the current batch of teachers who are 30 and over?

## 2. Buy Local

There was a time when hearing someone state, "I only buy locally grown oranges" stirred up images of some long-haired, bare-footed, hippy-granola archetype. Today, it is normal and accepted, even by the "suits and ties" types. This was a movement that took it step by step and curated an image. That image was ethical; it was one that said "the effort is worth it."

The effort is worth it. As a general rule, most people would advise deciding with your wallet. That is hard when we know that supermarket fruit and vegetables may be covered in pesticides, or be scary GMOs. If you do make the effort to buy local, you can sometimes get better quality produce that wasn't washed by robots in a factory and transported across the country by truck. You can support a local business and build a circle of trust. You can indeed trust the little, local farmer.

We can also trust privacy-respecting tech. Many great grassroots associations, like [Framasoft](https://framasoft.org), have the right idea. Image is everything, and we need to take that image and resize it. Framasoft, and similar associations, have a mission and a message that isn't based on fear. They are a respectful response to the question of how to protect our privacy online.

Very often, when I try to show somebody that application B is a drop-in replacement for application A, they argue that they would rather put their trust in an MNC like Apple than in some guy with a GitHub. This is akin to somebody not buying from the local market because they trust the supermarket strawberries imported from halfway around the world more. Bad analogy, I know, but the gist is there. Free and open source software that respects our privacy and has been vetted by users and associations deserves our trust and support the same way a local farmer does.

It is about time we changed the metaphors we use. Both application A and B do the same thing, but one may contain pesticides that can have an effect your well-being.

# Final words

This started as a list and just kept going. I do mean what I say, though. I am optimistic that a change is on the horizon. Thanks for reading and, please, shoot me an email or message on Mastodon to let me know what you think.
