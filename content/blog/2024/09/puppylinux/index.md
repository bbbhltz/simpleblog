---
title: "Who's a good boy? A Puppy Linux Mini-Review"
subtitle: "they said taking your puppy to work is good for your well-being"
description: "A rather positive review and comment on using the Puppy Linux operating system in the workplace"
slug: puppylinux
date: 2024-09-29T19:02:45+02:00
lastmod: 2024-10-10
featured_image: /blog/2024/09/puppylinux/featured.webp
draft: false
katex: false
tags:
  - Linux
  - Review
toc: true
next: true
nomenu: false
syndication:
  - https://jstpst.net/f/just_post/11291/who-s-a-good-boy-a-puppy-linux-mini-review
  - https://news.ycombinator.com/item?id=41769707
  - https://tilde.news/s/bz2lf0/who_s_good_boy_puppy_linux_mini_review
  - https://beehaw.org/post/16397165
  - https://framapiaf.org/@bbbhltz/113251387786471365
---

<details>

<summary>Background ramble...</summary>

Having a computer just for work is an unfortunate reality. Some of my colleagues use their personal devices every day. I do as well on occasion, usually when I need to do that one little thing that just frustrates me on Windows.

About eight years ago I was surprised when my employer gave us all laptops for work. Then, about three years ago they had us trade in our laptops for new laptops.

That new computer though. It is one of those Dell convertible deals with a touchscreen. Not complaining, but I have no use for that. I don't know if any of my colleagues use it. I must gripe though. It is stronger than me. I live in France, this is what people do here!

The new computer is a little heavier and the battery life is not great. The battery life has only been worse since this summer when a new update was pushed (I mean, the employer owns it, they have to update it) and after many reboots there was a new service running in the taskbar---one of those corporate device management things.

Now, not sure if it is the update or the new software installed, but going from a 9-hour battery life to just over two hours is the outcome. This first month back at work for the school year has been a pain in my back carrying around the charger as well as the laptop.

This, combined with not being able to change the featured apps in the start menu, have brought me where I am today: in need of a portable Linux distribution.

I tried the different variations of running a VM. It wasn't for me. I am not allowed to wipe the drive and install my favourite distro, I am not allowed to dual-install (well, I could probably get away with it).

18 years of being that one annoying person who uses Linux, and I've never tried a portable Linux distribution. It turns out there are a few quirks.

</details>

## What is Puppy Linux?

That's actually a pretty deep question. Puppy isn't a single distribution, it is a *family* of releases. The particularity of Puppy is that it is one of the distributions that---like Alpine, antiX, and Porteus---[can run from RAM](https://en.wikipedia.org/wiki/List_of_Linux_distributions_that_run_from_RAM). Puppy is meant to be easy to use, come preloaded with minimal tools for daily usage, and be light on resources.

The project dates back to 2003, the rest [is history](https://puppylinux-woof-ce.github.io/history.html). Today, the mainline collection of Puppy [releases](https://forum.puppylinux.com/puppy-linux-collection) includes Ubuntu-, Debian-, Slackware-, and Void-based variants. I decided to use the Debian 12 Bookworm-based version called BookwormPup64 (version 10.0.8). I did this because I am used to Debian and am fine with older software.

Puppy can run from a USB with persistent storage, but many users do install it to their hard drive. The advantage here, at least from my limited knowledge of Puppy, is that you can mess around with your system, and if you make a mistake you can reboot without writing those changes to a special file or folder. I opted for a *frugal* installation to a USB device.

## Installation

Parts of this are very similar to other Linux installations: get the ISO > use `dd` to write it to USB > boot from that USB > run installer. Despite having used Linux for many years, I always like to see how many clicks it takes to find instructions for writing to a USB (in part because I refuse to memorize this command). Puppy is a little confusing here. The homepage has two different links for downloads, a link to the forum, and a link to the wiki (more on that after). From the [homepage](https://puppylinux-woof-ce.github.io/index.html) to the [wiki page containing the required information](https://wikka.puppylinux.com/LiveUSB), it is three clicks.

The suggested command is:

```
# time sudo dd bs=4M conv=notrunc,fdatasync oflag=direct status=progress if=<DISK IMAGE FILE> of=/dev/sdX
```

After booting from the USB, choosing the keyboard layout and other options in a very easy-to-use setup program I used one of the provided installation programs to install Puppy to another USB. That's it. No Calamares, no TUI, just waiting to copy and format. There are more options to dig into but I chose a setup that got me going quickly. The nitty-gritty details are better explained across the Puppy forum, but the end result is an operating system on a USB flash drive that you can write to.

## Initial Impressions

The *expectations vs. reality* feeling was strong with Puppy. I very much expected something *janky*---the main website is hosted on GitHub, there are multiple places to find the ISOs, the wiki is a mess, the lexicon seems silly (pups, puplets, kennels, etc.)---but the reality did not give that impression.

It is fast and snappy, looks good (we're obviously not in GNOME or Plasma territory here), and I was connected to my home network in less than 60 seconds. Within another minute I found the link---through a context menu on the desktop---to remove the many desktop icons. [Conky](https://github.com/brndnmtthws/conky) starts automatically, there is a compositor running ([picom](https://github.com/yshui/picom)) and JWM is the window manager.

The applications menu is filled with applications and scripts---many of which I'd never heard of. It is clear that the Puppy team meant for the base install to cover many bases while still being minimal.

A major surprise was opening the terminal and seeing `#` instead of `$`. That's because you are the **root** user by default when running Puppy.

## Getting online at work

My workplace, a university, uses an MS-CHAPv2 + Protected EAP method for connecting, requiring an ID and a password. The first day I used Puppy at work I clearly hadn't had enough coffee and just couldn't get it to work. This is despite having access to *several* connections methods all presented in a single wizard.

The Internet Connection Wizard lets you launch and use different network connection tools, like ConnMan, SNS, Network Wizard, and Frisbee.

![Internet Connect Wizard Choices](networkchoices.webp "Internet Connection Wizard choices")

On the walk home from work my brain rebooted, and I realized that I just needed to edit `/etc/frisbee/wpa_supplicant.conf` like this:

```conf
ctrl_interface=/var/run/wpa_supplicant
update_config=1

network={
	ssid="my_ssid"
	key_mgmt=WPA-EAP
	eap=PEAP
	identity="my_identity"
	password="my_password"
	phase2="MSCHAPV2"
}
```

## Software

This tiny distro is packed with goodies. BookwormPup64 comes with around 900 packages from the Debian repository, and over 1,000 more pre-configured packages. Some of these packages are useful tools and forks that you could find yourself using regularly. PeasyScan, for example, is a lighter version of the XSane image scanner with a simpler interface.

The software from the Debian repository is all marked as "held" by default. That means that if you need to update a particular package, you would need to unmark it. It also means that if you decide to install a package from the Debian repository and update it later, you will only upgrade the packages that you installed.

There are loads of things to find when you start exploring: [LXTerminal](https://github.com/lxde/lxterminal) and [lxrandr](https://github.com/lxde/lxrandr) from the venerable [LXDE](https://www.lxde.org/) project, [ROX-Filer](https://github.com/rox-desktop/rox-filer), [JWM](https://github.com/rox-desktop/rox-filer), [qpdfview](https://launchpad.net/qpdfview), [DeaDBeef](https://deadbeef.sourceforge.io/), [EasyTAG](https://wiki.gnome.org/Apps/EasyTAG), [Leafpad](http://tarot.freeshell.org/leafpad/), [Viewnoir](https://github.com/hellosiyan/Viewnior), [mpv](https://mpv.io/), CUPS for printing, a firewall and advert blocker, [AbiWord](https://github.com/AbiWord/abiword), [Gnumeric](http://www.gnumeric.org/), [gFTP](https://github.com/masneyb/gftp), heck, it even comes with [jq](https://github.com/jqlang/jq). That's just what I can name off the top of my head.

![Pup Advert Blocker](adblock.webp "Pup Advert Blocker")

The added helpers and GUI front-ends get the job done. Most, if not all, of the different helpers have a "Help" button that open local documentation or link to a website or forum post that explain the functionalities. I found myself launching some of them out of curiosity and discovering more information than I needed.

### JWM

[JWM](https://joewing.net/projects/jwm/), or Joe's Window Manager, is the first thing you will notice using Puppy. Yes, there is a dated feeling to it. Even so, the modern conveniences are there---keyboard shortcuts for moving windows around, virtual desktops, tonnes of helper apps to make it look the way you want---and, most importantly, it isn't massive or use loads of memory. I cannot find a decent or recent source, but for a system that runs from RAM, you want to go as low as possible. Some sites say JWM uses as little as 3 MB while others say around 30 MB.

![JWMDesk](display.webp "JWMDesk, lxrandr, and display controls")

Initially, I thought I might replace JWM with something I have used before, like Openbox or LXQt, but that idea faded fast. In fact, I would even consider installing this on a modern machine.

### ROX-Filer

![ROX-Filer](rox.webp "ROX-Filer")

The file manager that comes with Puppy is ROX-Filer. 18 years on Linux and I've never heard of this piece of software. That makes sense because I've never heard of [ROX Desktop](https://en.wikipedia.org/wiki/ROX_Desktop). There are enough options under the hood appearance-wise, and it is more flexible than it looks. It has thumbnails and drag-and-drop and other niceties. Some keyboard shortcuts are a must, <kbd>/</kbd> opens the location bar, for instance, and <kbd>CTRL+E</kbd> will resize the window, so there is no white space.

Bottom line: ROX-Filer is light and works perfectly for this distribution.

### Firefox-ESR

Again, an *expectations vs. reality* situation: Firefox is pre-configured to reduce annoyances, and disable unwanted features, for example:

- Do Not Track is on by default (`privacy.donottrackheader.enabled=true`);
- Pocket is off (`extensions.pocket.enabled=false`);
- Firefox accounts is disabled (`identity.fxaccounts.enabled=false`)

This makes it a better experience than the default Firefox-ESR on Debian and was totally unexpected. The same goes for staying up-to-date. This Firefox-ESR is not from the Debian repository, it is installed by default and updates in place. Some people might find this annoying, but then again other people will want to have the latest version without checking the repository first.

![Firefox ESR about window updating](ffox.webp "Firefox ESR updating to the latest version")

Firefox and other Internet applications, like [Claws Mail](https://www.claws-mail.org/), all run as a special user, **spot**, instead of **root**.

{{< box info >}}
In Puppy Linux, **spot** is a restricted user account that allows you to run certain Internet applications with reduced permissions, enhancing security.

For example, the **home** directory for **spot** is in `/root/spot/` and a browser will be limited to editing files in this directory.
{{< /box >}}


### Adding something new

When it comes to adding new software, you will want to stop and read the manual. There are different ways to handle this. Yes, you can straight up install something from the repository. ~~But, installing large apps, like LibreOffice, can be handled more efficiently by choosing to install a PET or SFS version, or using an AppImage or Flatpak. I threw caution to the wind and decided to install TeX Live in order to do some work, and I've had no problems.~~

~~Still, judging from what I've gleaned from my dive into the forums, the rule of thumb is: if it is big, check for other methods of installation first.~~

**EDIT**: Using this version of Puppy (because it is based on Debian), it is recommended to prioritize the use of `apt` and install from the repository rather than using PET or SFS files. Obviously, Flatpak remains an option.

## Community & Documentation

Speaking of the [Puppy Linux Discussion Forums](https://forum.puppylinux.com/index.php), that is where you will want to go for documentation. There is a wiki, but it needs some TLC. The forum has 3600+ members and a small team of administrators and moderators that can help out with lots of issues. I highly recommended reading through the different posts on the [Getting Started](https://forum.puppylinux.com/viewforum.php?f=184) subforum. Thankfully, it is not hosted on Discord or some closed community, it is good ol' phpBB.

The more time spent browsing the forums, the more I came to realize that this *easy to use* distribution is quite complicated. In comparison to bog-standard Linux experience---download ISO; install; create account; use---there are elements to this distribution that are so unique that the solution to some problems are only on this forum and not discussed elsewhere. And this is not taking into the account that some threads veer off into discussions about derivatives and build scripts and remixes.

## Nitpicking

I do have a few things to nitpick.

First, the website needs clarity. There are two download buttons on the navigation pane and the wiki has less information than the forum, for example.

Next, some of the helper GUI's lack consistency. Sometimes a word is capitalized, sometimes not. Small details.

Finally, this distribution has a bit of an issue depending on some conditions:

- **If** this is your first Linux experience, the ease of use will be welcoming. You will get an idea of the limitations and strengths compared to other operating systems. But, what if you decide to continue using Linux and try something else? If that were to happen you might ask yourself, upon installing any other popular distribution, why there is a login screen when Puppy does not have one. Or, why there is a need to use `sudo` when Puppy does not, or other questions particular to Puppy.
- **If** this is not your first rodeo---my case---you may be stumped by the torrent of new terminology---[what is a `pupmode`](https://bkhome.org/archive/puppylinux/development/howpuppyworks.html)?. You will find yourself double-checking how things work and asking why the hell anyone would download a package from MediaFire that a random person on the Internet linked to on a forum. You might try to install one of your favourite applications, not understand why it won't run, and then remember that not every application will run as **root**.

Those *ifs* are just nitpicking, because if you had taken the time to peruse the forum, you'd have realized that this is not your average GNU/Linux affair. This is Puppy.

## Can I teach with it?

**tl;dr** YES.

After the Wi-Fi mishap, it was smooth sailing. I did add [ARandR](https://christian.amsuess.com/tools/arandr/) to help with connecting to projectors, but beyond that I could do what I needed:

- Install what I needed to make my slides.
- Modify office documents with the LibreOffice AppImage or something like PDFtk.
- Use the projector.
- Browse the Internet and send email.
- Annotate a PDF.
- Take screenshots.

(That is not a comprehensive list, obviously.)

## Conclusion

While Puppy, by name, was not unknown to me, the way it works was. Discovering it was akin to teaching an old dog a new trick. It beat my expectations and the out-of-the-box software works like a charm. I wouldn't, however, consider it as something easier to use than Mint or even Debian.

## Final Grade

*You're a good dog, yes you are!* 7/10.

---

## Syndication

*I don't normally do this, but I decided to put this post on more than just my Mastodon...*

- [jstpst](https://jstpst.net/f/just_post/11291/who-s-a-good-boy-a-puppy-linux-mini-review)
- ["hacker" "news"](https://news.ycombinator.com/item?id=41769707)
- [tilde.news](https://tilde.news/s/bz2lf0/who_s_good_boy_puppy_linux_mini_review)
- [beehaw](https://beehaw.org/post/16397165?scrollToComments=true)
- [mastodon](https://framapiaf.org/@bbbhltz/113251387786471365)
