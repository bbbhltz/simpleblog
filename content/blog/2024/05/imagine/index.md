---
title: "Imagine"
subtitle: "a better future is just around the corner"
description: "A satirical comment on Generative Artificial Intelligence where I announce my plans to become rich and famous"
slug: imagine
date: 2024-05-09T19:54:30+02:00
# lastmod:
# featured_image: /blog/2024/05/imagine/featured.png
draft: false
katex: false
tags:
  - AI
next: true
nomenu: false
---

# Introducing Your Future

Humans of earth.

I have an announcement to make.

I am going to be a musician.

Now, just to be completely upfront with you, I don't know how to play any instruments or sing 

Yet.

But, imagine if I could. Music has the power to change the world. I could bring about peace, I could raise awareness, I could save our planet for future generations. And you can be a part of this!

## What do I need from you?

Despite being entirely non-profit, I need your patience and support while I learn music theory and train. I also need your financial support to pay my rent and my bills. Your investment will fund my travels as I learn the world's languages and ~~appropriate~~ ~~plagiarize~~ study their musical and cultural contributions. Funds will also be used to purchase the required instruments and materials. Environmental costs are negligible because they will be offset (in the future) by the good I will bring to the world.

## How long will this take?

*How long are your legs?*

What I want to do is what the world wants and needs. So, the timeline for this is not set in stone, but we have a rough estimate:

- **(Phase 1) Pre-Training or Theory**: minimum 6 years for a Master's degree in music theory plus 2 to 4 years for a doctorate degree. I won't make my degrees public.

- **(Phase 2) Fine-Tuning and Training**: minimum 15 years to learn to approximately imitate all Western styles of music and build a "large music model." Non-Western styles are out of scope for this project. 

- **(Phase 3a) Taking Requests**: minimum 5 years of taking requests and playing songs, so other people can claim they wrote them. I plan on ~~exploiting~~ hiring people in developing nations to review requests. This will reduce poverty and raise standards of living in those countries.

- **(Phase 3b) Taking Paid Requests**: concurrent with Phase 3a, premium clients will have access to better songs. Possible use cases for this would be film and advertising.

- **(Phase 4) Becoming Recognized as a Master**: 10 to 15 years of non-stop world tours, collaborations, album production, and private concerts.

This means that by or around 2050 I will be able to produce something that can save the world.

## Possible Problems

- **Competition**: a lofty goal such as this one will attract competitors. Competition, though, is just proof of how powerful music can be and how important it is for you to ~~give me your money~~ invest today for a better tomorrow.

- ~~Hallucinations~~ **Duds**: the quality of the songs depends on the catalogue of ~~stolen~~ studied songs and styles. Continuous adjustments will need to be made. These dudes are not ~~bugs~~ failures, but rather a telltale feature of a worthwhile investment with a high return.

- **Theft of content from other artists**: if an individual requests a song and then tries to pass it off as their own, that is fair, even if the song is similar to an existing piece of work. Schools, studios, and labels will need to adjust their expectations. It will be impossible for artists to complain because my method of study is **opt-out**: it is the artists' responsibility to make it known they don't want their work to become part of my large music model.

## Exciting Opportunities

Partnering with me is one of the greatest opportunities the world has ever seen. You can increase the value of your brand. Here are just a few examples of how you can help yourself.

-  **Bespoke Musician**: I can be the official musician of your advertising agency, theatre, film, television or video game studio. Your current staff will be ~~gutted~~ free to pursue their own personal interests and career opportunities.

- **National Musician**: I can create the music that represents your nation around the world. The music I create will motivate your workforce, increase national pride and therefore create economic prosperity.

- **Electronics OEMs**: Phones need ringtones. Partnering with me will give your brand the unique and recognizable sounds and increase your bottom line.

- **Interviews and Articles**: by interviewing me and featuring my ~~grift~~ mission in your papers and online, your readership will increase. ~~Clickbait~~ Sincere stories featuring me in the title are a surefire way to raise awareness, because the more people talking about me, the more likelihood there will be of gaining a new reader.

- **Retail**: the right music can push consumers to buy more and be more indulgent with their money. My music will help you control customers in your retail locations.

- **Road Safety**: music calms the savage beast, and my music will reduce road rage and save lives.

- **International Relations**: in a world filled with uncountable horrors, a musician of the world and for the world is our only hope.

- **Interplanetary Exploration**: by mastering every style the (northern part of) planet has to offer, I will be able to write the music that takes our people beyond our galaxy and usher in a future of interplanetary exploration.

## Final Words

I'm hoping to grow into an institution. As a non-profit, my aim is to build value for everyone rather than a group of rich white guys.

If you too dream of a future and imagine a shared world of people living for today, in peace, with no fear or hunger, I hope some day you'll join me, and the world will live as one.

Show your support and save the future, today!

[Click here](https://ko-fi.com/bbbhltz)
