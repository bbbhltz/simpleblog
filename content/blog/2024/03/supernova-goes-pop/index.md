---
title: "Supernova Goes Pop"
subtitle: "...you think it's over, but the supernova don't stop"
description: "A blog post highlighting the negative aspects of Generative Artificial Intelligence using the lyrics of a hard rock song as subheadings"
slug: supernova-goes-pop
date: 2024-03-30T12:26:04+01:00
# lastmod:
featured_image: /blog/2024/03/supernova-goes-pop/featured.webp
draft: false
katex: false
tags:
  - AI
  - Technology
next: true
nomenu: false
---

{{< box info >}}

The title of this post is indeed a reference to the song Supernova Goes Pop by Powerman 5000 from their album *Tonight The Stars Revolt!* The headings are lyrics from the song.

<details close>
<summary>Song Lyrics</summary>

## Supernova Goes Pop by Powerman 5000

Are you the future or are you the past? \
Have you been chosen or are you the last? \
The message was sent, it seems so unreal \
'Cause now I'm made of plastic, wire and steel \
And steel

Follow for now and follow for this \
'Cause everybody follows for nothing at all \
Follow for now and follow for this \
'Cause everybody follows for nothing at all

Because, supernova, yeah, supernova \
Supernova goes pop \
Supernova, you think it's over, but \
The supernova don't stop

Can you explain just what you are? \
'Cause I've never been this close to a star, alright \
The message was sent, you know what to do \
'Cause everybody needs to be someone, don't you? \
Right, don't you?

Follow for now and follow for this \
'Cause everybody follows for nothing at all \
Follow for now and follow for this \
'Cause everybody follows for nothing at all

Because, supernova, yeah, supernova \
Supernova goes pop \
Supernova, you think it's over, but \
The supernova don't stop

Supernova, yeah, supernova \
Supernova goes pop \
Supernova, you think it's over, but \
The supernova don't stop

Let's go

Supernova, yeah, supernova \
That supernova goes pop \
Supernova, supernova \
That supernova don't stop

Supernova, supernova \
That supernova goes pop, alright \
Supernova, supernova \
Supernova don't, it don't, it don't

Stop

</details>

{{< /box >}}

## Are you the future or are you the past?

![alt](featured.webp "Artwork by Ezra Jack Keats, page 36 in Danny Dunn and the Homework Machine")

>[…] “Now Minny knows everything in all our school books.”
>
>“Phew!” said Joe, wiping his forehead, “You know, that was hard work storing all that information in the machine. I didn’t realize there was so much to know. Maybe it’d just be easier to do our homework every day.”
>
>“I don’t think so,” Danny said. “Sure, it was hard work. But now we’re free forever.”

The year was 1958. Before *my* time, but not before the time of common sense, forethinking, and Science Fiction. In *Danny Dunn and the Homework Machine*, by Jay Williams and Raymond Abrashkin, the titular Danny is up to shenanigans again. Along with his friends Joe and Irene, they begin using a computer (Miniac, or "Minny") to cheat on their homework. What could ever go wrong?

For starters, Minny needs information---lots of information. And, the person feeding the information into Minny needs to understand the problem:

>Professor Bullfinch shook his head. “No. It never can be Beethoven, Mrs. Dunn. No matter how intelligent the computer is, it is only a machine. It can solve problems in minutes that would take a man months to work out. But behind it there must be a human brain. It can never be a creator of music or of stories, or paintings, or ideas. It cannot even do our homework for us---we must do the homework. The machine can only help, as a textbook helps. It can only be a tool, as a typewriter is a tool.”

For decades now, writers have been telling us these fantastic tales of what the future could be. Asimov, Gibson, Stephenson, Wells, Leckie, Chambers, and an infinite number of other science and speculative fiction writers have filled our heads with wondrous and worrisome visions of what *may* come.

Well, is it coming or not? The future, I mean. It all seems a little dull, doesn't it? Like watching a film after having read the entire plot on Wikipedia.

The latest trend---AI---feels like some never-ending remake of a reboot of a spin-off of past trends. As a layperson who tries to keep their finger on the pulse of trends like this, because I like technology, I can't help but feel like we've been here before.

Generative AI, like ChatGPT, has been following a path set out by authors and experts for several years now. Like other trends (e.g. web3, blockchain, cryptocurrency, NFTs, etc.), it has grown to dangerous proportions and---keep in mind that I am just an observer---it is very likely that this bubble is going to "go pop."

## Follow for now and follow for this, 'cause everybody follows for nothing at all

Everybody and their neighbour is on the AI train now. I'm thinking of a shape, the name is on the tip of my tongue. Imagine a situation where someone rich or powerful wants to be richer or more powerful. The only way to do that is to get some people who are just a little less rich or powerful than you to give you their money or their loyalty. This situation continues until there are a great number of not-so-rich or powerful people giving something to those who are a little richer or more powerful than them.

This is what has happened with AI.

Companies like OpenAI have been blown out of proportion and are being copied left, right, and centre. Average folks have been had. *My students have been had!* Here we are getting ChatGPT to do things for us, like our homework, and for what? For the benefit of OpenAI. We are giving our time to OpenAI's ChatGPT. We are not employed my them, but our *prompts* are valuable information for them and their partners.

Every time we open our news feeds, we see story upon story about the apps (Copilot, Midjourney), models (GPT-4, Mistral), and a whole lot about the infrastructure (looking at you Nvidia and cloud-computing gang). As of writing, [There's An AI For That](https://theresanaiforthat.com/) lists over 13,000 AIs for some 16,000 tasks (many with [that *super sweet* `.ai` TLD](https://spectrum.ieee.org/ai-domains)). With all the talk of money, it's no wonder so many exist:

>The combined market value of Alphabet, Amazon and Microsoft has jumped by $2.5trn during the AI boom. Counted in dollars, that is less than three-quarters of the growth of the hardware layer, and barely a quarter in percentage terms. Yet compared with actual revenues that AI is expected to generate for the big-tech trio in the near term, this value creation far exceeds that in the other layers. It is 120 times the $20bn in revenue that generative AI is forecast to add to the cloud giants’ sales in 2024. The comparable ratio is about 40 for the hardware firms and around 30 for the model-makers.
>
> <cite>[*Just how rich are businesses getting in the AI gold rush?* in The Economist](https://www.economist.com/business/2024/03/17/just-how-rich-are-businesses-getting-in-the-ai-gold-rush)</cite>

Many of these AIs promise great savings to their clients. Some keep tabs on fast-food workers, like [Hoptix.ai](https://www.hoptix.ai/) which promises to "Increase your restaurants net profit by 15% in 90 days," and if you find that hard to believe, you haven't seen the *totally rad* graphic they have on their landing page:

![graphic stolen from Hoptix website showing how using Riley can lean to higher profit](wick.webp "Smile for the camera")

Not all employees like this idea. That isn't surprising. Hoptix's Riley makes a point on their website of how it can be used to *reward* employees. Surveillance in the workplace should not be taken lightly, however. If an employer trusts an app to decide who to reward, it probably isn't hard for the app to convince them to dismiss an employee.

## Can you explain just what you are?

Is the current form of AI intelligent? Is it even unbiased?

What do those words, 'intelligent' and 'unbiased' even mean?

Red flags have been going up from the start:

> Timnit Gebru didn’t set out to work in AI. At Stanford, she studied electrical engineering---getting both a bachelor’s and a master’s in the field. Then she became interested in image analysis, getting her Ph.D. in computer vision. When she moved over to AI, though, it was immediately clear that there was something very wrong.
>
>“There were no Black people---literally no Black people,” says Gebru, who was born and raised in Ethiopia. “I would go to academic conferences in AI, and I would see four or five Black people out of five, six, seven thousand people internationally.… I saw who was building the AI systems and their attitudes and their points of view. I saw what they were being used for, and I was like, ‘Oh, my God, we have a problem.’”
>
><cite>from [*These Women Tried to Warn Us About AI*](https://www.rollingstone.com/culture/culture-features/women-warnings-ai-danger-risk-before-chatgpt-1234804367/) by Lorena O'Neil (Rolling Stone)</cite>

{{< box tip >}}

I urge you to read the rest of that article. I took the quote from the very start because I didn't want to spoil any part of it.

{{< /box >}}

The people and companies behind the models have a lot in common:

- usually men in charge (OpenAI, Anthropic, Cohere, Hugging Face, etc.);
- usually US-based (Silicon Valley seems popular);
- and occasionally associated with names you've heard, but never realized had a foot in the AI door (Meta (Facebook) has LLaMA, Anthropic was founded by members of OpenAI, and most have received funding from Google, Nvidia, Amazon, AMD, Qualcomm, and other big tech names).

(Again I will underline that I am just observing.) 

From my vantage point, these models and apps are all coming from the same type of people. That is a red flag. Again, I cannot blame a person for taking advantage of a trend and trying to earn a buck. There are plenty of [ethical questions](https://axbom.com/aielements/) that can be raised, but that might be beyond the scope of this little blog post.

AI as it stands is not really that smart. Sure, it can spit out some words, but not in a very natural way. As a professor in a country where English is not the mother tongue, I have the *immense pleasure* of reading hundreds upon hundreds of papers written by non-native--speakers of English. Let me tell you one thing: any teacher or professor with at least a month of experience under their belt will immediately recognize a ChatGPT paper, report, or written activity.

(Occasionally, students are shocked by their grades. They point at their paper, steeling their nerves, and ask for feedback. A walk-through essay without a single bibliographical entry? Really?)

AI just isn't that bright for the moment. Some of them can *almost* [pass an IQ test](https://www.maximumtruth.org/p/ais-ranked-by-iq-ai-passes-100-iq). Not many, though, are particularly gifted when it comes to reason, [just ask Sally about her brothers and sister](https://benchmarks.llmonitor.com/sally).

## Supernova goes pop

>In January, the International Energy Agency (IEA) issued its forecast for global energy use over the next two years. Included for the first time were projections for electricity consumption associated with data centers, cryptocurrency, and artificial intelligence.
>
>The IEA estimates that, added together, this usage represented almost 2 percent of global energy demand in 2022---and that demand for these uses could double by 2026, which would make it roughly equal to the amount of electricity used by the entire country of Japan.
>
><cite>[*AI already uses as much energy as a small country. It’s only the beginning.*](https://www.vox.com/climate/2024/3/28/24111721/ai-uses-a-lot-of-energy-experts-expect-it-to-double-in-just-a-few-years) by Brian Calvert (Vox)</cite>

Things are heating up (in more ways that one). Climate-wise, this AI boom really could have chosen a better moment. And claiming that AI can solve the problem is *exactly* what would happen in a sci-fi novel before everything goes *boom*.

You know something is up when you see reports titled ["How AI Can Speed-Up Climate Action"](https://www.bcg.com/publications/2023/how-ai-can-speedup-climate-action) by the Boston Consulting Group and (\*sigh\*) Google. I for one am relieved to hear that a "**responsible deployment of AI**" could "[have] the **potential** to unlock **insights** that **could** help mitigate 5% to 10% of GHG emissions by 2030." And we can all breathe a sigh of relief knowing, now, that "87% of executives believe AI has potential to address their climate challenges"

Malarky.

Rubbish.

Horseshit.

>In the US, there is already evidence that the life of coal-fired power plants is being prolonged to meet the rising energy demands of AI. In just three years from now, AI servers could be consuming as much energy as Sweden does, separate research has found.
>
>Much of this increased energy demand comes from the added complexity of AI operations – generating AI queries could require as much as 10 times the computing power as a regular online search. Training ChatGPT, the OpenAI system, can use as much energy as 120 US households over the course of a year, the report claims.
>
> <cite>[*AI likely to increase energy use and accelerate climate misinformation – report*](https://www.theguardian.com/technology/2024/mar/07/ai-climate-change-energy-disinformation-report) by Oliver Milman (The Guardian)</cite>

Whether it is out of pure laziness, the desire to make a quick buck, or even the sincere hope to make things better, users of these tools have become complicit in the destruction of the planet in the same way Pepsi did with plastics.

![comic 199 from Mud Company: panel 1: Badger sits in front of a laptop. Badger: A.I., make me a Ronald McDonald as Han Solo.panel 2: A cord runs from Badger's laptop to a large industrial building in the background, labeled Server Farm. It sends huge clouds of brown smoke into the air and goes GRIND GRIND GRIND GRIND panel 3: A Ronald McDonald Han Solo appears on Badger's screen. Badger: ha ha!panel 4: Skunk, with beret, palette and brush: I could have drawn that for you. Badger: This way is cheaper.panel 5: Badger, holding a phone: Oh, fine. Do you take crypto?A cord runs from Badger's phone to another giant server farm, sending up more huge clouds of brown smoke.](grind.webp "Grind, baby, Grind https://mudcompany.thecomicseries.com/comics/199")

## The Supernova don't stop

I don't think the interest in AI will die down soon. But, "soon" can be interpreted in different ways.

It is time to look beyond this boom and to the next, because it is too late to be first in the door. The disruptors have gone and disrupted. Sam Altman has made it to the cover of magazines like so many other great disruptors (Elizabeth Holmes and Sam Bankman-Fried come to mind).

It is time that more of us simply refuse AI. KFC uses AI to track employees? Don't eat at KFC. Some search engine gives you creepy AI results? Change search engines. You get the idea.

This is not ludditism. This is not refusing to let AI loose on finding cures for diseases (despite the harm it does to the planet). This is refusing AI the same way many of us refuse to use a f-cking QR code instead asking for a printed menu in a café or restaurant, or when we refuse to install an app on our phones when the website works fine and our phones have browsers.

AI just isn't cool enough to be *everywhere*.

## Non-web References

Powerman 5000. (1999). Supernova Goes Pop [Song]. On *Tonight the Stars Revolt!*. DreamWorks Records.

Williams, J. & Abrashkin, R.. (1959). *Danny Dunn and the Homework Machine*. Whittlesey House.

