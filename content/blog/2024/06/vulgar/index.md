---
title: "Vulgar; Popular"
subtitle: "a mini-rant on self-help books"
description: "A ranting blog post about self-help books and their presence and prominence in bookstores"
slug: vulgar
date: 2024-06-22T10:05:35+02:00
lastmod: 2024-10-12
featured_image: /blog/2024/06/vulgar/featured.webp
draft: false
katex: false
tags:
  - Opinion
  - Books
next: true
nomenu: false
---

I got beef, and I'm old enough now to whinge about it. Also, my recent entrance into midlife has unlocked the profuse swearing buff.

Fuck.

People read books. I read books. You have probably read a book. Some books come from libraries, so from your own family bookshelf, and some from shops.

One of my hobbies is walking through the book section of the local FNAC (a French retail store that also happens to sell books) and shaking my head at the "Lifestyle and Self-Help" section. It is just so frustrating, for multiple reasons.

Young and stupid, I never would have even looked at this section. But, circa 2006 *The Secret*[^secret] was gaining popularity and I heard people talking about it. A heard it a lot. And, strolling through the book store I saw it prominently displayed on a [gondola](https://displaymaxinc.com/retail-fixtures/gondola-shelving/gondola-shelving-101/). It sported stickers advertising that it had already sold *so many* copies, and that it was based on the *hit* film.

I read the blurb and saw the words "law of attraction"[^law] and said, "fuck off," because even the 22-year-old country bumpkin that I was could tell this was some rank bullshit. Couldn't they?

That was then. This is now. In the years that have passed, that section, which was just a gondola, has grown. Multiple metres of shelves are home to pseudoscience, esoteric nonsense, and the ghostwritten manuscripts of forgotten YouTubers.

I had learned by then that the French use the word *vulgariser* to explain the phenomenon of making something accessible and popular. I subsequently learned that we use the same word in English (to vulgarize), and I find that perfect. Many of these books take a word from the PSYCH101 glossary, somehow smear it across several hundred pages of anecdotes, and slap a \*jazz hands\* *science* label on it. Vulgar, indeed.

And this is where my first complaint comes from. Mixed in with this utter horseshit, there are legitimate, competent, authors. They are, in fact, scientists and professionals, and they have their work trapped in this section alongside shysters. Daniel Goleman's *Emotional Intelligence* is labelled as a psychology book, but found in this section, for example. I feel like this is a suitable section for it, but it is not the same as the other books in the section.

As I write this, the most prominent books in this section are written by Natacha Calestrémé, Fabien Olicard, Joseph Murphy, and Mark Manson.

Spoiler: Calestrémé most known for promoting pseudoscience, mediumship, and numerology[^1]; Olicard is a mentalist with a YouTube channel[^2]; Murphy was a New Thought minister who died in 1981[^3]; and Manson, who is known for his book *The Subtle Art of Not Giving a F\*ck: A Counterintuitive Approach to Living a Good Life*---which is a (meta) self-help book about how self-help books offer meaningless and impractical advice---is a blogger that also self-published a book called *Models: Attract Women Through Honesty*.

(It is also cute to note that Murphy's *The Power of Your Subconscious Mind* is currently #11 on Amazon UK's [best-selling... address books](https://www.amazon.co.uk/gp/bestsellers/officeproduct/507850/ref=pd_zg_hrsr_officeproduct)?)

Without getting elbow-deep in this heap of shit, I'll move onto my second point: Some works, which are no more than self-help books, are found in business and management sections of the store.

Tim Ferriss' *4-Hour Workweek* (2007) is in the *Management>Human Resources* section of the shop. As a reminder, *The 4-Hour Workweek: Escape 9--5, Live Anywhere, and Join the New Rich* is indeed a self-help book, described by the author as "lifestyle design."[^4] And, if you read the book---like I did---you'll note that the author even (strongly) suggests generating income by selling an "information product." He is talking about a book or website.

Business is business, and these shops can put the books wherever they want, but if this were the films or music department, it would be a different story. I listen to Johnny Cash and Cattle Decapitation, but we cannot put them on the same shelf. *The Human Centipede* is a horror film. We put the Shōjo and Shōnen manga on different shelves and if I ask a salesperson for advice on what to buy for my *x*-year-old child, they will be able to give advice.

Here comes a little straw man argument to think about: if a parent with a teenager suffering from depression or GID asks for help, what will the salesperson do? Take them to the bestselling parenting books section, which happens to overlap with the self-help section? What book will they walk away with? Something written ages ago, like Spock's *Baby and Child Care*, something popular, like Faber and Mazlish's *How to Talk So Teens Will Listen and Listen So Teens Will Talk*, or something from the psychology section that would (hopefully) bring about the realization that this subject is too important to be *vulgarized*, and professional help is the best way forward?

Books are amazing, and the self-help section is not without merits. But, reading a 120-page paperback by an influencer will not grant you an honorary degree in that subject or make you a *researcher*. In many cases, it will make you a gullible rube; a mark. "Over a million copies sold" just means that the publisher bought the copies, or the influencer conned their followers to buy them; the shills make the scam look legitimate.

With AI-written books on the rise, the self-help section will probably become a playground for grifters. It is time to remind our friends and children that, while entertaining, they cannot replace science and common sense and may even be so open to interpretation that they are dangerous.

[^secret]: https://en.wikipedia.org/wiki/The_Secret_(Byrne_book)
[^law]: https://en.wikipedia.org/wiki/Law_of_attraction_(New_Thought)
[^1]: https://fr.wikipedia.org/wiki/Natacha_Calestr%C3%A9m%C3%A9
[^2]: https://fr.wikipedia.org/wiki/Fabien_Olicard
[^3]: https://en.wikipedia.org/wiki/Joseph_Murphy_(author)
[^4]: http://www.timferriss.com/
