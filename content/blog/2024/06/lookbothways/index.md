---
title: "Look both ways"
subtitle: "A short story about AI"
description: "A short story about Generative Artificial Intelligence using the metaphor of a traffic jam"
slug: lookbothways
date: 2024-06-06T14:55:35+02:00
# lastmod:
featured_image: /blog/2024/06/lookbothways/featured.webp
draft: false
katex: false
tags:
  - AI
  - Technology
  - Opinion
  - Short story
next: true
nomenu: false
---

![Photo by Tyler Nix of a road sign on a chain-link fence](featured.webp "Photo by Tyler Nix via Unsplash (link no longer available)")

I live near an intersection. My street is one-way and there are often traffic jams, and sometimes it is due to planned construction. As a simple pedestrian I am surprised by the number of people that can ignore the warning signs and continue down this street despite having had the opportunity to avoid frustration.

I see them. They are visible---bright yellow and orange---and they are in place days prior to any planned inconveniences. And yet, as I venture out to head further into town to run my errands I see people barrel down that street. Ignoring the signs, ignoring Waze<sup>TM</sup>, ignoring the world around them.

Putting myself in the position of the driver, I'm sure they are initially struck with a sense of fluidity and ease. They might think to themselves, "Hey, I'm making good time, there is no traffic today." Or maybe they think that perhaps the construction is finished, and the signs haven't been taken down, so taking the risk might be worth it.

Down the street I walk, and lo and behold, there is a driver standing in the street beside their vehicle, hands in the air, cursing the construction crew for ruining their day. But they are not alone. Other motorists saw the first one take a chance and didn't want to miss out on saving time either. They crane their heads out their windows, plumes of vape-smoke curling into the air.

I look over my shoulder and see that traffic has backed up to the intersection. Now traffic is congested in three directions as motorists try to manoeuvre their oversized vehicles---taking turns, and relying on the waving hand signals of strangers---into position so that they may continue their journeys.

That first driver, that leader of the pack, that *disrupter*, could have been right. They could have lucked out and found a shortcut. They tried, they failed. But, they took others with them and caused problems for the motorists who would have heeded the signs. Now the cautious motorists are wasting their time and fuel, hands in the air or honking their horns.

I shake my head and keep on walking.

---

I love technology. I like things with buttons and knobs. I love to read about technology despite not having a clue as to how some of these things work. In the world of technology I am a pedestrian sharing the street with motorists and I shake my head a lot.

Facebook was neat, until it wasn't. There were warning signs, but companies and newspapers went all in on that platform. Regular users suffered from the changes, the algorithms, and the ads---much like the people stuck in traffic that were following the rules.

Blockchain, and cryptocurrency, seemed neat from my pedestrian perspective. But it also seemed a little iffy. The warning signs on that highway were billboard-sized. Now have people and companies all around the world throwing their hands up and "geniuses" serving prison time.

The Metaverse? There were warning signs and wireless emergency alerts sent out via SMS. Traffic backed up on that highway until, with a shrug, the drivers made their 3-point-turns and cut their losses.

Nobody that thinks like me should assume they were right all along. No *I-told-you-sos* should be uttered. It's just that as a pedestrian, you need to be careful. Pedestrians are soft a squishy, and we sometimes forget to look both ways, just like motorists sometime make mistakes. Everyone needs to be cautious.

---

I had a productive walk. I ran my errands, went to work, saw friends and colleagues. It was a good long day, the sun is setting now, and I'm heading back home. In between songs I hear something. I pull out my earphones and the sound is deafening. What cacophony could this be?

I stop dead in my tracks and say, out loud, "Are you f---ing kidding me?"

That first driver is *still* there. Standing on top of their vehicle they wave their hands around, shouting at the top of their lungs. They are not alone. Some of the original followers are still there with them. And that's not all.

The traffic is now backed up in all directions. There are cars and trucks, motorcycles, 18-wheelers, buses, and even cyclists and pedestrians. They are all shouting and honking, revving their engines. The air is thick with pollution and the ground covered with refuse.

Bags of money are piled at the feet of that first driver as they shout the most banal of rallying calls:

*"We just need more money and supplies. Look at how many of us are here. This is temporary. Don't give up now!"*

Politicians, doctors, students, teachers, programmers, police, criminals, and others carrying pre-printed placards claiming they will "save lives," "make life easier," "be more ethical and equal," and "make you rich" in exchange for a course on LinkedIn flank the lead driver. 

Residents lean from their windows banging pots and pans. They have no money to give, so they throw their pictures, driving licences, birth certificates, income statements, and password lists instead.

One person has a handmade placard that says, "Be careful! Drive slow!" The drivers throw bottles of p-ss at them. They look just like that actress from the superhero film franchise.

An entire traffic-jam--themed marketplace has developed. There are artists and schools and musicians, food-delivery and courier services. Migrant workers deal with waste. The park across the street from my flat has been converted into an ad hoc overflow parking space.

When I climb the steps to my building I can see down the street. There is a bus filled with people that just want to get on their way, they didn't ask for this. There is an ambulance and a fire engine trying to save lives. And, way down the street, I make out the shape of the boom truck the construction site is waiting on.

I turn and look back to the source of the traffic jam. The vanity licence plates I can make out read OPENTJ, JAMLE, METAJAM, JAMAZON, JAMVIDIA, MICROJAM, and JAMTHROPIC.

I throw my hands in the air and shake my head.

At home, I sit down to decompress. `#trafficjam` is trending on all social networks. The top level domain for Tajikistan is being overrun by new registrations. An email from my employer announced their---*our?*---support for the traffic jam.

I drift to sleep on my couch while doomscrolling, a lullaby of revving engines and honking horns permeates my double pane windows.

Credits: [Photo link](https://unsplash.com/photos/road-work-ahead-signage-leaning-on-chain-link-fence-7kwjU9MMbYU)
