---
title: "Unlocking Knowledge"
subtitle: "and making money from your time and effort"
description: "A blog post about the social media website LinkedIn.com and their use of Generative Artificial Intelligence"
slug: unlocking-knowledge
date: 2024-04-13T18:53:58+02:00
# lastmod:
featured_image: /blog/2024/04/unlocking-knowledge/unlocking.webp
draft: false
katex: false
tags:
  - Social Networking
  - LinkedIn
  - AI
next: true
nomenu: false
---

![Unlocking Knowledge Header: black text on a blurred background states "We"re unlocking community knowledge with the help of AI](unlocking.webp "Sounds normal to me")

If you've been using LinkedIn over the past several months, you've maybe noticed an uptick in solicitation to chime in on certain topics of *expertise*.

I won't judge you for taking part. We've all been a little bored at work and taken personality or IQ tests, and the temptation of a social network is hard to resist. So, going on LinkedIn is at least not going to look as unprofessional as other websites.

You connect, and right there on the home page is a simple call to action. Only, this time LinkedIn is referring to *you* as an *expert*.

![Goofy aww shucks animated GIF](shucks.gif "Gorsh!")

There's a question begging for an answer. And, not just any answer. An answer from *you*. Because, you---yes **you**---are an expert. LinkedIn needs you.

So, you lean in and reset your glasses on the bridge of your nose and take a gander at the query:

**What do you do if you want to maximize your success as a retail sales professional working remotely?**

What?

People. I'm a teacher. Sure, I worked retail---**20 YEARS AGO**---but I am no expert in that topic. In fact, I wouldn't even deem myself worthy of that label as a teacher.

Personally, I wrote this idea off as dumb when I saw it. It is clearly a way to get engagement numbers up, or something. Really, I couldn't care less about the why. LinkedIn is one of the oddest social networks out there. It is greed and clout having a pissing contest. I have kept my account active, though, because it has helped me find jobs in the past.

This morning when I decided to take a look-see at LinkedIn, the post at the top of my feed was a notification that a colleague had contributed their expertise to one of these *collaborative articles*. The contribution was well-written. Perfect, in fact, especially for someone who does not speak that language fluently. Also, remember what I said about being asked questions about topics I am not an expert on? The retail thing? Think about that for a second when I tell you---and I mean no insult to this colleague---that the person answering that question is as close to an expert on the subject as I am to veterinary medicine.

At least LinkedIn got my attention, right?

## What's up with these Collaborative Articles?

In March 2023, Daniel Roth announced the launch of Collaborative Articles in [a post](https://archive.is/Q6Fux) titled *Unlocking nearly 10 billion years worth of knowledge to help you tackle everyday work problems*.

> We are introducing collaborative articles — knowledge topics published by LinkedIn, with insights and perspectives added by the LinkedIn community. These articles begin as <mark>AI-powered conversation starters</mark>, developed with our editorial team. Then, using LinkedIn’s Skills Graph, we match each article with relevant member experts who can contribute their lessons, anecdotes, and advice based on their professional experience.
> 
> And, that’s when the real magic happens: when professionals share real-life, specific advice by contributing their perspectives to the work questions we’re all facing every day. Because starting a conversation is harder than joining one, these collaborative articles make it easier for professionals to come together and add and improve ideas — which is how shared knowledge is created.
>
> Of course, feedback is part of this, too. When you read the collaborative articles, you can <mark>react to the contributions by clicking the “insightful” reaction</mark>, helping your network and peers quickly find great insights. Through the articles, you’ll discover new people to follow who will keep you learning about topics key for your job and career.  And to make sure that contributors are rewarded for giving their time and experience, they can earn a new <mark>Community Top Voice badge in their skill areas</mark> (e.g., “Top Sales Voice”) for adding their insights. You’ll be able to see the badge on profiles and next to contributions on the articles.

Allow me to **TL;DR** that for you:

*We (LinkedIn, owned by Microsoft) had AI write some prompts, and you are going to provide the words and answers we need to write the articles (well, AI will do the writing).*

*Then other "experts" will up- or down-vote the article.*

![Screenshot of the voting button for collaborative articles, has a thumbs up and thumbs down button](great.webp "We're not going to read this, so we're crowd sourcing quality assurance")

*In exchange, we will give you magical internet points in the form of a "badge" for your profile.*

![The 'carrot,' states that if you write 3 responses you earn a badge](badge.webp 'Is that all?')

Six months after launch, over **a million** contributions had been made. Collaborative articles became ["the fastest growing traffic driver to LinkedIn"](https://archive.is/WMBle). 

Wow. Good for them. Think of the shareholder value those one million contributions have made.

## Just stop contributing

These sweet badges can be tempting, but think about it like this:

* Contributors are giving their time to a company for free and getting JPG's in return.
* Contributors may not even be experts on the topic.
* Contributors could be using AI to write answers to a question asked by AI.
* Contributors could be poisoning the data by providing less than accurate answers and then voting on the legitimacy of the article.

The [FAQ](https://archive.is/VQEE4) is filled with other tidbits, including the fact that it isn't as easy to get those badges as you may have thought:

> Contributions made to collaborative articles are public

(that is the default and only option)

> You are automatically following skill pages created by LinkedIn

(you need to manually unfollow skills)

> LinkedIn identifies members who are likely to be experts in a certain topic based on their work experience, skills proficiency, and prior engagement on the platform.

(have enough keywords on your profile, and you get to participate)

> You must make three contributions in a specific skill to be eligible to earn the badge for that skill. \
> Once you've made three contributions, a progress tracker unlocks, showing your journey to earning the badge. If you're below 50% of contributors, the tracker encourages you to continue making insightful contributions for improvement. If you're above 50% but haven't earned a badge yet, it displays your percentage standing to earn the badge. \
> Once you achieve a badge, the tracker motivates you to showcase your achievement. 

(persuasive tech at its best: there are prompts, ability, and motivation)

## Final thoughts

**Never give too much information to a social network**. LinkedIn already has a tonne of information about its users, and being LinkedIn famous is the type of millennial flex that Gen Z laughs at and Gen X doesn't give a shit about.

**Don't provide free work for anything with the word "AI" in the description**. Myriad Chatbots have us writing prompts all day and the AI-hype-machine has somehow normalized the idea that "prompt engineer" is a job. If it is a job, why not reimburse us for our time?

**Real experts and other "legitimate voices" are not jumping through hoops for a badge**. They are probably too busy to bother with LinkedIn. If you are an expert in a field, consider creating a blog or a newsletter to engage with your audience. That way, you own the content you write instead of it being the property of a social network. You can also find ways to [monetize your work](https://ghost.org/resources/how-to-make-money-blogging/) without using ads or writing sponsored content.
