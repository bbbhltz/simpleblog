---
title: "Low Friction Introduction to Digital Privacy"
subtitle: "A guide for absolute beginners"
description: "How to get started with digital privacy while avoiding friction: A guide for your first steps."
featured_image: /blog/2022/03/guide-privacy/friction.webp
slug: guide-privacy
date: 2022-03-24
lastmod: 2025-01-28
draft: false
katex: false
toc: true
tags:
  - Guide
  - Technology
  - Privacy
next: true
nomenu: false
bold: true
---

{{< box info >}}
This guide is not meant to be a list of suggestions and recommendations in the traditional "you should do this" sense. It is up to each individual person to contemplate their needs and how far they are willing to go to achieve them. Your privacy, security, and anonymity matter. The decisions to take should not be decided by a stranger on the Internet. When the wording in this guide implies suggestion, it should be understood as "several websites and individuals on the Internet suggest," and not "if you don't do this you are doing it wrong." Take your time. You will need it. Find your own sources too. Don't hesitate to send me a message to tell me how wrong I am about things.

I don't talk about "big A" in this guide. I have no idea what the deal is vis-à-vis security, privacy or anonymity when it comes to the "fruit company".
{{< /box >}}

## Introduction

Several years ago, my interest in digital privacy and security ballooned. While I had been interested in some time, I found myself digging deeper, constantly changing settings. I became a little obsessed. The thing is, I am not an expert. I was putting the horse before the cart, so to speak. I made extreme choices that lead to lots of friction that could have been avoided had I taken my time.

This guide, which will hopefully be brief and light on opinion, is my attempt at proposing various things that most people can do. The target audience for this guide would be someone who is also interested in the topic and is in a position where they can start making changes without too much *friction*. The friction that I speak of is when the changes you make have unintended consequences on your digital life. An example would be switching to a new piece of software that none of your friends or family use, or forcing yourself to change phones and use something that you are neither happy, nor comfortable with as a device.

The suggestions that I make below will not go as far as [rooting your telephone](https://en.wikipedia.org/wiki/Rooting_(Android)) or installing a new operating system on your computer. You will not need to spend too much time configuring. **This guide will just be a taste, hopefully enough to give you an appetite**.

Each suggestion below will be qualified by its level of friction.

| **Level of Friction** |
|:-----------:|
| ◆ <br> **Little to no friction** <br> Your friends and family will not notice anything. You may need to install a new piece of software. |
| ◆ ◆ <br>**Some friction** <br> You may lose access to some things that you have become used to, or need to configure some things. |
| ◆ ◆ ◆ <br>**Lots of friction** <br> Noticeable changes. Friends and family might begin asking questions. |

Doing everything below will not make you 100% safe on the Internet, nor will it guarantee your privacy. It will only create a little more friction for the different actors (advertisers, hackers, etc.) to gain access to your [assets](#asset).

![Friction for "Hackers"](friction.webp 'Example of "Friction"')

The assets are anything we want to protect; a file, for example. We want to protect this file from a *bad actor*. How likely will you be hacked? Well, it happens quite often, even if you think you have nothing to hide. What will happen if you don't protect it? Perhaps you will lose access to your email accounts, or someone will make purchases on your Amazon account, or go as far as committing identity theft. Are you willing to make an effort to prevent this from happening? The answer to that is up to you. How much friction can you handle to create friction for the *bad actor*?

**Recommended viewing**: [Glenn Greenwald: Why privacy matters](https://youtube.com/watch?v=pcSlowAhvUk)

## Basics

These are not even suggestions. There isn't a single computer user that would advise you against doing these. They are extremely easy and can be set up with little to no friction.

### Using a Password Manager (◆)

This is something that a number of people don't think about. You might be using Chrome or Firefox, you log into a site, you let your browser save the password. Cool. No problem, right? Admittedly, I did this for quite some time. I was very happy with Chrome, or Firefox, having my passwords synced across devices. Well, what if your Google account is compromised? Now you don't just lose access to your email, but all of you accounts. Also, depending on how you use your computer, if someone steals your device from you while it is unlocked, they would also have access to all of your passwords.

![Bitwarden Landing Page](bw-landing.webp "Bitwarden.com")

[**Bitwarden**](https://bitwarden.com/) is a popular recommendation for password manager. There are many reasons for this. It works across many devices, it is open-source, and if you are willing to make the effort, you can even self-host your own server. The basic service is free and secure. They explain how to get your passwords from [Chrome](https://bitwarden.com/help/import-from-chrome/) and [Firefox](https://bitwarden.com/help/import-from-firefox/), as well as other services. The whole process takes minutes to complete.

After, you can install the app on your phone and the plugin for your browser (or use a standalone piece of software on your computer) and you are set. The only minor bit of friction is that you need to remember one master password. You can also access your passwords from an online vault.

The perks of using a password manager are many. I am particularly fond of having access to a password generator.

There are, obviously, [many password managers](https://alternativeto.net/software/bitwarden--free-password-manager/) to choose from if Bitwarden is not to your taste.

### Using an Authenticator (◆ ◆)

Now that your passwords are a little safer, it is time to make account access more difficult for anyone who isn't you!

You may have seen people doing this. Or, you have heard people talking about "[two-factor authentication](#2FA)". The process is actually easier than setting up a password manager. I do, however, rate this as a task that can result in some friction.

Most people use 2FA for their most important online activities: email, social media accounts, banking, etc. If something stores your personal information, photos, emails, or any asset you want to keep to yourself, use of 2FA is generally a baked-in feature. Facebook and Google use it and recommend that their users take advantage of it.

How does this cause friction? The friction comes from the perceived amount of time it takes to a) take out your phone, b) unlock your phone, c) launch the app on your phone, d) enter a pin, and finally e) type out the token on your computer or device. The process is fluid and fast, but for lots of people, it seems like a hassle.

It is worth it though, for the piece of mind that comes from knowing that the target on my digital back is smaller.

Like with password managers above, there are many apps to choose from. I ended up using [**Aegis Authenticator**](https://getaegis.app/), but before I also used the [Google Authenticator](https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2). If you have never seen the concept in action, it works like this:

* Go to website and login
* You will be prompted to enter a token or pin
* You use your phone (or in some cases another device) to get the token
* You are connected

You don't have to set up every account on the same day. But, be sure to add it to any account that you may ever access on a computer that is not your own!

### Using an Ad blocker (◆)

I suspect most people use an ad
blocker of some sort. Good on you. It goes without saying that browsing the web is awful when there are ads everywhere. The most highly recommended ad blocker that you can use is probably is [**uBlock Origin**](https://ublockorigin.com/) (often referred to as uBO). uBO is light on resources, available on most devices and browsers, highly customizable, and [open source](https://github.com/gorhill/uBlock).

There are many guides[^ubo] for uBO on the web. This is because uBO is more than an ad blocker. It is a content filter. In addition to blocking ads, it can block JavaScript, frames, images, 3<sup>rd</sup> party fonts, etc. It also helps prevent [tracking](#tracking). As such, this is the first tool presented in the guide that can increase your privacy while using the web.

The default options are good, but you can try activating the different lists and filters and see if it interferes with your daily browsing. Or, activate everything just to see what the Internet would be like without cookies, JavaScript, trackers, or ads.

{{< box warning >}}
There are many copies of uBlock Origin available. `ublock.org` is not the same thing, despite looking very similar.
{{< /box >}}

## Tough Choices

The next round of suggestions to increase your online privacy will cause friction. You will need to be thoughtful about your decisions and be prepared to make some tough choices.

### Choosing a new Email Provider (◆ ◆)

Changing email providers is a hassle. Yes, nearly every alternative service provides some sort of migration guide, but it is still annoying. No matter what, you will need to activate some form of email forwarding on your old account, check it from time to time, or set up automatic replies. That said, there will be other friction-inducing moments as well.

<div class='embed-container' align='center'>
<iframe src="https://mastodon.social/@gerowen/106978308085702358/embed" class="mastodon-embed" style="overflow: hidden; max-width: 100%; border: 0" width="600" height="560"></iframe>
<figcaption>Toot from @gerowen@mastodon.social regarding Gmail</figcaption>
</div>

If you are a user of Gmail, then changing email providers means changing a lot of things: cloud services, bookmark syncing, password syncing, etc. If you are trying to increase your digital privacy and reduce your attack surface, moving away from the Google, Yahoo! and Microsoft is a good idea. There are plenty of choices. I will only talk about three here, because I have only used these three. Feel free to try all of them as well, as well as any other you come across.

[**ProtonMail**](https://proton.me/) is generally the first stop. Using the clever tools of the public relations trade, they quickly made it on every list of alternative email providers. The keywords we are looking for are all there: independent, secure, encryption, 2FA, open source, Swiss. What's more, it has a modern and *fast* interface. According to their privacy policy,

>Our overriding policy is to collect as little user information (personal data included) as possible to ensure a completely private user experience when using the Services. We do not have the technical means to access the content of your encrypted emails, files, and calendar events.[^protonpriv]

There is a calendar, a drive (if you pay), a VPN, and a free plan with 500 MB of storage. When I was originally looking to change email providers, Proton did not yet have a calendar, which was very important for me. Also, the free version does not let you access your emails from apps or clients like Thunderbird. That, for me, meant when I was testing I was unable to have a preview of all the features without spending money. Spending money causes friction.

Since Proton did not have a calendar when I first looked at it, I had to look elsewhere. I landed on [**Mailbox.org**](https://mailbox.org/en/). I decided to stay there. There are some things that bother me about Mailbox, but they are very small things. They offer a free trial, which is great, because I wanted to see what it would be like to migrate from Google. They had the [different features](https://mailbox.org/en/services#e-mail-account) I was looking for and an extremely flexible pricing plan. Like several other alternative choices, they run on 100% green energy and publish yearly transparency reports. Almost everything about it was perfect for me. I say *almost* because there are two things that bother me:

1. I find the interface slow compared to Proton and Disroot.
2. The servers are in Germany, and Germany is part of the 14-eyes Surveillance agreement[^14].

[**Disroot**](https://disroot.org/en) is a community effort. Like with the previously mentioned alternatives, you get the whole kit and caboodle (2 GB cloud storage, online office, calendar, chat, video, etc.). If you are the rebellious type, and like to go out and protest or take part in actions that are socially good, Disroot may be your type of community.

>Disroot aims to change the way people are used to interact on the web. We want to encourage and show them not only that there are open and ethical alternatives but also that it is the only possible way to break free from the walled gardens proposed and promoted by proprietary software and corporations, either through our platform, others that have similar values and goals or even their own projects.[^disroot]

The interface is quite fast, but lacks options that other services have. In the end, it is free, and you can test it easily. I have used this email address the least, so I cannot make any claims to how well it works.

As this is a tough choice, it cannot be made for you by a list like this. Many articles and sites that propose "best alternatives" could be biased or misinformed, or be written by people like me (who have tested these services, but are not professionals). You should make a table. Decide how much you are willing to pay for email, and the services you would like with it. Also, consider the email address. The number of times I have had to correct people because my `@mailbox.org` address was written down as `gmail` or with a `.com` or even as `@mèlboxe.org`.

| Feature          | **Proton**   | **Mailbox** | **Disroot** |
|-----------------:|:------------:|:-----------:|:-----------:|
| Pricing          |  Freemium    | Flexible    | Free        |
| Calendar / Drive | ✓ / ✓        | ✓ / ✓       | ✓ / ✓       |
| IMAP/SMTP        | ✓            | ✓           | ✓           |
| 2FA              | ✓            | ✓           | ✓           |
| Open Source      | ✓            | ✓           | ✓           |
| Aliases          | ✓            | ✓           | ✓           |
| Your criterium   |              |             |             |

In addition to the services mentioned above, there are others:

<details>
  <summary markdown="span">A short list of Email Providers</summary>

  * [CTemplar](https://ctemplar.com/)
  * [Cock.li](https://cock.li/)
  * [CounterMail](https://countermail.com/)
  * [Dismail](https://dismail.de/)
  * [Posteo](https://posteo.de/)
  * [RiseUp](https://riseup.net/)
  * [Soverin](https://soverin.net/)
  * [Tutanota](https://tuta.com/)

</details>

If you do decide to switch email providers, then you will want to back up and maybe even import your old emails into your new email account. The entire process took several days for me, but I had been using Gmail since 2004 and Google Drive was where I kept *everything*.

### Quitting or Cleaning up your Social Networks (◆ ◆ ◆)

Prepare for friction. Quitting social networks is hard.

**Recommended viewing**: [Cal Newport: Quit social media](https://youtube.com/watch?v=3E7hkPZ-HTk)

If you cannot imagine doing it, then possibly it is not for you. Just know, it can be done, with difficulty.

Does everyone in your family use a certain social network? Your friends too? Well, you may just have to stay put. But, don't be naive. Meta/[Facebook](https://privacyspy.org/product/facebook/)/[Instagram](https://privacyspy.org/product/instagram/) do not have a good history when it comes to privacy and personal data. Facebook has an [entire Wikipedia entry on criticisms](https://en.wikipedia.org/wiki/Criticism_of_Facebook). Even social networks like [Reddit](https://privacyspy.org/product/reddit/) lack transparency. Everything you like, dislike, or comment on has value.

If you are a social person, and you like to interact with strangers on the web about different subjects, there are alternatives. Here are some examples:

| Network | Alternative Frontend | Alternative Network |
|---------|----------------------|---------------------|
| Facebook | - | [Friendica](https://friendi.ca/) |
| Twitter | ~~Nitter~~ (doesn't work as well anymore) | [Mastodon](https://joinmastodon.org/) |
| Reddit | ~~Teddit~~ / ~~Libreddit~~ (no longer reliable)| [Lemmy](https://join-lemmy.org/) |
| Instagram | [Proxigram](https://codeberg.org/ThePenguinDev/Proxigram/wiki/Instances) | [Pixelfed](https://pixelfed.org/how-to-join) |
| YouTube | [Piped](https://piped.kavin.rocks/) / [Indivious](https://invidious.io/) | [Peertube](https://joinpeertube.org/) |
| TikTok | [ProxiTok](https://github.com/pablouser1/ProxiTok/wiki/Public-instances) | - |

The alternative frontends let you access, and sometimes interact with, the social networks in a more privacy-friendly manner. Or, you could try out a different social network inspired by the original. It is a very hard choice, especially if that is how you stay in touch.

An easier option is "spring-cleaning." Take a few days to casually audit each social network you use. Do you need to follow so many brands? Do you need to like everything and always comment? Do those likes and comments need to stay there?

Let's take Facebook as an example. While I know that many younger people are not that enthused by Facebook, it is an important social network. I did not delete my account. I just stopped using it. No, I did not deactivate it. I wanted to make sure that my face and name on Facebook are me, and not someone else. All I did was clean it up.

* I *unliked* and unfollowed anything that was not a friend or person that I had met in real life. Even music and TV shows.
* I painstakingly deleted every like and comment I left on other posts.
* I deleted all photos except my profile photo.
* I left every group except the Alumni group for my university (I needed to have a video meeting and show my diploma to get access. That was a lot of effort for something so useless.)
* I signed out of the chat.
* I removed personal information from my "About me" page.

So, now the account sits there, doing its thing. Nobody has contacted me in about a year. When I do visit, Facebook struggled with recommendations. Today I checked, and they were trying to sell me an online course for something ridiculous.

But, I am not the average social media user. It was easy for me to walk away. I have never used Instagram or TikTok, and my Twitter account lasted about a year before I forgot about it. You will need to figure things out on your own or just accept that some companies and services will have information about you.

## Communicating

Beyond email and social media, there are many ways to stay in touch.

[![Obligatory xkcd](https://imgs.xkcd.com/comics/preferred_chat_system.png 'Preferred Chat System via xkcd')](https://xkcd.com/1254/)

### Email Clients (◆)

If you have already made the decision to step away from Gmail or Outlook, maybe it is time to change email clients too. For years, I ignored the existence of desktop clients. Outlook was never my cup of tea. If you like having a desktop client, for accessing multiple accounts maybe, you can look at some choices below.

* [**Thunderbird**](https://www.thunderbird.net/): Very similar to Outlook in functionality. This is another Mozilla product, like Firefox. There are loads of add-ons for it too for blocking ads and trackers. With some friction and time, this program can replace Outlook and even more.
* [**Claws Mail**](https://www.claws-mail.org/): The extreme lightweight approach. Just email. The interface will seem dated, but there are themes and plugins to extend the functionality. I use this, but I haven't even bothered syncing my contacts. Too much of a bother for the moment.

Android has some *great* email clients. Two very similar options are K-9 and FairEmail.

* [**K-9 Mail**](https://k9mail.app/): an open source email client focused on making it easy to chew through large volumes of email.
* [**FairEmail**](https://email.faircode.eu/): fully featured, open source, privacy-oriented email app for Android.

### Planning

If you have decided to leave Gmail or Outlook, you may also be looking for a way to read your emails on your desktop without going through the web client. And, if you have also left Facebook, you might be looking to replace the Events feature. There are options for both.

* **Thunderbird**: Mozilla's answer to Outlook has a calendar that lets your interact with different services. You can sync your contacts with various services as well.
* [**Mobilizon**](https://mobilizon.org/en/): A platform for organizing events.

### Messaging

[![second obligatory xkcd - messaging systems](https://imgs.xkcd.com/comics/messaging_systems.png "Messaging Systems via xkcd")](https://xkcd.com/2365/)

WhatsApp and Messenger can be replaced too. **Signal** quickly became the WhatsApp replacement during the pandemic. **Telegram** too has a large fanbase. While I don't use the latter, I can tell you that both are good examples of "drop-in replacements". My 70-year-old mother switched to Signal without issue or help from anyone, and my partner's entire family went on to Telegram.

Here is a very short list of the many "instant messaging" / "group chat" services available:

* [**Signal**](https://signal.org/download/): Edward Snowden's messenger of choice.
* [**Telegram**](https://telegram.org/): Another full-featured messenger.
* [**Threema**](https://threema.ch/en/) (never used this one, so I won't even leave a comment or description)
* [**Delta Chat**](https://delta.chat/en/): A different concept of chat --- one without central servers! It works through email accounts, so you don't even need to even a new account.

### Video Chat

Zoom took over during the pandemic, but like Jedi and the Sith, there are always more. Some of them do not even require an account!

[**Jitsi**](https://meet.jit.si/) is simple and secure, it doesn't even require an app, it runs in the browser. No account is required. It has saved me a few times. Some other services mentioned here integrate with Jitsi (Mailbox provides a Jitsi instance).

And, if you are a teacher, [**BigBlueButton**](https://bigbluebutton.org/) has some excellent features. BBB is made for teachers, by a team that works with teachers. It is not a second-tier video chat, just [look at the features added in their last release](https://bigbluebutton.org/2022/01/19/bigbluebutton-2-4-learn-more-about-our-exciting-update/).

## Browsing

While browsing the web, cookies received from the different websites you visit are stored by the browser.

Some of them contain login information or site preferences, and some are used for tracking user behaviour over long periods of time.

This is why browsers are popular targets for hackers (and governments), who try to steal information, destroy files, and commit other malicious activities.

The browser is probably the most used programs on your computer. For some, it is also one of the most used apps on your phone. Staying with the default browser is a choice that you can make. After all, in most cases, you cannot delete the browser, only disable it and ignore that it is there, taking up space. You do you.

### Changing your Browser (◆ ◆)

Why would you change your browser? It is a legitimate question. The short version is that by using the default browser, you are making it easier for corporations to track you. Chrome, the most used browser in the world, has some privacy features, but in the end Alphabet (Google) is a for-profit company. When a company gives something for free, that means you are the product.

**Recommended viewing**: [Richard Serra: Television Delivers People (1973)](https://youtube.com/watch?v=LvZYwaQlJsg)

Changing your browser creates some friction. It shouldn't, but it does. The choices of browsers are many; the opinions on those browsers are strong. A given browser may indeed provide excellent protection against tracking and ads, but company or people behind the project may not be as transparent as they claim.

As I have not had the Apple experience, I will make no attempt to provide specific advice about the options available for their users. Outside the fruit company's offering, you will surely have come across the two main families of browsers: The Chromium Family and the Firefox Family.

Chrome sits on their thrown with little to fear. So little, in fact, that the competition usually includes the Google search engine as one of the default options. There was a time when many hoped Firefox would overtake Chrome. It is unlikely that will happen. Why so few options? Simple. It is incredibly complicated, expensive, and time-consuming to build a new browser engine. Furthermore, most websites are built with Chrome in mind these days.

#### Google / Chrome / Chromium Family

![Chromium-based browsers](chromium.webp "The Chromium Family")

The code upon which the Chrome browser is built is open source, so there are many browsers that are *copies* of Chrome with modified features. Even Microsoft Edge is based on that code. In the logos above you might recognize Vivaldi, Brave, Opera, and the Android browser Bromite. There are more, but listing them here will not help you decide.

If you have gone ahead with some steps above, you may have decided to change your email provider. If it was Gmail, and you have decided to begin distancing yourself from that giant, there is less reason to use Chrome, or even Chromium, and look at some other options.

[**Brave**](https://brave.com/) is frequently presented as a great alternative. Opinions and [controversies](https://en.wikipedia.org/wiki/Brave_(web_browser)#Controversies) aside, Brave fairs quite well when [put to the test](https://privacytests.org/). It comes with built-in ad blocking, too.

[**Vivaldi**](https://vivaldi.com/) also makes the same claim, and also proposes a translation tool, a mail client, and [other features](https://vivaldi.com/features/).

Conveniently, Brave and Vivaldi are both available on mobile. Some people like to use similar apps on their phone. It is a point to consider.

[**Bromite**](https://github.com/bromite/bromite) is an Android-only affair. Similar to its cousins, it has ripped the Google stuff out of the code and added extra features and customizations.

I do not want to rank anything in this guide, but if I were, I don't know if I could give an unbiased opinion of these browsers. I have tried Vivaldi, Brave, Bromite, and another called [ungoogled-chromium](https://ungoogled-software.github.io/). I used the latter two for the longest amount of time, and my current browser ([qutebrowser](https://qutebrowser.org/)) is based on Chromium.

#### Mozilla / Firefox Family

![](FF.webp "The Firefox Family")

*The year was 2002. Netscape had lost the [browser war](https://en.wikipedia.org/wiki/Browser_wars#First_Browser_War_(1995%E2%80%932001)). From its ashes a new project was born. It would grow into **Firefox**.*

Firefox is developed today by the Mozilla Foundation and Corporation. The Foundation is a non-profit that leads the open source Mozilla project, which was founded back in 1998. The Corporation is a subsidiary of the Foundation. The Corporation reinvests its profits in the Foundation.

>**How Mozilla Operates**
>
>Mozilla is unique. Founded as a community open source project in 1998, Mozilla currently consists of two organizations: the 501(c)3 Mozilla Foundation, which backs emerging leaders and mobilizes citizens to create a global movement for the health of the internet; and its wholly owned subsidiary, the Mozilla Corporation, which creates products, advances public policy and explores new technologies that give people more control over their lives online, and shapes the future of the internet platform for the public good. Each is governed by a separate board of directors. The two organizations work in concert with each other and a global community of tens of thousands of volunteers under the single banner: Mozilla.
>
>Because of its unique structure, Mozilla stands apart from its peers in the technology and social enterprise sectors globally as one of the most successful social enterprises.[^mozilla]

You might wonder where that profit comes from.

>Today, the majority of Mozilla Corporation revenue is generated from global browser search partnerships, including the deal negotiated with Google in 2017 following Mozilla's termination of its search agreement with Yahoo/Oath (which was the subject of litigation the parties resolved in 2019.)[^mozilla]

Google, and others, pay the Corporation to include their search engines by default in the browser. Also, since 2015, the proprietary service [Pocket](https://getpocket.com) has been integrated into Firefox (which is open source). These are points of controversy for many. The controversy has grown since 2017, when the Corporation acquired Pocket. If you have used Pocket, you may have noticed that suggests articles to users, which implies that user data is being accessed.

Despite this, Firefox remains the "private" choice when compared to Chromium-based browsers. There are lots of add-ons and guides to help you harden your Firefox installation. And, it is possible to strip Pocket from Firefox, and disable the bookmark syncing service.

Firefox is open source, there are a number of forks, and a number of ways to customize the way it works.

[**LibreWolf**](https://librewolf.net/) is one of these forks. The elevator pitch is that they are "an independent fork of Firefox, with the primary goals of privacy, security and user freedom" with [a long list of features](https://librewolf.net/docs/features/). Using LibreWolf with an add-on like uBO and the right filters would be a very easy way to access the web with little to no time spent configuring settings.

[**Waterfox**](https://www.waterfox.net/) is a slightly better-known fork of Firefox has been around since 2011. Their pitch is similar, but there are some concerns:

> Waterfox web browser has been sold to System1 recently, the same company that bought the Startpage search engine some time ago. To be precise, Startpage was bought by Privacy One Group Ltd which System1 owns. System1 is an advertising company that tries to "make advertising better and safer, while respecting consumer privacy".[^wf]

On mobile, you can find [Fennec](https://f-droid.org/packages/org.mozilla.fennec_fdroid/), which is essentially Firefox with the "proprietary bits and telemetry removed".

#### A word on Tor

[**Tor**](https://www.torproject.org/) is another Firefox fork, with security enhancement allowing it to connect to the Tor network.

I am not a Tor user. The Tor browser may very well apply to my threat model, but I have never bothered with it.

Before downloading and using the Tor browser, though, it is in your interest to read [the "about" page from the Tor manual](https://tb-manual.torproject.org/about/) and look over the [Tor training resources](https://community.torproject.org/training/resources/). Those two sources will give you an understanding on what Tor is all about. On many sites, you will likely come across a list of best practices regarding Tor that tend to include:

* **Do not** install any other add-ons
* **Avoid** resizing your window
* **Do not** log into bookmark syncing services

If you like syncing bookmarks, or using particular add-ons, then maybe Tor is not for you.

But, Tor is an important tool for people from [all walks of life](https://2019.www.torproject.org/about/torusers.html) (journalists, activists, human rights defenders, parents who want to protect their children while online, and people in abusive situations). There are many threats that need to be considered, like who may have access to your browser history, anybody you may have angered in the past, the software on your computer, corporations, data criminals, casual hackers, local law enforcement, private investigators, foreign and domestic intelligence agencies, etc. The list is very long.

Think about what type of [digital security profile](https://www.wired.com/2017/12/digital-security-guide/) applies to you. Perhaps install Tor and just keep it up to date in case you need it, or, use it every day.

The final decision of browser will be a matter of taste. If you like tweaking, there is something for you. If you are more of an "install and browse" person, there is also something for you.

### Searching (◆ ◆)

Google Search is amazing. You cannot deny it. They have crawled so many pages, indexed them, and cross-referenced them. They know what you are looking for even if you cannot spell it. Prior to Google, there were many search engines. Since Google, many have tried to take their throne. They are the kings of the castle for a reason. It is the same reason why when Google Search launched when I was about 11 or 12 years old, that our IT teacher printed out a banner (dot matrix printers, you know) that read "USE GOOGLE.COM" and pinned it to the bulletin board: It works very well.

It also increases your attack surface by providing data brokers with information about you and your preferences. Google Search knows things about you, and you can get away from them. You can do it right now.

There are [many alternative search engines](https://www.searchenginemap.com/) that are more privacy focused than Google and Bing. We will look at three of them:

1. [DuckDuckGo](https://start.duckduckgo.com/)
2. [Qwant](https://www.qwant.com/)
3. [Mojeek](https://www.mojeek.com/)

But first, you need to know something. DuckDuckGo and Qwant are [*metasearch engines*](#metasearch). They both depend on Bing for their results (and Yandex in some instances). Bing is a [*crawler*](#crawler). So are Google, Yandex, and a few others. Qwant and DuckDuckGo will return similar results. If you want a better and more detailed explanation of this, Seirdy has [an excellent post](https://seirdy.one/2021/03/10/search-engines-with-own-indexes.html) on his blog where he also talks about why someone would even bother using a non-mainstream search engine.

I generally use DuckDuckGo. It seems to return what I am looking for most of the time. When it doesn't, it provides an magnificent feature called [Bangs](https://duckduckgo.com/bang) that allow you to use shortcuts to search on other sites. All you need is a `!`. At the moment, there are 13,565 bangs available. For example, starting your search with `!yt` lets you search YouTube, `!g` Google, `!reddit` Reddit. If a site has a search function, <abbr title="DuckDuckGo">DDG</abbr> has banged it. Using DDG as your default search engine means that you can search most of your favourite sites from a single place.

DDG promotes itself as a privacy-oriented search alternative, just like Qwant, and they also promise not to track us. I don't use Qwant (someday I will write something about Qwant, just know they are sketchy). In the end, these two search engines will assist you in your mission to protect your private information online.

But...

>[...] The New York Times reviewed the top 20 search results on Google, Bing and DuckDuckGo for more than 30 conspiracy theories and right-wing topics. Search results can change over time and vary among users, but the comparisons provide a snapshot of what a single user might have seen on a typical day in mid-February.
>
>For many terms, Bing and DuckDuckGo surfaced more untrustworthy websites than Google did, when results were compared with website ratings from the Global Disinformation Index, NewsGuard and research published in the journal Science. (While DuckDuckGo relies on Bing's algorithm, their search results can differ.)
>
>Search results on Google also included some untrustworthy websites, but they tended to be less common and lower on the search page.[^nyt]

**Mojeek** is a different beast. It is a search engine with its own crawler. They are on a mission to build, slowly but surely, their own index. Mojeek has been working on this since 2006 and recently passed "7 billion pages indexed" mark[^5b]. The scope of the mission is wide. There are the privacy promises (no cookies, no tracking, no selling of data, no outsourcing[^mojeekprivacy]), the environmental (Mojeek is hosted and run from a green data centre[^mojeekgreen]), and governance.

>[W]e would never intentionally manipulate our results to show any particular point of view. We don't believe a search engine should have an agenda, whether political or otherwise, and returning as relevant but opposing views should be a goal.[^mojeekreddit]

This focus on <abbr title="Environmental - Social - Governance">ESG</abbr>[^esg] speaks to me, but finding the things I want on Mojeek requires patience. I have been using it daily now for about two weeks. Sometimes, I do give up and head back to DDG. Then I remember that when I first used DDG, the results were not always great either. Time will tell. I suggest that you at least bookmark Mojeek, because it is growing.

### General Recommendations (◆)

This is short and easy. Just a few points.

1. [**Use bookmarks**](/blog/2021/01/guide-browser#bookmarks): It saves time. Why do so many people search for something they use every day? Bookmarks are useful. Keep them organized.
2. Avoid creating unnecessary accounts.
3. Avoid "login with Google/Facebook/etc." If you need that service, create an account. I myself accidentally created a Spotify account this way, and it still exists, but I cannot access it because the account I "logged in with" no longer exists.
4. Avoid letting your browser save your passwords.
5. Many email providers offer the option to create aliases and disposable accounts if you need to use your email somewhere sketchy.
6. Don't trust everything you read on random blogs.

### Add-ons (◆)

In addition to uBlock Origin, you can use other add-ons and extensions to improve your privacy. The more you use, however, the more unique your "[fingerprint](#finger)" will be. Again, I am not suggesting you install everything listed below, but being aware of their existence is important.

* [LibRedirect](https://libredirect.github.io/): A web extension that redirects YouTube, Twitter, Instagram... requests to alternative privacy-friendly frontends and backends.
* [xBrowserSync](https://www.xbrowsersync.org/): a free and open-source alternative to browser syncing tools offered by companies like Google, Firefox, Opera and others. The project was born out of a concern for the over-reliance on services provided by big tech, who collect as much personal data as they can and have demonstrated that they do not respect their user's privacy.

## The Cloud

Convenience comes at a cost. Services like Dropbox and Google Drive offer so much cloud storage for free, it almost feels like you *have* to have an account.

This category can create some friction. Here are two possible solutions to leaving Big Cloud.

### Sharing with just yourself (◆ ◆)

If you only need to share some files, maybe some photos, or some Excel documents, between the devices you own, you can avoid the Cloud altogether with [**Syncthing**](https://syncthing.net/).

Syncthing keeps your information private and encrypted. It is easy, but I did give it a medium friction rating because it is not something many people are used to doing with their devices.

With Syncthing, you would install it on the devices you plan to use and share between. As long as the device is on the same network, the file is shared between the device. This is how I sync my work between my home computer and my work computer. At home, I prepare. The files sync *to* my phone. When I get to work, the files sync to the work computer *from* my phone. I also sync my photos, but only to my personal computer.

### Sharing and collaborating with others (◆ ◆)

This is where things can get rough. Collaborating on files is something we need to do regularly. If it is for work, you can use your work cloud and online office tools. But, what if we don't have those? This is where [**Nextcloud**](https://nextcloud.com/) comes to the rescue.

Nextcloud is a service with features we sometimes need: file sharing, collaborative tools, groupware. You *can* install this on your own and have your own personal cloud, if you have the time and will to host your own server. There are also [free providers](https://nextcloud.com/signup/). Disroot, which we mentioned in the email section, uses Nextcloud. As do a few others.

## Your Phone

I see a few friends with these "cleaner" apps on their phones. I don't want to cite names, because that is slander, but modern Android phones don't really need them. Head over to [εxodus](https://reports.exodus-privacy.eu.org/en/) and look up some of the apps you use. For example, one of the famous "cleaner" apps contains more than a few trackers. Why would an app that clears out useless files need to send data to these companies? Your guess is as good as mine (actually, it is probably money).

![εxodus privacy screenshot](exodus.webp "Screenshot from εxodus")

Your phone is a pocket-sized tracking device combined with a wallet and a closet filled with secrets. It has your search history, your location history, your chats, your purchases, some health information, and photos, among other pieces of information you don't want to share with the world.

{{< youtube M3mQu9YQesk >}}

### Cleaning Out Your Wallet (◆ ◆ ◆)

You need to take a moment to consider the apps on your device, and the accounts saved on your device. We have been spoiled over the years, and have taken shortcuts to completing tasks. Facebook has an app; do you *need* to use it? Did you stop using Gmail? Disconnect your Google account on your phone (don't worry, there are ways to stay up to date). In fact, if you want to create some real friction, reset your phone to factory settings and do not ever enter your Google information. Then start thinking about what you need on the phone and where you can get it without the Google Play Store.

If you look at [F-Droid](https://f-droid.org/en/) or [Droid-ify / NeoStore](https://github.com/NeoApplications/Neo-Store) you will see that your can install many apps without Google!

{{< box tip >}}
As far as I know, there is nothing unsafe about using F-Droid. The app even tells you if there are trackers or if the app connects to other services. The apps on the store are audited in a way, and, in most cases, you can go see the code used in the apps. That said, some people are of the opinion that putting your trust in the hands of an individual is a preposterous idea, and that large companies, like Google and Facebook, are actually more trustworthy. This, like many things, will be your decision to make. I'm not saying "do your own research," I am reminding you that you can have your own opinion.

* If you want to try other options there is [Accrescent](https://accrescent.app/) and [Obtainium](https://github.com/ImranR98/Obtainium).
{{< /box >}}

However you feel, it won't hurt to look and try some things out. Here are some examples:

* Install a password manager (e.g. Bitwarden)
* Install an 2FA authentication app (e.g. Aegis)
* Install something to help block ads, trackers, and other Internet traffic (e.g. [InviZible Pro](https://f-droid.org/en/packages/pan.alexander.tordnscrypt.stable/))
* Install an email client (e.g. FairEmail)
* Install a calendar app (e.g. [Etar](https://github.com/Etar-Group/Etar-Calendar))
* Install something to watch YouTube content with (e.g. [NewPipe](https://f-droid.org/en/packages/org.schabi.newpipe/), [LibreTube](https://f-droid.org/en/packages/com.github.libretube/))
* Install chat clients (e.g. Signal, Telegram)
* Install a different browser (e.g. [Privacy Browser](https://www.stoutner.com/privacy-browser-android/changelog/))

## Review Your Work

You can take a moment to see if your work is paying off.

Below are some sites that test for ads, trackers, and other sneaky Internet trickery. Don't worry about getting 100%. Browser fingerprinting is very tricky to avoid, especially if you want to use your computer or device without too much friction.

* [Ad Blocker Test](https://d3ward.github.io/toolz/adblock.html)
* [AdBlock Tester](https://adblock-tester.com/)
* [Cover Your Tracks](https://coveryourtracks.eff.org/) (ads, tracking, fingerprinting)
* [BrowserLeaks](https://browserleaks.com/)
* [CreepJS](https://abrahamjuliot.github.io/creepjs/) (JavaScript trickery)
* [No-JavaScript fingerprinting](https://noscriptfingerprint.com/)
* [Browser Privacy Check by EXPERTE.com](https://www.experte.com/browser-privacy-check)

## Next Step: The Main Course {id="next"}

All of this was just the starter --- the appetizer. If you have followed along, even just casually reading and checking out the links, you have only had a taste. Digital privacy is more complex than having a password generator, or avoiding Big Tech, or following a guide from a forum written by a guy with a tinfoil hat.

If none of the above causes too much friction, don't stop. Keep going. Here are a few more things you can try.

* ~~Begin using [PGP](#pgp) for your emails.~~ [That's a waste of time it turns out.](https://soatok.blog/2024/11/15/what-to-use-instead-of-pgp/)
* Stop using MS Office on your computer.
* Delete all social networks.
* Replace more apps on your phone. For example, you can replace the camera, the keyboard, the photo viewer, the maps, the SMS app, etc.
* Take a look at the different communities on [Matrix](https://joinmatrix.org/) or try out [XMPP](https://xmpp.org/)
* Try out Linux[^lin] on your computer (**NB: installing Linux is not a magic shield that protects you --- Linux has malware and Linux can be hacked**), or [LineageOS](https://lineageos.org/) for your phone. If you have a Pixel phone, you can also try [GrapheneOS](https://grapheneos.org/) or [CalyxOS](https://calyxos.org/). Another option is [/e/OS](https://e.foundation/).

## Further Reading and Resources {id="further"}

* **Agencies, Organizations, Associations**
    * [Center for Humane Technology](https://www.humanetech.com/)
    * [EFF Surveillance Self-Defense](https://ssd.eff.org/en)
    * [PrivacySpy: We track online privacy](https://privacyspy.org/)
    * [Redecentralize.org](https://redecentralize.org/)
    * [CHATONS](https://www.chatons.org/en)
    * [La Quadrature du Net](https://www.laquadrature.net/en/)
    * [FDN - French Data Network](https://www.fdn.fr/)
    * [La Contre-Voie](https://lacontrevoie.fr/en/)

* **Guides and Lists**
    * [Privacy Guides](https://www.privacyguides.org/en/)
    * [Guide: How to encrypt your entire life in less than an hour](https://www.freecodecamp.org/news/tor-signal-and-beyond-a-law-abiding-citizens-guide-to-privacy-1a593f2104c3)
    * [A Good Privacy List](https://brainfucksec.github.io/a-good-privacy-list) and [Android FOSS Apps List](https://brainfucksec.github.io/android-foss-apps-list) *by brainf+ck*
    * [degoogle](https://degoogle.jmoore.dev/) *A list of alternatives to Google products*
    * [PrivacyTests.org: open-source tests of web browser privacy](https://privacytests.org/)
    * [Awesome Humane Tech: Promoting Solutions that Improve Wellbeing, Freedom and Society](https://github.com/humanetech-community/awesome-humane-tech)
    [Curated List of Privacy Respecting Services and Software](https://github.com/nikitavoloboev/privacy-respecting)
    * [Awesome Privacy: Limiting personal data leaks on the internet](https://github.com/KevinColemanInc/awesome-privacy)
    * [PrivacySec](https://github.com/retiolus/privacysec)
    * [Lissy93/personal-security-checklist: 🔒 A curated checklist of 300+ tips for protecting digital security and privacy in 2022](https://github.com/Lissy93/personal-security-checklist)

* **Search Engines**
    * [The Most Private Search Engine](https://foliovision.com/2021/12/most-private-search-engine)
    * [Mojeek](https://www.mojeek.com/)
    * [Teclis: Non-commercial Web Search](http://teclis.com/)
    * [Kagi Search](https://kagi.com/)
    * [Spot](https://spot.ecloud.global/) (Search engine based on [Searx](https://searx.github.io/searx/), created by the [e Foundation](https://e.foundation/))

* **Articles**
    * [Why not Signal?](https://github.com/dessalines/essays/blob/master/why_not_signal.md)
        - [Why Not Signal? | Hacker News](https://news.ycombinator.com/item?id=30872361)
    * [Centralized vs Decentralized vs Distributed Systems on Berty Technologies](https://berty.tech/blog/decentralized-distributed-centralized/)
    * [It is important for free software to use free software infrastructure](https://drewdevault.com/2022/03/29/free-software-free-infrastructure.html)
    * [Muting videoconferencing apps may not prevent them from listening - gHacks Tech News.](https://www.ghacks.net/2022/04/17/videoconferncing-muting-may-not-prevent-apps-from-listening/)
    * [Why the Past 10 Years of American Life Have Been Uniquely Stupid - The Atlantic](https://www.theatlantic.com/magazine/archive/2022/05/social-media-democracy-trust-babel/629369/) (Spoiler: Social media helped)
    * [How to Be More Anonymous Online - WIRED](https://www.wired.com/story/how-to-be-more-anonymous-online/)

* **Videos**
    * [You Should Check Out the Indie Web](https://youtube.com/watch?v=rTSEr0cRJY8)

* **Tools**
    * [UAD: tool to debloat non-rooted android devices](https://github.com/0x192/universal-android-debloater)

## Terms

**2FA / two-factor authentication & OTP / one-time passwords** <a id="2FA"></a>
: (2FA) An electronic authentication method in which a user is granted access only after successfully presenting two or more *factors* to an authentication mechanism[^2FA]
: (OTP) A password that is valid for only one login session or transaction, on a computer system or other digital device[^OTP]

**asset** <a id="asset"></a>
: Any piece of data or a device that needs to be protected

**centralized** <a id="centralized"></a>
: When all information comes from one server
: e.g. Facebook

**cookie** <a id="cookie"></a>
: A collection of information, usually including a username and the current date and time, stored on the local computer of a person using the web
: Used by websites to identify users who have previously registered or visited the site

**crawler** <a id="crawler"></a>
: An Internet bot that systematically browses the web and that is typically operated by search engines for the purpose of indexing

**decentralized** <a id="decentralized"></a>
: When information is distributed among multiple hubs
: e.g. [Mastodon](https://en.wikipedia.org/wiki/Mastodon_(software))

**fingerprinting** <a id="finger"></a>
: “Browser fingerprinting” is a method of tracking web browsers by the configuration and settings information they make visible to websites, rather than traditional tracking methods such as IP addresses and unique cookies[^finger]

**fork** <a id="fork"></a>
: A project fork is when developers take a copy of source code from one software package and start independent development on it, creating a distinct and separate piece of software

**harden** <a id="harden"></a>
: To make less vulnerable to attack

**metasearch engine** <a id="metasearch"></a>
: An online information retrieval tool that uses the data of a web search engine to produce its own results

**open-source software** <a id="open-source"></a>
: Open-source software is released under a licence in which the copyright holder grants users the rights to use, study, change, and distribute the software and its source code to anyone and for any purpose
: It may be developed in a collaborative public manner
: The ability to examine the code facilitates public trust in the software [^oss]

**PGP** <a id="pgp"></a>
: Pretty Good Privacy (PGP) is an encryption program that provides cryptographic privacy and authentication for data communication [^pgp]

**proprietary software** <a id="proprietary"></a>
: Proprietary software, also known as non-free software or closed-source software, is computer software for which the software's publisher or another person reserves some licensing rights to use, modify, share modifications, or share the software
: It is the opposite of open-source or free software [^proprietary]

**tracking** <a id="tracking"></a>
: When operators of websites and third parties collect, store and share information about visitors' activities on the web [^tracking]





[^2FA]: [Multi-factor authentication - Wikipedia](https://en.wikipedia.org/wiki/Multi-factor_authentication)
[^OTP]: [One-time password - Wikipedia](https://en.wikipedia.org/wiki/One-time_password)
[^proprietary]: [Proprietary software - Wikipedia](https://en.wikipedia.org/wiki/Proprietary_software)
[^oss]: [Open-source software - Wikipedia](https://en.wikipedia.org/wiki/Open-source_software)
[^ubo]: One such guide is [uBlock Origin Suggested Settings – 12Bytes.org](https://12bytes.org/articles/tech/firefox/ublock-origin-suggested-settings/). Doing everything in that guide will create a little more friction that you might desire.
[^tracking]: [Web tracking - Wikipedia](https://en.wikipedia.org/wiki/Web_tracking)
[^protonpriv]: [ProtonMail - Privacy Policy](https://proton.me/legal/privacy)
[^14]: [UKUSA Agreement - Wikipedia](https://en.wikipedia.org/wiki/UKUSA_Agreement#14_Eyes)
[^disroot]: [About | Disroot](https://disroot.org/en/about)
[^nyt]: [Fed Up With Google, Conspiracy Theorists Turn to DuckDuckGo - The New York Times](https://www.nytimes.com/2022/02/23/technology/duckduckgo-conspiracy-theories.html)
[^5b]: [Mojeek Updates, August 2023 | Mojeek Blog](https://blog.mojeek.com/2023/08/mojeek-updates.html)
[^mojeekprivacy]: [Mojeek Privacy Policy](https://www.mojeek.com/about/privacy/)
[^mojeekgreen]: [Doing what's right, Mojeek and the environment | Mojeek Blog](https://blog.mojeek.com/2018/07/mojeek-and-the-environment.html)
[^mojeekreddit]: [We're building – Mojeek – the world's alternative search engine which respects your privacy, and provides its own unique and unbiased results. Ask us anything. : IAmA](https://reddit.com/r/IAmA/comments/apvfrj/were_building_mojeek_the_worlds_alternative/egbqo92/#c)
[^esg]: [Environmental, social and corporate governance - Wikipedia](https://en.wikipedia.org/wiki/Environmental,_social_and_corporate_governance)
[^finger]: [Cover Your Tracks | About](https://coveryourtracks.eff.org/about#browser-fingerprinting)
[^mozilla]: [The State of Mozilla: 2018 Annual Report — Mozilla](https://www.mozilla.org/en-US/foundation/annualreport/2018/)
[^wf]: [Waterfox web browser sold to System1 - gHacks Tech News](https://www.ghacks.net/2020/02/14/waterfox-web-browser-sold-to-system1/)
[^pgp]: [Pretty Good Privacy - Wikipedia](https://en.wikipedia.org/wiki/Pretty_Good_Privacy)
[^lin]: I will not mention any specific distribution. Take a look around, you will find one to try on your own.
