---
title: "A Quick Look at Distro Rankings"
subtitle: "A Comparison of Different Ranking Methods"
description: "A look at the DistroWatch rankings, talks about Mr. Robot and sentiment towards Linux Distributions."
date: 2022-03-05
lastmod: 2024-10-12
draft: false
katex: false
tags:
  - Linux
next: true
nomenu: false
---

I'm afraid I have the distrohop bug again. But, luckily for my sanity and my family, I will not be doing that anytime soon. My partner has seen what happens when that happens. I disappear for the better part of a day. Thinking about it is a nice exercise, though, and it allows me to learn what's going on in the world of Linux distributions.

I am a "desktop Linux user". I don't program anything, and it has been months since I've had to build a package that is not available in the Debian package repository. If something doesn't work, I just star it on GitHub or forget about it. Like many desktop users, I have the habit of taking a peek at DistroWatch from time to time. I tend to glance at the [DistroWatch page rankings](https://distrowatch.com/dwres.php?resource=popularity) at least once a month.

I was first told about DistroWatch in 2002. When I was at university, my good friend was a Linux user. I believe he used Slackware at the time. His desktop was confusing and interesting at the same time. Since then, I look at the site. It is truly very useful, if only for knowing what distros are [based on Ubuntu LTS](https://distrowatch.com/search.php?basedon=Ubuntu%20(LTS)#simple) and getting little updates about releases. I can only assume that a good number of people use that site as well.

I also like to see the difference between the page hit rankings, the [visitor ratings](https://distrowatch.com/dwres.php?resource=ranking), and the [trends](https://distrowatch.com/dwres.php?resource=trending). This is how I got to writing this post. Do these rankings actually mean anything? What are the true "mainstream" distros?

## Ranking Comparison

As I had some free time today, I started making comparisons. I chose a not-very-reliable method. I took the top 21 distros from DistroWatch's page hit rankings from the last six months (to include Arch) and compared those to the global rankings from SimilarWeb. SimilarWeb is an alternative to [Alexa.com](https://www.alexa.com), which will soon shut down. As I write this, I realize that I should have used just one country, and not the global rankings. But, the end results works out well.

The table below lists the distros by DistroWatch rank. The rightmost column is the global ranking from SimilarWeb.

**[DistroWatch Page Hit Ranking Trends](https://distrowatch.com/dwres.php?resource=trending) vs [SimilarWeb Rankings](https://www.similarweb.com)**

(information gathered 2022-03-05)

| DW Rank | Distro      | SW Rank |
|--------:|:-----------:|--------:|
|       1 | MX Linux    | 123,888 |
|       2 | EndeavourOS |  69,927 |
|       3 | Mint        |  19,755 |
|       4 | Manjaro     |  28,875 |
|       5 | Pop!_0S     |  75,268 [^1] |
|       6 | Ubuntu      |   8,959 |
|       7 | Garuda      | 107,654 |
|       8 | Debian      |  22,133 |
|       9 | Fedora      |  57,025 |
|      10 | elemantary  | 128,442 |
|      11 | Zorin       |  46,294 |
|      12 | openSUSE    |  68,228 |
|      13 | Slackware   | 288,450 |
|      14 | antiX       | 404,101 |
|      15 | KDE Neon    | 40,615 [^2] |
|      16 | Lite        | 200,193 |
|      17 | Solus       | 199,704 |
|      18 | Kali        |  21,201 |
|      19 | PCLinuxOS   | 357,687 |
|      20 | Kubuntu     | 217,700 |
|      21 | Arch        |  16,512 |

There are no surprises here. The top six SimilarWeb rankings are:

1. Ubuntu
2. Arch
3. Mint
4. Kali
5. Debian
6. Manjaro

The top six distros include what we expect. User-friendly choices Ubuntu, Mint and Manjaro, [Unixporn](https://www.reddit.com/r/unixporn) favourite Arch, and old man Debian. Kali might not seem like a top choice, but it has become synonymous with "ethical hacking", and received a significant boost being featured on the TV Series *Mr. Robot*.

<figure><embed type="image/svg+xml" src="mrrobot.svg" /><figcaption>Google Trends: Mr. Robot vs Kali</figcaption></figure>

The first season of the series ran from 24 June 2015 to 2 September 2015. Season 2 from 13 July to 21 September 2016. Season 3 from 11 October to 13 December 2017. The final season ran from 6 October to 22 December 2019. You'll need to zoom in a little, but there are tiny upticks for Kali Linux coinciding with each season of Mr. Robot. As people lost interest in the series, during a hiatus between seasons, there was also a slight loss in interest for Kali. I have just now realized that I did not watch that final season.

## Sentiment

Social media plays an important role in everything, so I decided to take a look at the "social buzz" of those six distros. I used [Social Searcher](https://www.social-searcher.com) for this. This is not the perfect solution, as it does not really take into account things like forums or Mastodon, but the numbers are interesting to look at.

With Social Searcher, it is possible to have a general look at the "sentiment" users have towards these distros.

<figure><embed type="image/svg+xml" src="comparison.svg" /><figcaption>Comparison of sentiment toward Linux Distributions</figcaption></figure>

You can see that for the most part, people are quite neutral in their opinion. Kali seems to win in the "Positive Sentiment" category, with Manjaro coming in a close second. Ubuntu proves that the more users/customers that you have, the more complaints. If there were a way to search across more sources, I am sure we could paint a better picture.

Not wanting to leave MX without its fair due, I took a look. And I was a little surprised.

<figure><embed type="image/svg+xml" src="mxsent.svg" /><figcaption>Sentiment towards MX Linux</figcaption></figure>

MX appears to have a low percentage of negative sentiment shared on social networks. A very low percentage! Positive opinion is off the charts. *Is it the lack of systemd?* Having used MX in the past, I can agree that I was very happy with that distro. It was great for my old computer.

## Conclusion

The main conclusion I can draw from these numbers is that **Arch is far more mainstream than its users make it out to be**. Either that, or its users need to visit the site so often to look for direction that it boosts the distro's global page ranking. Also, Ubuntu needs to do something to increase the positive sentiment. As the door to the world of Linux for many users, the experience should be more positive. Finally, it might be worth noting that many Linux websites are less than inviting. As Matt from The Linux Cast pointed out recently, [Linux has a website problem](https://youtube.com/watch?v=q_BfVvHZV1Q).

Arch has things down with access to documentation, MX has lots of YouTube videos posted by the devs, Mint gives us an easy to find download button and instructions, but Ubuntu has company logos everywhere and a variety of choices for download (do I want LTS or the newest version?). Debian's website has a big download button, but it is for the version without non-free firmware, meaning that users could spend time installing something that may not work, and a new user would not understand why (I know, I've been there).

[^1]: The URL to the distro was not ranked, I used the domain.
[^2]: The URL to the distro was not ranked, I used the domain.
