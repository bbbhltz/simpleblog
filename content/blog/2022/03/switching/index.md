---
title: "Switching Desktops"
subtitle: "Rediscovering KDE Plasma"
description: "A quick and positive blog post about the KDE Plasma Desktop Environment."
date: 2022-03-09T11:02:11+01:00
lastmod: 2024-10-13
featured_image: /blog/2022/03/switching/featured.webp
draft: false
katex: false
tags:
  - Linux
next: true
nomenu: false
---

"Variety is the spice of life"

"The more, the merrier"

"Too much is better than too little"

Look up from your phone, or computer, for a moment. Are you alone? If not, and the person near has their phone or computer out, take a quick look. No! Not at what they're doing. Just look to see their screen, whether it is their phone screen or their computer screen.

There is a very good chance that you will see the "out-of-the-box" default. I have noticed that many Android users, for example, just use their phone as-is. There is nothing wrong with that, even though some phones come with bloatware on the home screen. My mother's phone, a Samsung, has icons for Facebook, Twitter, and an NHL app on the home screen. None of those are useful to her, but she leaves them be and instructed me not to delete them.

I do not know that many iPhone users, but from the few that I see, they tend to leave some defaults in place, in terms of app placement and also choice. My students are nearly all Mac users these days. While there is some variety, I tend to see a large collection of icons on the desktop, Safari, sometimes Chrome, and a small collection of defaults in the dock.

Windows users are in the same boat. They have a set of quick launch buttons, or launchers in the start menu, that they like to leave in place. Who can blame them? When I was handed my work computer, it came with Edge, Firefox, and Chrome. All three browsers were in the quick launch bar.

When I was a student, things were not like this. Oh, no. We were a different breed. Call us kitsch or cliché, but when you don't have all the streaming services and social networks, you get to wandering through menus and testing different options. At least, that is what we did when I was at university.

You need to remember that back in those days, everything came with "skins". Many of your favourite applications were skinable. [Winamp](https://en.wikipedia.org/wiki/Winamp) had a [ridiculous number of skins](https://archive.org/details/winampskins) to choose from. Just too many, really, if there is such a thing. The idea of letting users customize the appearance of programs was fuelled by Windows' boring appearance compared to the simplistic beauty of Apple interfaces.

![Winamp Screenshot](archiveorgwinamp.webp "Screenshop from Archive.org")

*Wired* magazine even wrote about this phenomenon in October 2000 in an article called ["GUIs Just Want to Have Fun"](https://12ft.io/proxy?q=https%3A%2F%2Fwww.wired.com%2F2000%2F10%2Fskins%2F).

>**The faceless interface is dead. Long live skins, the hyper-personal edge of desktop computing.**

The article goes on to cite the names of dozens of skinable programs, as well as software that still exists today. I won't soon forget the time I spent tweaking Windows, from the vendor logo, the loading and login screens, and the shell itself. The article mentions Litestep and WindowBlinds, both of which I used and enjoyed. It was just the way we did things. We all had similar looking devices, we needed to make the screen look a little different at least.

The madness of skinning did die down, I think, but not before Apple got a little upset.

>There's no better example than Russ Schwenkler's desktop. On one hand, it's a thing of striking beauty. On the other, you practically need a walking tour to understand how it works. It starts with a Windows extension called NextStart and adds a suite of matching skins for more than a dozen programs. Instead of the standard Windows taskbar, it features a docking area where icon aliases can be shuffled in and out; Schwenkler adds a skinned version of the Launchkaos program manager. Custom minitools include audio playback controls, a hexadecimal calculator, system meters, and file viewers. A WindowBlinds skin holds it all together.
>
>The aesthetic, Schwenkler says, is "tailored, with lots of attention to detail. I use a variety of surfaces that work together visually, along with lights, shadows, and highlights - things a button that's just a dot or a big broad X isn't going to give you."
>
>Schwenkler became an instant skins celebrity this past January when WinAqua, his WindowBlinds-based clone of MacOS X, hit the streets before the beta release of Apple's operating system. Posted on skinz .org, WinAqua was downloaded more than 10,000 times in a single day before the site pulled it at the request of Apple attorneys.

I have rarely seen a customized desktop outside the Linux world. But, the reason for that is simple. Linux's users are blessed with an abundance of options. And it isn't just a question of skins, Linux users can, and have the habit of, customizing the entire interface of their desktop to pixel-perfect precision. And, if for some reason that *one thing* they want to change cannot be changed, well, they pack up their dotfiles and head on to newer pastures: a different desktop *environment*.

## Paradox of Choice

Just when you are starting to feel comfortable, like you have everything you need, you look over at your neighbour, and his grass seems greener. This was my conundrum very recently. For at least three years, I was happy in my environment. But, things started to get in the way. There was a lack of flexibility. I looked and looked for solutions to this minor problem. And, the solution I found was to change.

For about three years, like I said, I was comfortable. LXDE and LXQt were all I required. I could work and play. But, I started to get an itch. I just wanted to take a look at something new. Looking is OK, right?

While at first it may seem like you are limited, it is not the case. You can have a DIY minimal keyboard-controlled tiled window manager, with no theming whatsoever, a slick modern Mac-like desktop, or a traditional desktop with a panel and a start menu.

The obvious choices when changing desktops are the big players: KDE, Gnome, Xfce along with some forks like Cinnamon and MATE. After a terse look, and to cut to the chase, I chose KDE.

## KDE: The Winamp of Desktops

The last time I used KDE was on [Netrunner](https://www.netrunner.com/netrunner-overview/). I think it was on a netbook. It used KDE 4. While I didn't hate it, it was not adapted to the device. Also, I had no interest in widgets and customizing at the time. I found it clunky, and it really slogged along on that Eeepc.

(I did dabble with ricing and tiling window managers later, but it is not my thing)

KDE Plasma is not KDE 4. Things are faster, integrated, and can be fine-tuned. Even the default layout is agreeable to use, down to the wallpaper. The thing that many people would enjoy is that you can make it work the way you want.

As a very basic example, I have added buttons to the text editor, Kate, that launch a spelling/grammar checker in an external terminal. There are also buttons to help me convert files to different formats without opening up a terminal and typing in an entire command.

![Kate custom tools](katetools.webp "Custom External Tools in Kate")

Once things are working the way you want, you can make things look different, and here is where things become very flexible. All forms of colours and shapes can be modified and tweaked. Just like with Winamp, there are [stores filled with scores of themes, icons, widgets, decorations and scripts](https://store.kde.org/browse/).

The recent version of KDE Plasma comes with a great overview mode too, which is something that I was missing during my years of LXDE/Qt usage.

![KDE Overview Mode](overview.webp "KDE Overview Mode")

There is far too much to see and do. This is not a bad thing, but if you like this sort of thing, it can be a massive time suck. No matter how you look at it, though, it is a prime example of the "hyper-personal" desktop that Wired talked about.

## Konqlusion

Blissfully ignorant to what KDE can do, my several days of rediscovery have reminded me that when you own a computer, you shouldn't be confined to the default appearance and functionality. Your device should work the way you want it. It was one of my reasons for abandoning Windows all those years ago.

KDE is a mature and powerful desktop environment that could convince most people that Linux is not all command lines and clunky interfaces. It has quickly surpassed all my expectations. If you haven't taken KDE for a spin in the past few years, it is worth your time.
