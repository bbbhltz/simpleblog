---
title: "Privacy Browser"
subtitle: "Down the Rabbit Hole again..."
description: "A post about the author discovering Privacy Browser (by Stoutner) and the Mojeek search engine."
slug: privacy-browser
date: 2022-03-10T12:03:07+01:00
lastmod: 2024-10-13
draft: false
katex: false
tags:
  - Privacy
  - Technology
  - Review
next: true
nomenu: false
---


The escape from the [rabbit hole](/blog/2021/04/privacy-security-rabbit-hole/) left you stronger.

It still beckons you from time to time. You are wise to the ways now, though. You can get close, and lean in, without fear of falling. You're fine. You can so see what it wants.

Ah, yes, of course. The same news as before, another comparison of browser privacy. Well, not just *another*. [This one is rather detailed](https://privacytests.org/). You share it with the strangers of the internet, and continue on your merry way.

A voice...

"Hey! Get back here!"

"No. You can f-ck right off! I know how this deal works. I am not some journalist or researcher. I am not a hacker. I have my ad blocker, I block the invisible trackers, I use https, and I know my fingerprint is *not* unique. I don't like it, but I am *fine* with it."

"No... Just... Come on, take a look. It won't take long."

"You have 5 minutes."

## Just a Peek...

This rabbit hole goes on forever. Your time can be put to use elsewhere. The folks doing the work to inform are doing a great job. More and more people around you talk about "protecting their privacy online." No need to go down, let's sit on the edge of this deep dark hole and look.

*If what you seek\
is to take a peek,\
then spend a week\
with...*

![Mojeek Logo](Green.webp "Mojeek Logo")

"That's a search engine, not a browser."

"Patience. You said I had 5 minutes."

"Go on..."

>Our vision is to be the world's alternative search engine; a search engine that does what's right, that values and respects your privacy, whilst providing its own unique and unbiased search results.

*Mojeek* is the result of years of work. The work of Marc Smith, to be precise. He began this project in 2004 and today employs six other team members. Mojeek is a *crawler based search engine* with its very own index of web pages.

On 8 March 2022, Mojeek announced that they had surpassed 5 Billion Pages indexed[^mojeek].

As of 8 March 2024, Mojeek has indexed "7.8bn and a bit" pages[^mojeekpages].

[^mojeekpages]: https://mastodon.social/@Mojeek/112060297863694606

You agree. Mojeek is cool. [Their servers a "green"](https://blog.mojeek.com/2018/07/mojeek-and-the-environment.html), they are [independent and unbiased](https://blog.mojeek.com/2018/08/independent-and-unbiased-search-results.html), and they have a [tracker free newsletter and RSS feed](https://buttondown.email/Mojeek) and a [Mastodon account](https://mastodon.social/@Mojeek).

"Thanks for the info. See ya!"

"I still have a minute or two!"

"What about a privacy-centric Android browser with Mojeek as the default search engine?"

"Send me the link. I'll give it a shot. I'm out!"

## Fun with Privacy Browser

Venturing away from the rabbit hole feels good. Your phone beeps. It is a link:

[Privacy Browser Android](https://www.stoutner.com/privacy-browser-android/) (**The only way to prevent data from being abused is to prevent it from being collected in the first place.**)

![Privacy Browser Logo](Privacy-Browser.webp "Privacy Browser Logo")

You bite. It is on [F-Droid](https://f-droid.org/packages/com.stoutner.privacybrowser.standard/), so, why not?

Everything checks out...

- [X] Open source
- [X] Blocks ads, JavaScript, cookies, and DOM storage by default
- [X] Easy to switch user agents
- [X] Default search engine is [Mojeek](https://www.stoutner.com/switching-from-startpage-to-mojeek/)
- [X] Other search engines must pass [Requirements](https://www.stoutner.com/requirements-for-a-search-engine-to-be-included/) to be included
- [X] Dev is on [Fosstodon](https://fosstodon.org/@privacybrowser)

Oh, that's different: "uses Android's built-in WebView to render web pages".

Let's give it a (screen)shot.

*Ah. OK. Now we're talking. You cannot even screenshot the app without going into the settings to turn that on...*

Take two.

![Privacy Browser home](pbhome.webp "Privacy Browser Home")

After a quick load, Privacy Browser takes you to Mojeek. From here you discover that there is a left menu and a right menu.

The left menu takes you to navigation and global controls.

![Privacy Browser Menu](pblmenu.webp "Settings Menu")

The right menu takes you to page and domain settings, as well as sharing and bookmarks.

![Privacy Browser Menu](pbrmenu.webp "Page Menu")

![Privacy Browser Bookmarks](pbmarks.webp "Bookmarks")

Domain settings inherit from global settings, but can make the experience for certain sites better. Many sites with drop down menus need JavaScript.

![Privacy Browser Domain Settings](pbdsettings.webp "Domain Settings")

I leave this as default, but there are a good number of preconfigs here.

![Privacy Browser User Agent](pbua.webp "User Agent Settings")

Finally, just to show how pervasive JavaScript is. This very blog. There is one post that requires JavaScript to render maths. Without JavaScript, I suppose it is still human-readable. But, I did spend about 10 minutes getting Katex working (don't worry, it is only needed for that post) so I hope you have JS on for that post.

![Privacy Browser My Site with no JS](pblottonojs.webp "My site without JS")

![Privacy Browser My Site with JS](pblottojs.webp "My site with JS")

**Can I work with this browser?**

That really is my only requirement. I need the browser for one specific thing: Accessing my schedule. In order to get to it, I needed to make domain settings for `*.microsoftonline.*` and `*.myworkdomain.*` and activate JavaScript, Cookies, and DOM storage. A minor annoyance.

**Can I waste time with this browser?**

Yes. But, sometimes mobile pages don't load automagically. Not a big problem. Apparently, different user agents return different pages.

Something that I do like is the "save as" functionality.

![PB save notification](saveas2.webp "Save as notification")

Instead of the standard UX, Privacy Browser has a nice little progress notification at the bottom (a "snackbar", not in the Android menu). There is **no** persistent notification in the Android menu. According to the dev,

>"[T]here are a couple of things about Android's download manager that are unsatisfactory. First, it doesn't provide a mechanism to specify a proxy. Second, it doesn't work at all on Android 7.0 when a VPN is enabled."[^dl]

You are also able to edit the URL before you download. All of this is very nice. I, for one, do not need any more notifications on my device, nor do I want anyone seeing what I have been downloading (for clarity, it is mostly PDF files of articles and magazines from my school's library).

And, very important, even for me, is the ability to clear cache and cookies when exiting. This is on my default.

**Is there a future?**

Yes! It looks like this project will continue and expand to desktop. Android's WebView will be swapped out for Privacy WebView (a fork of the former with added privacy features). There will be Linux, Windows and macOS browsers.[^future]

## Silly Rabbit

Well, it seems the little rabbit was right: This was worth my time. I should go thank him...

No.

Not falling for that again. Both of these new tools are good enough for now.

[^mojeek]: [Mojeek Surpasses 5 Billion Pages | Mojeek Blog](https://blog.mojeek.com/2022/03/five-billion-pages.html)
[^dl]: [Privacy Browser 3.4 – Stoutner](https://www.stoutner.com/privacy-browser-3-4/)
[^future]: [3.x Series Roadmap – Stoutner](https://www.stoutner.com/3-x-series-roadmap/)

