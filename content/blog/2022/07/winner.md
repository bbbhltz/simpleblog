---
title: "We Have a Winner"
subtitle: "A Short Story"
description: "A short story about a young adult winning a contest, the story has echoes of Amazon."
date: 2022-07-12T00:00:00+02:00
# lastmod:
draft: false
katex: false
tags: ['Short story']
---

>**FOR IMMEDIATE RELEASE**
>
># We Have a Winner
>
>SEATTLE, Washington --- We are happy to announce that we have a winner to our yearly lottery draw. As with our previous draws, the winner was randomly selected in the presence of the CEO and the board of directors. The winner has yet to claim their prize, and due to anonymity laws we are unable to divulge the name, location, or any other personal data about the individual.
>
>As a reminder, in order for the winner to claim their prize, the individual must meet the following requirements:
>
>* Be at least 18 years of age.
>* Have made at least one purchase on the site or through one of the partners in the last 12 months.
>* Have a completed profile, including confirmed postal address.
>* Have scanned and uploaded their palm print in one of our physical stores.
>
>This year's winner has yet to upload their palm print.
>
>We urge all users who have yet to complete this step to make their way to one of our physical stores.
>
>**You might be the world's next billionaire!**
>
><center>###</center>
>
>**OneStop.shop** has been the world's leading online and physical shopping experience since 1995. Lead by billionaire philanthropist and innovator, Gina Troy, we make the world a better place by giving you the best deal.

"The press release is live."

"Do we know who the user is?"

"Ma'am, we can find out, but we would violate our own privacy policy."

"Who is it?"

"Are you sure?"

"Damn you, just tell me!"

"The personal information cannot be decrypted. User a55o-omah-UyaU-9Ggh-fuAj-9aUM is the winner."

"Online activity?"

"They made one purchase this year: a pack of 24 BIC ballpoint pens and a notepad, $16.48. The account was created on the same day as the order. Payment via gift voucher purchased at a physical store. They haven't even connected to the site since to use the remainder of the balance."

"And the winner is...somebody who still uses pens and paper?"

"Ma'am?"

"Make sure nobody can tell we looked this up. I'm going home to drink wine and forget about the fact that somebody who spent less than what I spend a day on coffee is going to be a billionaire."

## <center>Breakfast</center>

The news is blaring in the kitchen. Day 17 of the lottery watch. It has been a slow month. Do we really idolize wealth that much? Someone might be rich! Oh! la la!

"Can you turn it down?" I yell.

Mouth full of food, they answer back with an onomatopoeia best transcribed as, "yeahmmu'," which I guess is "yeah, mum."

I complain about it, but I am a hypocrite. I was online watching the draw when it happened. **THANKS FOR PLAYING! TRY AGAIN NEXT YEAR!** is burned into my eyes. I see it when I blink. Time to put on the good mum face. Time to pretend like we aren't drowning under debt. "Plans today?"

They swallow, take a breath, and with an eye roll reply, "Job search. Yippee!"

Adolescent sarcasm still prevalent at 18. Maybe I did baby them too much. Too long with the dummy when they were young? Too lenient? At least they aren't some tweaker sleeping on the street, and they clean up after themselves. "So, you'll be here all day?" I ask.

"Nope. Gonna hit the streets. Walk in. Face to face cold-call!"

"You're kidding!"

To be a fly on the wall to watch them walk in the door --- piercings, candyfloss coloured hair, torn jeans, and chewing gum --- and announce they are looking for a job! The thought lightens my mood. "Can't wait to hear about it! Fingers crossed!"

"I bet," they smirk back at me.

"Love you."

"Yup. Love you too mum."

Pain in the ass that they are, I know they're sincere when they say that.

## <center>Lunch</center>

Get a job; get money; help mum. Easy-peasy. Belly is full enough to get to lunch without fainting. Been in America for several generations and mum still maintains that "stiff upper lip" thing, but she can't fool anyone. She's been wearing the same clothes for the past five years. She shines her shoes and mends her stockings like we're living in... a long time ago. I make a note of the food I eat, what she pays for my clothes, my hair, my medication. I'll pay her back. Just need a job.

Here we go. Alix goes job hunting. Round one. Fast food. Shouldn't be too hard. They hire everyone, right?

"Did you fill out the form online?" says the person who I assume is a manager (they have a grey short-sleeved shirt on and a black tie).

I clear my throat, "Form?"

"All applicants must apply using the online form."

"Oh? No. I thought I could..."

He's shaking his head, "Sorry kid. I don't decide who works here. You gotta apply online, and the AI does the rest."

"Thanks, I guess."

Well, nobody gets a job on their first try. Right? I mean. I guess I could have asked around how people get jobs. Or at least paid closer attention in class. An enterprising young person should be enough, or so I thought. Die and retry, like in the games. Into the next overpriced, neon-signed, burger joint we go.

"All openings are announced on our social."

...and the next.

"We are managed remotely. I'm not even from here, I was relocated after training."

Again and again. The same answers. Mum might have had a point, but I don't like using social or the net for that matter. I know mum has to pay, so I keep it simple.

Clothing stores next? It isn't even lunch, and I have already been shut down about 9 times. What's a little more humiliation?

"Hi! I was wondering if you were hiring?"

The girl behind the counter lifts her glasses and looks at me from head to toe, and then again from toe to head. "No," she states. "If you don't plan on buying anything, I'm gonna have to ask you to leave."

Okay. Early lunch it is. And I know just the place.

"My man with the frying pan! Whaddya got for me today?" I blurt out as I hop over the mat at the entrance. That damn *ding-dong* sound annoys me to no end.

"Ah, my worst customer who won't even tell me their name. You have money today?"

"Come on, some kind soul must have paid something forward today! And that isn't nice. I'm not your worst customer. I'd have to pay in order to be a customer!"

"'Come on,' they say. I say, 'come on and pay me for once.' You clean. Have place to live. Pink and blue hair; not cheap. You buy someday?"

"Someday, but not today, my man! Soon, though. Out looking for a job, and I need *sustenance*."

"I give you grilled cheese. To go!  But first, I need laugh. Say something with that funny accent."

"You're one to talk! You sound like a racist stereotype, ha!"

"This? *This* character, kid. Talk like *this* just for customers. Come on. Just a word." He looks around and points at a large roll of thin shiny metal hanging near the kitchen entrance, "you call this what?"

"Aluminium," I say, the way my mum says it, the way her parents said it, the way many people from outside of America said it back when.

"ALUMINEEEEYUUUM! Hahaha! That's good. Classy. Not like American accent. They all talk like they have hot food in their mouths. *Aluminum*. Ha!"

"Oooookay. You got your laugh. Hit me up with some hot carbs and casein. Gotta hit the streets."

"Here. And, if you get job, I give you free dessert with first purchase!"

I walk out the door as he changes the "Open" sign to "Be Back in 30 Minutes" and head on my merry way. Maybe I should ask him his name. I make a note in my journal of the free grilled cheese. A block later I am done eating and decide on a little digestive rest on a bench. Cars zip by silently. People walk carrying bags. A couple strolls by carrying armloads of produce in a small wooden crate. That's different. I look down that street and see a fruit and vegetable stand at the very end of the block.

Could be worth a shot.

"Hi. Nice, uh, stuff," I say to the lady behind the stand.

"Thanks. Do you want some of this *stuff*?"

No glasses. No watch. This looks promising.

"I was actually wondering if you were looking for help? Like, hiring?"

"You wanna to work for me? *Here*?" she says incredulously.

"I mean. I need a job. I've asked around, but it isn't my lucky day. I just need a little money to help my mum out." Emotional manipulation might work on someone other than my mum, right?

"If you're serious --- *If you are really serious* --- be here tomorrow morning, 5:30. At 5:31, I leave, and you won't get a second chance."

"Do you need anything? Like, an online application? Because, I don't have any of that. I don't even use social. I don't even have a résumé."

"A bank account is all you need for this job. And don't wear anything nice. We don't just sell, you know? We pick it ourselves."

*Holy crap!*

*This is a job.*

*I got a job. I can't wait to tell mum.*

So caught up in the moment that I was, I didn't see the person walking towards me carrying an armload of groceries. I walked smack into them. Everything fell on the ground. "Oh no! I'm so sorry! Let me help you," I say without even thinking.

"No problem. I live right here," said the man while pointing at the door of the building we were standing in front of.

I picked up some things and followed him as he used a badge to unlock the door. I took a step inside and felt a prick on the side of my neck. My knees. Something...is...wrong... "I need to thit down..." *Was that my voice?* My mouth is numb. There are two men standing over me.

"Look what I caught us, Ronnie," says the grocery guy to the other man.

The other man, Ronnie, starts to sign something back.

"OK then. This is how we do this," Ronnie is crouched down beside me, holding my face. "You're gonna give us your money, and in a few hours you'll be all better. Nobody gets hurt."

"Don't...got...money..." I manage to mumble.

"No money! Everyone has money. Where's your phone?"

Ronnie and grocery guy's hands start patting me down. They're looking in my bag.

"A pen and a book?" exclaims grocery guy, shocked.

Ronnie is signing something back to him.

"Well, Ronnie. I didn't know you were into that sort of thing. This here's just a boy if you didn't notice. If you wanna do that, well, I won't be watching your back."

Ronnie signs again. Points at me.

"What? A woman? What do you mean?"

Grocery guy leans in closer, "Well...fuck me! I must be higher than I thought, ha! This here is a girl. And not no kid either. Ain't much of a looker, though."

Here I am, completely doped out of my gourd, about to be *who-knows-whated*, and I decide to correct two cretins about my desired pronouns: "They," I squeak out.

The two men are looking down at me. I can't do much more than blink and breathe. I feel a tear roll out of my left eye, down my temple, and into my ear. Blurry-eyed, I look up at them, hoping that there is a thread of empathy that I can latch onto.

I hear something crunch. I can't lift me head. Did they just break my leg?

No.

Light.

A bang.

Someone just came in the door.

"Thefuck..." says grocery guy.

A third person is standing over me. Ronnie, or grocery guy --- who knows --- throws a punch. Door guy --- or girl?... Door person seems to swat the punch away. I hear a slight *ping* as he blocks it.

"Oh fuck dude! My hand! You broke my hand. You're gonna get it! Ronnie!"

Why are they all still standing over me? Can't they take this outside? Ronnie has a knife now. Maybe? Ronnie has something shiny, and he is pointing it at door person.

"Be smart or be dead," says door person. Man's voice. Sounds familiar.

I see Ronnie cock his arm back. He takes an awkward swing. Door person catches the knife with his hand. In his hand? Whatever they drugged me with is good. Is this tripping? I think I am tripping. I wish my mum were here.

Door person is holding Ronnie's hand in his. I hear a crunch. Ronnie groans and whimpers, falls back out my my field of vision.

Now grocery guy is up behind door person. He grabs him around the neck. Door person reaches up and grabs his arm. Another snap, followed by screaming and swearing. Then door person turns around and clocks him in the head. I hear his body slump on the ground.

"I'm taking them. Don't even try to follow me."

I feel an arm slide under my shoulders, another under my knees. Door person is picking me up.

"Let's get you to a clinic. Closest isn't far. Can you walk a little?"

"Who...?"

"John. I'm John."

"Alix."

"Nice to finally know your name, Alix. Job hunt going well?"

"...grilled cheese?"

"Yes. I made you the grilled cheese."

"...alu...minium..."

"They really drugged you good. The clinic is just across the street."

"No money."

"I know. But, we'll deal with that after."

They carry me through the door. I see the logo. It is one of OneStop.shop's clinics. This is going to be expensive. Someone is putting my hand on a palm reader. Then I am on a stretcher. I let myself fall asleep.

## <center>Dinner & Dessert</center>

Awake.

I look around the room.

Someone is here with me. I wipe my eyes. It is...the guy from the restaurant!

"What the..."

"Oh good. You're awake. I can get back to work."

"How?" I say, while making punching and chopping motions.

"Oh? Yeah. Titanium prosthetic." He holds up one hand and spreads the rip in the prosthetic skin to reveal shiny metal bits. "Accident with a door a few years back. Long story. I was walking back from my lunch break when I saw those guys grab you."

"Your...voice."

"I told you. Just marketing. My name is John, and you're Alix. Your mom should be here soon. You were only out for 30 minutes. They flushed your system. The miracles of modern medicine, I guess."

"How much?"

"How much what? Money? I don't know. They didn't ask for payment; said it was taken care of. They found your photo in the system and scanned you in. No questions asked."

This is confusing. My account has about $12 on it. I hear shoes clip-clopping down the hall. Unmistakable shoes. My mum's five-year-old shoes.

"Oh sweetie! I was so scared!" she walks in, ignoring John, and throws herself onto me with more hugs than I could have ever imagined. "And you," she says looking at John. "Thank you, thank you, thank you." She looks back at me, "tell me what happened."

John excuses himself and walks out, nodding to me.

"I got a job, and then some guys grabbed me," I answer. "Then John --- the old guy that was here --- found me and brought me here. I'm sorry, mum. I just wanted to help. How much is the bill?"

"What bill?"

"For the clinic."

"I guess they'll tell us when we leave."

We chitchat some. I tell her about the bad morning of job hunting, and the fruit and vegetable stand, and how I need to be up early tomorrow. A nurse passes by and takes out the IV.

We head to the reception and ask for the bill. We're told there is no bill. That it was paid.

"By whom?" asks mum.

The answer: "By your daughter."

We look at each other. She looks as confused as I am. Her head swivels around, and she grabs my arm --- the arm that had the IV in it --- hauling me toward a OneStop.shop banking machine.

"Ouch."

"Sorry honey..."

She puts me in front of the machine.

"I've never used one of these," I tell her.

She takes my hand and slaps it on the palm reader.

A loading screen and then...**CONGRATULATIONS! YOU ARE OUR WINNER!**

"What the hell did I win?"

"You won...the draw," she whispers to me.

"I won the --"

"Shhh! Be quiet. You won the yearly draw. The big one. It was you. You won..."

The notification fades away, and I see my balance: $999,999,275.35.

My eyes fill with tears. I reach into my bag and pull out my notebook. Mum is shivering. Speechless.

"What are you doing?" asks mum.

"Taking care of business," I reply.

I set up a transfer and then grab her hand and slap it on the palm reader.

"Alix, no! Stop! You don't have to."

"No, mum. I do. Besides, what I owe you is a drop in the bucket." And then, I look at my watch.

"I'm getting hungry. Are you hungry? My treat. I know a *great* place that owes me a free dessert. Let's get you some new shoes along the way."

## <center>Indigestion</center>

*Bzzzz-bzzzz-bzzzz*

Gina Troy picks up her phone and answers. "Yes?"

"Ma'am. We have a winner."

"Please tell me that it's not some idiot druggy that's gonna get themselves killed."

"No. I think this will play out well. Winner is 18 years old. Low income. Very low income family. Lots of debt. Found out they were the winner in one of our clinics where she was dropped off after being attacked."

"Do we have video?"

"Yes."

"Can we use it?"

"I'll let you decide. Sending it to you now."

"... Pink and blue hair? Cute-ish. Maybe she will agree to be in promotional material. I almost feel good about being a *philanthropist*."

"They go by 'they,' I'll start getting the press---"

"Wait!"

"Ma'am?"

"Who brought them in? Who is *that man*? Do we have ID?"

"Uh...no. We have a clear shot of him leaving though. Sending now."

"Motherfu--- Get the press release ready. I trust you. Just send it as soon as you can. You don't need my approval. I have other business. Keep in touch."

Gina ends the call and pulls a small notepad from the bottom drawer of her desk. After a few pages of flipping she finds what she needs. She gets another phone from the same place she found the notepad and dials a number. Someone answers after just one ring.

"Yes?"

"It's me. Tell him I found *him*. I found *John*. He can meet me at our usual spot."
