---
title: "Homework"
subtitle: "A Short Story"
description: "A short story about discovering the truth."
date: 2022-07-02T12:00:00+02:00
# lastmod:
draft: false
katex: false
tags:
  - Short story
---

"Dinner time!"

"Coming, mom!"

"What were you doing in your room? You were being very quiet for once."

*What was I doing in my room?*

"Homework."

"What subject?"

"...history?"

"Is that a question, or an answer?"

"Just tired, mom. History. Not done yet. Can I eat in my room?"

"Yeah, sure. But bring the plate out when you're done. Your room smells bad enough!"

"Ha ha, you are *so* funny."

*Now, where was I?*

`> truthseeker29: When I was a kid I saw a guy with no gear. Freaked me out.`

`> unblinded99: yeah right i call bs`

*Fuckin' chatroom trolls.*

`> truthseeker29: fu you don't know me, I saw the dude, the one who the police say fucked up the restaurant owner that time, I was like 6 or 7.`

`> unblinded99: this one [link]?`

*OK, still a troll, but at least they reply quickly.*

>## Police save restaurant owner from deranged man
>
>WASHINGTON -- Time and place were on the side of the law today, when a deranged individual was arrested while attacking a restaurant owner.
>
>"He came in my restaurant, and I told him I could not accept cash or card, he needed to use the app like everyone else," said the owner, John Hu. "Not a second later he grabbed me and started hitting my hand with a hammer he had with him."
>
>Luckily for the owner, three off-duty officers were in the vicinity and heard the commotion. Without hesitation, they raced into action and apprehended the individual.
>
>Once in custody, they determined that the perpetrator had been cited for previous violations of disturbing the peace. He was taken into custody pending trial.
>
>"I'm just so thankful that those officers were nearby. Once my hand is healed, I would love to shake theirs," says John.
>
>---
>
>**UPDATE:**
>
>After his arrest the perpetrator of this heinous act was evaluated in a psychiatric facility. There, it was quickly determined that due to prolonged disconnection from social networks, he had fallen into a deep depression. This depression exacerbated his anti-social behaviour which eventually lead to a mental break with reality.
>
>"It is important to remain in touch with friends and relatives as much as possible. To deprive oneself of a technology used by most of the developed world is to open the door to grave consequences," says Dr. Phil Framer, psychiatrist at UMHW.

`> unblinded99: done?`

`> truthseeker29: fuck`

`> truthseeker29: no way that dude had a hammer, my mom was there 2, she looked right at him`

`> unblinded99: no way you remember if he had a hammer or not, i bet you didnt see shit`

`> truthseeker29: how can you be so fuckin stupid to believe in this shit tho?`

`> truthseeker29: you really think that ungeared ppl are all crazy?`

`> unblinded99: open your eyes @truthseeker29`

`> unblinded99: you came here a normie, now you learn the truth, i gtg`

`# @unblinded99 has left the chat`

`# @truthseeker29 has left the chat`

*No fuckin' way did that dude have a hammer. I can't deal with that level of crazy.*

"Mom?"

"Done your homework?"

"Uh, yeah, can I ask you somethin'?"

"Shoot."

"Do you remember when I was little, and we saw that ungeared guy get arrested?"

"*Michael...*"

"What?"

"His name was Michael, we went to school together, until we were about 13."

"Why didn't you tell me?"

"Why would I? You never asked?"

"So, do you think he was crazy?"

"What? No! Not him. He was just a bit...different."

"So, you don't think he attacked the restaurant owner?"

"What do you mean?"

"Look, someone sent me this article."

"Honey, where did you get that? That isn't even a real news site."

"Huh?"

"Here, look, first, no author. And, I bet that that doctor, isn't a doctor, or doesn't exist. And look at the other junk on that site. Do you really think that there is some sort of global conspiracy to take over the world," she said sarcastically, wiggling her fingers in the air like she was casting a spell, "that people without implants spread diseases, that the president rigged the votes? Look at that one! This is an article about how billionaires and film stars are cannibals! Fuck off --- *Are they still going on about Pizzagate?*"

Mom stopped reading and looked at me, tilting her head to the side little. "Paul," she began, "my mother told me stories her mother told her. I know that you are smart and mature, but even smart people can get sucked into Internet fantasies. Back when social networks were still figuring things out, there were lots of problems with them. Many people were sucked into believing things that were just not true, or real, or... Some of it was make-believe, like, obvious nonsense, and even when they found out who was doing it, people still believed it.

"I am not going to police you on the net. I don't want to know what you're looking at anyway," she stopped to smile at me.

"Don't be weird, mom."

"Just...don't believe everything you read."

*So, what am I? Gullible? Stupid? I'll show that little troll...*

*John Hu still owns the restaurant. I just have to swing by tomorrow and ask if he has seen Michael lately, that my mom and him are old friends.*

*DIIIINGDOOONNG*

A little bell rings as I step into the restaurant. It smells like any other restaurant in the city: fusion! Burgers and fries, vinegar, tofu, fish, sweet, sour...and sweat. Behind the counter is the man from the article, John Hu. I bring up the ordering app on my glasses and pick two cheap items and a drink. I stand at the counter and wait for him to bring my order. I decide to send a quick message to that little dipshit to let him know what I'm doing. Before I can hit send, though...

"Order up! This one for you kid?"

"Uh, yes. Yes. For me. Um, hey...little, uh, question. My mom wanted to know if her friend still comes around here?"

"That depends. What's friend's name?"

"Michael."

"..."

"Are...you OK?"

"Michael still comes in. Now you get out. Customers waiting. Go tell your mom he comes by almost every day."

"Oh. Thanks."

*Every day! I could just wa...*

"Hey John, how's it going?"

"Michael! Just talking about you. With that boy. Your order almost ready. You two talk. He says his *momma* wants to meet you, haha!"

Michael looked at me. I might be 17, but I suddenly felt a lot younger. It wasn't often that I looked at people so closely. He had the connected glasses and watch, but was for sure the guy I saw when I was a child. Before he could say anything, verbal diarrhoea kicked in.

"We saw you. My mom and I, I mean. We saw you the day that you came here...with the police. She knows you. My mom went to school with you."

He didn't flinch. "Follow me," he grunted. "John! Can I use the restroom?"

"I say 'no' you do it anyway."

I followed him behind the counter. We went through the kitchen and down a hallway. He stopped and I nearly ran into him.

"We can talk here. White zone. What do you want?"

I told him. I told him about when I was a kid, what I saw, and what the news says happened. He told me what happened, for real.

"But, you still come in here?"

"Of course, John is my friend. He was hurt, but we know who was to blame. Who is to blame."

"The cops!"

"No. They were just being jerks. Violent and dangerous jerks. I hate those cops, but not all cops."

"So, who? The government?"

"A little, but the problem is a little bigger and harder to explain. *They* are to blame."

"*They*?"

"You know? *They*. Sure, the government is partially to blame. The companies that made all this garbage we have to wear. They are some true fuckers. But *they* did this when *they* gave up."

*I am totally lost here. Maybe this guy is crazy. How do I get outta here?*

"Back when, you know, before my grandparents' time and all that. Like, early 2000s," he began, clearly seeing my confusion, "the tech companies and the government and advertisers and all that were up to some unethical stuff. People, some NGOs and stuff, they started to push back. But then, *poof*, no more pushing. The people fighting for our rights just...gave up. People called them retro-nerds, and neo-Luddites, and the media convinced everyone that what they were doing was a trend or something.

"*They* were the government, and the monopolies, but *they* were also the people, like us, that were just so complacent."

"That's...a lot...to believe."

"Yup. And you shouldn't believe it, I wish it weren't like that. But, I know what happened to me, and I can tell you one thing. That story you read? It didn't happen like that. Other things that people say? Lots of bullshit. The Internet is like a dog park after a nice sunny day."

"A what?"

"It's full of shit. Mainstream news? They just want viewership. They spin it, they make it spectacular. They are a business, hard to blame them sometimes. But, just the walking path through the dog park. *Alternative news sites*," he said making air-quotes, "is all half-truths, falsities, misinformation, and omissions preying on weak-minded people for the same thing: likes and clicks. Today, *they* make what I do hard. Tall grass, you know? Might be safe, might be full of shit."

"And, what *do* you *do*?"

"I recruit people."

"To do...what?"

"I could tell you, or I could give you a little job to do for me, and you can find out for yourself."

"Like...homework?"

His eyebrows raised behind his glasses. He smiled a little.

"Sure, kid, like homework. I'll be in touch. Get out of here, before John throws a hissy fit."
