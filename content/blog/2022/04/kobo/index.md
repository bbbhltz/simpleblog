---
title: "I like E-books"
subtitle: "No, seriously. I do."
description: "A blog post where the author positively speaks about e-books and the Kobo e-reader."
date: 2022-04-20
featured_image: /blog/2022/04/kobo/stats.webp
draft: false
katex: false
tags:
  - Technology
  - Books
next: true
nomenu: false
---

No joke. I do, truly, like e-books. Of course, real books are better. It isn't a cliché to say that. Most of the time, a real, printed, book makes more sense than reading an e-book.

In many instances, the printed book may cost less, and, in a way is easier to transport and travel with. I say this because there is less concern about losing a book or having it stolen. If you lose a copy of a recent paperback, you lose a little money and one book. If you lose a brand-new e-reader, you lose a collection of books, the money used to purchase the e-reader and the books, and possibly some online credentials and banking info.

The printed book can be handled, and mishandled. It can sit in the sun and be left in a glovebox, fall down, and even get a little wet. You can dog-ear the pages and take notes and write your name on it. Your copy of a printed book is yours.

E-books are a different beast.

## How I wound up with an e-reader

I have actually had three different e-readers. All of them are the same brand. All of them still work. Two of them were the same model.

It all started well over 10 years ago. I wanted to get into reading again, but living in France, it isn't always easy to have immediate access to new releases in English. Reading a translated version of a book does not interest me. I have no problem reading a French book, if that is the original language, and when I do want to read a French book, I still just buy the printed version.

So, I bought a Kobo e-reader. I travelled to China with that device to visit someone, and they borrowed it. I never got it back. So, returning to France I went straight to the shop next to the train station and bought the same model.

## What got me hooked on e-readers?

That first year with an e-reader was wonderful. I read 12 books. That is nothing compared to hardcore book readers, but I didn't think I would read more than five. It is certainly more than the one book (actually, a play) I had read the previous year.

The two books that got me hooked were *1Q84* by Haruki Murakami and *The Martian* by Andy Weir. I don't even remember how I ended up with copies of these books on my device. I know I didn't buy them.

Reading with the e-reader was **motivating**. I like seeing numbers and knowing how many minutes are left in a chapter, or in the book. I could only estimate if reading a printed book. Not only that, but I also liked being in a bubble when I am on the metro: mp3 player plus e-book means nobody knows what I am listening to, or what I am reading.

![Kobo Stats](stats.webp "Reading Stats on Kobo")

There was also the aspect of **reading at night**. My flat at the time was as awful as it was small. There was no way for me to have a night stand with a light. E-readers, with backlit screens, solved that problem. Also, I love reading and falling asleep mid-sentence. I could do that with an e-book, and it would shut down automatically, leaving me to sleep peacefully in the dark.

Finally, there is a **practical** side. Yes, your library can fit in your backpack, but you can also **highlight sections and create annotations**. This is a function that I use on nearly every book I read. There is always a sentence, or a quote, or an entire page that needs to be highlighted for later use. This functionality, mixed with the other obvious functions, left me hooked on e-books.

## How I use my e-reader

Where do my books come from if I don't buy them? I cannot tell you, but you can guess. Except for a few books, my collection is all [sideloaded](https://en.wikipedia.org/wiki/Sideloading) onto the device as EPUB files. This is why I bought a Kobo instead of a Kindle. I knew that I could get EPUB files from many places, usually the day of release. I did learn after that it is possible to accomplish the same thing with a Kindle.

One can just drag and drop the files and be off to the beach for a long read. Or, one can use a piece of software called Calibre. Calibre is not a secret. Even people who have never dealt with e-books have caught wind of this beast.

Since the point here is not a guide, I won't go into getting the Kobo to work with Calibre. It is easy. There are plugins. There are guides. Everything you need to know is on the [MobileRead Forums](https://www.mobileread.com/forums/forumdisplay.php?f=223) It works like this:

- get an e-book
- open Calibre and import the e-book
- ask Calibre to get the metadata (cover, series info, tags) and save it to the file
- upload the file to your Kobo --- and if you have the right plugins, it will upload as a "kepub"

Managing your library is easy like this, and even allows for **exporting annotations to your computer**.

![Getting annotations with Calibre](annotations.webp "Getting annotions from a book with Calibre")

Kobo also has a "My Words" feature. It is just a list of words. It seems banal at first, but after some time you do amass a list of words that are not part of your working vocabulary.

![Screenshot: My Words](words.webp "My Words")

## Tweaking the e-reader

Kobo has features that cannot be accessed easily. It is a pity, some of them are useful. Kobo e-readers come with several games, a web browser, a "dark mode", and the ability to take screenshots. None of those features, other than the browser, can be accessed without some tweaks.

While we can all understand the concept of "beta features", it would be nice to be able to test them without going through a whole rigmarole of modifications. Luckily, there is [**NickelMenu**](https://pgaskin.net/NickelMenu/).

**NickelMenu** adds a custom menu to Kobo devices. It is quick and easy to install. Once again, I will spare you all the "how to" part of this. It is documented on the website and different forums.

My NickelMenu config looks like this:

```
# NICKELMENU CONFIG

# Main Menu Items

menu_item : main : KOReader  : cmd_spawn : quiet : exec /mnt/onboard/.adds/koreader/koreader.sh
menu_item : main  :Unblock It :nickel_extras : unblock_it
menu_item : main  :Solitaire  :nickel_extras : solitaire
menu_item : main  :Sudoku     :nickel_extras : sudoku
menu_item : main  :Word Scramble :nickel_extras : word_scramble
menu_item : main  :Sketch Pad :nickel_extras : sketch_pad
menu_item : main: Screenshot : nickel_setting : toggle : screenshots

# Library Items
menu_item : library : Rescan Library : nickel_misc : rescan_books_full

# Reader Items
menu_item : reader : Screenshot : nickel_setting : toggle : screenshots
menu_item : reader : Dark Mode : nickel_setting : toggle : dark_mode
```

![NickelMenu](nm.webp "NickelMenu on Kobo")

With it, I am able to access or toggle things like...

**Solitaire**

![Solitaire on Kobo](solitaire.webp "Solitaire on Kobo: I have no clue why it is in colour")

**Sudoku**

![Sudoku on Kobo](sudoku.webp "Sudoku on Kobo")

A **Sketchpad**

![Sketchpad on Kobo](sketch.webp "Sketchpad on Kobo")

(it saves as an svg)

**Unblock It**

![Unblock It on Kobo](unblock.webp "Unblock It Game on Kobo")

A "Boggle" clone called **Word Scramble**

![Word Scramnle on Kobo](boggle.webp "Word Scramble on Kobo")

**Dark Mode**

![Kobo Dark Mode](dm.webp "Dark Mode on Kobo")

## Extending the e-reader with KOReader

[KOReader](https://github.com/koreader/koreader) is a document viewer for E-Ink devices. It runs like an app on your Kobo. Essentially, it shuts down the Kobo software and launches a different, alternative, operating system. Installation is as easy as it sounds here: [Installation on Kobo devices](https://github.com/koreader/koreader/wiki/Installation-on-Kobo-devices).

I have it installed, but don't use it for reading everything. I use it for reading PDF files, accessing Wikipedia articles, and reading articles saved via Wallabag. You can also configure it to work wirelessly with Calibre.

I recommend installing KOReader, even if you don't think you will ever use it, just to have a better way to read PDF files.

## The downsides of Kobo

According to [**novou**](https://www.mobileread.com/forums/member.php?u=134071) on the MobileRead forums, usage data from your Kobo is sent to Google Analytics. Not a single person would ever be surprised by that.

> They don't tie metrics to accounts and clear UID's and unsent callbacks across sessions (meaning they don't track devices across reboots.)
>
> But aiming to report each time you load the home-screen, or read a book for X minutes nonetheless seems like overkill to me. Their metrics are not super-invasive, and an extremely far cry from stuff like CarrierIQ, or individual menu button tracking.
>
> That said, for me at least, they still do cross the line into 'creepy,' especially since the callbacks are being sent to Google, where those stats will be saved and analysed as a part of Google's hoard of personal data. (If it were Kobo infrastructure, I'd still be concerned, but much less so. Kobo isn't a business built on harvesting personal data.)[^mr]

[^mr]: [Touch KT's Google Analytics integration, and how to disable it. - MobileRead Forums](https://www.mobileread.com/forums/showthread.php?t=162713)

You can disable Google Analytics by editing the `/etc/hosts` file on your Kobo.

The easiest way I have found to do that is to install KOReader, using the nice one-click method above, connect to your Wi-Fi, enable SSH, and just go change it by hand.

My `/etc/hosts` file:

```
127.0.0.1 host localhost.localdomain localhost localhost localhost.localdomain # default
127.0.0.1 www.google-analytics.com ssl.google-analytics.com google-analytics.com # GA loopback
```

The other thing about the Kobo is that it comes with several baked in features that I have come to dislike:

1. A system of badges that were once meant to be shared on social networks, like Facebook, but can no longer be shared. This is fun for about two minutes, but it is just cruft.
2. When searching the web from a highlight, it defaults to searching with Google, and you need to accept the cookie policy every time.
3. Pocket integration, which I did use for years, but which cannot be disabled. It didn't sync some articles, like comics, either.

## The state of e-books, e-readers, and e-book readers

Worldwide, the number of e-book readers is growing --- slowly, but surely --- but that growth will taper off in the coming years[^stat1]. In terms of revenue, the same growth can be measured[^stat2].

![Bar chart showing the number of e-book readers worldwide 2017-2026 (in millions)](worldwide.webp "Number of e-book readers worldwide 2017-2026 (in millions)")

![Bar chart of the e-book market revenue worldwide 2017-2026 (in million U.S. dollars)](revenue.webp "E-book market revenue worldwide 2017-2026 (in million U.S. dollars)")

In France, a country that is all about culture and supporting small independent libraries, Amazon has the majority of the market (63% as of May 2020, Kobo had 33%[^stat3]), e-books are not that popular. They have managed to get some traction with the 20 to 29 year-olds --- 43% of which had read an e-book at least once, but the majority of French people do not read e-books (Statista, 2017[^stat4]).

The future of e-books, though, is colour[^ink] and more functionalities:

![Example of colour e-ink screen](https://www.eink.com/uploads/files/Kaleido%20Plus%20(%E5%B7%A6)%E8%88%87Kaleido%203(%E5%8F%B3)%E6%AF%94%E8%BC%83.jpg "Rich colour on e-ink screen (E-Ink)")

* [PocketBook](https://pocketbook.ch/en-ch/catalog) is already selling colour devices.
* [Vivlio](https://www.vivlio.com/en/home/) makes e-readers with flexible software that can even play audio files.
* [Bookeen](https://bookeen.com/en) has a rather stylish, but chunky, e-reader as well as other e-ink devices for note-taking.

## Final words

E-readers are developing quickly. Almost quickly enough that an e-reader you buy today could be obsolete in two years. This is as good as it is bad. New features mean that consumers will feel the need to buy a new device. Hesitant consumers may buy a newer device, one of the colour e-readers, for example, and find themselves disappointed with the experience. We all know that e-waste is a growing problem, and this type of situation is one that could lead to even more waste.

As someone who enjoys reading e-books, I do want to see some development in the sector, but there is no way I will ever replace my e-reader every two years. An ideal e-reader for me would be something a little more flexible, in terms of configuration, or perhaps something more open and privacy-centric. I would purchase a "bare e-reader", if such a thing existed, so I could control what kind of software went on it.

Now, back to reading!

## Call to action

If you have made it this far, you should consider joining one of the [BookWyrm instances](https://joinbookwyrm.com/instances/). These are social networks for book lovers. If you know what Goodreads is, but don't want to use another service owned by Amazon, give BookWyrm a shot.

Also, add me to your contacts while you are there: [bbbhltz on BookWyrm](https://bookwyrm.social/user/bbbhltz).

[^stat1]: Statista. (February 3, 2022). Number of e-book readers worldwide 2017-2026 (in millions) [Graph]. In Statista. Retrieved April 11, 2022, from https://www-statista-com.library.ez.neoma-bs.fr/forecasts/1294239/number-of-ebook-users-global

[^stat2]: Statista. (March 4, 2022). E-book market revenue worldwide 2017-2026 (in million U.S. dollars) [Graph]. In Statista. Retrieved April 11, 2022, from https://www-statista-com.library.ez.neoma-bs.fr/forecasts/1294207/ebook-market-revenue-worldwide

[^stat4]: Statista. (juillet 1, 2017). Fréquence de lecture d'e-books par les consommateurs français en 2017, selon l'âge des répondants [Graphique]. In Statista. Retrieved avril 11, 2022, from https://fr-statista-com.library.ez.neoma-bs.fr/statistiques/761557/media-technologie-frequence-lecture-ebooks-france-selon-age/

[^stat3]: Foxintelligence. (June 16, 2020). E-reading market share of the most popular digital e-reader brands in France between March and May 2020 [Graph]. In Statista. Retrieved April 11, 2022, from https://www-statista-com.library.ez.neoma-bs.fr/statistics/1147874/online-market-share-of-e-readers-by-brand-france/

[^ink]: E Ink's New Color Electronic Paper Is Fast Enough for Video – https://gizmodo.com/e-inks-new-color-electronic-paper-is-fast-enough-for-vi-1848768109
