---
title: "Crosscall"
subtitle: "A write-up on a French phone-maker that I've ignored for too long"
description: "A positive introduction to the French phone-maker, Crosscall."
date: 2022-11-27T22:00:00+01:00
lastmod: 2024-04-13
draft: false
katex: false
featured_image: /blog/2022/11/crosscallprofile/devices.webp
tags:
  - Technology
  - Review
next: true
nomenu: false
---


{{< box info >}}
When I wrote this, I did not own any of the devices mentioned below. 

While searching for new phone I came across a brand that I have known about for some time, but never considered as I am not a target customer. I was, however, wowed by the presentation of their website and repairability promises. The more I read, the more I found myself convinced by this French brand. I am strongly considering becoming their next customer.
{{< /box >}}

![](LOGO.svg)

## Summary

**Crosscall** is an ethical mobile device manufacturer that targets segments of the population who need something more rugged than a regular black rectangle. This French company stands out from the crowd with its devices and commitments in different fields. There is strong competition in the sector globally, and the market for such devices is growing. **Crosscall** will soon be one of the few manufacturers making phones in France instead of China.

By sticking to their commitments, **Crosscall**'s customer base will likely grow. Innovation in their field should remain their constant, and more presence on social networks (even alternative ones) to share these innovations would be a plus.

## Introduction

Shopping for a new smartphone is only as hard as you make it. Looking closely at the details of each device, while ignoring their similar exteriors, is easy for consumers, but hard for shoppers. The difference being that consumers are generally well-informed, until they are confronted with an overwhelming number of similar products as a shopper. Smartphones are all very similar in appearance --- ubiquitous black rectangles --- with some exceptions: the somewhat recent arrival of foldable smartphones, and the "rugged" phones.

This is a brief report on the French company, **Crosscall**, and their smartphone offer (using their latest device, the Core-Z5, as an example). We will look at the company, their products, their competition, and overall presentation. That will be followed up with a conclusion and recommendations. The goal of this report is to underline that --- despite outward appearances, limited target market, and high prices --- **Crosscall**'s range of devices deserves consideration when shopping for a new smartphone.

## Details

**Crosscall** was founded in 2009 by [Cyril Vidal](https://www.linkedin.com/in/cyril-vidal-94712020/). Since then, they have designed [over 25 different mobile devices along with 20 different accessories](https://www.crosscall.com/en_FR/userguides/). They have (or have had) contracts with **Decathlon**, the **SNCF**, the **French Ministry of the Interior** and [many others](https://www.crosscall.com/en_FR/nos-references.html). One of their devices, the [Action-X5](https://www.crosscall.com/en_FR/action-x5-1001020701220.html), [won the **iF Design** award](https://ifdesign.com/en/winner-ranking/project/action-x5/350158) for telecommunications product in 2022. The current VP, David Eberlé, is the former president of Samsung Electronics France.

This company, one of the few French mobile phone brands (Kapsys, Wiko, Archos, Murena, and Thomson are a few others), sets itself apart in more than one way. The first being that they are [moving production from China to France in 2023](https://www.lefigaro.fr/societes/crosscall-va-produire-ses-premiers-smartphones-en-france-20221125), a unique endeavour.

> The brand had the choice between "doubling its workforce in Asia to ensure quality control or relocating the facility and strengthening the value chain." An industrial choice, but also a societal one. The company is following the trend of promoting projects aimed at restoring France's digital and industrial sovereignty.

The other unique features of the company are their promises and the very devices they make.

### Fundamental Uniqueness

It is so common for a company to make promises regarding the 3Ps (people, planet, and profit; the foundation of Corporate Social Responsibility) that we take it for granted. Most companies have a page on their website dedicated to this purpose. **Crosscall** does not differ here. Their promises do, however.

Manufacturers of any device tend to propose warranties. **Crosscall** one-ups this trend by offering a 5-year warranty and 10 years of replacement parts (on their most recent 5-series). This is an effort to fight against planned obsolescence they refer to as their "greatest promise:"

> As well as reflecting the durability of our products, this warranty could make a real difference on the telephony market, where the duration of manufacturers' warranties for the majority of phones is still only two years. At Crosscall we strive to design devices that last, so that we can move away from the current cycle of feverish smartphone replacement, which is much too fast and bad for the planet.

And, naturally, **Crosscall** guarantees 3 years of security patches and one major Android update.

Their other commitments are detailed in their CSR report ([download at the bottom of this page](https://www.crosscall.com/en_FR/engagements-rse.html)) where they talk about offsetting the environmental impact of production through a "second life" program (refurbished phones) and contributing to sustainable development goals by providing spare parts. They use packaging without plastic and plant-based inks. Sea-based transportation methods are used. They also train all employees in cybersecurity.

The fundamental uniqueness behind these buzzwords is the 5-year warranty, availability of spare parts, and the right to repair your own device. This is in contrast to other big brands that glue their phones shut. The most recent model, the Core-Z5, can be disassembled and repaired using a single Phillips-head screwdriver.

### Smartphones and Accessories

**Crosscall**'s range of consumer products --- which includes [smartphones](https://www.crosscall.com/en_FR/mobiles/smartphones/), [feature phones](https://www.crosscall.com/en_FR/mobiles/mobiles/), [tablets](https://www.crosscall.com/en_FR/mobiles/tablet/) and a growing collection of [accessories](https://www.crosscall.com/en_FR/accessories/) --- are all designed around "rising to the challenge" and durability, while "prioritising [their] users' needs". A clear visual identity is observed across the range. The naming scheme is consistent as well.

![Crosscall Devices](devices.webp "Selecton of Crosscall devices, clockwise from top left, Core-S4, Core-T5, Core-X5, Core-Z5, Action-X5, X-Vibes")

A universal sign of durability in 2022 is the [IP rating](IPRatings.jpg) and meeting the [MIL-STD-810 H](https://mil810.com/versions/mil-std-810-h/) military standards --- the eighth version of the test standard most commonly used to determine if a product can withstand the effects of difficult environmental conditions. All of **Crosscall**'s devices have an IP rating, and most of their mobile devices meet the military standards.

These ratings are important because of **Crosscall**'s target market: extreme sports enthusiasts, military, police, firefighters, paramedics, and anyone who works or plays in harsh conditions. Communicating with their target market is done mostly through ambassadors and events (*as far as I can tell*). A person needing this kind of device would likely go to them out of need for a rugged device. As such, finding reviews of their products is not as easy as finding a review for a recent model by more popular brands. **Crosscall**'s website is well-designed and up-to-date compared to some of their [competitors](#competitors) and their [YouTube channel](https://yewtu.be/channel/UCXr5t3EKnRyuXHCLe18yZ4g) is slowly filling with informative videos, interviews, unboxings, events, and the like.

**Crosscall**'s devices are built upon a magnesium chassis with an I-beam inspired shape, inside a polycarbonate and thermoplastic elastomer body. As can be expected, the screens are usually *Corning Gorilla Glass* that allow for glove- and wet-touch. The devices are tested in **Crosscall**'s own lab in an effort to confirm the device will last 5 years: e.g. 300 1.2-metre drops on concrete, heat, cold, immersion in water, scratches, etc.

Additionally, a feature of **Crosscall**'s devices is their compatible with a collection of accessories that leverage Magconn™ technology for recharging. They call it **X-LINK** and combine it with a clip called the **X-BLOCKER** to secure your device to an external battery, car or bicycle attachments, selfie-sticks, armbands, chest harnesses, and a docking station. This is an attractive and unique proposition in a world where purchasing accessories made by 3<sup>rd</sup> parties can (somehow) void warranties or damage your device.

### The Rugged Phones Market

**Crosscall** faces both direct and indirect competition. Direct competitors focus on rugged devices and target a similar segment of the population. American manufacturer **Caterpillar**, like **Crosscall**, targets logistics, health, agriculture, construction, security and production industries. China's **Ulefone** targets those same segments as well as the extreme sports enthusiasts that **Crosscall** also targets. There are additional direct competitors, like **Blackview**, that make rugged devices as well as non-rugged smartphones.

Both **Caterpillar** and **Ulefone** include <abbr title="Forward-looking infrared">FLIR</abbr> in their devices, making them more suited to certain industries than others.

| Phone | IP rating | Screen | Camera | Chipset | Battery | Price |
|:-----:|:---:|:------:|:------:|:-------:|:-------:|:---:|
| ![Photo of Crosscall Core Z5](CoreZ5_01.webp)<br>Crosscall Core Z5 | IP68 | 6.08" | 48 MP | Qualcomm QCM6490 | 4,950 mAh | €800 |
| ![Photo of CAT S53](CatS53.webp)<br>CAT S53 | IP68/IP69K | 6.5" | 48 MP | Qualcomm SM4350 Snapdragon 480 | 5,500 mAh | €530 |
| ![Photo of Ulefone Armor 18T](Ulefone18t.webp)<br>Ulefone Power Armor 18T | IP68/IP69K | 6.58" | 108 MP | MediaTek Dimensity 900 | 9,600 mAh | €640 |

**Crosscall**'s screen, battery, and camera are less impressive than the others above, but comes with a (slightly) newer SoC (the QCM6490 was announced 6 months later than the Snapdragon 480 --- 7 June 2021 and 4 January 2021, respectively; the Dimensity 900 was announced in May 2020).

The market for rugged phones is growing, so the list of competitors will likely grow as well. This is due to growth in the "oil & gas, aerospace, automotive, transportation, telecommunications, and logistics [industries, and] owing to the fact that rugged phones are water-resistant, dust-proof, and can operate against shock and extreme temperatures."[^1]

![Rugged Phones Market, Source: Research and Markets](rugged-phones-market.webp "Rugged Phone Market Growth, Source: Markets and Reseearch")

CNET reported in June 2021 that rugged phones have "gone from oddball to mainstream":

>"No matter how elaborate or expensive a phone you buy, the manufacturer generally assumes you'll take it upon yourself to immediately swaddle it in an after market case, so it can survive daily use. There's no other consumer electronic product that leaves the factory so not ready for the real world."

They also pointed out that it is not just the *ruggedness* and lack of need for a case that draw users to these devices, but the presence of other niche features like programmable buttons, thermal imaging cameras, dual SIM card trays, and replaceable batteries.[^2]

In addition to the companies mentioned above, other competitors in this sector are Kyocera, DOOGEE, OUKITEL, Juniper Systems, AGM Mobile, Sonim, Zebra, and Unitech Electronics.

### Price Range and Availability

The number of phones in the same price range as the Core-Z5 --- ~€800 --- is limitless. Many shops (*in France*) dedicate the most space on the shelves to two brands: Samsung and Xiaomi. These devices, due to their price, are indirect competitors. Some customers tend to buy with their wallets and may, when presented with the option, put their money on a more well-known device and name brand, like the [**Samsung Galaxy S21 FE 5G**](https://www.gsmarena.com/samsung_galaxy_s21_fe_5g-10954.php) and the [**Xiaomi 12T Pro**](https://www.gsmarena.com/xiaomi_12t_pro-11887.php), which can be purchased in-store more readily than **Crosscall** devices.

{{< box info >}}
Based solely on personal observation, it can be said that **Crosscall** devices are not easy to test hands-on. Even here in France, home of the brand, stores generally do not keep the devices on display. In my area, the main electronic shops do not have any models in stock.
{{< /box >}}

### Overall Presentation

Aside from a handful of English errors (typos really), the only thing negative that can be said about the **Crosscall** website is that it is *not* lite. Similar to other rugged phone manufacturers, **Crosscall** has opted to give as much information as possible, in the shiniest way possible. Scrolling down the product page for the [Core-Z5](https://www.crosscall.com/en_FR/core-z5-1001011601265.html) reveals animations and videos. The photos of the device are [large enough to make out the details](bigZ5.webp).

The entire website is designed to give you more than enough information to make your decision. The company comes off as proud, trustworthy, and transparent. With great detail, however, comes great resources. [Website Carbon Calculator](https://www.websitecarbon.com/website/crosscall-com/) rates their website as "dirtier than 65% of the web pages tested" but also states the website is running on sustainable energy. [Ecograder](https://ecograder.com/report/h4R373LsNKYGoE4PBGI3Tj3N) gives the site a 78/100 with some points of improvement.

## Conclusion

**Crosscall** is a serious company that will likely branch out into other sectors. Their consumer devices that come with guarantees and repairability promises make them excellent choices, and the company's focus on CSR and transparency does not go unnoticed.

While the style and design of the objects are not what the average smartphone customers are accustomed to, they are necessary in order to achieve durability and ratings, such as the IP ratings.

In conclusion, **Crosscall** is undeserving of being under the radar, but does have to keep on eye on their competitors who have different ratings and features, pricing, and communication strategies.

## Recommendations

**Crosscall** must maintain its visual identity. Changing would be a mistake. An optimistic recommendation would be to push into other sectors and create a less angular device, but other telephone manufacturers tried changing their style, and [we have not forgetten the BlackBerry Storm](https://bgr.com/general/worst-smartphone-ever-blackberry-storm/).

Without a doubt, **Crosscall** must continue making devices that go beyond the call of duty. Why have an IP68 rating when there are competitors with IP69 ratings? With each device, they must do something better than the competition as they have done in the past.

A small suggestion would be a price breakdown of a device on their blog. Not one of the recent devices, but perhaps one of the discontinued models. Give us the transparency provided throughout the website. What was the cost of the device? How much time went into design? How many prototypes before finding the right balance? These are things consumers need to know in order to accept paying the price for these devices. The prices are, however, acceptable.

Communication needs a small boost. **Crosscall** has ambassadors, and is present at events, but finding reviews online is not easy, nor is finding a store where the device is on display. YouTube has some video reviews, and there are written reviews on different sites, but consumers need to have more than Amazon reviews to rely upon. If **Crosscall** is opposed to *oversharing* on social media, perhaps considering an alternative social network, like Mastodon, would be an option. An area where less would certainly be more is the website: Perhaps a "lite" version is in order for 2023.

## Links

* [CROSSCALL Homepage](https://www.crosscall.com/en_FR/)
* [CROSSCALL on LinkedIn](https://www.linkedin.com/company/crosscall/)
* [CROSSCALL on Twitter](https://twitter.com/crosscallmobile)
* [CROSSCALL on YouTube](https://yewtu.be/channel/UCXr5t3EKnRyuXHCLe18yZ4g)
* [CROSSCALL on Facebook](https://www.facebook.com/crosscall/)

[^1]: [Worldwide Rugged Phones Industry to 2027 - Rising 5G Deployments to Complement the Growth are Driving the Market](https://www.prnewswire.com/news-releases/worldwide-rugged-phones-industry-to-2027---rising-5g-deployments-to-complement-the-growth-are-driving-the-market-301248461.html)
[^2]: [These are rugged times, here are the best rugged phones for them](https://www.cnet.com/tech/mobile/rugged-phones-used-to-be-clunky-bricks-now-theyre-the-cool-sleeper-choice/)
