---
title: "Coolpad Cool S"
subtitle: "A phone I did not buy"
description: "A blog post about the Murena One, which is just a rebranded ODM."
date: 2022-11-30T21:50:00+01:00
lastmod: 2025-02-20
featured_image: /blog/2022/11/murena/coool.webp
draft: false
tags:
    - Technology
next: true
nomenu: false
---

I am in the market for a new phone.

One of the phones I nearly purchased is the [Coolpad Cool S](https://web.archive.org/web/20240106013153/https://murena.com/shop/smartphones/brand-new/murena-one/).

Wait.

Sorry.

That link isn't right.

*This* is the [Coolpad Cool S](https://www.gsmarena.com/coolpad_cool_s-10685.php).

You *might* have noticed a striking similarity between these devices.

Look. See?

![Phone comparison](coool.webp "Are you seeing it?")

Moving on.

I did not buy that <strike>rectangle</strike> phone, nor did I buy the phone that is not that phone.

Why am I not saying the name of the phone that is not the Coolpad Cool S. Well, for one, they are not *exactly* the same. It looks like one has a bigger battery than the other. The other reason I don't want to explicitly type out the name of the company that makes the phone that is not the Coolpad Cool S is because they are trying to take things in the right direction. They really are. If it gets people thinking outside of the Android/iOS box, that cannot be a bad thing.

You might have also seen a pricing difference. I was uncertain, so I did the math:

\[
\begin{gather*}
\because \\ 350 - 160 = 190 \\ \therefore \\ 350 \gt 160
\end{gather*}
\]

Why pay more, right?

Moving on, *iterum*.

We are not all so naïve. I'm sure that many of you have seen a phone exactly like you own, but with a different logo on it. I have. It is abundantly clear that many <abbr title="Original Equiment Manufacturer">OEM</abbr>s sell us devices that they did not concoct in their secret labs. This is where <abbr title="Original Design Manufacturer">ODM</abbr>s come into play.

I only just learned about this in the last few hours. Clicking around the web, as one does at work, I came across an [old-ish post on Reddit](https://old.reddit.com/r/Android/comments/ky9xkh/rise_of_thirdparty_phone_designs_odms/) which linked to [an article on Nokiamob.net](https://web.archive.org/web/20230213204843/https://nokiamob.net/2021/01/17/large-number-of-modern-phones-including-nokia-are-designed-by-odm-companies/) which states that

> "in 2019, some 9% of Samsung phones were designed by ODM companies, 17% of Huawei devices, 49% of LG devices, and most importantly 54% of Nokia devices. In 2020, the percentage rose and now some 22% of Samsung devices, 18% of Huawei, 74% of Xiaomi, 89% of Lenovo, and 88% of Nokia phones are designed outside of the house. This means that the brand OEM is just tuning the details, but companies like Wingtech is doing all the rest."
>
> ![Table comparing OEM to ODM showing rise in use of ODMs](https://i0.wp.com/nokiamob.net/wp-content/uploads/2021/01/Nokia-Out-of-the-house-Design.jpg)

After that, I thought I would look up one of the phones I was considering. It was as easy as heading over to [GSMArena](https://www.gsmarena.com) and using their "Phone Finder" tool to look up devices with the same processor, a fingerprint scanner on the side, a 48MP camera, etc. and *poof*, there it was: a near identical match the phone I was looking into purchasing.

Well. It pays to look around and do your research. I do hope that the next phone from the company that does not sell the Coolpad Cool S is worth the price they ask for it.
