---
title: "Computer Know-how"
subtitle: "I ain't no slouch, but I have work to do."
description: "A blog post about students and computer literacy."
date: 2022-01-13T18:18:37+01:00
draft: false
tags:
  - Technology
  - Education
---

Discussions with my colleagues and students regarding computers and knowing how to use them are both similar and different. We have the colleagues on one side who complain, among other things, that the students are useless and lacking in creativity when put in front of a computer. On the opposing side, the students, who proudly claim on their CVs (which I have the honour of reviewing and correcting on occasion), that they are not just "proficient" with computers, but in some cases "expert" users of certain software.

Now, those same colleagues, who are teachers, mind you, consider themselves to be pretty decent computer users. You can probably imagine what my young students think of their teachers' computer skills. Spoiler: They are not impressed.

Recently, I had the pleasure of coming across an article, titled *Who is the target user?*, in which the author discusses the skills of the average computer user [^target]. He immediately brings up a table-top role-playing game and the skills one could acquire within the game regarding computers.

Below is the table (paraphrased):

**Skill Level Table**

>*This knowledge represents the ability to operate and program computers, as well as the savvy to keep up with the latest technology.*

| SKILL LEVEL | DESCRIPTION                     |
|-------------|---------------------------------|
| ●           | POINT AND CLICK                 |
| ● ●         | PROCESS DATA WITH RELATIVE EASE |
| ● ● ●       | DESIGN SOFTWARE                 |
| ● ● ● ●     | MAKE A LIVING AS A CONSULTANT   |
| ● ● ● ● ●   | BLEEDING EDGE                   |

I cannot claim to know what the students know, but I have a strong hunch based on years of watching students struggle through basic tasks, that (with the exception of my engineering students) they do not get beyond a two-dot score. On the other hand, I can make some claims about what my colleagues know. I know this because I asked them. It was fun because the results turned out much like we could have imagined.

Several years ago, one of my employers decided to play catch up and carry out a much-talked-about digital transformation. People were hired. VR headsets were bought. Good times. Things were changing, and teachers were tasked with participating in this change. So, we were divided into groups. One of the groups was tasked with the technology side of things.

It was put forward that training was needed. Training in what, though? "At least Office and a few other basics", said I.

Questionnaires were sent out. We asked about their knowledge of things on this list: "[The 20 Digital Skills Every 21st Century Teacher should Have](https://www.educatorstechnology.com/2012/06/33-digital-skills-every-21st-century.html)". They provided answers, claiming training was not necessary. They were OK!

That is exactly what was expected. Much like our students, we consider ourselves to be more proficient than we actually are. How are we to know better? So, more questionnaires were sent out. We asked them to describe their skills as *weak*, *intermediate*, or *very good*. Most ranked themselves as intermediate and very good with Word and PowerPoint, Excel had some weaker users.

We then asked specific questions about the software. Things like, "Do you know how to include a Table of Contents automatically?" (65% no) and "Do you know how to include a bibliography in a document?" (77% no). Excel, being the most difficult and least used, had mostly negative responses. PowerPoint got off easy.

The other questions highlighted the obvious. My colleagues were, for the most part, unable to create or edit audio or video files, and certainly had no idea how to share large files on the Internet. Screencasting was probably a mean question to ask too.

Almost 60% of my colleagues were still using DVDs in class!

I could go on. I will sum up the pages and pages of questionnaires and responses and analysis for you: **they all asked for training**.

They were (and might still be) somewhere between one and two dots on our skill assessment chart above.

My students, the next generation, the Zeds, are supposed to be "digital natives" (that term is one of my pet peeves, by the way), but, study after study proves that it means nothing:

>You've heard it before: Today's kids are “digital natives,” raised in a world of technology that they know like the back of their hand. As it turns out, that's not necessarily true about Generation Z (the demographic cohort following the millennials born in the mid- to late 1990s).\
>Results were recently released from the International Computer and Information Literacy 2018 study, and they were sobering: Only 2 percent of students scored at the highest levels implied by digital native status, and only another 19 percent of the 42,000 students assessed in 14 countries and educational systems could work independently with computers as information-gathering and management tools. [^natives]

The "[International Computer and Information Literacy 2018 study](https://nces.ed.gov/surveys/icils/icils2018/theme1.asp?tabontop)" mentioned above is a fascinating read.

Now, back to the dots...

## Those dots

I am maybe a two-dot user. I am [working on](https://codeberg.org/bbbhltz/learningpython) getting to three. I know this fact. I have only met two people in my life who are four- or five-dot users. They are scary when it comes to computers. So scary. Finding that table, with the dots, and the accompanying article felt so nice. But, it also affirmed an overwhelming idea: many of my students will not get beyond one-dot. They struggle with any software that doesn't give them a template to work with, they don't bother looking through menus to see what the software can do. They don't use their computers for more than typing and Netflix.

The nice part is what I want to focus on to wrap things up.

I have always liked computers. When I was younger it was the buttons and screen combination. Press a button, see a result. Just typing out things that didn't need to be typed out was pleasing when I was young. Too bad I had to wait until I was 15 or 16 to have a computer in my house! I didn't imagine I would ever bother with programming after university, but, as I mentioned above, it is something I am working on.

What if I get to three-dots?

Well, that's the neat part. **There are still more dots to get**.

[^target]: See Nate. "Who Is the Target User?" Adventures in Linux and KDE. November 30, 2021. [https://pointieststick.com/2021/11/29/who-is-the-target-user/](https://pointieststick.com/2021/11/29/who-is-the-target-user/).
[^natives]: See Strauss, V. "Today's kids might be digital natives — but a new study shows they aren't close to being computer literate" The Washington Post. 19 November, 2018. [https://www.washingtonpost.com/education/2019/11/16/todays-kids-may-be-digital-natives-new-study-shows-they-arent-close-being-computer-literate/](https://www.washingtonpost.com/education/2019/11/16/todays-kids-may-be-digital-natives-new-study-shows-they-arent-close-being-computer-literate/)
