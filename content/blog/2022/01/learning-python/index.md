---
title: "A Month of Python"
subtitle: "Learning something new, a handful of hours each week"
description: "A blog post where the author talks about how fun it is to learn the Python programming language."
slug: learning-python
date: 2022-01-22T22:48:14+01:00
lastmod: 2024-10-13
draft: false
tags:
  - Technology
  - Education
  - Programming
next: true
nomenu: false
---

*A month ago today, I began pondering what to do with my time off. My schedule from now until the end of the school year has provided me with a day off. In contrast to previous years, when I would have gone out and found another employer, I wanted to learn something.*

## Teaching the Teacher

My employer provides us with training. We just have to ask for it and convince our manager, and it is approved (most of the time, if it isn't too ridiculous). Personally, I have nothing against learning something new. I was "school smart", and I really enjoyed my studies. On more than one occasion, I have signed up for training. Three times, in fact. Three times I took the same course: Spanish.

The first year started out fine, and I felt some progress, until my schedule changed, and I was unable to continue attending the class. The next year, the same thing. The following year, I took a Spanish course online. It was a disaster. A total joke. Many language courses start off with a test to determine your level, and very good online training software uses computerized adaptive testing to continuously evaluate the learner. The online course I took did not have this option, and the initial test was a multiple choice test about colours and numbers. It was determined that I have an excellent level of Spanish, which is not the case. I completed the 20 hours. It was confusing and difficult, and I learned nothing that I can use in daily conversation.

Learning Spanish is on the back burner for now.

I dove deep into the available options. I had several options:

* Take a course provided by my employer
* Take a course available through the "[Compte personnel de formation](https://fr.wikipedia.org/wiki/Compte_personnel_de_formation)" (a French financing scheme for training)
* Take a course and pay for it myself
* Learn by myself

Taking courses through my employer is an exceptional idea, but I missed a deadline. I began preparing a shortlist on my CPF. There are too many courses to even think about, but I managed to find quite a few that met my criteria (hours, cost). I searched for courses that were not available through the first two options, but found it hard to determine which training courses were reputable, worth my money, or total scams.

I hesitated even thinking about learning my myself. As mentioned above, I was "school smart", but the context is different now. I am not a student, I don't have deadlines or homework. Would I even bother? Hard to say.

Then, while doing my research and preparing my lists and weighing my choices, something dawned on me. I liked being a student, and I need that regular (weekly) dose of learning to maintain a sense of progress. I know from my job that it takes more than an hour or two per week to progress. Finally, learning something new by myself is not that crazy. It is what teachers and professors do regularly when given a new subject to teach.

So, that is what I have been doing the past three weeks.

I am trying to learn Python.

How? By preparing a lesson plan, creating my own exercises (so I feel less inclined to give up and just go look at the answers), and [presenting it as a course that I would teach](https://codeberg.org/bbbhltz/learningpython).

Yes, I am aware that the *beginner teaching beginner* thing is very cliché. I cannot argue with results, though.

## A Month of Python

![Screenshot: https://codeberg.org/bbbhltz/learningpython](LearningPython.webp "Screenshot of https://codeberg.org/bbbhltz/learningpython (now archived")

These past few weeks have been very enjoyable. I have not enjoyed sitting in front of a screen this much since I first installed Linux. I have been making slow, but steady, progress.

The last time I tried programming was over 15 years ago at university. It was Java. This is a summary of how it went:

* Excellent in-class experience with great professors
* Great labs with good projects and fun TAs
* Challenging, but not impossible, homework assignments
* **Exams that made me think I sat down at the wrong table!**

I still remember leaving an exam, going back to my dorm room, and looking through my notes. The last lesson was easy. So, *how* did we get from "Make an input box that asks for your birthday and prints your age" to "Make Tetris"? A mystery for sure, and a very low grade. Second semester, it was the same. The exam was far more difficult than the class, but I was ready. I did not produce anything close to a program. I just wrote out everything we had learned in class, and made sure to comment it to prove I knew what I was doing, but didn't know how to get all those things to work together. I got a B+ and decided that programming wasn't for me.

Clearly, I have been putting this off for too long --- both learning Python, and publicly keeping track of that learning.

## Lesson Building

A favourite class when I was a student was learning about teaching. There was a lot of theory, which is what one would expect. At the time there was a lot of talk about online learning (this was around 2002), and the approach to online learning was meant to be the same as what was applied in face-to-face learning (i.e. instructional design). There are variations on these methods. The one I always found easiest to apply was **ADDIE** model.

ADDIE --- analyse, design, develop, implement, evaluate --- dictates that I (paraphrasing here) look at my needs first. Well, that is easy. Close to zero programming know-how, so, I start at the beginning. Then, I would need to design and develop. So, I looked at a rather nice collection of books, comparing their table of contents and proposed learning paths to see a common thread. I also looked at what MOOCs recommended, along with the previously mentioned courses provided by my employer and other services. 

I didn't follow the model to the letter here. I did not sit down like I would when preparing my real lessons. I did no storyboarding. I prepared no definitive bibliography. A lot is based on the work of others.

Currently, I am in the implementation phase: doing the actual work. I have had to backtrack a few times due to unfamiliar concepts that required me to review fundamentals.

My personal learning path can be broken down as follows:

* Pre-requisites (not actual Python, but computer science notions that I stop and look into)
* Fundamentals (basics, but the easiest of basics)
* Basics (where things become more challenging, still in progress)
* Intermediate
* Advanced

While I know what I will be doing in the latter sections, I don't know how I will feel about it. I do need to go back and review the previous sections and think of different exercises to test myself.

## Early Success

Without even getting beyond the beginner chapters, there are already a few things of which I am particularly proud. They are not beautiful, colourful, or elegant. They work, and they made me think.

### The Witch (the what?!)

If you have ever played the [Witch Drinking Game](https://www.barnonedrinks.com/games/w/witch-410.html), you know what this is about. If you haven't, it doesn't matter. The game is not as hard as it appears.

What I enjoy about this game is not the drinking. It is an example that I use in class with my students. I have them do this game as an icebreaker. What happens when I describe it is confusion. My students are not native speakers, so they initially think they misunderstood something. Then, when they play, it all makes sense.

Learning Python is analogous to this, for me at least. I read, I try, I doubt my understanding, I try again, and things turn out fine.

So, [I gave myself a challenge](https://codeberg.org/bbbhltz/learningpython/src/branch/main/content/2.2-Basics2.md#practice-13). It is just a script that generates a dialogue, and I found a solution that works. It is very motivating. Initially, I did not think I understood, or would be able to do it. Then it started working. At the end, I smiled and laughed. Much like my students. A tiny success; an immeasurable win.

## Month Two

As I continue learning, I continue having new ideas. And, accordingly, the idea that I have requires what I planned on working on next. I like the challenge, and what it highlights: despite being relatively good with computers, there are notions that are foreign to me.

The goal of learning this is coming into view. If I continue down this path, I will be able to put this tool to work at my job and automate a few tasks. Alternatively, I can show what I have learned to my managers, and they may allow me to develop a course that I can teach. Who knows? Personally, and professionally, I can see how learning one of the most popular programming languages will help me in the future.
