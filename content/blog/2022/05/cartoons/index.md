---
title: "Children's Cartoons"
subtitle: "grab a bowl of cereal, it's time to talk toons"
description: "Some of my favourite cartoons for children including Hey Duggee, Stillwater, Bluey, and Tumble Leaf."
date: 2022-05-25T11:00:00+02:00
lastmod: 2024-10-27
featured_image: /blog/2022/02/cartoons/rocky.webp
draft: false
katex: false
tags:
    - Cartoons
    - Parenting
next: true
nomenu: false
toc: true
---

The 1980s were a wonderful time for children's cartoons. No questions were asked, it seems. It gave birth to things that today would have no chance of survival against television censors and parents[^capt]. Sure, there were rules about violence, but they were fundamentally long-form toy ads, and some of them were great.

My childhood was *Transformers*, *GI Joe*, *ThunderCats*, *SilverHawks*, *M.A.S.K*, *The Masters of the Universe*, and the like. Most of the time, the premise was straightforward: Good versus Evil. And nearly all of them had kick-ass opening credits, that were often *very* similar...

{{< youtube id=HcGNqrAtsgg title="ThunderCats Opening Credits" >}}

Interesting, no? Let's try again...

{{< youtube id=EqsgLxuMhLE title="SilverHawks Opening Credits" >}}

OK, *amazingly* similar. And these cartoons were produced *after* the toys[^rudy].

So, as a fan of cartoons (and toys) I was delighted and filled with joy by the idea of watching cartoons with my son. Now, I know, TV is bad for children. No screens before three years old and all that. I read the studies and I promised myself I would try not to overload him with TV.

I failed...

I knew, though, that things would not be like in the 80s and 90s. The shows for younger children are not like before. There is a little less action, things are a little less scary, the vocabulary is *subdued*.

I did not know I would be up against literal crack for kids...

## PAW Patrol

***Guru Studio & Spin Master Entertainment***

PAW Patrol should need no introduction. It is a TV show designed to sell merchandise, there is no doubt about that. It took some great ideas from the past, and diluted them into an addictive slurry for young minds.

Don't get me wrong, I let my son watch it and I have seen every single episode, special, and film. I can say, based on my hundreds of hours of watching TV since the 1980s, that this show is a drug for kids.

Before we get into this, I recommend watching [The Science of Cute](https://yewtu.be/watch?v=wybGz0ykjSk). Skip to around the 9 minute 39 second mark.

This mini-documentary is about why we find puppies cute. In the video, Julie Hecht says:

> Cuteness is incredibly important for us as a human species [...]. Konrad Loranz --- one of the ethologists, people that study animal behaviour --- introduced the idea that there might be certain physical features that are particularly cute and endearing that we are attracted to.
>
> They would have a high and protruding forehead, very big eyes, [and] big cheeks. [They would have] a little nose, and a little mouth. And they have short, pudgy, extremities.
>
> [A]ll of these features would essentially elicit our attraction [...]

Shortly after this explanation, an artist draws the definition of *cute*. It is a puppy that is similar to the pups in PAW Patrol:

![Screenshot from "The Science of Cute": An artists uses digital painting software to sketch a puppy with big eyes](puppy.webp 'Screenshot from "The Science of Cute"')

![Rocky, from PAW Patrol. The grey mixed-breed recycling pup](rocky.webp "Rocky the recycling pup")

PAW Patrol takes one of the animals that we find the cutest, puppies, and creates exaggeratedly cute versions of them as heroes. Even the humans follow the description given above. The whole concept is excellent. And they can't be blamed for wanting profit, but they can be blamed for cutting corners!

The CGI animation used in this show is awfully basic. Over the course of nine seasons, not a whisker has been changed. As little detail as possible is used. Yes, they have added *new characters*, and *new vehicles*. Yes, just in case you didn't know, the PAW Patrol have the OG vehicles, dog houses that transform into themed vehicles, as well as a slew of others: sea patrol vehicles, city vehicles, race cars, motorcycles, planes, knights' chariots, dinosaur themed vehicles, superhero vehicles, and "ultimate rescue" vehicles. You need to have something new to sell, right? But, they have never updated the quality of the animation.

The content of the show, on the surface, seems fine. The pups rescue people in danger and teach kids to be careful. There are attempts at inclusivity as well --- one of the pups has physical handicap that doesn't prevent him from being a hero, another is supposed to be hyperactive, one of them speaks Spanish, and there is even a cat who occasionally helps out. But, if you look at something like *Blue's Clues* and then watch this, you will immediately notice that the pacing is off:

* A problem occurs in town, so The PAW Patrol is called. Sure, there is an entire sequence involved with pups getting HQ to receive their orders, but *there is no invitation to the audience to guess which pups will be called upon to solve this problem*. This sequence is very similar to the one used in M.A.S.K., so I must ask you watch the opening credits.

{{< youtube id=o2Z1yLO9C-Q title="M.A.S.K. Opening Credits" >}}

Want further comparison? I have been thinking about this for years now, so you will have to bear with me. Both shows have:

* transforming vehicles
* themed heroes (e.g. water specialist, flying, etc.)
* specialized equipment (helmets in M.A.S.K., backpacks ("Pup-Packs") in PAW Patrol)
* a villain with a moustache who also has access to innovative technology
    * no explanation as to where said technology comes from
* a robot
* occasional non-hero characters that help out
* a "get to HQ" sequence
* a "get your costumes on" sequence
* catchy opening credits
* an attempt at providing a moral for younger children

In the end, PAW Patrol is essentially very cheap eye-candy, but, you might think that there is a moral of some sort? Nope. No moral. The major protagonist, Mayor Humdinger (voiced by the wonderful [Ron Pardo](https://en.wikipedia.org/wiki/Ron_Pardo)) is never punished for his wrongdoings. The [pirate](https://pawpatrol.fandom.com/wiki/Sid_Swashbuckle_the_Pirate) who steals stuff always gets away. The villains clean up their messes, and they go home.

Also, the adults in the show are awful. The safety of a town, the nearby city, the [jungle](https://pawpatrol.fandom.com/wiki/The_jungle), the Kingdom of [Barkingburg](https://pawpatrol.fandom.com/wiki/Barkingburg), the oceans and the skies is left in the hands of a 10-year-old boy and some puppies. The adults are clumsy and inept, incapable of taking care of a [chicken](https://pawpatrol.fandom.com/wiki/Chickaletta) in some instances. The only ones with any sense are the children and the pups. What message is this for children?

Finally, it is confusing. Really confusing. Children like stories, but this show has no story really. There are no parents in the main town, but there is a child, [Alex](https://pawpatrol.fandom.com/wiki/Alex_Porter), who lives with his grandfather. There are two other kids that come visit their aunt. The main character is 10 years old, but never says anything about his parents. Where did these people come from?

PAW Patrol is not the only show like this. Off the top of my head I can say that PJ Masks is on the same level. Cheap animation, no morals, designed to sell toys.

Speaking of toys, PAW Patrol managed to botch this too. Whether it is by design or some other reason, it is quite difficult to find toys of the human characters. They *were* produced, but have since been discontinued. Your kid can be addicted to the show, and get a truckload of toys, and imagine their hearts away, but, they will have nobody to save and nobody to save them from --- they will have multiple versions of the pups in their different vehicles, and maybe a dragon.

{{< box info >}}

I feel it important to note that I do not *hate* this show. I just think it can be better. The PAW Patrol movie shows that: better animation style, a story that provides some background for a main character, a lesson for children on overcoming fears, and so on. Ryder, though, is freakin' creepy looking in the movie:

![Poster for PAW Patrol: The Movie - logo in the centre, vehicles underneath, characters above, Ryder centre](https://image.tmdb.org/t/p/original/ic0intvXZSfBlYPIvWXpU1ivUCO.jpg "PAW Patrol: The Movie Poster")

And, I am looking forward to the movie sequel *and* the spin-off series in the works. I can't help it, I am hooked now too.

{{< /box >}}

## Two Can Play

It has not been as easy as it should have been to find TV shows that my son likes *and* that I would call *good*. Shows that have an educational side, a funny side, or an emotional side that demonstrate an effort from the creators to make something *good*.

I want to talk about five of these shows. You will notice that some of these shows have taken advantage of the "big head, big eyes, small body" science mentioned earlier. The difference is that these shows go the extra mile. Sometimes in terms of content and message, sometimes in the quality of the presentation, sometimes both.

### Tumble Leaf

***Amazon Studios***

The first big hit in our household, before PAW Patrol, was *Tumble Leaf*. This show was beautiful to watch, and the creators were nice enough to give it an emotional finale. The stories were perfect for kids and follow the same premise: Fig the fox finds an object and must use that object to have fun. This is what kids do. A mirror can be a play thing. A drumstick can be more than a drumstick. A tool can be used more than one way. The overall message: go play!

{{< youtube id=9Bo3UFSmvcs title="Tumble Leaf Teaser" >}}

Furthermore, the show is calm. It is soft, and quiet, and subtle. There are no screeching characters, or overly anxiety-inducing scenes. While this is not the case of my son, I am aware that some kids are sensitive to sounds and action scenes. This is a show that you could let your children watch unattended, but you won't, because it is too gorgeous. You will also be happy to know that there is no merchandise available. That means you will not be buying toys or stuffed animals or lunch boxes or anything like that to please you children.

{{< youtube p6IxAakLyzM "Tumble Leaf Making of Featurette" >}}

I did play my cards wrong. *Tumble Leaf* is a great show for children of all ages, but I should have started with something like *Daniel Tiger's Neighborhood* that is explicitly for very young children. I do occasionally propose an episode of *Tumble Leaf*

### Daniel Tiger's Neighborhood

***Fred Rogers Productions, 9 Story Media Group, 9 Story USA***

*Daniel Tiger* is the continuation of *Mr. Rogers' Neighborhood*. That is all you need to know. Appearance and content-wise, this is the most childish of the shows in this list. It is for 3- and 4-year-old children, after all.

> The series centers around Daniel Tiger [...]. Two 11-minute segments are linked by a common socio-emotional theme, such as disappointment and sadness or anger and thankfulness. The theme also uses a musical motif phrase, which the show calls "strategy songs," to reinforce the theme and help children remember the life lessons. Many of the "strategy songs" are available in albums or as singles under the artist name "Daniel Tiger's Neighborhood." The program is targeted at preschool-aged children; it teaches emotional intelligence, kindness, and human respect. Its content follows a curriculum based on Fred Rogers' teaching and new research into child development. [^dt]

In short, this is a safe show for children.

### Hey Duggee

***Studio AKA***

This one is just as entertaining for parents as it is for children. It is silly and loud, but educational. It is a show that gets the audience involved through the use of a narrator, and teaches things in the same way that something like *Sesame Street* does.

The concept is that question kids ask all the time, *what are we going to do today?* And while doing that thing, we learn. We learn about tadpoles, elections, trains, kites, building, shapes, singing, whistling, water, being sick, being quiet, and, yes, [sticks](https://youtube.com/watch?v=K05N2jqFHc8).

The show is about a group of children at day camp. Their camp counsellor is Duggee, a dog, who always has ideas and seems to know about everything. The group of children is loud and colourful: There is a sensitive rhino named Tag; Norrie, a quiet and calm mouse; Betty, a know-it-all octopus; Happy, an alligator obsessed with water; and a loud, hyperactive, disruptive, but yoga-practicing hippo named Rolly. Of course, there are a series of other characters that they interact with.

Adults will have no problem watching this show with their children, because there is always a little joke in there for them. Keep in mind, though, that some of these jokes do require a little knowledge of British culture --- note the reference to [Spaghetti Trees](https://en.wikipedia.org/wiki/Spaghetti-tree_hoax) in this episode:

{{< youtube CbQuAbGWBBg "Hey Duggee - Gardening" >}}

### Stillwater

***Gaumont Animation & Scholastic Entertainment***

Calm and Zen; soft-spoken, but powerful. *Stillwater* is visually beautiful to watch, and filled with detail that children's shows normally ignore. As an example, the characters in this show have different outfits in nearly every episode. They have detailed expressions, and flowing hair, and fur. The environment is rich and there are different locations and weather events. Finally, each episode has a story within a story using a different animation style to present an anecdote in response to a problem.

Stillwater is a panda, he lives next door to a family with three children who, as humans do, encounter difficulties. Jealousy, dismay about the outcome of a race, growing up, getting a haircut, and so on. The anecdotes used to help the children to find solutions to their problems are retellings of Zen Buddhist and Taoist stories, mostly, and use lots of metaphors and analogies.

{{< youtube zz1GkcvkT1g "Stillwater Trailer" >}}

This series is based on a book, which is a good thing. It means that behind it there is an author, an [IP](https://en.wikipedia.org/wiki/Intellectual_property), and a vision maintaining the course of the production. It isn't something for your children to watch religiously when a new episode appears --- there are only 20 episodes containing two stories each. This is something that you can use like a story book for children, and say, "hey, how about a Stillwater?" And, like when you read a story to your child, there is a certain memorability to each episode and story that does stick.

### Bluey

***Ludo Studio***

*Bluey* is pure, unfiltered, entertainment. It is a lesson for children and their parents. It is the source of roaring laughter in my house. It is all about play and creativity. In my opinion, Bluey represents the standard in children's television programming. It is the ideal show to transition from the more "preschool"-style program --- think *Daniel Tiger*, or shows that have a narrator like *Hey Duggee* --- to something that reflects the psychological development that children experience at this time in their lives.

As we grow, many of us use [private speech](https://en.wikipedia.org/wiki/Private_speech) during play. You will hear children muttering and telling stories with their toys. They *narrate* the events, and ask themselves questions out loud. At some point, we grow out of this and begin using [inner speech](https://en.wikipedia.org/wiki/Intrapersonal_communication). There are many scenes where we see the main characters of the show reflecting on what happened, but no narrator is there to help, and the character does not look at [fourth wall](https://en.wikipedia.org/wiki/Fourth_wall) and ask for guidance.

{{< youtube id=W3GpdLhgHVA title="Bluey: Hotel" >}}

*Bluey* stands out in 2022. It has a very Saturday morning aesthetic, whereas many shows have taken a more 3D-CGI route. The characters have lives and jobs and backgrounds: Bluey is in a bit of an alternative school, she has a little sister named Bingo, a mother who works part-time, Chilli, and a father, Bandit, an archaeologist. There are aunts and uncles, babysitters, neighbours, teachers, friends, and even an episode dedicated to one of those friends. The production team spends about three months on each episode, after, the whole Ludo team gets together on a Friday to view the almost-finished product with friends and family, and their children [^abc]. The children in the show also voiced by the children of the team. Finally, there is another minor twist.

It goes without saying that many shows like to focus on the mother as the main caregiver in a family, and the father as the breadwinner. Doing the opposite was often seen as a gimmick, some little twist --- it is a show about a family, but *different*. *Bluey* does not feel like that at all. You hardly register the fact that in many episodes it is Bandit getting wrapped up in his daughters' strange games, trying to keep them entertained. It is this aspect that draws parents in, at least in my case. There is a realness to Bandit, and the rest of the characters, that when seen through the eyes of an adult does not register as "this is a silly kids' show", but rather as "holy crap this is brilliant."

**UPDATE 2024-10-27**

The podcast, [Twenty Thousand Hertz](https://www.20k.org), has produced two *azmazing* episodes about this lovely show talking about the sounds and the voices of *Blue*.

Have a listen:

* [The Sound of... Bluey!!!](https://www.20k.org/episodes/thesoundofbluey)
* [The Voices of... Bluey!!!](https://www.20k.org/episodes/thevoicesofbluey)

## Conclusion

It won't come as a surprise to learn that I do feel some regret. My son has seen too many episodes of these shows. Time that could have been spent outside, playing, or being creative and actively learning was wasted on passive entertainment. Ideally, I would have known about these shows before my son was born, and therefore avoided the dreaded path of the PAW Patrol. I also would have changed the order: *Daniel Tiger* first, then *Hey Duggee*, followed by *Stillwater*, *Tumble Leaf*, and finally *Bluey*.

If you are a cartoon lover, or parent, I recommend that you watch these shows --- even PAW Patrol, because it is important to be ready when your son or daughter comes home from school and asks why they have never seen that show. I also recommend that you try and see TV shows from your own childhood.

(I say this because it is amazing how things have changed. An innocuous little cartoon from our childhood, when seen through adult eyes, can be traumatic. [The Animals of Farthing Wood](https://en.wikipedia.org/wiki/The_Animals_of_Farthing_Wood_(TV_series)) is quite heavy, and just look at the synopsis for the first episode of *Capital Critters*[^critters] which my mother let me watch: *"After Max the mouse's family is murdered by pest control workers, he moves to Washington, D.C. to live with his cousin Berkley."*)

Finally, always check the merchandise situation of a show before letting your children get hooked. PAW Patrol is a vector for toy sales, whereas the other programs mentioned above are not.

[^capt]: Read about the "Adult Story Line" and cancellation of *[Captain Power and the Soldiers of the Future](https://en.wikipedia.org/wiki/Captain_Power_and_the_Soldiers_of_the_Future#Adult_storyline)*
[^rudy]: It may surprise you to know that the two examples above, *ThunderCats* and *SilverHawks*, were produced by [Rankin/Bass Animated Entertainment](https://en.wikipedia.org/wiki/Rankin/Bass_Animated_Entertainment) who were previously known for their stop-motion Christmas specials, like [Rudolph the Red-Nosed Reindeer](https://yewtu.be/watch?v=VaPrGWlfvd4).
[^dt]: [Daniel Tiger's Neighborhood - Wikipedia](https://en.wikipedia.org/wiki/Daniel_Tiger%27s_Neighborhood#Production)
[^abc]: [I took my daughter to see how they make Bluey at Ludo Studios in Brisbane and it sort of blew her mind - ABC News (Australian Broadcasting Corporation)](https://www.abc.net.au/news/2019-12-02/i-took-my-toddler-to-see-where-bluey-is-made-ludo-studio/11742386?nw=0&r=HtmlFragment)
[^critters]: [Capitol Critters - Wikipedia](https://en.wikipedia.org/wiki/Capitol_Critters#Episodes)
