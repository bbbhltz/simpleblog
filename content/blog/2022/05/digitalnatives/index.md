---
title: "In Search of Digital Natives"
slug: digital-natives
subtitle: "Where are you all hiding?"
description: "A post about digital natives and how, despite having the world in their pocket, the younger generation is less proficient than the previous."
date: 2022-05-07
draft: false
katex: false
tags:
  - Technology
  - Education
---

Promises were made, people. I'm not talking about flying cars, I'm talking about the people that were supposed to make those cars and the cool things to go along with them. We were promised a generation of *digital natives*. Where are they?

If you're reading this, there is a good chance you are one of those digital natives we were promised. You are everywhere. There are throngs of you across the globe. I have met many of you, and the things you can do! You programme and solder; you make robots; your front-ends are gorgeous, your back-ends robust; you are makers and tinkerers. But, we were told there would be an entire cohort of digital natives.

The term was coined back in the 90s, but I have been teaching since the early 2000s and every year I hear students give the same speech. They say, "my generation is the first generation to grow up with the internet, we are digital natives." Millennials, like myself, have also been saying the same thing.

We experienced the joys of 300bps modems, dial-up, and broadband. The arrival of the World Wide Web, Amazon, eBay, and the first dating sites. There was ICQ, MSN, AOL, GeoCities, Rotten.com and Warez. We had floppies and diskettes and burned CDs. We had monochrome CRT monitors. We installed games that were distributed on multiple diskettes. The sneakernet was a thing! We had laptops that didn't have Wi-Fi. We were friends with Tom. We used websites like AltaVista and Alt.box.sk.

But, all along, were told that the next generation would be the real deal. Their level of digital literacy would dwarf ours. Everyone and their dog would be the next Zuckerberg. I must say that I am underwhelmed.

Again, you will tell me they are everywhere. And, I will agree with you and nod my head, but follow up with a question: **How is it that so many are so digitally illiterate?**

Was it social media? It is definitely one of the factors. Was it because Google and Wikipedia took away their curiosity and willingness to learn? Could be. Did the smartphones make things so accessible within three clicks that they all decided they were hackers? I cannot say, and as this is meant to be a short blog post, some stream of consciousness, I am not going to stop and do the research to answer those questions.

With each year that passes, my students become less and less adept when it comes to computers and technology. Obviously, where I work, I cannot expect to run into the prodigies that are self-caught programmers, but hell, would it kill them to learn a thing or two?

My average student keeps all of their files on the desktop of their MacBook. They have at least three browsers installed, an upgrade warning, and 97 tabs open. I ask them to go to their webmail and watch in disbelief as they proceed:

1. Open browser
2. New tab
3. Google search the school website
4. Login
5. Navigate to the link for outlook.office365.com
6. Login again
7. Scroll through screens if unread emails instead of searching
8. Open email
9. Figure out how to download the attachment
10. Open the attachment from the browser

Meanwhile, noises. The sounds of overlapping Facebook notifications. Because they have Facebook open in multiple tabs. Their €1,400 MacBook is revving up. It sounds like I'm standing under a wind turbine, but in reality I am in a classroom. It locks up. They look at me with one of those, "whatcha gonna do?" expressions.

Another students steps in, but is also unable to present the work. Why? Because somehow, after three years of school, they do not have an adapter for their computer. Does someone have an adapter? Of course someone does. We are saved, but then comes a new horror.

This student opens Facebook and asks the first student to send her the file through that platform. So, student A gets out their iPhone, downloads the attachment, and sends it over Facebook. Lo and behold, we have a PowerPoint... but not.

Because, unbeknownst to us, these students made their presentation with Canva and downloaded the PDF, which, clearly, would not contain videos and animations, right? A look of shock and panic, "Sir, can we try a different computer?"

Up comes teammate #3, "The Maker." Their computer has HDMI. Battery levels are optimal. They connect. Now six hands are jamming away to get the screen to stop extending in order to mirror the content. Success. Angels weep for my soul as I die a slow and agonizing death --- on the inside. There is no sound. There was no need for animations. They read, word-for-word, the text from their presentation.

At the end, "The Maker" holds down the power button, forcing a hard shutdown.

This is not a one-time thing. This is not bi-weekly. This is multiple times per class per week per semester. The digital natives I work with think PowerPoint is too hard. They don't know how to turn on spellcheck or put page numbers on their documents. They copy and paste and submit Wikipedia articles as their own, *but leave in the hyperlinks and footnotes!* They use their phones to take pictures of their computer screens. And, on their CVs, they proudly proclaim:

* **Computer Skills: *Proficient* user, *Expert* in Microsoft Office, Social Networks, Windows, and Mac.**

I digress.

Digital natives, what went wrong? You got off on the right foot: Powerful pocket-sized computers, high speed internet, streaming services, the cloud, laptops with batteries that last longer than two hours, but you can write a report faster with two thumbs on your 6-inch slab of plastic and glass than you can with 10 fingers and 105 keys. They mock you and call you "zoomers," but many of you cannot seem to activate your microphones and cameras in order to use Zoom. I ask you to send me a file, you say it is too big for email, so you send it with WeTransfer, and it isn't even 4 MB.

Natives and immigrants of the digital age, you know in your hearts that I speak the truth. So, tell me, please, how can we bridge the gap? How can we teach you the skills you need? How can it be made interesting for your hyper-connected generation to learn the basics?
