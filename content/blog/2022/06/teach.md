---
title: "Teach(p)ing"
subtitle: "sort of a poem"
description: "A poem about teaching and technology."
date: 2022-06-13T12:24:07+02:00
# lastmod:
draft: false
katex: false
tags:
  - Poetry
  - Education
---

Another notification, \
What could it be? \
Click this, open that, \
Why was this sent to me?

An email from my manager \
To say I need to check Teams. \
Let's install another app \
And choose from one of six themes.

Come navigate this list of groups \
And help me find my way. \
It's not like I have something to do, \
I only have to teach today.

There. We found it. \
Ah, a file to download. \
Fill out this spreadsheet, \
Log into to Moodle and upload.

Now, time to go to work. \
Teaching is what I like best, \
Not Yammer this and Doodle that, \
Not another software to beta test.

Turn on the laptop. \
Plug it in. \
Connect to WiFi. \
Why can't I log in?

Down for maintenance, \
Yet again. \
The hours on OneDrive, \
The weeks of eyestrain.

A training session for Slack, \
Do we really need that? \
Yes, it is the same but different. \
A lot of effort for group chat.

Reply all. \
Out of office. \
What to do? \
I'm nauseous.

How many accounts must we use \
Just to prove we've gone digital? \
This feels like more work, \
Not a little bit paradisiacal.

Listen here, administrators: \
Nothing about this is streamlined. \
The teachers are run ragged, \
They've lost all peace of mind.

Technological progress in the name of efficiency \
Creates more work in the long run. \
If the teachers are all exhausted, \
Our school won't be number one.

Do you want to make things easier? \
Do you want a suggestion? \
It would be easier to teach \
Without all this interruption.

Stop sending me messages. \
I don't want another app. \
Don't take it the wrong way, \
But it's all a load of crap.

I'll still do my work \
Without this DX. \
You'll still be happy \
As you cash all those cheques.

You want to be innovative, \
But you just copy the rest. \
I would give you a zero \
If you did that on a test.

So, turn off those screens. \
Time to disconnect. \
It's the only way I know \
To teach the present perfect.

I have spoken, \
You have heard. \
Please don't make me remember \
Another password.
