---
title: "Learning about the Lottery"
subtitle: "I never understood why people play the same numbers every week"
description: "A post about the odds of winning the lottery and using Python to produce random number sets."
slug: python-lottery
date: 2022-02-06
lastmod: 2025-02-18
draft: false
tags:
    - Programming
next: true
nomenu: false
---

*I had a busy week. It seems that the busier the week, the easier it is to get distracted by everything.*

As is the case with nearly every person I have ever met, my head is filled with information that, albeit *cool*, is less than useful on a daily basis. I remember a large quantity of the dialogue from *Transformers: The Movie* (1986), and bits and pieces of my high school AP math classes, for example.

This week, a student asked about the chances of randomly guessing their way to a perfect score on a multiple choice test. I don't know why they would ask the English teacher, but my brain gave them the answer:

If there are 200 multiple choice questions with 4 options each...

$$\frac{1}{4^{200}} = \frac{1}{2.58224987809e+120}$$

That is a big number, and it put them on the path to studying.

Unfortunately, it put me on the path to thinking about the lottery.

I come from the [Maritimes](https://en.wikipedia.org/wiki/The_Maritimes), in Canada. We have a lottery there called [Lotto 6/49](https://en.wikipedia.org/wiki/Lotto_6/49). As the name suggests, you choose 6 numbers out of 49 possible numbers.

So, I sat with my calculator (i.e. computer) and started figuring things out with a little help from the web...

## Calculating Chances and Combinations

The formula for calculating chances looks like this:

$$C(n,r) = \frac{n!}{r!(n-r)!}$$

This might be called the *r-combination* or *"n choose r"* or the *binomial coefficient*.

The formula gives us the number of ways a sample of **r** elements can be obtained from a larger set of **n** distinguishable objects, where order does not matter and repetitions are not allowed.

### Using all 49 numbers

$$\begin{align*}C(n,r) &= \frac{49!}{6!(49-6)!} \cr &=13,983,816\end{align*}$$

There are 13,983,816 possible combinations. You would have **one chance in 13,983,816** of guessing this number.

### Using only Odd Numbers

There are 25 odd numbers between 1 and 49, so there are 177,100 possible combinations.

$$\begin{align*}C(n_\text{odd},r) &= \frac{25!}{6!(25-6)!} \cr &=177,100\end{align*}$$

13,983,816 (total possible combinations) - 177,100 (possible combinations with odd numbers) = **13,806,716 losing combinations!** That doesn't bode well.

### Using only Even Numbers

There are 24 even numbers, so, 134,596 possible combinations.

$$\begin{align*}C(n_\text{even},r) &= \frac{24!}{6!(24-6)!} \cr &=134,596\end{align*}$$

That leaves us **13,849,220 losing combinations**. Again, not great.

### Using 3 Odds and 3 Evens

According to [How to Win the Lotto 6/49 According To Math](https://lotterycodex.com/lotto-649/), you can choose 3 odd and 3 even numbers to up your chances.

The author states there are 4,655,200 ways to win.

While I can see that that number is the product of these r-combinations:

$$\begin{align*} \frac{25!}{3!(25-3)!} \times \frac{24!}{3!(24-3)!} = 4,655,200\end{align*}$$

I am unsure of the logic here. I believe it, but I admit that I lost some interest at this point.

And... my interest went in another direction.

## How long would it take to win?

(and what would it cost?!)

Lotto 6/49 has two draws per week. A single "line" or chance costs $3.00 CAD.

Let's imagine I will play one line per draw and play the same numbers for both of those draws.

And, let's also imagine that the payout never changes and is based on the payout from [29 January 2022](https://www.alc.ca/content/alc/en/our-games/lotto/lotto-6-49.html?date=2022-01-29):

| **Match**       |          **Prize** |
|-----------------|-------------------:|
| 6 Mains         |      $7,102,956.40 |
| 5 Mains + Bonus |         $60,724.30 |
| 5 Mains         |          $2,595.10 |
| 4 Mains         |             $87.10 |
| 3 Mains         |             $10.00 |
| 2 Mains + Bonus |              $5.00 |
| 2 Mains         | Free Play (=$3.00) |

I slapped together some sloppy Python, which I am only in the process of learning, so be kind:

```python
import random
import babel.numbers

# 6/49 ASK FOR 6 NUMBERS BETWEEN 1 AND 49 (Inclusive)

YOURNUMBERS = []
YOURBONUS = 0

print("Choose 6 numbers between 1 and 49")

x = 1

while x != 7:
    CHOICE = input(f"Number {x}/6: ")
    if int(CHOICE) <= 49 and CHOICE not in YOURNUMBERS:
        YOURNUMBERS.append(CHOICE)
        x += 1
    else:
        pass

# Get a Bonus

while True:
    RANDOM49 = random.randint(1, 49)
    if RANDOM49 not in YOURNUMBERS:
        YOURBONUS = RANDOM49
        break
    else:
        continue

print(f"Your Bonus is {YOURBONUS}")

# Payout Calculations

WINNING_BONUS = 0
PURCHASES = 0
WINNINGS = 0.0
WINNING_NUMBERS = []
MATCHES = []

while len(MATCHES) != 6:
    MATCHES.clear()
    WINNING_NUMBERS.clear()

    y = 1

    while y != 7:
        CHOICE = random.randint(1, 49)
        if str(CHOICE) not in WINNING_NUMBERS:
            WINNING_NUMBERS.append(str(CHOICE))
            y += 1

    while True:
        RANDOM49 = random.randint(1, 49)
        if RANDOM49 not in WINNING_NUMBERS:
            WINNING_BONUS = RANDOM49
            break
        else:
            continue

    for num in WINNING_NUMBERS:
        if num in YOURNUMBERS:
            MATCHES.append(num)

    if len(MATCHES) == 0:
        WINNINGS += 0

    if len(MATCHES) == 1:
        WINNINGS += 0

    if len(MATCHES) == 2 and YOURBONUS == WINNING_BONUS:
        WINNINGS += 5

    if len(MATCHES) == 2 and YOURBONUS != WINNING_BONUS:
        WINNINGS += 3

    if len(MATCHES) == 3:
        WINNINGS += 10

    if len(MATCHES) == 4:
        WINNINGS += 87.10

    if len(MATCHES) == 5 and YOURBONUS == WINNING_BONUS:
        WINNINGS += 2_595.10

    if len(MATCHES) == 5 and YOURBONUS != WINNING_BONUS:
        WINNINGS += 60_724.30

    if len(MATCHES) == 6:
        WINNINGS += 7_102_956.40

    PURCHASES += 1

    # print(PLAYS)

TOTALCOST = babel.numbers.format_currency(PURCHASES * 3, "CAD", locale="en_CA")
GROSS = babel.numbers.format_currency(WINNINGS, "CAD", locale="en_CA")
NET = babel.numbers.format_currency(WINNINGS - PURCHASES * 3, "CAD", locale="en_CA")

WINNING_NUMBERS.sort()
YOURNUMBERS.sort()

print(
    f"""
My Numbers {' - '.join(YOURNUMBERS)} + {YOURBONUS}
Winning Numbers {' - '.join(WINNING_NUMBERS)} + {WINNING_BONUS}
You would have to buy {PURCHASES:,} tickets before winning the grand prize.
The cost of playing would be {TOTALCOST}.
You would GROSS {GROSS}.
Your NET winnings would be {NET}."""
)
```

And went about playing with it.

As I am writing, I just received this output:

```
Choose 6 numbers between 1 and 49
Number 1/6: 2
Number 2/6: 20
Number 3/6: 42
Number 4/6: 7
Number 5/6: 33
Number 6/6: 49
Your Bonus is 36

My Numbers 2 - 20 - 33 - 42 - 49 - 7 + 36
Winning Numbers 2 - 20 - 33 - 42 - 49 - 7 + 25
You would have to buy 15,731,789 tickets before winning the grand prize.
The cost of playing would be $47,195,367.00.
You would GROSS $35,228,605.40.
Your NET winnings would be -$11,966,761.60.
```

I have run this script about 20 times. On one occasion I had a positive net win. But, as you can see, I would have to play more times than possible in a lifetime.

## The Takeaway

It is much more interesting to save $6 every week, or only play from time to time just for fun, than it is to try the same numbers every week. I suppose I could also try playing a random set of numbers every week. That might improve my chances.
