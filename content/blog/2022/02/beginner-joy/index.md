---
title: "The Joys of Being a Beginner"
subtitle: "I could do this all day"
description: "A short blog post detailing the pleasure of just starting out when learning something new. In this case, beginner Python exercises with Turtle."
slug: beginner-joy
date: 2022-02-22T14:14:09+01:00
lastmod: 2024-04-14
draft: false
katex: false
tags:
  - Programming
next: true
nomenu: false
---

Have you ever player *[Stardew Valley](https://en.wikipedia.org/wiki/Stardew_Valley)*? It is a sort of idle game, an improved version of *[Harvest Moon](https://en.wikipedia.org/wiki/Harvest_Moon_(2007_video_game_series))*, about farming and fishing, among other things. When you first play the game, you want nothing more than to advance. You want to have a bigger house, a horse, visit the areas that you couldn't at the start of the game. The further you progress, the more there is to do. Very quickly, you will find yourself rushing around without an idle moment. You will wake up, check calendars and weather reports, tend to the garden, go to town, go to the mines, go fishing, and realize that you need to get back home before time runs out. The fun remains, but there is also a nostalgia for that first day. To be able to roll out of bed, water a few parsnips and not really worry about the rest. This is the joy of being a new player. The joy of being a beginner.

---

A certain number of years ago, I was a student. I absolutely enjoyed being a student. There were some classes that I just couldn't get my head around. One of those was beginner programming. Much like when I started playing *Stardew Valley* a few years back, I was fed up with the beginner stuff. I wanted more. When the exams came, I was not ready. I took my bad grades, and decided that programming was not something for me.

Reflecting on this part of my life, I am now certain the professors were doing an excellent job teaching the subject. I was just not ready to be a beginner. Western culture wants us to be *experts* and *proficient*. We need to be *fast learners* with *high uptake bandwidth* with innate logic and *critical thinking* skills. Just being *literate* is not enough. Would you even bother putting a skill on your CV/résumé if you were only a *novice* at that task?

Since those days, much of which I have now concluded were focused on *learning to learn*, I have, in fact, become slightly more proficient at learning. It is a recent development, though, as far as I can tell. Perhaps it was learning little things related to computers, or living in another country, or meeting new people, or having different jobs with different tasks. Whatever the reason, whenever it happened, the joys of being a beginner have revealed themselves to me.

Plenty of pompous people produce pithy posts parleying the pros of practising self-improvement and the perfectionment of one's cognitive processes. *Learning is life, life is learning*, yadda yadda yadda. Rarely do they stop to remind us that, when we were children, we enjoyed learning something new. Learning to ride a bike, learning to play badminton, learning to swim. Toddlers look like they are having fun just learning to walk! But, those same toddlers, once they become proficient, find themselves bored with the activity and demand to be carried.

If I were to describe my current skill level in programming, I would likely use the term *sub-toddler*. I would say it with a large smile. The last time I have had this much fun was when I was first learning about Linux, but this is more akin to when I was learning French at age 12. Each day, there is a new thing, and it is great. It is learning to walk, and water parsnips.

Take for example what I learned this morning:

![Turtle/Frogger](turtle.gif "Frogger in Python/Turtle")

That is a *game* made using Python with the Turtle module. I don't think I need to tell many of you, but just in case, this is the description of Turtle:

>Turtle graphics is a popular way for introducing programming to kids. It was part of the original Logo programming language developed by Wally Feurzeig, Seymour Papert and Cynthia Solomon in 1967. [^turtle]

Notice that is something meant for *introducing programming to kids*. I am not a *kid*, and *kid* is not even considered academic English where I teach. When I saw the exercise I was meant to work on, I thought to myself, "really?" How will this teach me anything? I have been slowly plodding along learning some Python basics, and now I am drawing shapes like I use to on my calculator in high school?

It only took me 10 minutes to realize that <mark>**I could do this all day**</mark>. It is akin to learning how to use a [Bescherelle](https://en.wikipedia.org/wiki/Bescherelle). It is dry, it is not intrinsically *fun*, you would never brag about it, but it gives you some fundamental skills that allow you to progress. Most importantly, though, it is *novel* when you have no skill whatsoever. This is one of the most captivating things I have done with a computer since getting a Raspberry Pi, or flashing a new ROM to my phone. The steps are few. The requirements are low. But, there is a little voice asking if it will work; when I click the button, a moment of anxiety as it starts to load; and a spark of joy upon seeing shapes and colours on the screen.

Of course, I won't be able to dork around with Turtle forever. In a few weeks, I will have likely forgotten how I managed to solve the problem and get things to work per the instructions, but I will not soon forget how *wonderful* it is to spend a *little* more time than necessary on the beginner exercises.

Would it be cliché to end with a call to action to *never stop learning*? To say *learning is fun*? What about a little, *if I can do this, so can you*? 

Yes, yes, and yes.

It should, however, go without saying that being a *beginner* is not to be frowned upon. In a few weeks --- if I continue down my path of trying to expand my skills, and these beginner exercises get further behind me --- I suspect the quale of the experience shall remain present.

Now, back to drawing shapes.

[^turtle]: [turtle — Turtle graphics — Python 3.10.2 documentation](https://docs.python.org/3/library/turtle.html)
