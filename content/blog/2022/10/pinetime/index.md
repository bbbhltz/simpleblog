---
title: "I have a PineTime"
slug: pinetime
subtitle: "...and learned a thing or two"
description: "A post announcing the purchase of a new gadget, using ImageMagick, and digitally signing PDF files under Linux."
date: 2022-10-21T11:50:00+02:00
# lastmod:
featured_image: /blog/2022/10/pinetime/gb1.webp
draft: false
katex: false
tags:
  - Technology
next: true
nomenu: false
---

*It has been long enough since my last update that I honestly forgot how to add a new post...*

This is going to be a bit of a [hodgepodge](https://tasteofnovascotia.com/recipes/hodge-podge/) of information and updates.

# PineTime

After a short exchange over on Fosstodon, I needed little convincing to buy a PineTime. For starters, this is the exact type of "end-user-geek" gadget that I love. The setup and updating is easy, though not as direct as other smartwatches may be.

I bought it for one reason, and you might think that reason is privacy. That is a benefit of using the PineTime, though not the feature that I wanted it for. I just wanted a cheap heart rate monitor that does not require sharing my info with a company. So, I suppose the final deciding factor was the benefit of privacy. Maybe you were right.

![Screenshoot: Gadgetbridge homescreen showing InfiniTime connected](gb1.webp "Gadgetbridge homescreen showing InfiniTime connected")

I had already used the companion app, [Gadgetbridge](https://www.gadgetbridge.org/) with my hybrid Fossil Q. So, it made sense for me to continue using that. Installing the latest firmware or update was pretty clear, and fast. You just need to navigate to the [releases page on GitHub](https://github.com/InfiniTimeOrg/InfiniTime/releases/tag/1.11.0), download an archive, open it with Gadgetbridge and wait.

The latest update did require an extra step. To give you an idea of how easy this was, the entire process, including:
* turning on my laptop,
* installing [the InfiniTime Daemon](https://gitea.arsenm.dev/Arsen6331/itd) and figuring out how to connect to my watch,
* downloading the archive containing resources,
* and launching the command \
`itctl res load infinitime-resources-1.10.0.zip`

was all over and done with in less than 6 minutes.

I do, however, maintain one of my principal gripes with smartwatches: charging. Smartwatches are not the [Casios](https://fosstodon.org/web/tags/casio) of yore. The battery does not last 5 or even 10 years. My worries, while confirmed, are not actually that much of a problem. I won't be taking my PineTime on holiday with me, because I don't want to travel with the charging dongle, but I haven't succeeded in draining the battery to 0 yet.

![Screenshoot: Gadgetbridge battery data](gb2.webp "Gadgetbridge battery log")

# Always Learning

There is always some new thing to deal with, and opportunity to learn. Since the last update I *finally* completed the [CS50P Introduction to Python](https://cs50.harvard.edu/python/2022/) course. I also had to figure out some basic ImageMagick commands, and wrap my head around something that should be clearer than it is.

The two following "tricks" I am putting here --- so I don't forget them.

## ImageMagick

I had a stupid simple thing I needed to do. Place two images side-by-side, with labels and a title; and one of the images needed to be resized. There was nothing to this. ImageMagick is a tool that I use from time to time and I have always managed to find the solutions I need through search engines. I was surprised by the results this time around. I think it turned out OK.

The solution I needed is a combination of the [Montage](https://imagemagick.org/script/montage.php) command and [Text to Image Handling](https://imagemagick.org/Usage/text/).

```bash
montage -label A -resize 'x800' crime-real-manip.webp -label B crime-real.webp -tile 2x1 -geometry +20+20 -background none -shadow -title 'Real and Better' -pointsize 30 -font Humor-Sans real-better.webp
```

![Montage of 2 line graphs side-by-side](real-better.webp)

I added the shadow for good measure.

{{< box info >}}

You can find the name of the font you want to use by running:

```bash
identify -list font | grep "Humor"
```

where "Humor" would be the name of the font you are looking for.

{{< /box >}}

## Digitally sign a document in Okular

This should be presented in a user-friendly manner, or streamlined somehow. I mean, once I found the commands and zipped through the [man page](https://www.mankier.com/1/certutil), all the information was there. There were even examples and step-by-step instructions. But, for some reason my searches lead me down overcomplicated, winding paths.

The solution was simple.

This is all there was to it.

1. First, install `certutil`. On my Debian install, you need to run `sudo apt install libnss3-tools` to have this binary.

2. Create a directory; `cd` into it.

```bash
mkdir newcert
cd newcert/
```

3. Run two commands.

```bash
certutil -N -d ~/newcert/ # creates new databases

certutil -S -s "CN=name,O=organisation,E=email" -n examplecert -x -t "CT,C,C" -v 120 -m 1234 -d ~/newcert/ # creates the certificate we need
```

You will be asked for a password. Don't forget it. I did.

4. Add the certificate to the Okular backend by changing the "Certificate Database" location.

![Screenshot of Okular Backends settings](okular1.webp "Settings > Configure Backends")

5. Sign your life away.

![Screenshot of Okular Digitally Sign menu](okular2.webp "Tools > Digitall Sign...")

![Screenshot of Okular Digital Signature](okular3.webp "It ain't pretty, but it works!")

As always, if you see mistakes that need fixing, let me know.

I'm off to get my steps in.
