---
title: "More"
description: "Uses, Favourites and other places to look for content"
featured_image: /more/featured.webp
# lastmod: 2024-10-31
toc: true
---

## Stats

* See the [stats](/stats)!

## Credits for this site

### Site Tools

*Tools I use or have used in the past to help me*

- **Hugo** ([https://gohugo.io/](https://gohugo.io/)): *an open-source static site generator*.
- The theme used for this site is a *gently* modified copy of **[Hugo Dead Simple Theme](https://hugo-dead-simple.netlify.app/post/hugo-dead-simple/)** by barklan.
- The fonts used for the featured image title cards is [Karrik](https://velvetyne.fr/fonts/karrik/).
- **Dark Visitors** ([https://darkvisitors.com/](https://darkvisitors.com/)): *a list of known AI agents on the internet to help make your `robots.txt` file*.
- **PageSpeed Insights** ([https://pagespeed.web.dev/](https://pagespeed.web.dev/analysis/https-bbbhltz-codeberg-page/3nlq37iqbs))
- **The Orginal Website Carbon Calculator** ([https://www.websitecarbon.com/](https://www.websitecarbon.com/website/bbbhltz-codeberg-page/))
- **Carbon Neutral Website** ([https://carbonneutralwebsite.org/](https://carbonneutralwebsite.org/calculate))
- **NetPositive Error Messages** ([https://8325.org/haiku/](https://8325.org/haiku/)): *used on the 404 page*.
- **Disable RSS for one (or more) specific posts in a blog** [https://discourse.gohugo.io/t/disable-rss-for-one-or-more-specific-posts-in-a-blog/52995](https://discourse.gohugo.io/t/disable-rss-for-one-or-more-specific-posts-in-a-blog/52995)
- **Customize RSS in Hugo Website** [https://codingnconcepts.com/hugo/custom-rss-feed-hugo/#exclude-specific-pages-from-rssxml](https://codingnconcepts.com/hugo/custom-rss-feed-hugo/#exclude-specific-pages-from-rssxml)

<details>
<summary>Previously Used...</summary>

- **Simple.css** ([https://simplecss.org/](https://simplecss.org/)): *framework to make a good-looking site quickly*.
- **Tabler Icons** ([https://tabler-icons.io/](https://tabler-icons.io/)): *free and open source icons*
- **Hugo Cloak Email** ([https://github.com/martignoni/hugo-cloak-email](https://github.com/martignoni/hugo-cloak-email)): *a theme component to cloak email address*
* [mogwai's tutorial for applying Simple.css to Hugo](https://mogwai.be/creating-a-simple.css-site-with-hugo/)
* [A human-readable RSS feed with Jekyll by Minutes to Midnight](https://minutestomidnight.co.uk/blog/build-a-human-readable-rss-with-jekyll/)
* [Admonitions in Hugo by Bounded Infinity](https://boundedinfinity.github.io/2017/09/admonitions-in-hugo/)
* [Adding meta tags in Hugo blog by Erin Jeong](https://erinjeong.com/posts/metatags-hugo/)
* [Three Steps to Improve Hugo's RSS Feeds by Joseph Heyburn](https://dev.to/jdheyburn/three-steps-to-improve-hugo-s-rss-feeds-58ob)
* [Hugo - Selectively Add Table of Contents to Post by Justin James](https://digitaldrummerj.me/hugo-table-of-contents/)

</details>

### Other software

* For more information on the software I like to use, [click here](/software).

## Clubs, Blogroll, Webrings

### Clubs, etc.

* [The Darktheme Club](https://darktheme.club/)
* [250KB CLUB](https://250kb.club/) ([link](https://250kb.club/bobbyhiltz-com/))
* [512KB CLUB](https://512kb.club/) ([rejected](https://github.com/kevquirk/512kb.club/pull/1653#issuecomment-2442148997))
* [1MB CLUB](https://1mb.club/)
* [Marginalia Search](https://marginalia-search.com/) [[site profile](https://marginalia-search.com/site/bobbyhiltz.com?view=info)]\
  *An independent DIY search engine that focuses on non-commercial content*
* [Blog of the .Day](https://blogofthe.day/)
* [Blogroll.org](https://blogroll.org/)\
  *A curated list of personal and independent blogs*
* [Indieseek.xyz Indieweb Directory](https://indieseek.xyz/)\
  *A human edited, Indieweb directory*
* [PersonalSit.es](https://personalsit.es/)\
  *A site built to share and revel in each others' personal sites*
* [Weird Wide Webring](https://weirdwidewebring.net/)\
  *The web needs a little more weird*
* [Fediring.net](https://fediring.net/)\
  *Webring for the personal sites of any member of the fediverse*
* [indieblog.page](https://indieblog.page/)\
  *Discover the IndieWeb, one blog post at a time*
* [Static.Quest](https://static.quest/)\
  *A web ring focused to list all the amazing static websites*
* [ooh.directory](https://ooh.directory/)\
  *A collection of blogs about every topic (someday my blog will be listed...someday...)*

### Some Blogs I follow

* [Evan Boehs](https://boehs.org/in/blog) <a href="https://boehs.org/in/blog.xml">(RSS)</a>
* [Buck 65](https://buck65.substack.com/) <a href="https://buck65.substack.com/feed">(RSS)</a> [Substack]
* [Citation Needed by Molly White](https://www.citationneeded.news/) <a href="https://www.citationneeded.news/rss/">(RSS)</a>
* [Adam's Desk](https://www.adamsdesk.com/) <a href="https://www.adamsdesk.com/feed/blog.xml">(RSS)</a>
* [Garrit Franke](https://garrit.xyz/) <a href="https://garrit.xyz/rss.xml">(RSS)</a>
* [Jakov (sheepdev)](https://sheepdev.xyz/) <a href="https://sheepdev.xyz/feed">(RSS)</a>
* [Joelchrono](https://joelchrono.xyz) <a href="https://www.joelchrono.xyz/feed.xml" >(RSS)</a>
* [JP](https://moddedbear.com/) <a href="https://moddedbear.com/blog/index.xml" >(RSS)</a>
* [Light Thoughts](https://sanderium.codeberg.page/) <a href="https://sanderium.codeberg.page/posts/rss.xml">(RSS)</a>
* [McSinyx](https://cnx.gdn/) <a href="https://cnx.gdn/feed.xml" style="text-decoration:      none">(RSS)</a>
* [Mojeek Blog](https://blog.mojeek.com/) <a href="https://blog.mojeek.com/atom.xml" >(RSS)</a>
* [Ploum](https://ploum.net/) [FR] <a href="https://ploum.net/atom.xml">(RSS)</a>
* [David Revoy](https://www.davidrevoy.com/) <a href="https://www.davidrevoy.com/feed/(RSS)">(RSS)</a>
* [Brandon Rozek](https://brandonrozek.com/) <a href="https://brandonrozek.com/blog/index.xml">(RSS)</a>
* [Seirdy](https://seirdy.one/) <a href="https://seirdy.one/atom.xml" >(RSS)</a>
* [So1o](https://so1o.xyz/) <a href="https://so1o.xyz/feed.xml" >(RSS)</a>
* [Hyde Stevenson](https://lazybea.rs/) <a href="https://lazybea.rs/index.xml" >(RSS)</a>
* [Bradley Taunt](https://btxx.org/ ) <a href="https://btxx.org/index.rss">(RSS)</a>
