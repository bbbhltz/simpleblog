All the bits and pieces for my [blog](https://bbbhltz.codeberg.page)

The public folder is available here https://codeberg.org/bbbhltz/pages

## Credits

The currently used code for this site layout and CSS is a modified version of [hugo-dead-simple](https://github.com/barklan/hugo-dead-simple) by [barklan](https://github.com/barklan).

- [layouts/quitter](layouts/quitter) by me
- [layouts/cv](layouts/cv) by me
- [layouts/partials/haikus.html](layouts/partials/haikus.html) by me
- [layouts/stats](layouts/stats) by [Luke Harris](https://github.com/lkhrs/hugo-cactus-theme/blob/main/layouts/_default/stats.html)

## Notes

This repository is maintained on Codeberg.

[![Please don't upload to GitHub](https://nogithub.codeberg.page/badge.svg)](https://nogithub.codeberg.page)
