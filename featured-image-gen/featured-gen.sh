#!/usr/bin/env bash

# makes a 1200x628 webp image with a supplied title and subtitle in the centre
# Karrik font from https://velvetyne.fr/

read -p "Post title? " postTitle

convert -size 1200x628 -gravity center \( -size 1200x628 plasma:fractal -blur 0x20 \) \( -background none -fill white -font Karrik -size 1000x500 -density 120 -gravity center caption:"$postTitle" \) -composite featured.webp

echo "Done!"
