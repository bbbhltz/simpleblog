# bbbhltz.codeberg.page source

Any issues can be signaled here: [simpleblog: all the bits and pieces for my blog](https://codeberg.org/bbbhltz/simpleblog)

## Credits for this site

See [https://bbbhltz.codeberg.page/more/#credits-for-this-site](https://bbbhltz.codeberg.page/more/#credits-for-this-site) or [https://codeberg.org/bbbhltz/simpleblog/src/branch/master/content/more.md](https://codeberg.org/bbbhltz/simpleblog/src/branch/master/content/more.md) for credits
