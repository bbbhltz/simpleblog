#!/usr/bin/env bash

read -p "Commit comment: " comment

#echo "BLOGROLLING"

#./getfeeds.sh &&\

echo "BUILDING..."

hugo &&\

echo "PUSHING BUILD FILES AND PUBLIC..."

git add . &&\
git commit -m "$comment" &&\
git push &&\

cd public/ &&\
git add . &&\
git commit -m "$comment" &&\
git push &&\

echo "DONE!"
