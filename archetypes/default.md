---
title: "{{ replace .Name "-" " " | title }}"
subtitle: ""
description: ""
slug: {{ .Name }}
date: {{ .Date }}
#lastmod: {{ .Date | time.Format "2006-01-02" }}
#featured_image: /blog/{{ now.Format "2006" }}/{{ now.Format "01" }}/{{ .Name }}/featured.webp
toc: false
bold: false
tags:
  - blog
next: true
nomenu: false
notitle: false
draft: true
#syndication:
---

content
